var JSON;if(!JSON)JSON={};(function(){"use strict";function c(a){return a<10?"0"+a:a}if(typeof Date.prototype.toJSON!=="function"){Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+c(this.getUTCMonth()+1)+"-"+c(this.getUTCDate())+"T"+c(this.getUTCHours())+":"+c(this.getUTCMinutes())+":"+c(this.getUTCSeconds())+"Z":null};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()}}var h=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,f=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,a,d,i={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},b;function g(a){f.lastIndex=0;return f.test(a)?'"'+a.replace(f,function(a){var b=i[a];return typeof b==="string"?b:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function e(m,n){var h,j,i,k,l=a,f,c=n[m];if(c&&typeof c==="object"&&typeof c.toJSON==="function")c=c.toJSON(m);if(typeof b==="function")c=b.call(n,m,c);switch(typeof c){case"string":return g(c);case"number":return isFinite(c)?String(c):"null";case"boolean":case"null":return String(c);case"object":if(!c)return"null";a+=d;f=[];if(Object.prototype.toString.apply(c)==="[object Array]"){k=c.length;for(h=0;h<k;h+=1)f[h]=e(h,c)||"null";i=f.length===0?"[]":a?"[\n"+a+f.join(",\n"+a)+"\n"+l+"]":"["+f.join(",")+"]";a=l;return i}if(b&&typeof b==="object"){k=b.length;for(h=0;h<k;h+=1)if(typeof b[h]==="string"){j=b[h];i=e(j,c);i&&f.push(g(j)+(a?": ":":")+i)}}else for(j in c)if(Object.prototype.hasOwnProperty.call(c,j)){i=e(j,c);i&&f.push(g(j)+(a?": ":":")+i)}i=f.length===0?"{}":a?"{\n"+a+f.join(",\n"+a)+"\n"+l+"}":"{"+f.join(",")+"}";a=l;return i}}if(typeof JSON.stringify!=="function")JSON.stringify=function(h,c,f){var g;a="";d="";if(typeof f==="number")for(g=0;g<f;g+=1)d+=" ";else if(typeof f==="string")d=f;b=c;if(c&&typeof c!=="function"&&(typeof c!=="object"||typeof c.length!=="number"))throw new Error("JSON.stringify");return e("",{"":h})};if(typeof JSON.parse!=="function")JSON.parse=function(a,c){var b;function d(f,g){var b,e,a=f[g];if(a&&typeof a==="object")for(b in a)if(Object.prototype.hasOwnProperty.call(a,b)){e=d(a,b);if(e!==undefined)a[b]=e;else delete a[b]}return c.call(f,g,a)}a=String(a);h.lastIndex=0;if(h.test(a))a=a.replace(h,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)});if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){b=eval("("+a+")");return typeof c==="function"?d({"":b},""):b}throw new SyntaxError("JSON.parse");}})();var _insideGraph=function(w,d){function setCookie(name,value,expires,path,domain,secure){var today=new Date();today.setTime(today.getTime());if(expires)
expires=expires*1000*60*60*24;var expires_date=new Date(today.getTime()+(expires));document.cookie=name+"="+escape(value)+((expires)?";expires="+expires_date.toGMTString():"")+((path)?";path="+path:"")+((domain)?";domain="+domain:"")+((secure)?";secure":"");};function getCookie(name){var start=d.cookie.indexOf(name+"=");var len=start+name.length+1;if((!start)&&(name!=d.cookie.substring(0,name.length)))
return null;if(start==-1)
return null;var end=d.cookie.indexOf(";",len);if(end==-1)
end=d.cookie.length;return unescape(d.cookie.substring(len,end));};function defer(callback,test,interval){if(test()){callback();return;}
var _ramp=(typeof(interval)=="undefined");var _interval=_ramp?10:interval;var _spin=function(){if(test()){callback();}else{if(_ramp)
_interval=_interval>=1000?1000:_interval*2;setTimeout(_spin,_interval);}};setTimeout(_spin,_interval);}
function loadJS(src,instance,onload){var s=d.createElement("script");s.type="text/javascript";s.src=src;s.async=true;if(onload)loadCallback(instance,onload);var head=d.getElementsByTagName("head")[0];if(head){head.appendChild(s);}else{d.body.appendChild(s);}};function loadCSS(href,callback){var s=d.createElement('link');s.setAttribute('rel','stylesheet');s.setAttribute('type','text/css');s.setAttribute('media','screen');s.setAttribute('href',href);if(callback){defer(callback,function(){for(var i in d.styleSheets){if(d.styleSheets[i].href==href){return true;}}
return false;});}
var head=d.getElementsByTagName('head')[0];if(head){head.appendChild(s);}else{d.body.appendChild(s);}};function loadCallback(check,callback){defer(callback,function(){return eval(check);});};function log(){if(_logging&&typeof(console)!="undefined"&&console!=null&&typeof(console.log)=="function"){var i=-1,l=arguments.length,args=[],fn='console.log(args)';while(++i<l)
args.push('args['+i+']');fn=new Function('args',fn.replace(/args/,args.join(',')));fn(arguments);}}
function error(){if(typeof(console)!="undefined"&&console!=null&&typeof(console.error)=="function"){var i=-1,l=arguments.length,args=[],fn='console.error(args)';while(++i<l)
args.push('args['+i+']');fn=new Function('args',fn.replace(/args/,args.join(',')));fn(arguments);}}
var ieVersion=(function(){var version,matches;if(window.navigator.appName==='Microsoft Internet Explorer'){matches=/MSIE ([0-9]+\.[0-9]+)/.exec(window.navigator.userAgent);if(matches){version=window.parseFloat(matches[1]);}}
return version;})();w["_insideProtocol"]=ieVersion<=9&&d.location.protocol=="http: "?"http://":"https://";w["_insideCluster"]="au3";w["_insideGraphUrl"]=w["_insideProtocol"]+"au3-track.inside-graph.com/";w["_insideSocialUrl"]=w["_insideProtocol"]+"au3-live.inside-graph.com/";w["_insideCDN"]=w["_insideProtocol"]+"au3-cdn.inside-graph.com/";w["_insideCDN2"]="au3-cdn.inside-graph.com/";w["_insideScriptVersion"]="20170726030141";w["_insideLive"]=true;w["_insideIsLive"]=w["_insideLive"];var _callback={};var _pid=getCookie("inside-pid")||null;var _domain=typeof(_insideCookieDomain)!="undefined"?_insideCookieDomain:null;var _path=typeof(_insideCookiePath)!="undefined"?_insideCookiePath:null;var _locked=false;var _logging=(typeof(_insideLogging)=="boolean"&&_insideLogging)||!w["_insideIsLive"]?true:false
if(_pid==""||_pid=="null")_pid=null;if(_domain==""||_domain=="null")_domain=null;if(_path==""||_path=="null")_path=null;var _ua=navigator.userAgent;var _dev=_ua.match(/GoogleTV|SmartTV|Internet.TV|NetCast|NETTV|AppleTV|boxee|Kylo|Roku|DLNADOC|CE\-HTML/i)?'4':_ua.match(/Xbox|PLAYSTATION.3|PlayStation 4|Wii/i)?'4':_ua.match(/iPad/i)||_ua.match(/tablet/i)&&!_ua.match(/Tablet PC/)&&!_ua.match(/RX-34/i)||_ua.match(/FOLIO/i)?'3':_ua.match(/Linux/i)&&_ua.match(/Android/i)&&!_ua.match(/Fennec|mobi|HTC.Magic|HTCX06HT|Nexus.One|SC-02B|fone.945/i)?'3':_ua.match(/Kindle/i)||_ua.match(/Mac.OS/i)&&_ua.match(/Silk/i)?'3':_ua.match(/GT-P10|SC-01C|SHW-M180S|SGH-T849|SCH-I800|SHW-M180L|SPH-P100|SGH-I987|zt180|HTC(.Flyer|\_Flyer)|Sprint.ATP51|ViewPad7|pandigital(sprnova|nova)|Ideos.S7|Dell.Streak.7|Advent.Vega|A101IT|A70BHT|MID7015|Next2|nook/i)||_ua.match(/MB511/i)&&_ua.match(/RUTEM/i)?'3':_ua.match(/BOLT|Fennec|Iris|Maemo|Minimo|Mobi|mowser|NetFront|Novarra|Prism|RX-34|Skyfire|Tear|XV6875|XV6975|Google.Wireless.Transcoder/i)?'2':_ua.match(/Opera/i)&&_ua.match(/Windows.NT.5/i)&&_ua.match(/HTC|Xda|Mini|Vario|SAMSUNG\-GT\-i8000|SAMSUNG\-SGH\-i9/i)?'2':_ua.match(/Windows.(NT|XP|ME|9)/)&&!_ua.match(/Phone/i)||_ua.match(/Win(9|.9|NT)/i)?'1':_ua.match(/Macintosh|PowerPC/i)&&!_ua.match(/Silk/i)?'1':_ua.match(/Linux/i)&&_ua.match(/X11/i)?'1':_ua.match(/Solaris|SunOS|BSD/i)?'1':_ua.match(/Bot|Crawler|Spider|Yahoo|ia_archiver|Covario-IDS|findlinks|DataparkSearch|larbin|Mediapartners-Google|NG-Search|Snappy|Teoma|Jeeves|TinEye/i)&&!_ua.match(/Mobile/i)?'1':'2';var tracking={ready:false,current:null,onload:null,pid:"",accessKey:"",accessCheck:"",enabled:[],ieVersion:ieVersion}
tracking.getTracker=function(tracker){var _initialised=false,_queue=[];var _tracker={timing:[],account:tracker.account,subsiteId:typeof(tracker.subsiteId)!="undefined"&&tracker.subsiteId!=null?tracker.subsiteId:"",visitorId:typeof(tracker.visitorId)!="undefined"&&tracker.visitorId!=null?tracker.visitorId:"",visitorName:typeof(tracker.visitorName)!="undefined"&&tracker.visitorName!=null?tracker.visitorName:"",visitorData:typeof(tracker.visitorData)!="undefined"?tracker.visitorData:null,device:_dev,url:d.location.href,referrer:d.referrer,transItems:[],trafficLimiter:null,init:function(){if(_initialised)
return;_initialised=true;for(var i=0;i<_queue.length;i++){if(_queue[i].action=="trackView")
_tracker.trackView(_queue[i].data);else if(_queue[i].action=="trackForm")
_tracker.trackForm(_queue[i].data);else if(_queue[i].action=="trackOrder")
_tracker.trackOrder(_queue[i].data);else if(_queue[i].action=="addItem")
_tracker.addItem(_queue[i].data);}
_queue=[];},trackView:function(view){if(_initialised){if(_tracker.account!=""){if(checkVisitorCookieLockout({"tracker":_tracker,"view":view}))
return;if(_tracker.timing.length==0&&typeof(performance)!="undefined"&&typeof(performance.timing)!="undefined"){try
{var t=performance.timing;_tracker.timing=[t.responseStart- t.requestStart,t.responseEnd- t.responseStart,t.domInteractive- t.domLoading,t.domComplete- t.domInteractive,t.domComplete- t.fetchStart];}
catch(e){}}
var tltsid=getCookie("TLTSID");if(tltsid!=null&&tltsid!=""){if(typeof(view.data)!=="object")
view.data={};view.data["tltsid"]=tltsid;}
var jsessionid=getCookie("JSESSIONID");if(jsessionid!=null&&jsessionid!=""){if(typeof(view.data)!=="object")
view.data={};view.data["jsessionid"]=jsessionid.split(':')[0];}
var UrCapture=getCookie("UrCapture");if(UrCapture!=null&&UrCapture!=""){if(typeof(view.data)!=="object")
view.data={};view.data["UrCapture"]=UrCapture;}
var TL_RTG=getCookie("TL_RTG");if(TL_RTG!=null&&TL_RTG!=""){if(typeof(view.data)!=="object")
view.data={};view.data["UrCapture"]=TL_RTG;}
var connid=null;try{connid=_insideGraph.jQuery.inside.connection.id;}catch(ex){connid=null;}
var url=w["_insideSocialUrl"]+"trackview?_="+((1*new Date())+ Math.random());var data="acc="+ encodeURIComponent(_tracker.account)
+"&sid="+ encodeURIComponent(_tracker.subsiteId)
+"&url="+ encodeURIComponent(_tracker.url)
+(_tracker.visitorId!=""?"&mid="+ encodeURIComponent(_tracker.visitorId):"")
+(_tracker.visitorName!=""?"&mn="+ encodeURIComponent(_tracker.visitorName):"")
+(_tracker.visitorData!=null?"&md="+ encodeURIComponent(JSON.stringify(cd(_tracker.visitorData))):"")
+"&dev="+ encodeURIComponent(_dev)
+"&dt="+ encodeURIComponent(view.type)
+"&dv="+ encodeURIComponent(view.name)
+"&hn="+ encodeURIComponent(w.location.hostname)
+(typeof(view.category)!="undefined"?"&cat="+ encodeURIComponent(view.category):"")
+(typeof(view.sku)!="undefined"?"&sku="+ encodeURIComponent(view.sku):"")
+(typeof(view.price)!="undefined"?"&price="+ encodeURIComponent(view.price):"")
+(typeof(view.orderId)!="undefined"?"&oid="+ encodeURIComponent(view.orderId):"")
+(typeof(view.orderTotal)!="undefined"?"&tot="+ encodeURIComponent(view.orderTotal):"")
+(typeof(view.currentOrders)=="object"&&typeof(view.currentOrders.length)!="undefined"?"&orders="+ encodeURIComponent(JSON.stringify(cd(view.currentOrders))):"")
+(typeof(view.img)!="undefined"?"&img="+ encodeURIComponent(view.img):"")
+(typeof(view.url)!="undefined"?"&curl="+ encodeURIComponent(view.url):"")
+(typeof(_tracker.referrer)!="undefined"?"&ref="+ encodeURIComponent(_tracker.referrer):"")
+(typeof(view.tags)!="undefined"?"&tags="+ encodeURIComponent(view.tags):"")
+(typeof(view.data)==="object"?"&data="+ encodeURIComponent(JSON.stringify(cd(view.data))):"")
+(typeof(view.node)!="undefined"?"&node="+ encodeURIComponent(view.node):"")
+"&timing="+(_tracker.timing.length>0?_tracker.timing.join():"")
+"&lang="+(w.navigator.languages?w.navigator.languages[0]:(w.navigator.language||w.navigator.userLanguage))
+"&tz="+(new Date()).getTimezoneOffset()
+"&scw="+(screen.width)+"&sch="+(screen.height)
+(connid!=null?"&connid="+ connid:"");track(url,data);}}
else
_queue.push({"action":"trackView","data":view});},trackForm:function(form){if(_initialised){if(_tracker.account!=""){var url=w["_insideSocialUrl"]+"trackform?_="+ +((1*new Date())+ Math.random());var data="acc="+ encodeURIComponent(_tracker.account)
+"&sid="+ encodeURIComponent(_tracker.subsiteId)
+"&url="+ encodeURIComponent(d.location.href)
+(_tracker.visitorId?"&mid="+ encodeURIComponent(_tracker.visitorId):"")
+"&dev="+ encodeURIComponent(_dev)
+"&hn="+ encodeURIComponent(w.location.hostname)
+"&name="+ encodeURIComponent(form.name)
+"&status="+ encodeURIComponent(form.status)
+"&complete="+(typeof(form.complete)!="undefined"&&form.complete?"1":"0")
+(typeof(form.data)==="object"?"&data="+ encodeURIComponent(JSON.stringify(cd(form.data))):"");track(url,data);}}
else
_queue.push({"action":"trackForm","data":form});},trackOrder:function(trans){if(_initialised){if(_tracker.account!="")
{var itemstr="";if(_tracker.transItems.length>0)
{for(var i=0;i<_tracker.transItems.length;i++)
{var item=this.transItems[i];if(typeof(item.sku)!="undefined")
itemstr+="&i"+i+"_s="+ encodeURIComponent(item.sku);if(typeof(item.upc)!="undefined")
itemstr+="&i"+i+"_u="+ encodeURIComponent(item.upc);if(typeof(item.productId)!="undefined")
itemstr+="&i"+i+"_id="+ encodeURIComponent(item.productId);itemstr+="&i"+ i+"_n="+ encodeURIComponent(item.name);if(typeof(item.category)!="undefined")
itemstr+="&i"+ i+"_cat="+ encodeURIComponent(item.category);if(typeof(item.price)!="undefined")
itemstr+="&i"+i+"_p="+ encodeURIComponent(item.price);if(typeof(item.qty)!="undefined")
itemstr+="&i"+i+"_q="+ encodeURIComponent(item.qty);if(typeof(item.img)!="undefined")
itemstr+="&i"+i+"_i="+ encodeURIComponent(item.img);}
_tracker.transItems=[];}
var url=w["_insideSocialUrl"]+"trackorder?_="+ +((1*new Date())+ Math.random());var data="acc="+ encodeURIComponent(_tracker.account)
+"&sid="+ encodeURIComponent(_tracker.subsiteId)
+"&url="+ encodeURIComponent(d.location.href)
+(_tracker.visitorId?"&mid="+ encodeURIComponent(_tracker.visitorId):"")
+"&dev="+ encodeURIComponent(_dev)
+"&oid="+ encodeURIComponent(trans.orderId)
+"&tot="+ encodeURIComponent(trans.orderTotal)
+"&com="+(typeof(trans.complete)=="string"&&trans.complete.toLowerCase()=="true"||typeof(trans.complete)=="boolean"&&trans.complete?"1":"0")
+"&upd="+(typeof(trans.update)=="string"&&trans.update.toLowerCase()=="true"||typeof(trans.update)=="boolean"&&trans.update?"1":"0")
+"&hn="+ encodeURIComponent(w.location.hostname)
+(typeof(trans.data)==="object"?"&data="+ JSON.stringify(cd(trans.data)):"")
+(typeof(trans.newOrderId)!="undefined"?"&noid="+ encodeURIComponent(trans.newOrderId):"")
+ itemstr;track(url,data);}}
else
_queue.push({"action":"trackOrder","data":trans});},addItem:function(item){if(_initialised)
_tracker.transItems[_tracker.transItems.length]=item;else
_queue.push({"action":"addItem","data":item});}}
tracking.current=_tracker;var c1=getCookie("inside-"+ w["_insideCluster"]);if(!c1||c1=="null"){if(localStorage){c1=localStorage.getItem("inside-"+ w["_insideCluster"]);}
else{c1=null;}}
var c2=getCookie("inside-pid");if(c2==""||c2=="null")c2=null;_pid=c1!=null?c1:c2;var trackingHost;if(w._insideLive)
trackingHost=w._insideSocialUrl.replace("http:","https:");else
trackingHost=w._insideSocialUrl;setCookie("inside-c1","OK",false,"/");loadJS(trackingHost+"gettracker?acc="+ tracker.account+"&pid="+(_pid||"")+"&c1="+(getCookie("inside-c1")||"")+"&dev="+ _dev+"&url="+ encodeURIComponent(d.location.href.replace(/([^:]+:\/\/[^\/]+).*/,"$1")));setCookie("inside-c1","",-1,"/");return _tracker;};tracking.setTracker=function(data){if(data==null)
return;_pid=data.pid;if(_domain==null){_domain=data.domain;_path="/";}
if(typeof(_path)=="undefined"||_path==null)
_path="/";if(typeof(data.trafficLimiter)!="undefined")
tracking.current.trafficLimiter=data.trafficLimiter;setCookie("inside-pid","",-1);setCookie("inside-pid","",-1,"/");setCookie("inside-pid","",-1,"/",data.domain);if(_domain!=data.domain)
setCookie("inside-pid","",-1,"/",_domain);setCookie("inside-"+ w["_insideCluster"],_pid,1000,_path,_domain);if(localStorage)
localStorage.setItem("inside-"+ w["_insideCluster"],_pid);_insideGraph.current.init();};tracking.setCookie=setCookie;tracking.getCookie=getCookie;tracking.loadCSS=loadCSS;tracking.loadJS=loadJS;tracking.defer=defer;tracking.bind=function(eventName,eventCallback){if(typeof eventCallback=="function"){if(!_callback[eventName])
_callback[eventName]=[];_callback[eventName].push(eventCallback);}};tracking.doEvent=function(eventName,eventData){var eventCallbackList=_callback[eventName];if(eventCallbackList)
for(var i=0;i<eventCallbackList.length;i++)
eventCallbackList[i](eventData);}
tracking.hasEvent=function(eventName){return typeof(_callback[eventName])!="undefined"&&_callback[eventName].length>0;}
tracking.getVisitorDetails=function(){return null;}
function startInside(data){if(tracking.ready||data==null)
return;tracking.pid=data.pid;tracking.accessKey=data.key;tracking.accessCheck=data.check;if(typeof(data.enabled)=="undefined"||data.enabled==null)
return;tracking.enabled=data.enabled;defer(loadInside,function(){return d.readyState!="loading"&&d.readyState!="interactive";});}
function loadInside(){if(tracking.ready)
return;tracking.ready=true;_initModule("realtime");_initModule("front");}
var _modules={};tracking.getModules=_getModules;function _getModules(){return _modules;}
tracking.initModule=_initModule;function _initModule(name){var module=_modules[name];if(typeof(module)!="undefined"&&!module.initialized){module.callback(tracking);module.initialized=true;return true;}
return false;}
tracking.registerModule=_registerModule;function _registerModule(name,callback){_modules[name]={name:name,callback:callback,initialized:false};}
tracking.processQueue=processQueue;function processQueue(){var tracker=tracking.current;if(typeof _inside==="object"&&typeof _inside.length!="undefined")
{for(var i=0;i<_inside.length;i++)
{if(typeof _inside[i]==="object")
{if(_inside[i].action=="getTracker")
tracker=tracking.getTracker(_inside[i]);if(tracker!=null){if(_inside[i].action=="trackView")
tracker.trackView(_inside[i]);else if(_inside[i].action=="trackOrder")
tracker.trackOrder(_inside[i]);else if(_inside[i].action=="addItem")
tracker.addItem(_inside[i]);else if(_inside[i].action=="trackForm")
tracker.trackForm(_inside[i]);}
if(_inside[i].action=="bind"){try{tracking.bind(_inside[i].name,_inside[i].callback);}
catch(ex){}}
if(_inside[i].action=="getVisitorDetails"&&typeof(_inside[i].callback)=="function"){tracking.getVisitorDetails=_inside[i].callback;}}
else if(typeof _inside[i]==="function"&&tracker!=null)
_inside[i](tracker);}
_inside.length=0;}
tracking.doEvent("onload",tracker);}
function ajax(method,url,data,callback){var xhr=null;if(typeof(XDomainRequest)!="undefined"&&ieVersion<=9)
{if(document.location.protocol=="http:"&&url.indexOf("https:")==0)
url=url.replace("https:","http:");var xdr=new XDomainRequest();xdr.open(method,url);xdr.onload=function(){if(this.responseText!="")
callback(JSON.parse(this.responseText));};xdr.onprogress=function(){};xdr.ontimeout=function(){};xdr.onerror=function(){};xdr.timeout=0;xdr.send(data);return;}
else if(typeof(XMLHttpRequest)!="undefined")
{xhr=new XMLHttpRequest();}
else
{try{try{xhr=new ActiveXObject("MSXML2.XMLHTTP.3.0");}
catch(ex){xhr=new ActiveXObject("Microsoft.XMLHTTP");}}catch(ex){}}
if(xhr!=null)
{xhr.onreadystatechange=function(){if(xhr.readyState==4&&xhr.status==200&&xhr.responseText!="")
callback(JSON.parse(xhr.responseText));};try{xhr.open(method,url,true);xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");xhr.send(data);}catch(ex){jsonp(url,data,callback);}}}
var _jsonp={},_jsonpi=1;function jsonp(url,data,callback){url=url+(url.indexOf("?")==-1?"?":"&")+ data;if(url.length>2000)
return;var s=d.createElement("script");var i=0;if(typeof(callback)=="function"){i=_jsonpi++;_jsonp[i.toString()]={"callback":callback,"script":s};}
s.type="text/javascript";s.src=url+"&cb="+i;s.async=true;var head=d.getElementsByTagName("head")[0];if(head){head.appendChild(s);}else{d.body.appendChild(s);}}
tracking.jsonpCallback=jsonpCallback;function jsonpCallback(cb,data){var entry=_jsonp[cb];if(entry!="undefined"){entry.callback(data);entry.script.parentNode.removeChild(entry.script);delete _jsonp[cb];}}
function track(url,data){ajax("POST",url,data+"&pid="+ _pid,function(data){startInside(data);});}
function eh(str){var p=d.createElement("p");p.innerHTML=str.replace(/</g,'&lt;').replace(/>/g,'&gt;');return String(p.textContent||p.innerText).replace(/</g,'&lt;').replace(/>/g,'&gt;');}
function cd(d){var nd;if(typeof(d)=="object"&&typeof(d.length)!="undefined")
{nd=[];for(var key in d)
{if(typeof(d[key])!="function")
{if(typeof(d[key])==="object")
nd.push(cd(d[key]));else if(typeof(d[key])=="number"||typeof(d[key])=="boolean")
nd.push(d[key]);else if(d[key]!=null)
nd.push(d[key].toString());}}}
else if(typeof(d)=="object")
{nd={};for(var key in d)
{if(typeof(d[key])!="function")
{if(typeof(d[key])==="object")
nd[key]=cd(d[key]);else if(typeof(d[key])=="number"||typeof(d[key])=="boolean")
nd[key]=d[key];else if(d[key]!=null)
nd[key]=d[key].toString();}}}
else if(typeof(d)=="number"||typeof(d)=="boolean")
nd=d;else if(typeof(d.toString)!="undefined")
nd=d.toString();else
nd="";return nd;}
function checkVisitorCookieLockout(context){var lockout=false;var visitorCookie=null;try
{if(_pid!=null){visitorCookie=_pid.split("-");if(visitorCookie.length>=4&&(visitorCookie[2]!="0"||visitorCookie[3]!="0")){try
{var pageLock=parseInt(visitorCookie[2]);var timeLock=parseInt(visitorCookie[3]);if(timeLock>0){if(new Date()<new Date(timeLock*60000)){if(pageLock>0){lockout=true;pageLock--;if(pageLock==0)
timeLock=0;}}}else if(pageLock>0){lockout=true;pageLock--;}
if(!lockout){pageLock=0;timeLock=0;}
visitorCookie[2]=pageLock.toString();visitorCookie[3]=timeLock.toString();_pid=visitorCookie.join("-");setCookie("inside-"+w["_insideCluster"],_pid,1000,_path,_domain);}
catch(ex){}}}}catch(ex){error("[INSIDE] Error parsing inside-pid: ",ex);}
if(typeof(context)=="undefined"||context==null)
return true;var tracker=context.tracker;var view=context.view;if(typeof(tracker)!="undefined"&&typeof(view)!="undefined"&&tracker!=null&&view!=null&&tracker.trafficLimiter!=null){if(checkTraffic(context)){if(lockout){lockout=false;if(visitorCookie!=null){visitorCookie[2]="0";visitorCookie[3]="0";_pid=visitorCookie.join("-");}
setCookie("inside-"+w["_insideCluster"],_pid,1000,_path,_domain);}}else if(!lockout){lockout=true;if(visitorCookie!=null){visitorCookie[2]=tracker.trafficLimiter.pageLock.toString();visitorCookie[3]=(parseInt(new Date().getTime()/ 60000) + tracker.trafficLimiter.timeLock).toString();_pid=visitorCookie.join("-");}
setCookie("inside-"+w["_insideCluster"],_pid,1000,_path,_domain);}}
return lockout;}
function checkTraffic(context){if(typeof(context)=="undefined"||context==null)
return true;if(typeof(context.tracker)!="undefined"&&context.tracker.trafficLimiter!=null){if(context.tracker.trafficLimiter.filters.length==0)
return false;for(var i=0;i<context.tracker.trafficLimiter.filters.length;i++){var filter=context.tracker.trafficLimiter.filters[i];var prop=filter.property.split(".");var source=context[prop[0]];if(source!=null){var check=checkFilter(filter,source);if(filter.exclude)
check=!check;if(!check)
return false;}}}
return true;}
function checkFilter(filter,source){try
{var obj=source;var prop=filter.property.split(".");for(var i=1;i<prop.length;i++){if(typeof(obj[prop[i]])!="undefined")
obj=obj[prop[i]];else
obj=null;break;}
if(filter.propertyType=="number")
obj=obj==null?0:parseFloat(obj);else if(filter.propertyType=="string")
obj=obj==null?"":obj.toString().toLowerCase();var isArray=Object.prototype.toString.call(filter.value)==="[object Array]";if(filter.operation=="="||filter.operation=="==")
return obj==filter.value;else if(filter.operation=="!="||filter.propertyType=="<>")
return obj!=filter.value;else if(filter.operation==">")
return obj>filter.value;else if(filter.operation==">=")
return obj>=filter.value;else if(filter.operation=="<")
return obj<filter.value;else if(filter.operation=="<=")
return obj<=filter.value;else if(filter.operation=="in"&&isArray)
return filter.value.indexOf(obj)>=0;else if(filter.operation=="between"&&isArray)
return obj>=filter.value[0]&&obj<=filter.value[1];else if(filter.operation=="match"&&typeof(filter.value)=="string"){var pattern=new RegExp(filter.value);return pattern.test(obj.toString());}}
catch(ex){error("[INSIDE] Error processing filter\r\n :: property = \""+ filter.property+"\", operation = \""+ filter.operation+"\", value = "+ JSON.stringify(filter.value)+"\r\n :: ",ex);}
return false;}
setTimeout(processQueue,0);return tracking;}(window,document);_insideGraph["_jQuery"]=window.jQuery;_insideGraph["_$"]=window.$;if(typeof jQuery!="undefined"){jQuery.noConflict();}
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="1.12.0",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a){return n.each(this,a)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(e.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(n.isPlainObject(c)||(b=n.isArray(c)))?(b?(b=!1,f=a&&n.isArray(a)?a:[]):f=a&&n.isPlainObject(a)?a:{},g[d]=n.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray||function(a){return"array"===n.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){var b=a&&a.toString();return!n.isArray(a)&&b-parseFloat(b)+1>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==n.type(a)||a.nodeType||n.isWindow(a))return!1;try{if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(!l.ownFirst)for(b in a)return k.call(a,b);for(b in a);return void 0===b||k.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a},globalEval:function(b){b&&n.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(s(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(h)return h.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,g=0,h=[];if(s(a))for(d=a.length;d>g;g++)e=b(a[g],g,c),null!=e&&h.push(e);else for(g in a)e=b(a[g],g,c),null!=e&&h.push(e);return f.apply([],h)},guid:1,proxy:function(a,b){var c,d,f;return"string"==typeof b&&(f=a[b],b=a,a=f),n.isFunction(a)?(c=e.call(arguments,2),d=function(){return a.apply(b||this,c.concat(e.call(arguments)))},d.guid=a.guid=a.guid||n.guid++,d):void 0},now:function(){return+new Date},support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=!!a&&"length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},da=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a)))if(f=o[1]){if(9===x){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(o[2])return H.apply(d,b.getElementsByTagName(a)),d;if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName)return H.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x)w=b,s=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";while(h--)r[h]=l+" "+qa(r[h]);s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b}if(s)try{return H.apply(d,w.querySelectorAll(s)),d}catch(y){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(Q,"$1"),b,d,e)}function ga(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ha(a){return a[u]=!0,a}function ia(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ja(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function la(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ia(function(a){var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return ka(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},fa.matches=function(a,b){return fa(a,null,null,b)},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fa(b,n,null,[a]).length>0},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ha(function(a){return function(b){return fa(a,b).length>0}}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Y.test(a.nodeName)},input:function(a){return X.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:na(function(){return[0]}),last:na(function(a,b){return[b-1]}),eq:na(function(a,b,c){return[0>c?c+b:c]}),even:na(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:na(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=la(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=ma(b);function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=R.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));for(g in d.filter)!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fa.error(a):z(a,i).slice(0)};function qa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j,k=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f)return k[2]=h[2];if(i[d]=k,k[2]=a(b,c,g))return!0}}}function sa(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ta(a,b,c){for(var d=0,e=b.length;e>d;d++)fa(a,b[d],c);return c}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b},h,!0),l=ra(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ra(sa(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))}m.push(c)}return sa(m)}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=F.call(i));u=ua(u)}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ha(f):f}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xa(e,d)),f.selector=a}return f},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=W.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ia(function(a){return null==a.getAttribute("disabled")})||ja(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fa}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},v=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;function z(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(y.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return n.inArray(a,b)>-1!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;e>b;b++)if(n.contains(d[b],this))return!0}));for(b=0;e>b;b++)n.find(a,d[b],c);return c=this.pushStack(e>1?n.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(z(this,a||[],!1))},not:function(a){return this.pushStack(z(this,a||[],!0))},is:function(a){return!!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length}});var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||A,"string"==typeof a){if(e="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b))for(e in b)n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}if(f=d.getElementById(e[2]),f&&f.parentNode){if(f.id!==e[2])return A.find(a);this.length=1,this[0]=f}return this.context=d,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};C.prototype=n.fn,A=n(d);var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};n.fn.extend({has:function(a){var b,c=n(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(n.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?n.inArray(this[0],n(a)):n.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function F(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return u(a,"parentNode")},parentsUntil:function(a,b,c){return u(a,"parentNode",c)},next:function(a){return F(a,"nextSibling")},prev:function(a){return F(a,"previousSibling")},nextAll:function(a){return u(a,"nextSibling")},prevAll:function(a){return u(a,"previousSibling")},nextUntil:function(a,b,c){return u(a,"nextSibling",c)},prevUntil:function(a,b,c){return u(a,"previousSibling",c)},siblings:function(a){return v((a.parentNode||{}).firstChild,a)},children:function(a){return v(a.firstChild)},contents:function(a){return n.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||(e=n.uniqueSort(e)),D.test(a)&&(e=e.reverse())),this.pushStack(e)}});var G=/\S+/g;function H(a){var b={};return n.each(a.match(G)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return n.each(arguments,function(a,b){var c;while((c=n.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?n.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=!0,c||j.disable(),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(d>1)for(i=new Array(d),j=new Array(d),k=new Array(d);d>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))}});function J(){d.addEventListener?(d.removeEventListener("DOMContentLoaded",K),a.removeEventListener("load",K)):(d.detachEvent("onreadystatechange",K),a.detachEvent("onload",K))}function K(){(d.addEventListener||"load"===a.event.type||"complete"===d.readyState)&&(J(),n.ready())}n.ready.promise=function(b){if(!I)if(I=n.Deferred(),"complete"===d.readyState)a.setTimeout(n.ready);else if(d.addEventListener)d.addEventListener("DOMContentLoaded",K),a.addEventListener("load",K);else{d.attachEvent("onreadystatechange",K),a.attachEvent("onload",K);var c=!1;try{c=null==a.frameElement&&d.documentElement}catch(e){}c&&c.doScroll&&!function f(){if(!n.isReady){try{c.doScroll("left")}catch(b){return a.setTimeout(f,50)}J(),n.ready()}}()}return I.promise(b)},n.ready.promise();var L;for(L in n(l))break;l.ownFirst="0"===L,l.inlineBlockNeedsLayout=!1,n(function(){var a,b,c,e;c=d.getElementsByTagName("body")[0],c&&c.style&&(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",l.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(e))}),function(){var a=d.createElement("div");l.deleteExpando=!0;try{delete a.test}catch(b){l.deleteExpando=!1}a=null}();var M=function(a){var b=n.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b},N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(O,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}n.data(a,b,c)}else c=void 0}return c}function Q(a){var b;for(b in a)if(("data"!==b||!n.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function R(a,b,d,e){if(M(a)){var f,g,h=n.expando,i=a.nodeType,j=i?n.cache:a,k=i?a[h]:a[h]&&h;if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||n.guid++:h),j[k]||(j[k]=i?{}:{toJSON:n.noop}),("object"==typeof b||"function"==typeof b)&&(e?j[k]=n.extend(j[k],b):j[k].data=n.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[n.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[n.camelCase(b)])):f=g,f}}function S(a,b,c){if(M(a)){var d,e,f=a.nodeType,g=f?n.cache:a,h=f?a[n.expando]:n.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){n.isArray(b)?b=b.concat(n.map(b,n.camelCase)):b in d?b=[b]:(b=n.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!Q(d):!n.isEmptyObject(d))return}(c||(delete g[h].data,Q(g[h])))&&(f?n.cleanData([a],!0):l.deleteExpando||g!=g.window?delete g[h]:g[h]=void 0)}}}n.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?n.cache[a[n.expando]]:a[n.expando],!!a&&!Q(a)},data:function(a,b,c){return R(a,b,c)},removeData:function(a,b){return S(a,b)},_data:function(a,b,c){return R(a,b,c,!0)},_removeData:function(a,b){return S(a,b,!0)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=n.data(f),1===f.nodeType&&!n._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])));n._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){n.data(this,a)}):arguments.length>1?this.each(function(){n.data(this,a,b)}):f?P(f,a,n.data(f,a)):void 0},removeData:function(a){return this.each(function(){n.removeData(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=n._data(a,b),c&&(!d||n.isArray(c)?d=n._data(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return n._data(a,c)||n._data(a,c,{empty:n.Callbacks("once memory").add(function(){n._removeData(a,b+"queue"),n._removeData(a,c)})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=n._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}}),function(){var a;l.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,e;return c=d.getElementsByTagName("body")[0],c&&c.style?(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(d.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(e),a):void 0}}();var T=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,U=new RegExp("^(?:([+-])=|)("+T+")([a-z%]*)$","i"),V=["Top","Right","Bottom","Left"],W=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)};function X(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return n.css(a,b,"")},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&U.exec(n.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,n.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var Y=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)Y(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},Z=/^(?:checkbox|radio)$/i,$=/<([\w:-]+)/,_=/^$|\/(?:java|ecma)script/i,aa=/^\s+/,ba="abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";function ca(a){var b=ba.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}!function(){var a=d.createElement("div"),b=d.createDocumentFragment(),c=d.createElement("input");a.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",l.leadingWhitespace=3===a.firstChild.nodeType,l.tbody=!a.getElementsByTagName("tbody").length,l.htmlSerialize=!!a.getElementsByTagName("link").length,l.html5Clone="<:nav></:nav>"!==d.createElement("nav").cloneNode(!0).outerHTML,c.type="checkbox",c.checked=!0,b.appendChild(c),l.appendChecked=c.checked,a.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!a.cloneNode(!0).lastChild.defaultValue,b.appendChild(a),c=d.createElement("input"),c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),a.appendChild(c),l.checkClone=a.cloneNode(!0).cloneNode(!0).lastChild.checked,l.noCloneEvent=!!a.addEventListener,a[n.expando]=1,l.attributes=!a.getAttribute(n.expando)}();var da={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:l.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]};da.optgroup=da.option,da.tbody=da.tfoot=da.colgroup=da.caption=da.thead,da.th=da.td;function ea(a,b){var c,d,e=0,f="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||n.nodeName(d,b)?f.push(d):n.merge(f,ea(d,b));return void 0===b||b&&n.nodeName(a,b)?n.merge([a],f):f}function fa(a,b){for(var c,d=0;null!=(c=a[d]);d++)n._data(c,"globalEval",!b||n._data(b[d],"globalEval"))}var ga=/<|&#?\w+;/,ha=/<tbody/i;function ia(a){Z.test(a.type)&&(a.defaultChecked=a.checked)}function ja(a,b,c,d,e){for(var f,g,h,i,j,k,m,o=a.length,p=ca(b),q=[],r=0;o>r;r++)if(g=a[r],g||0===g)if("object"===n.type(g))n.merge(q,g.nodeType?[g]:g);else if(ga.test(g)){i=i||p.appendChild(b.createElement("div")),j=($.exec(g)||["",""])[1].toLowerCase(),m=da[j]||da._default,i.innerHTML=m[1]+n.htmlPrefilter(g)+m[2],f=m[0];while(f--)i=i.lastChild;if(!l.leadingWhitespace&&aa.test(g)&&q.push(b.createTextNode(aa.exec(g)[0])),!l.tbody){g="table"!==j||ha.test(g)?"<table>"!==m[1]||ha.test(g)?0:i:i.firstChild,f=g&&g.childNodes.length;while(f--)n.nodeName(k=g.childNodes[f],"tbody")&&!k.childNodes.length&&g.removeChild(k)}n.merge(q,i.childNodes),i.textContent="";while(i.firstChild)i.removeChild(i.firstChild);i=p.lastChild}else q.push(b.createTextNode(g));i&&p.removeChild(i),l.appendChecked||n.grep(ea(q,"input"),ia),r=0;while(g=q[r++])if(d&&n.inArray(g,d)>-1)e&&e.push(g);else if(h=n.contains(g.ownerDocument,g),i=ea(p.appendChild(g),"script"),h&&fa(i),c){f=0;while(g=i[f++])_.test(g.type||"")&&c.push(g)}return i=null,p}!function(){var b,c,e=d.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(l[b]=c in a)||(e.setAttribute(c,"t"),l[b]=e.attributes[c].expando===!1);e=null}();var ka=/^(?:input|select|textarea)$/i,la=/^key/,ma=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,na=/^(?:focusinfocus|focusoutblur)$/,oa=/^([^.]*)(?:\.(.+)|)/;function pa(){return!0}function qa(){return!1}function ra(){try{return d.activeElement}catch(a){}}function sa(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)sa(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=qa;else if(!e)return a;return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)})}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=n.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return"undefined"==typeof n||a&&n.event.triggered===a.type?void 0:n.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(G)||[""],h=b.length;while(h--)f=oa.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=n.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=n.event.special[o]||{},l=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},i),(m=g[o])||(m=g[o]=[],m.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,l):m.push(l),n.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n.hasData(a)&&n._data(a);if(r&&(k=r.events)){b=(b||"").match(G)||[""],j=b.length;while(j--)if(h=oa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=m.length;while(f--)g=m[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(m.splice(f,1),g.selector&&m.delegateCount--,l.remove&&l.remove.call(a,g));i&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(k)&&(delete r.handle,n._removeData(a,"events"))}},trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];if(i=m=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!na.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),h=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),l=n.event.special[q]||{},f||!l.trigger||l.trigger.apply(e,c)!==!1)){if(!f&&!l.noBubble&&!n.isWindow(e)){for(j=l.delegateType||q,na.test(j+q)||(i=i.parentNode);i;i=i.parentNode)p.push(i),m=i;m===(e.ownerDocument||d)&&p.push(m.defaultView||m.parentWindow||a)}o=0;while((i=p[o++])&&!b.isPropagationStopped())b.type=o>1?j:l.bindType||q,g=(n._data(i,"events")||{})[b.type]&&n._data(i,"handle"),g&&g.apply(i,c),g=h&&i[h],g&&g.apply&&M(i)&&(b.result=g.apply(i,c),b.result===!1&&b.preventDefault());if(b.type=q,!f&&!b.isDefaultPrevented()&&(!l._default||l._default.apply(p.pop(),c)===!1)&&M(e)&&h&&e[q]&&!n.isWindow(e)){m=e[h],m&&(e[h]=null),n.event.triggered=q;try{e[q]()}catch(s){}n.event.triggered=void 0,m&&(e[h]=m)}return b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,d,f,g,h=[],i=e.call(arguments),j=(n._data(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())(!a.rnamespace||a.rnamespace.test(g.namespace))&&(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[n.expando])return a;var b,c,e,f=a.type,g=a,h=this.fixHooks[f];h||(this.fixHooks[f]=h=ma.test(f)?this.mouseHooks:la.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;while(b--)c=e[b],a[c]=g[c];return a.target||(a.target=g.srcElement||d),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,h.filter?h.filter(a,g):a},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button,h=b.fromElement;return null==a.pageX&&null!=b.clientX&&(e=a.target.ownerDocument||d,f=e.documentElement,c=e.body,a.pageX=b.clientX+(f&&f.scrollLeft||c&&c.scrollLeft||0)-(f&&f.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(f&&f.scrollTop||c&&c.scrollTop||0)-(f&&f.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&h&&(a.relatedTarget=h===a.target?b.toElement:h),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==ra()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===ra()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return n.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=d.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)}:function(a,b,c){var d="on"+b;a.detachEvent&&("undefined"==typeof a[d]&&(a[d]=null),a.detachEvent(d,c))},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?pa:qa):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={constructor:n.Event,isDefaultPrevented:qa,isPropagationStopped:qa,isImmediatePropagationStopped:qa,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=pa,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=pa,a&&!this.isSimulated&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=pa,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!n.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),l.submit||(n.event.special.submit={setup:function(){return n.nodeName(this,"form")?!1:void n.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=n.nodeName(b,"input")||n.nodeName(b,"button")?n.prop(b,"form"):void 0;c&&!n._data(c,"submit")&&(n.event.add(c,"submit._submit",function(a){a._submitBubble=!0}),n._data(c,"submit",!0))})},postDispatch:function(a){a._submitBubble&&(delete a._submitBubble,this.parentNode&&!a.isTrigger&&n.event.simulate("submit",this.parentNode,a))},teardown:function(){return n.nodeName(this,"form")?!1:void n.event.remove(this,"._submit")}}),l.change||(n.event.special.change={setup:function(){return ka.test(this.nodeName)?(("checkbox"===this.type||"radio"===this.type)&&(n.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._justChanged=!0)}),n.event.add(this,"click._change",function(a){this._justChanged&&!a.isTrigger&&(this._justChanged=!1),n.event.simulate("change",this,a)})),!1):void n.event.add(this,"beforeactivate._change",function(a){var b=a.target;ka.test(b.nodeName)&&!n._data(b,"change")&&(n.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||n.event.simulate("change",this.parentNode,a)}),n._data(b,"change",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return n.event.remove(this,"._change"),!ka.test(this.nodeName)}}),l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=n._data(d,b);e||d.addEventListener(a,c,!0),n._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=n._data(d,b)-1;e?n._data(d,b,e):(d.removeEventListener(a,c,!0),n._removeData(d,b))}}}),n.fn.extend({on:function(a,b,c,d){return sa(this,a,b,c,d)},one:function(a,b,c,d){return sa(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=qa),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});var ta=/ jQuery\d+="(?:null|\d+)"/g,ua=new RegExp("<(?:"+ba+")[\\s/>]","i"),va=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,wa=/<script|<style|<link/i,xa=/checked\s*(?:[^=]|=\s*.checked.)/i,ya=/^true\/(.*)/,za=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,Aa=ca(d),Ba=Aa.appendChild(d.createElement("div"));function Ca(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function Da(a){return a.type=(null!==n.find.attr(a,"type"))+"/"+a.type,a}function Ea(a){var b=ya.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Fa(a,b){if(1===b.nodeType&&n.hasData(a)){var c,d,e,f=n._data(a),g=n._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)n.event.add(b,c,h[c][d])}g.data&&(g.data=n.extend({},g.data))}}function Ga(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!l.noCloneEvent&&b[n.expando]){e=n._data(b);for(d in e.events)n.removeEvent(b,d,e.handle);b.removeAttribute(n.expando)}"script"===c&&b.text!==a.text?(Da(b).text=a.text,Ea(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),l.html5Clone&&a.innerHTML&&!n.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&Z.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}}function Ha(a,b,c,d){b=f.apply([],b);var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);if(r||o>1&&"string"==typeof q&&!l.checkClone&&xa.test(q))return a.each(function(e){var f=a.eq(e);r&&(b[0]=q.call(this,e,f.html())),Ha(f,b,c,d)});if(o&&(k=ja(b,a[0].ownerDocument,!1,a,d),e=k.firstChild,1===k.childNodes.length&&(k=e),e||d)){for(i=n.map(ea(k,"script"),Da),h=i.length;o>m;m++)g=k,m!==p&&(g=n.clone(g,!0,!0),h&&n.merge(i,ea(g,"script"))),c.call(a[m],g,m);if(h)for(j=i[i.length-1].ownerDocument,n.map(i,Ea),m=0;h>m;m++)g=i[m],_.test(g.type||"")&&!n._data(g,"globalEval")&&n.contains(j,g)&&(g.src?n._evalUrl&&n._evalUrl(g.src):n.globalEval((g.text||g.textContent||g.innerHTML||"").replace(za,"")));k=e=null}return a}function Ia(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||n.cleanData(ea(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&fa(ea(d,"script")),d.parentNode.removeChild(d));return a}n.extend({htmlPrefilter:function(a){return a.replace(va,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h,i=n.contains(a.ownerDocument,a);if(l.html5Clone||n.isXMLDoc(a)||!ua.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(Ba.innerHTML=a.outerHTML,Ba.removeChild(f=Ba.firstChild)),!(l.noCloneEvent&&l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(d=ea(f),h=ea(a),g=0;null!=(e=h[g]);++g)d[g]&&Ga(e,d[g]);if(b)if(c)for(h=h||ea(a),d=d||ea(f),g=0;null!=(e=h[g]);g++)Fa(e,d[g]);else Fa(a,f);return d=ea(f,"script"),d.length>0&&fa(d,!i&&ea(a,"script")),d=h=e=null,f},cleanData:function(a,b){for(var d,e,f,g,h=0,i=n.expando,j=n.cache,k=l.attributes,m=n.event.special;null!=(d=a[h]);h++)if((b||M(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)m[e]?n.event.remove(d,e):n.removeEvent(d,e,g.handle);j[f]&&(delete j[f],k||"undefined"==typeof d.removeAttribute?d[i]=void 0:d.removeAttribute(i),c.push(f))}}}),n.fn.extend({domManip:Ha,detach:function(a){return Ia(this,a,!0)},remove:function(a){return Ia(this,a)},text:function(a){return Y(this,function(a){return void 0===a?n.text(this):this.empty().append((this[0]&&this[0].ownerDocument||d).createTextNode(a))},null,a,arguments.length)},append:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.appendChild(a)}})},prepend:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&n.cleanData(ea(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&n.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return Y(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(ta,""):void 0;if("string"==typeof a&&!wa.test(a)&&(l.htmlSerialize||!ua.test(a))&&(l.leadingWhitespace||!aa.test(a))&&!da[($.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(ea(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return Ha(this,arguments,function(b){var c=this.parentNode;n.inArray(this,a)<0&&(n.cleanData(ea(this)),c&&c.replaceChild(b,this))},a)}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=0,e=[],f=n(a),h=f.length-1;h>=d;d++)c=d===h?this:this.clone(!0),n(f[d])[b](c),g.apply(e,c.get());return this.pushStack(e)}});var Ja,Ka={HTML:"block",BODY:"block"};function La(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");return c.detach(),d}function Ma(a){var b=d,c=Ka[a];return c||(c=La(a,b),"none"!==c&&c||(Ja=(Ja||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Ja[0].contentWindow||Ja[0].contentDocument).document,b.write(),b.close(),c=La(a,b),Ja.detach()),Ka[a]=c),c}var Na=/^margin/,Oa=new RegExp("^("+T+")(?!px)[a-z%]+$","i"),Pa=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e},Qa=d.documentElement;!function(){var b,c,e,f,g,h,i=d.createElement("div"),j=d.createElement("div");if(j.style){j.style.cssText="float:left;opacity:.5",l.opacity="0.5"===j.style.opacity,l.cssFloat=!!j.style.cssFloat,j.style.backgroundClip="content-box",j.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===j.style.backgroundClip,i=d.createElement("div"),i.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",j.innerHTML="",i.appendChild(j),l.boxSizing=""===j.style.boxSizing||""===j.style.MozBoxSizing||""===j.style.WebkitBoxSizing,n.extend(l,{reliableHiddenOffsets:function(){return null==b&&k(),f},boxSizingReliable:function(){return null==b&&k(),e},pixelMarginRight:function(){return null==b&&k(),c},pixelPosition:function(){return null==b&&k(),b},reliableMarginRight:function(){return null==b&&k(),g},reliableMarginLeft:function(){return null==b&&k(),h}});function k(){var k,l,m=d.documentElement;m.appendChild(i),j.style.cssText="-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",b=e=h=!1,c=g=!0,a.getComputedStyle&&(l=a.getComputedStyle(j),b="1%"!==(l||{}).top,h="2px"===(l||{}).marginLeft,e="4px"===(l||{width:"4px"}).width,j.style.marginRight="50%",c="4px"===(l||{marginRight:"4px"}).marginRight,k=j.appendChild(d.createElement("div")),k.style.cssText=j.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",k.style.marginRight=k.style.width="0",j.style.width="1px",g=!parseFloat((a.getComputedStyle(k)||{}).marginRight),j.removeChild(k)),j.style.display="none",f=0===j.getClientRects().length,f&&(j.style.display="",j.innerHTML="<table><tr><td></td><td>t</td></tr></table>",k=j.getElementsByTagName("td"),k[0].style.cssText="margin:0;border:0;padding:0;display:none",f=0===k[0].offsetHeight,f&&(k[0].style.display="",k[1].style.display="none",f=0===k[0].offsetHeight)),m.removeChild(i)}}}();var Ra,Sa,Ta=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ra=function(b){var c=b.ownerDocument.defaultView;return c.opener||(c=a),c.getComputedStyle(b)},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c.getPropertyValue(b)||c[b]:void 0,c&&(""!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),!l.pixelMarginRight()&&Oa.test(g)&&Na.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0===g?g:g+""}):Qa.currentStyle&&(Ra=function(a){return a.currentStyle},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Oa.test(g)&&!Ta.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Ua(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Va=/alpha\([^)]*\)/i,Wa=/opacity\s*=\s*([^)]*)/i,Xa=/^(none|table(?!-c[ea]).+)/,Ya=new RegExp("^("+T+")(.*)$","i"),Za={position:"absolute",visibility:"hidden",display:"block"},$a={letterSpacing:"0",fontWeight:"400"},_a=["Webkit","O","Moz","ms"],ab=d.createElement("div").style;function bb(a){if(a in ab)return a;var b=a.charAt(0).toUpperCase()+a.slice(1),c=_a.length;while(c--)if(a=_a[c]+b,a in ab)return a}function cb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=n._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&W(d)&&(f[g]=n._data(d,"olddisplay",Ma(d.nodeName)))):(e=W(d),(c&&"none"!==c||!e)&&n._data(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function db(a,b,c){var d=Ya.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function eb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+V[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+V[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+V[f]+"Width",!0,e))):(g+=n.css(a,"padding"+V[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+V[f]+"Width",!0,e)));return g}function fb(b,c,e){var f=!0,g="width"===c?b.offsetWidth:b.offsetHeight,h=Ra(b),i=l.boxSizing&&"border-box"===n.css(b,"boxSizing",!1,h);if(d.msFullscreenElement&&a.top!==a&&b.getClientRects().length&&(g=Math.round(100*b.getBoundingClientRect()[c])),0>=g||null==g){if(g=Sa(b,c,h),(0>g||null==g)&&(g=b.style[c]),Oa.test(g))return g;f=i&&(l.boxSizingReliable()||g===b.style[c]),g=parseFloat(g)||0}return g+eb(b,c,e||(i?"border":"content"),f,h)+"px"}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Sa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":l.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;if(b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=U.exec(c))&&e[1]&&(c=X(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Sa(a,b,d)),"normal"===f&&b in $a&&(f=$a[b]),""===c||c?(e=parseFloat(f),c===!0||isFinite(e)?e||0:f):f}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Xa.test(n.css(a,"display"))&&0===a.offsetWidth?Pa(a,Za,function(){return fb(a,b,d)}):fb(a,b,d):void 0},set:function(a,c,d){var e=d&&Ra(a);return db(a,c,d?eb(a,b,d,l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),l.opacity||(n.cssHooks.opacity={get:function(a,b){return Wa.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=n.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===n.trim(f.replace(Va,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Va.test(f)?f.replace(Va,e):f+" "+e)}}),n.cssHooks.marginRight=Ua(l.reliableMarginRight,function(a,b){return b?Pa(a,{display:"inline-block"},Sa,[a,"marginRight"]):void 0}),n.cssHooks.marginLeft=Ua(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Sa(a,"marginLeft"))||(n.contains(a.ownerDocument,a)?a.getBoundingClientRect().left-Pa(a,{marginLeft:0},function(){return a.getBoundingClientRect().left}):0))+"px":void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+V[d]+b]=f[d]||f[d-2]||f[0];return e}},Na.test(a)||(n.cssHooks[a+b].set=db)}),n.fn.extend({css:function(a,b){return Y(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Ra(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return cb(this,!0)},hide:function(){return cb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){W(this)?n(this).show():n(this).hide()})}});function gb(a,b,c,d,e){return new gb.prototype.init(a,b,c,d,e)}n.Tween=gb,gb.prototype={constructor:gb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=gb.propHooks[this.prop];return a&&a.get?a.get(this):gb.propHooks._default.get(this)},run:function(a){var b,c=gb.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):gb.propHooks._default.set(this),this}},gb.prototype.init.prototype=gb.prototype,gb.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)}}},gb.propHooks.scrollTop=gb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},n.fx=gb.prototype.init,n.fx.step={};var hb,ib,jb=/^(?:toggle|show|hide)$/,kb=/queueHooks$/;function lb(){return a.setTimeout(function(){hb=void 0}),hb=n.now()}function mb(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=V[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function nb(a,b,c){for(var d,e=(qb.tweeners[b]||[]).concat(qb.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ob(a,b,c){var d,e,f,g,h,i,j,k,m=this,o={},p=a.style,q=a.nodeType&&W(a),r=n._data(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,m.always(function(){m.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=n.css(a,"display"),k="none"===j?n._data(a,"olddisplay")||Ma(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(l.inlineBlockNeedsLayout&&"inline"!==Ma(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",l.shrinkWrapBlocks()||m.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],jb.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(o))"inline"===("none"===j?Ma(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=n._data(a,"fxshow",{}),f&&(r.hidden=!q),q?n(a).show():m.done(function(){n(a).hide()}),m.done(function(){var b;n._removeData(a,"fxshow");for(b in o)n.style(a,b,o[b])});for(d in o)g=nb(q?r[d]:0,d,m),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function pb(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function qb(a,b,c){var d,e,f=0,g=qb.prefilters.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=hb||lb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:hb||lb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for(pb(k,j.opts.specialEasing);g>f;f++)if(d=qb.prefilters[f].call(j,a,k,j.opts))return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d;return n.map(k,nb,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(qb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return X(c.elem,a,U.exec(b),c),c}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);for(var c,d=0,e=a.length;e>d;d++)c=a[d],qb.tweeners[c]=qb.tweeners[c]||[],qb.tweeners[c].unshift(b)},prefilters:[ob],prefilter:function(a,b){b?qb.prefilters.unshift(a):qb.prefilters.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(W).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=qb(this,n.extend({},a),f);(e||n._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=n._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&kb.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=n._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(mb(b,!0),a,d,e)}}),n.each({slideDown:mb("show"),slideUp:mb("hide"),slideToggle:mb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=n.timers,c=0;for(hb=n.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||n.fx.stop(),hb=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){ib||(ib=a.setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){a.clearInterval(ib),ib=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a,b=d.createElement("input"),c=d.createElement("div"),e=d.createElement("select"),f=e.appendChild(d.createElement("option"));c=d.createElement("div"),c.setAttribute("className","t"),c.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=c.getElementsByTagName("a")[0],b.setAttribute("type","checkbox"),c.appendChild(b),a=c.getElementsByTagName("a")[0],a.style.cssText="top:1px",l.getSetAttribute="t"!==c.className,l.style=/top/.test(a.getAttribute("style")),l.hrefNormalized="/a"===a.getAttribute("href"),l.checkOn=!!b.value,l.optSelected=f.selected,l.enctype=!!d.createElement("form").enctype,e.disabled=!0,l.optDisabled=!f.disabled,b=d.createElement("input"),b.setAttribute("value",""),l.input=""===b.getAttribute("value"),b.value="t",b.setAttribute("type","radio"),l.radioValue="t"===b.value}();var rb=/\r/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(rb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a))}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)if(d=e[g],n.inArray(n.valHooks.option.get(d),f)>=0)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var sb,tb,ub=n.expr.attrHandle,vb=/^(?:checked|selected)$/i,wb=l.getSetAttribute,xb=l.input;n.fn.extend({attr:function(a,b){return Y(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?tb:sb)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)?xb&&wb||!vb.test(c)?a[d]=!1:a[n.camelCase("default-"+c)]=a[d]=!1:n.attr(a,c,""),a.removeAttribute(wb?c:d)}}),tb={set:function(a,b,c){return b===!1?n.removeAttr(a,c):xb&&wb||!vb.test(c)?a.setAttribute(!wb&&n.propFix[c]||c,c):a[n.camelCase("default-"+c)]=a[c]=!0,c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=ub[b]||n.find.attr;xb&&wb||!vb.test(b)?ub[b]=function(a,b,d){var e,f;return d||(f=ub[b],ub[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,ub[b]=f),e}:ub[b]=function(a,b,c){return c?void 0:a[n.camelCase("default-"+b)]?b.toLowerCase():null}}),xb&&wb||(n.attrHooks.value={set:function(a,b,c){return n.nodeName(a,"input")?void(a.defaultValue=b):sb&&sb.set(a,b,c)}}),wb||(sb={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},ub.id=ub.name=ub.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},n.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:sb.set},n.attrHooks.contenteditable={set:function(a,b,c){sb.set(a,""===b?!1:b,c)}},n.each(["width","height"],function(a,b){n.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),l.style||(n.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var yb=/^(?:input|select|textarea|button|object)$/i,zb=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return Y(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return a=n.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):yb.test(a.nodeName)||zb.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),l.hrefNormalized||n.each(["href","src"],function(a,b){n.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this}),l.enctype||(n.propFix.enctype="encoding");var Ab=/[\t\r\n\f]/g;function Bb(a){return n.attr(a,"class")||""}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,Bb(this)))});if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Bb(c),d=1===c.nodeType&&(" "+e+" ").replace(Ab," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,Bb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Bb(c),d=1===c.nodeType&&(" "+e+" ").replace(Ab," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,Bb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=n(this),f=a.match(G)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(void 0===a||"boolean"===c)&&(b=Bb(this),b&&n._data(this,"__className__",b),n.attr(this,"class",b||a===!1?"":n._data(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+Bb(c)+" ").replace(Ab," ").indexOf(b)>-1)return!0;return!1}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}});var Cb=a.location,Db=n.now(),Eb=/\?/,Fb=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;n.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=n.trim(b+"");return e&&!n.trim(e.replace(Fb,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():n.error("Invalid JSON: "+b)},n.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new a.DOMParser,c=d.parseFromString(b,"text/xml")):(c=new a.ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c};var Gb=/#.*$/,Hb=/([?&])_=[^&]*/,Ib=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Jb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Kb=/^(?:GET|HEAD)$/,Lb=/^\/\//,Mb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Nb={},Ob={},Pb="*/".concat("*"),Qb=Cb.href,Rb=Mb.exec(Qb.toLowerCase())||[];function Sb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(G)||[];if(n.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Tb(a,b,c,d){var e={},f=a===Ob;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Ub(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&n.extend(!0,a,c),a}function Vb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Wb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Qb,type:"GET",isLocal:Jb.test(Rb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Pb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Ub(Ub(a,n.ajaxSettings),b):Ub(n.ajaxSettings,a)},ajaxPrefilter:Sb(Nb),ajaxTransport:Sb(Ob),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var d,e,f,g,h,i,j,k,l=n.ajaxSetup({},c),m=l.context||l,o=l.context&&(m.nodeType||m.jquery)?n(m):n.event,p=n.Deferred(),q=n.Callbacks("once memory"),r=l.statusCode||{},s={},t={},u=0,v="canceled",w={readyState:0,getResponseHeader:function(a){var b;if(2===u){if(!k){k={};while(b=Ib.exec(g))k[b[1].toLowerCase()]=b[2]}b=k[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===u?g:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return u||(a=t[c]=t[c]||a,s[a]=b),this},overrideMimeType:function(a){return u||(l.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>u)for(b in a)r[b]=[r[b],a[b]];else w.always(a[w.status]);return this},abort:function(a){var b=a||v;return j&&j.abort(b),y(0,b),this}};if(p.promise(w).complete=q.add,w.success=w.done,w.error=w.fail,l.url=((b||l.url||Qb)+"").replace(Gb,"").replace(Lb,Rb[1]+"//"),l.type=c.method||c.type||l.method||l.type,l.dataTypes=n.trim(l.dataType||"*").toLowerCase().match(G)||[""],null==l.crossDomain&&(d=Mb.exec(l.url.toLowerCase()),l.crossDomain=!(!d||d[1]===Rb[1]&&d[2]===Rb[2]&&(d[3]||("http:"===d[1]?"80":"443"))===(Rb[3]||("http:"===Rb[1]?"80":"443")))),l.data&&l.processData&&"string"!=typeof l.data&&(l.data=n.param(l.data,l.traditional)),Tb(Nb,l,c,w),2===u)return w;i=n.event&&l.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),l.type=l.type.toUpperCase(),l.hasContent=!Kb.test(l.type),f=l.url,l.hasContent||(l.data&&(f=l.url+=(Eb.test(f)?"&":"?")+l.data,delete l.data),l.cache===!1&&(l.url=Hb.test(f)?f.replace(Hb,"$1_="+Db++):f+(Eb.test(f)?"&":"?")+"_="+Db++)),l.ifModified&&(n.lastModified[f]&&w.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&w.setRequestHeader("If-None-Match",n.etag[f])),(l.data&&l.hasContent&&l.contentType!==!1||c.contentType)&&w.setRequestHeader("Content-Type",l.contentType),w.setRequestHeader("Accept",l.dataTypes[0]&&l.accepts[l.dataTypes[0]]?l.accepts[l.dataTypes[0]]+("*"!==l.dataTypes[0]?", "+Pb+"; q=0.01":""):l.accepts["*"]);for(e in l.headers)w.setRequestHeader(e,l.headers[e]);if(l.beforeSend&&(l.beforeSend.call(m,w,l)===!1||2===u))return w.abort();v="abort";for(e in{success:1,error:1,complete:1})w[e](l[e]);if(j=Tb(Ob,l,c,w)){if(w.readyState=1,i&&o.trigger("ajaxSend",[w,l]),2===u)return w;l.async&&l.timeout>0&&(h=a.setTimeout(function(){w.abort("timeout")},l.timeout));try{u=1,j.send(s,y)}catch(x){if(!(2>u))throw x;y(-1,x)}}else y(-1,"No Transport");function y(b,c,d,e){var k,s,t,v,x,y=c;2!==u&&(u=2,h&&a.clearTimeout(h),j=void 0,g=e||"",w.readyState=b>0?4:0,k=b>=200&&300>b||304===b,d&&(v=Vb(l,w,d)),v=Wb(l,v,w,k),k?(l.ifModified&&(x=w.getResponseHeader("Last-Modified"),x&&(n.lastModified[f]=x),x=w.getResponseHeader("etag"),x&&(n.etag[f]=x)),204===b||"HEAD"===l.type?y="nocontent":304===b?y="notmodified":(y=v.state,s=v.data,t=v.error,k=!t)):(t=y,(b||!y)&&(y="error",0>b&&(b=0))),w.status=b,w.statusText=(c||y)+"",k?p.resolveWith(m,[s,y,w]):p.rejectWith(m,[w,y,t]),w.statusCode(r),r=void 0,i&&o.trigger(k?"ajaxSuccess":"ajaxError",[w,l,k?s:t]),q.fireWith(m,[w,y]),i&&(o.trigger("ajaxComplete",[w,l]),--n.active||n.event.trigger("ajaxStop")))}return w},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){if(n.isFunction(a))return this.each(function(b){n(this).wrapAll(a.call(this,b))});if(this[0]){var b=n(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))}):this.each(function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}});function Xb(a){return a.style&&a.style.display||n.css(a,"display")}function Yb(a){while(a&&1===a.nodeType){if("none"===Xb(a)||"hidden"===a.type)return!0;a=a.parentNode}return!1}n.expr.filters.hidden=function(a){return l.reliableHiddenOffsets()?a.offsetWidth<=0&&a.offsetHeight<=0&&!a.getClientRects().length:Yb(a)},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var Zb=/%20/g,$b=/\[\]$/,_b=/\r?\n/g,ac=/^(?:submit|button|image|reset|file)$/i,bc=/^(?:input|select|textarea|keygen)/i;function cc(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||$b.test(a)?d(a,e):cc(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)cc(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)cc(c,a[c],b,e);return d.join("&").replace(Zb,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&bc.test(this.nodeName)&&!ac.test(a)&&(this.checked||!Z.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(_b,"\r\n")}}):{name:b.name,value:c.replace(_b,"\r\n")}}).get()}}),n.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return this.isLocal?hc():d.documentMode>8?gc():/^(get|post|head|put|delete|options)$/i.test(this.type)&&gc()||hc()}:gc;var dc=0,ec={},fc=n.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in ec)ec[a](void 0,!0)}),l.cors=!!fc&&"withCredentials"in fc,fc=l.ajax=!!fc,fc&&n.ajaxTransport(function(b){if(!b.crossDomain||l.cors){var c;return{send:function(d,e){var f,g=b.xhr(),h=++dc;if(g.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(f in b.xhrFields)g[f]=b.xhrFields[f];b.mimeType&&g.overrideMimeType&&g.overrideMimeType(b.mimeType),b.crossDomain||d["X-Requested-With"]||(d["X-Requested-With"]="XMLHttpRequest");for(f in d)void 0!==d[f]&&g.setRequestHeader(f,d[f]+"");g.send(b.hasContent&&b.data||null),c=function(a,d){var f,i,j;if(c&&(d||4===g.readyState))if(delete ec[h],c=void 0,g.onreadystatechange=n.noop,d)4!==g.readyState&&g.abort();else{j={},f=g.status,"string"==typeof g.responseText&&(j.text=g.responseText);try{i=g.statusText}catch(k){i=""}f||!b.isLocal||b.crossDomain?1223===f&&(f=204):f=j.text?200:404}j&&e(f,i,j,g.getAllResponseHeaders())},b.async?4===g.readyState?a.setTimeout(c):g.onreadystatechange=ec[h]=c:c()},abort:function(){c&&c(void 0,!0)}}}});function gc(){try{return new a.XMLHttpRequest}catch(b){}}function hc(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}n.ajaxPrefilter(function(a){a.crossDomain&&(a.contents.script=!1)}),n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=d.head||n("head")[0]||d.documentElement;return{send:function(e,f){b=d.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||f(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var ic=[],jc=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=ic.pop()||n.expando+"_"+Db++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(jc.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&jc.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(jc,"$1"+e):b.jsonp!==!1&&(b.url+=(Eb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,ic.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),l.createHTMLDocument=function(){if(!d.implementation.createHTMLDocument)return!1;var a=d.implementation.createHTMLDocument("");return a.body.innerHTML="<form></form><form></form>",2===a.body.childNodes.length}(),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||(l.createHTMLDocument?d.implementation.createHTMLDocument(""):d);var e=x.exec(a),f=!c&&[];return e?[b.createElement(e[1])]:(e=ja([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))};var kc=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&kc)return kc.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=n.trim(a.slice(h,a.length)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(g,f||[a.responseText,b,a])})}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};function lc(a){return n.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&n.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,n.contains(b,e)?("undefined"!=typeof e.getBoundingClientRect&&(d=e.getBoundingClientRect()),c=lc(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===n.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(c=a.offset()),c.top+=n.css(a[0],"borderTopWidth",!0)-a.scrollTop(),c.left+=n.css(a[0],"borderLeftWidth",!0)-a.scrollLeft()),{top:b.top-c.top-n.css(d,"marginTop",!0),left:b.left-c.left-n.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Qa})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);n.fn[a]=function(d){return Y(this,function(a,d,e){var f=lc(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?n(f).scrollLeft():e,c?e:n(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ua(l.pixelPosition,function(a,c){return c?(c=Sa(a,b),Oa.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return Y(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var mc=a.jQuery,nc=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=nc),b&&a.jQuery===n&&(a.jQuery=mc),n},b||(a.jQuery=a.$=n),n});_insideGraph.jQuery=window.jQuery;if(typeof(_insideGraph._jQuery)!="undefined")
window.jQuery=_insideGraph._jQuery;if(typeof(_insideGraph._$)!="undefined"){window.$=_insideGraph._$;$=window.$;}
(function(n,t,i){function w(t,i){var u,f;if(n.isArray(t)){for(u=t.length-1;u>=0;u--)f=t[u],n.type(f)==="string"&&r.transports[f]||(i.log("Invalid transport: "+f+", removing it from the transports list."),t.splice(u,1));t.length===0&&(i.log("No transports remain within the specified transport array."),t=null)}else if(r.transports[t]||t==="auto"){if(t==="auto"&&r._.ieVersion<=8)return["longPolling"]}else i.log("Invalid transport: "+t.toString()+"."),t=null;return t}function b(n){return n==="http:"?80:n==="https:"?443:void 0}function a(n,t){return t.match(/:\d+$/)?t:t+":"+b(n)}function k(t,i){var u=this,r=[];u.tryBuffer=function(i){return t.state===n.signalR.connectionState.connecting?(r.push(i),!0):!1};u.drain=function(){if(t.state===n.signalR.connectionState.connected)while(r.length>0)i(r.shift())};u.clear=function(){r=[]}}var f={nojQuery:"jQuery was not found. Please ensure jQuery is referenced before the SignalR client JavaScript file.",noTransportOnInit:"No transport could be initialized successfully. Try specifying a different transport or none at all for auto initialization.",errorOnNegotiate:"Error during negotiation request.",stoppedWhileLoading:"The connection was stopped during page load.",stoppedWhileNegotiating:"The connection was stopped during the negotiate request.",errorParsingNegotiateResponse:"Error parsing negotiate response.",errorDuringStartRequest:"Error during start request. Stopping the connection.",stoppedDuringStartRequest:"The connection was stopped during the start request.",errorParsingStartResponse:"Error parsing start response: '{0}'. Stopping the connection.",invalidStartResponse:"Invalid start response: '{0}'. Stopping the connection.",protocolIncompatible:"You are using a version of the client that isn't compatible with the server. Client version {0}, server version {1}.",sendFailed:"Send failed.",parseFailed:"Failed at parsing response: {0}",longPollFailed:"Long polling request failed.",eventSourceFailedToConnect:"EventSource failed to connect.",eventSourceError:"Error raised by EventSource",webSocketClosed:"WebSocket closed.",pingServerFailedInvalidResponse:"Invalid ping response when pinging server: '{0}'.",pingServerFailed:"Failed to ping server.",pingServerFailedStatusCode:"Failed to ping server.  Server responded with status code {0}, stopping the connection.",pingServerFailedParse:"Failed to parse ping server response, stopping the connection.",noConnectionTransport:"Connection is in an invalid state, there is no transport active.",webSocketsInvalidState:"The Web Socket transport is in an invalid state, transitioning into reconnecting.",reconnectTimeout:"Couldn't reconnect within the configured timeout of {0} ms, disconnecting.",reconnectWindowTimeout:"The client has been inactive since {0} and it has exceeded the inactivity timeout of {1} ms. Stopping the connection."};if(typeof n!="function")throw new Error(f.nojQuery);var r,h,s=t.document.readyState==="complete",e=n(t),c="__Negotiate Aborted__",u={onStart:"onStart",onStarting:"onStarting",onReceived:"onReceived",onError:"onError",onConnectionSlow:"onConnectionSlow",onReconnecting:"onReconnecting",onReconnect:"onReconnect",onStateChanged:"onStateChanged",onDisconnect:"onDisconnect"},v=function(n,i){if(i!==!1){var r;typeof t.console!="undefined"&&(r="["+(new Date).toTimeString()+"] SignalR: "+n,t.console.debug?t.console.debug(r):t.console.log&&t.console.log(r))}},o=function(t,i,r){return i===t.state?(t.state=r,n(t).triggerHandler(u.onStateChanged,[{oldState:i,newState:r}]),!0):!1},y=function(n){return n.state===r.connectionState.disconnected},l=function(n){return n._.keepAliveData.activated&&n.transport.supportsKeepAlive(n)},p=function(i){var f,e;i._.configuredStopReconnectingTimeout||(e=function(t){var i=r._.format(r.resources.reconnectTimeout,t.disconnectTimeout);t.log(i);n(t).triggerHandler(u.onError,[r._.error(i,"TimeoutException")]);t.stop(!1,!1)},i.reconnecting(function(){var n=this;n.state===r.connectionState.reconnecting&&(f=t.setTimeout(function(){e(n)},n.disconnectTimeout))}),i.stateChanged(function(n){n.oldState===r.connectionState.reconnecting&&t.clearTimeout(f)}),i._.configuredStopReconnectingTimeout=!0)};r=function(n,t,i){return new r.fn.init(n,t,i)};r._={defaultContentType:"application/x-www-form-urlencoded; charset=UTF-8",ieVersion:function(){var i,n;return t.navigator.appName==="Microsoft Internet Explorer"&&(n=/MSIE ([0-9]+\.[0-9]+)/.exec(t.navigator.userAgent),n&&(i=t.parseFloat(n[1]))),i}(),error:function(n,t,i){var r=new Error(n);return r.source=t,typeof i!="undefined"&&(r.context=i),r},transportError:function(n,t,r,u){var f=this.error(n,r,u);return f.transport=t?t.name:i,f},format:function(){for(var t=arguments[0],n=0;n<arguments.length-1;n++)t=t.replace("{"+n+"}",arguments[n+1]);return t},firefoxMajorVersion:function(n){var t=n.match(/Firefox\/(\d+)/);return!t||!t.length||t.length<2?0:parseInt(t[1],10)},configurePingInterval:function(i){var f=i._.config,e=function(t){n(i).triggerHandler(u.onError,[t])};f&&!i._.pingIntervalId&&f.pingInterval&&(i._.pingIntervalId=t.setInterval(function(){r.transports._logic.pingServer(i).fail(e)},f.pingInterval))}};r.events=u;r.resources=f;r.ajaxDefaults={processData:!0,timeout:null,async:!0,global:!1,cache:!1};r.changeState=o;r.isDisconnecting=y;r.connectionState={connecting:0,connected:1,reconnecting:2,disconnected:4};r.hub={start:function(){throw new Error("SignalR: Error loading hubs. Ensure your hubs reference is correct, e.g. <script src='/signalr/js'><\/script>.");}};e.load(function(){s=!0});r.fn=r.prototype={init:function(t,i,r){var f=n(this);this.url=t;this.qs=i;this.lastError=null;this._={keepAliveData:{},connectingMessageBuffer:new k(this,function(n){f.triggerHandler(u.onReceived,[n])}),lastMessageAt:(new Date).getTime(),lastActiveAt:(new Date).getTime(),beatInterval:5e3,beatHandle:null,totalTransportConnectTimeout:0};typeof r=="boolean"&&(this.logging=r)},_parseResponse:function(n){var t=this;return n?typeof n=="string"?t.json.parse(n):n:n},_originalJson:t.JSON,json:t.JSON,isCrossDomain:function(i,r){var u;return(i=n.trim(i),r=r||t.location,i.indexOf("http")!==0)?!1:(u=t.document.createElement("a"),u.href=i,u.protocol+a(u.protocol,u.host)!==r.protocol+a(r.protocol,r.host))},ajaxDataType:"text",contentType:"application/json; charset=UTF-8",logging:!1,state:r.connectionState.disconnected,clientProtocol:"1.5",reconnectDelay:2e3,transportConnectTimeout:0,disconnectTimeout:3e4,reconnectWindow:3e4,keepAliveWarnAt:2/3,start:function(i,h){var a=this,v={pingInterval:3e5,waitForPageLoad:!0,transport:"auto",jsonp:!1},d,y=a._deferral||n.Deferred(),b=t.document.createElement("a"),k,g;if(a.lastError=null,a._deferral=y,!a.json)throw new Error("SignalR: No JSON parser found. Please ensure json2.js is referenced before the SignalR.js file if you need to support clients without native JSON parsing support, e.g. IE<8.");if(n.type(i)==="function"?h=i:n.type(i)==="object"&&(n.extend(v,i),n.type(v.callback)==="function"&&(h=v.callback)),v.transport=w(v.transport,a),!v.transport)throw new Error("SignalR: Invalid transport(s) specified, aborting start.");return(a._.config=v,!s&&v.waitForPageLoad===!0)?(a._.deferredStartHandler=function(){a.start(i,h)},e.bind("load",a._.deferredStartHandler),y.promise()):a.state===r.connectionState.connecting?y.promise():o(a,r.connectionState.disconnected,r.connectionState.connecting)===!1?(y.resolve(a),y.promise()):(p(a),b.href=a.url,b.protocol&&b.protocol!==":"?(a.protocol=b.protocol,a.host=b.host):(a.protocol=t.document.location.protocol,a.host=b.host||t.document.location.host),a.baseUrl=a.protocol+"//"+a.host,a.wsProtocol=a.protocol==="https:"?"wss://":"ws://",v.transport==="auto"&&v.jsonp===!0&&(v.transport="longPolling"),a.url.indexOf("//")===0&&(a.url=t.location.protocol+a.url,a.log("Protocol relative URL detected, normalizing it to '"+a.url+"'.")),this.isCrossDomain(a.url)&&(a.log("Auto detected cross domain url."),v.transport==="auto"&&(v.transport=["webSockets","serverSentEvents","longPolling"]),typeof v.withCredentials=="undefined"&&(v.withCredentials=!0),v.jsonp||(v.jsonp=!n.support.cors,v.jsonp&&a.log("Using jsonp because this browser doesn't support CORS.")),a.contentType=r._.defaultContentType),a.withCredentials=v.withCredentials,a.ajaxDataType=v.jsonp?"jsonp":"text",n(a).bind(u.onStart,function(){n.type(h)==="function"&&h.call(a);y.resolve(a)}),a._.initHandler=r.transports._logic.initHandler(a),d=function(i,s){var c=r._.error(f.noTransportOnInit);if(s=s||0,s>=i.length){s===0?a.log("No transports supported by the server were selected."):s===1?a.log("No fallback transports were selected."):a.log("Fallback transports exhausted.");n(a).triggerHandler(u.onError,[c]);y.reject(c);a.stop();return}if(a.state!==r.connectionState.disconnected){var p=i[s],h=r.transports[p],v=function(){d(i,s+1)};a.transport=h;try{a._.initHandler.start(h,function(){var i=r._.firefoxMajorVersion(t.navigator.userAgent)>=11,f=!!a.withCredentials&&i;a.log("The start request succeeded. Transitioning to the connected state.");l(a)&&r.transports._logic.monitorKeepAlive(a);r.transports._logic.startHeartbeat(a);r._.configurePingInterval(a);o(a,r.connectionState.connecting,r.connectionState.connected)||a.log("WARNING! The connection was not in the connecting state.");a._.connectingMessageBuffer.drain();n(a).triggerHandler(u.onStart);e.bind("unload",function(){a.log("Window unloading, stopping the connection.");a.stop(f)});i&&e.bind("beforeunload",function(){t.setTimeout(function(){a.stop(f)},0)})},v)}catch(w){a.log(h.name+" transport threw '"+w.message+"' when attempting to start.");v()}}},k=a.url+"/negotiate",g=function(t,i){var e=r._.error(f.errorOnNegotiate,t,i._.negotiateRequest);n(i).triggerHandler(u.onError,e);y.reject(e);i.stop()},n(a).triggerHandler(u.onStarting),k=r.transports._logic.prepareQueryString(a,k),a.log("Negotiating with '"+k+"'."),a._.negotiateRequest=r.transports._logic.ajax(a,{url:k,error:function(n,t){t!==c?g(n,a):y.reject(r._.error(f.stoppedWhileNegotiating,null,a._.negotiateRequest))},success:function(t){var i,e,h,o=[],s=[];try{i=a._parseResponse(t)}catch(c){g(r._.error(f.errorParsingNegotiateResponse,c),a);return}if(e=a._.keepAliveData,a.appRelativeUrl=i.Url,a.id=i.ConnectionId,a.token=i.ConnectionToken,a.webSocketServerUrl=i.WebSocketServerUrl,a._.pollTimeout=i.ConnectionTimeout*1e3+1e4,a.disconnectTimeout=i.DisconnectTimeout*1e3,a._.totalTransportConnectTimeout=a.transportConnectTimeout+i.TransportConnectTimeout*1e3,i.KeepAliveTimeout?(e.activated=!0,e.timeout=i.KeepAliveTimeout*1e3,e.timeoutWarning=e.timeout*a.keepAliveWarnAt,a._.beatInterval=(e.timeout-e.timeoutWarning)/3):e.activated=!1,a.reconnectWindow=a.disconnectTimeout+(e.timeout||0),!i.ProtocolVersion||i.ProtocolVersion!==a.clientProtocol){h=r._.error(r._.format(f.protocolIncompatible,a.clientProtocol,i.ProtocolVersion));n(a).triggerHandler(u.onError,[h]);y.reject(h);return}n.each(r.transports,function(n){if(n.indexOf("_")===0||n==="webSockets"&&!i.TryWebSockets)return!0;s.push(n)});n.isArray(v.transport)?n.each(v.transport,function(t,i){n.inArray(i,s)>=0&&o.push(i)}):v.transport==="auto"?o=s:n.inArray(v.transport,s)>=0&&o.push(v.transport);d(o)}}),y.promise())},starting:function(t){var i=this;return n(i).bind(u.onStarting,function(){t.call(i)}),i},send:function(n){var t=this;if(t.state===r.connectionState.disconnected)throw new Error("SignalR: Connection must be started before data can be sent. Call .start() before .send()");if(t.state===r.connectionState.connecting)throw new Error("SignalR: Connection has not been fully initialized. Use .start().done() or .start().fail() to run logic after the connection has started.");return t.transport.send(t,n),t},received:function(t){var i=this;return n(i).bind(u.onReceived,function(n,r){t.call(i,r)}),i},stateChanged:function(t){var i=this;return n(i).bind(u.onStateChanged,function(n,r){t.call(i,r)}),i},error:function(t){var i=this;return n(i).bind(u.onError,function(n,r,u){i.lastError=r;t.call(i,r,u)}),i},disconnected:function(t){var i=this;return n(i).bind(u.onDisconnect,function(){t.call(i)}),i},connectionSlow:function(t){var i=this;return n(i).bind(u.onConnectionSlow,function(){t.call(i)}),i},reconnecting:function(t){var i=this;return n(i).bind(u.onReconnecting,function(){t.call(i)}),i},reconnected:function(t){var i=this;return n(i).bind(u.onReconnect,function(){t.call(i)}),i},stop:function(i,h){var a=this,v=a._deferral;if(a._.deferredStartHandler&&e.unbind("load",a._.deferredStartHandler),delete a._.config,delete a._.deferredStartHandler,!s&&(!a._.config||a._.config.waitForPageLoad===!0)){a.log("Stopping connection prior to negotiate.");v&&v.reject(r._.error(f.stoppedWhileLoading));return}if(a.state!==r.connectionState.disconnected)return a.log("Stopping connection."),o(a,a.state,r.connectionState.disconnected),t.clearTimeout(a._.beatHandle),t.clearInterval(a._.pingIntervalId),a.transport&&(a.transport.stop(a),h!==!1&&a.transport.abort(a,i),l(a)&&r.transports._logic.stopMonitoringKeepAlive(a),a.transport=null),a._.negotiateRequest&&(a._.negotiateRequest.abort(c),delete a._.negotiateRequest),a._.initHandler&&a._.initHandler.stop(),n(a).triggerHandler(u.onDisconnect),delete a._deferral,delete a.messageId,delete a.groupsToken,delete a.id,delete a._.pingIntervalId,delete a._.lastMessageAt,delete a._.lastActiveAt,a._.connectingMessageBuffer.clear(),a},log:function(n){v(n,this.logging)}};r.fn.init.prototype=r.fn;r.noConflict=function(){return n.connection===r&&(n.connection=h),r};n.connection&&(h=n.connection);n.connection=n.signalR=r})(_insideGraph.jQuery,window),function(n,t,i){function s(n){n._.keepAliveData.monitoring&&l(n);u.markActive(n)&&(n._.beatHandle=t.setTimeout(function(){s(n)},n._.beatInterval))}function l(t){var i=t._.keepAliveData,u;t.state===r.connectionState.connected&&(u=(new Date).getTime()-t._.lastMessageAt,u>=i.timeout?(t.log("Keep alive timed out.  Notifying transport that connection has been lost."),t.transport.lostConnection(t)):u>=i.timeoutWarning?i.userNotified||(t.log("Keep alive has been missed, connection may be dead/slow."),n(t).triggerHandler(f.onConnectionSlow),i.userNotified=!0):i.userNotified=!1)}function e(n,t){var i=n.url+t;return n.transport&&(i+="?transport="+n.transport.name),u.prepareQueryString(n,i)}function h(n){this.connection=n;this.startRequested=!1;this.startCompleted=!1;this.connectionStopped=!1}var r=n.signalR,f=n.signalR.events,c=n.signalR.changeState,o="__Start Aborted__",u;r.transports={};h.prototype={start:function(n,r,u){var f=this,e=f.connection,o=!1;if(f.startRequested||f.connectionStopped){e.log("WARNING! "+n.name+" transport cannot be started. Initialization ongoing or completed.");return}e.log(n.name+" transport starting.");f.transportTimeoutHandle=t.setTimeout(function(){o||(o=!0,e.log(n.name+" transport timed out when trying to connect."),f.transportFailed(n,i,u))},e._.totalTransportConnectTimeout);n.start(e,function(){o||f.initReceived(n,r)},function(t){return o||(o=!0,f.transportFailed(n,t,u)),!f.startCompleted||f.connectionStopped})},stop:function(){this.connectionStopped=!0;t.clearTimeout(this.transportTimeoutHandle);r.transports._logic.tryAbortStartRequest(this.connection)},initReceived:function(n,i){var u=this,f=u.connection;if(u.startRequested){f.log("WARNING! The client received multiple init messages.");return}u.connectionStopped||(u.startRequested=!0,t.clearTimeout(u.transportTimeoutHandle),f.log(n.name+" transport connected. Initiating start request."),r.transports._logic.ajaxStart(f,function(){u.startCompleted=!0;i()}))},transportFailed:function(i,u,e){var o=this.connection,h=o._deferral,s;this.connectionStopped||(t.clearTimeout(this.transportTimeoutHandle),this.startRequested?this.startCompleted||(s=r._.error(r.resources.errorDuringStartRequest,u),o.log(i.name+" transport failed during the start request. Stopping the connection."),n(o).triggerHandler(f.onError,[s]),h&&h.reject(s),o.stop()):(i.stop(o),o.log(i.name+" transport failed to connect. Attempting to fall back."),e()))}};u=r.transports._logic={ajax:function(t,i){return n.ajax(n.extend(!0,{},n.signalR.ajaxDefaults,{type:"GET",data:{},xhrFields:{withCredentials:t.withCredentials},contentType:t.contentType,dataType:t.ajaxDataType},i))},pingServer:function(t){var e,f,i=n.Deferred();return t.transport?(e=t.url+"/ping",e=u.addQs(e,t.qs),f=u.ajax(t,{url:e,success:function(n){var u;try{u=t._parseResponse(n)}catch(e){i.reject(r._.transportError(r.resources.pingServerFailedParse,t.transport,e,f));t.stop();return}u.Response==="pong"?i.resolve():i.reject(r._.transportError(r._.format(r.resources.pingServerFailedInvalidResponse,n),t.transport,null,f))},error:function(n){n.status===401||n.status===403?(i.reject(r._.transportError(r._.format(r.resources.pingServerFailedStatusCode,n.status),t.transport,n,f)),t.stop()):i.reject(r._.transportError(r.resources.pingServerFailed,t.transport,n,f))}})):i.reject(r._.transportError(r.resources.noConnectionTransport,t.transport)),i.promise()},prepareQueryString:function(n,i){var r;return r=u.addQs(i,"clientProtocol="+n.clientProtocol),r=u.addQs(r,n.qs),n.token&&(r+="&connectionToken="+t.encodeURIComponent(n.token)),n.data&&(r+="&connectionData="+t.encodeURIComponent(n.data)),r},addQs:function(t,i){var r=t.indexOf("?")!==-1?"&":"?",u;if(!i)return t;if(typeof i=="object")return t+r+n.param(i);if(typeof i=="string")return u=i.charAt(0),(u==="?"||u==="&")&&(r=""),t+r+i;throw new Error("Query string property must be either a string or object.");},getUrl:function(n,i,r,f,e){var h=i==="webSockets"?"":n.baseUrl,o=h+n.appRelativeUrl,s="transport="+i;return!e&&n.groupsToken&&(s+="&groupsToken="+t.encodeURIComponent(n.groupsToken)),r?(o+=f?"/poll":"/reconnect",!e&&n.messageId&&(s+="&messageId="+t.encodeURIComponent(n.messageId))):o+="/connect",o+="?"+s,o=u.prepareQueryString(n,o),e||(o+="&tid="+Math.floor(Math.random()*11)),o},maximizePersistentResponse:function(n){return{MessageId:n.C,Messages:n.M,Initialized:typeof n.S!="undefined"?!0:!1,ShouldReconnect:typeof n.T!="undefined"?!0:!1,LongPollDelay:n.L,GroupsToken:n.G}},updateGroups:function(n,t){t&&(n.groupsToken=t)},stringifySend:function(n,t){return typeof t=="string"||typeof t=="undefined"||t===null?t:n.json.stringify(t)},ajaxSend:function(t,i){var h=u.stringifySend(t,i),c=e(t,"/send"),o,s=function(t,u){n(u).triggerHandler(f.onError,[r._.transportError(r.resources.sendFailed,u.transport,t,o),i])};return o=u.ajax(t,{url:c,type:t.ajaxDataType==="jsonp"?"GET":"POST",contentType:r._.defaultContentType,data:{data:h},success:function(n){var i;if(n){try{i=t._parseResponse(n)}catch(r){s(r,t);t.stop();return}u.triggerReceived(t,i)}},error:function(n,i){i!=="abort"&&i!=="parsererror"&&s(n,t)}})},ajaxAbort:function(n,t){if(typeof n.transport!="undefined"){t=typeof t=="undefined"?!0:t;var i=e(n,"/abort");u.ajax(n,{url:i,async:t,timeout:1e3,type:"POST"});n.log("Fired ajax abort async = "+t+".")}},ajaxStart:function(t,i){var h=function(n){var i=t._deferral;i&&i.reject(n)},s=function(i){t.log("The start request failed. Stopping the connection.");n(t).triggerHandler(f.onError,[i]);h(i);t.stop()};t._.startRequest=u.ajax(t,{url:e(t,"/start"),success:function(n,u,f){var e;try{e=t._parseResponse(n)}catch(o){s(r._.error(r._.format(r.resources.errorParsingStartResponse,n),o,f));return}e.Response==="started"?i():s(r._.error(r._.format(r.resources.invalidStartResponse,n),null,f))},error:function(n,i,u){i!==o?s(r._.error(r.resources.errorDuringStartRequest,u,n)):(t.log("The start request aborted because connection.stop() was called."),h(r._.error(r.resources.stoppedDuringStartRequest,null,n)))}})},tryAbortStartRequest:function(n){n._.startRequest&&(n._.startRequest.abort(o),delete n._.startRequest)},tryInitialize:function(n,t){n.Initialized&&t()},triggerReceived:function(t,i){t._.connectingMessageBuffer.tryBuffer(i)||n(t).triggerHandler(f.onReceived,[i])},processMessages:function(t,i,r){var f;u.markLastMessage(t);i&&(f=u.maximizePersistentResponse(i),u.updateGroups(t,f.GroupsToken),f.MessageId&&(t.messageId=f.MessageId),f.Messages&&(n.each(f.Messages,function(n,i){u.triggerReceived(t,i)}),u.tryInitialize(f,r)))},monitorKeepAlive:function(t){var i=t._.keepAliveData;i.monitoring?t.log("Tried to monitor keep alive but it's already being monitored."):(i.monitoring=!0,u.markLastMessage(t),t._.keepAliveData.reconnectKeepAliveUpdate=function(){u.markLastMessage(t)},n(t).bind(f.onReconnect,t._.keepAliveData.reconnectKeepAliveUpdate),t.log("Now monitoring keep alive with a warning timeout of "+i.timeoutWarning+", keep alive timeout of "+i.timeout+" and disconnecting timeout of "+t.disconnectTimeout))},stopMonitoringKeepAlive:function(t){var i=t._.keepAliveData;i.monitoring&&(i.monitoring=!1,n(t).unbind(f.onReconnect,t._.keepAliveData.reconnectKeepAliveUpdate),t._.keepAliveData={},t.log("Stopping the monitoring of the keep alive."))},startHeartbeat:function(n){n._.lastActiveAt=(new Date).getTime();s(n)},markLastMessage:function(n){n._.lastMessageAt=(new Date).getTime()},markActive:function(n){return u.verifyLastActive(n)?(n._.lastActiveAt=(new Date).getTime(),!0):!1},isConnectedOrReconnecting:function(n){return n.state===r.connectionState.connected||n.state===r.connectionState.reconnecting},ensureReconnectingState:function(t){return c(t,r.connectionState.connected,r.connectionState.reconnecting)===!0&&n(t).triggerHandler(f.onReconnecting),t.state===r.connectionState.reconnecting},clearReconnectTimeout:function(n){n&&n._.reconnectTimeout&&(t.clearTimeout(n._.reconnectTimeout),delete n._.reconnectTimeout)},verifyLastActive:function(t){if((new Date).getTime()-t._.lastActiveAt>=t.reconnectWindow){var i=r._.format(r.resources.reconnectWindowTimeout,new Date(t._.lastActiveAt),t.reconnectWindow);return t.log(i),n(t).triggerHandler(f.onError,[r._.error(i,"TimeoutException")]),t.stop(!1,!1),!1}return!0},reconnect:function(n,i){var f=r.transports[i];if(u.isConnectedOrReconnecting(n)&&!n._.reconnectTimeout){if(!u.verifyLastActive(n))return;n._.reconnectTimeout=t.setTimeout(function(){u.verifyLastActive(n)&&(f.stop(n),u.ensureReconnectingState(n)&&(n.log(i+" reconnecting."),f.start(n)))},n.reconnectDelay)}},handleParseFailure:function(t,i,u,e,o){var s=r._.transportError(r._.format(r.resources.parseFailed,i),t.transport,u,o);e&&e(s)?t.log("Failed to parse server response while attempting to connect."):(n(t).triggerHandler(f.onError,[s]),t.stop())},initHandler:function(n){return new h(n)},foreverFrame:{count:0,connections:{}}}}(_insideGraph.jQuery,window),function(n,t){var r=n.signalR,u=n.signalR.events,f=n.signalR.changeState,i=r.transports._logic;r.transports.webSockets={name:"webSockets",supportsKeepAlive:function(){return!0},send:function(t,f){var e=i.stringifySend(t,f);try{t.socket.send(e)}catch(o){n(t).triggerHandler(u.onError,[r._.transportError(r.resources.webSocketsInvalidState,t.transport,o,t.socket),f])}},start:function(e,o,s){var h,c=!1,l=this,a=!o,v=n(e);if(!t.WebSocket){s();return}e.socket||(h=e.webSocketServerUrl?e.webSocketServerUrl:e.wsProtocol+e.host,h+=i.getUrl(e,this.name,a),e.log("Connecting to websocket endpoint '"+h+"'."),e.socket=new t.WebSocket(h),e.socket.onopen=function(){c=!0;e.log("Websocket opened.");i.clearReconnectTimeout(e);f(e,r.connectionState.reconnecting,r.connectionState.connected)===!0&&v.triggerHandler(u.onReconnect)},e.socket.onclose=function(t){var i;this===e.socket&&(c&&typeof t.wasClean!="undefined"&&t.wasClean===!1?(i=r._.transportError(r.resources.webSocketClosed,e.transport,t),e.log("Unclean disconnect from websocket: "+(t.reason||"[no reason given]."))):e.log("Websocket closed."),s&&s(i)||(i&&n(e).triggerHandler(u.onError,[i]),l.reconnect(e)))},e.socket.onmessage=function(t){var r;try{r=e._parseResponse(t.data)}catch(u){i.handleParseFailure(e,t.data,u,s,t);return}r&&(n.isEmptyObject(r)||r.M?i.processMessages(e,r,o):i.triggerReceived(e,r))})},reconnect:function(n){i.reconnect(n,this.name)},lostConnection:function(n){this.reconnect(n)},stop:function(n){i.clearReconnectTimeout(n);n.socket&&(n.log("Closing the Websocket."),n.socket.close(),n.socket=null)},abort:function(n,t){i.ajaxAbort(n,t)}}}(_insideGraph.jQuery,window),function(n,t){var i=n.signalR,u=n.signalR.events,e=n.signalR.changeState,r=i.transports._logic,f=function(n){t.clearTimeout(n._.reconnectAttemptTimeoutHandle);delete n._.reconnectAttemptTimeoutHandle};i.transports.serverSentEvents={name:"serverSentEvents",supportsKeepAlive:function(){return!0},timeOut:3e3,start:function(o,s,h){var c=this,l=!1,a=n(o),v=!s,y;if(o.eventSource&&(o.log("The connection already has an event source. Stopping it."),o.stop()),!t.EventSource){h&&(o.log("This browser doesn't support SSE."),h());return}y=r.getUrl(o,this.name,v);try{o.log("Attempting to connect to SSE endpoint '"+y+"'.");o.eventSource=new t.EventSource(y,{withCredentials:o.withCredentials})}catch(p){o.log("EventSource failed trying to connect with error "+p.Message+".");h?h():(a.triggerHandler(u.onError,[i._.transportError(i.resources.eventSourceFailedToConnect,o.transport,p)]),v&&c.reconnect(o));return}v&&(o._.reconnectAttemptTimeoutHandle=t.setTimeout(function(){l===!1&&o.eventSource.readyState!==t.EventSource.OPEN&&c.reconnect(o)},c.timeOut));o.eventSource.addEventListener("open",function(){o.log("EventSource connected.");f(o);r.clearReconnectTimeout(o);l===!1&&(l=!0,e(o,i.connectionState.reconnecting,i.connectionState.connected)===!0&&a.triggerHandler(u.onReconnect))},!1);o.eventSource.addEventListener("message",function(n){var t;if(n.data!=="initialized"){try{t=o._parseResponse(n.data)}catch(i){r.handleParseFailure(o,n.data,i,h,n);return}r.processMessages(o,t,s)}},!1);o.eventSource.addEventListener("error",function(n){var r=i._.transportError(i.resources.eventSourceError,o.transport,n);this===o.eventSource&&(h&&h(r)||(o.log("EventSource readyState: "+o.eventSource.readyState+"."),n.eventPhase===t.EventSource.CLOSED?(o.log("EventSource reconnecting due to the server connection ending."),c.reconnect(o)):(o.log("EventSource error."),a.triggerHandler(u.onError,[r]))))},!1)},reconnect:function(n){r.reconnect(n,this.name)},lostConnection:function(n){this.reconnect(n)},send:function(n,t){r.ajaxSend(n,t)},stop:function(n){f(n);r.clearReconnectTimeout(n);n&&n.eventSource&&(n.log("EventSource calling close()."),n.eventSource.close(),n.eventSource=null,delete n.eventSource)},abort:function(n,t){r.ajaxAbort(n,t)}}}(_insideGraph.jQuery,window),function(n,t){var r=n.signalR,e=n.signalR.events,o=n.signalR.changeState,i=r.transports._logic,u=function(){var n=t.document.createElement("iframe");return n.setAttribute("style","position:absolute;top:0;left:0;width:0;height:0;visibility:hidden;"),n},f=function(){var i=null,f=1e3,n=0;return{prevent:function(){r._.ieVersion<=8&&(n===0&&(i=t.setInterval(function(){var n=u();t.document.body.appendChild(n);t.document.body.removeChild(n);n=null},f)),n++)},cancel:function(){n===1&&t.clearInterval(i);n>0&&n--}}}();r.transports.foreverFrame={name:"foreverFrame",supportsKeepAlive:function(){return!0},iframeClearThreshold:50,start:function(n,r,e){var l=this,s=i.foreverFrame.count+=1,h,o=u(),c=function(){n.log("Forever frame iframe finished loading and is no longer receiving messages.");e&&e()||l.reconnect(n)};if(t.EventSource){e&&(n.log("Forever Frame is not supported by SignalR on browsers with SSE support."),e());return}o.setAttribute("data-signalr-connection-id",n.id);f.prevent();h=i.getUrl(n,this.name);h+="&frameId="+s;t.document.documentElement.appendChild(o);n.log("Binding to iframe's load event.");o.addEventListener?o.addEventListener("load",c,!1):o.attachEvent&&o.attachEvent("onload",c);o.src=h;i.foreverFrame.connections[s]=n;n.frame=o;n.frameId=s;r&&(n.onSuccess=function(){n.log("Iframe transport started.");r()})},reconnect:function(n){var r=this;i.isConnectedOrReconnecting(n)&&i.verifyLastActive(n)&&t.setTimeout(function(){if(i.verifyLastActive(n)&&n.frame&&i.ensureReconnectingState(n)){var u=n.frame,t=i.getUrl(n,r.name,!0)+"&frameId="+n.frameId;n.log("Updating iframe src to '"+t+"'.");u.src=t}},n.reconnectDelay)},lostConnection:function(n){this.reconnect(n)},send:function(n,t){i.ajaxSend(n,t)},receive:function(t,u){var f,e,o;if(t.json!==t._originalJson&&(u=t._originalJson.stringify(u)),o=t._parseResponse(u),i.processMessages(t,o,t.onSuccess),t.state===n.signalR.connectionState.connected&&(t.frameMessageCount=(t.frameMessageCount||0)+1,t.frameMessageCount>r.transports.foreverFrame.iframeClearThreshold&&(t.frameMessageCount=0,f=t.frame.contentWindow||t.frame.contentDocument,f&&f.document&&f.document.body)))for(e=f.document.body;e.firstChild;)e.removeChild(e.firstChild)},stop:function(n){var r=null;if(f.cancel(),n.frame){if(n.frame.stop)n.frame.stop();else try{r=n.frame.contentWindow||n.frame.contentDocument;r.document&&r.document.execCommand&&r.document.execCommand("Stop")}catch(u){n.log("Error occured when stopping foreverFrame transport. Message = "+u.message+".")}n.frame.parentNode===t.document.body&&t.document.body.removeChild(n.frame);delete i.foreverFrame.connections[n.frameId];n.frame=null;n.frameId=null;delete n.frame;delete n.frameId;delete n.onSuccess;delete n.frameMessageCount;n.log("Stopping forever frame.")}},abort:function(n,t){i.ajaxAbort(n,t)},getConnection:function(n){return i.foreverFrame.connections[n]},started:function(t){o(t,r.connectionState.reconnecting,r.connectionState.connected)===!0&&n(t).triggerHandler(e.onReconnect)}}}(_insideGraph.jQuery,window),function(n,t){var r=n.signalR,u=n.signalR.events,e=n.signalR.changeState,f=n.signalR.isDisconnecting,i=r.transports._logic;r.transports.longPolling={name:"longPolling",supportsKeepAlive:function(){return!1},reconnectDelay:3e3,start:function(o,s,h){var a=this,v=function(){v=n.noop;o.log("LongPolling connected.");s()},y=function(n){return h(n)?(o.log("LongPolling failed to connect."),!0):!1},c=o._,l=0,p=function(i){t.clearTimeout(c.reconnectTimeoutId);c.reconnectTimeoutId=null;e(i,r.connectionState.reconnecting,r.connectionState.connected)===!0&&(i.log("Raising the reconnect event"),n(i).triggerHandler(u.onReconnect))},w=36e5;o.pollXhr&&(o.log("Polling xhr requests already exists, aborting."),o.stop());o.messageId=null;c.reconnectTimeoutId=null;c.pollTimeoutId=t.setTimeout(function(){(function e(s,h){var g=s.messageId,nt=g===null,k=!nt,tt=!h,d=i.getUrl(s,a.name,k,tt,!0),b={};(s.messageId&&(b.messageId=s.messageId),s.groupsToken&&(b.groupsToken=s.groupsToken),f(s)!==!0)&&(o.log("Opening long polling request to '"+d+"'."),s.pollXhr=i.ajax(o,{xhrFields:{onprogress:function(){i.markLastMessage(o)}},url:d,type:"POST",contentType:r._.defaultContentType,data:b,timeout:o._.pollTimeout,success:function(r){var h,w=0,u,a;o.log("Long poll complete.");l=0;try{h=o._parseResponse(r)}catch(b){i.handleParseFailure(s,r,b,y,s.pollXhr);return}(c.reconnectTimeoutId!==null&&p(s),h&&(u=i.maximizePersistentResponse(h)),i.processMessages(s,h,v),u&&n.type(u.LongPollDelay)==="number"&&(w=u.LongPollDelay),f(s)!==!0)&&(a=u&&u.ShouldReconnect,!a||i.ensureReconnectingState(s))&&(w>0?c.pollTimeoutId=t.setTimeout(function(){e(s,a)},w):e(s,a))},error:function(f,h){var v=r._.transportError(r.resources.longPollFailed,o.transport,f,s.pollXhr);if(t.clearTimeout(c.reconnectTimeoutId),c.reconnectTimeoutId=null,h==="abort"){o.log("Aborted xhr request.");return}if(!y(v)){if(l++,o.state!==r.connectionState.reconnecting&&(o.log("An error occurred using longPolling. Status = "+h+".  Response = "+f.responseText+"."),n(s).triggerHandler(u.onError,[v])),(o.state===r.connectionState.connected||o.state===r.connectionState.reconnecting)&&!i.verifyLastActive(o))return;if(!i.ensureReconnectingState(s))return;c.pollTimeoutId=t.setTimeout(function(){e(s,!0)},a.reconnectDelay)}}}),k&&h===!0&&(c.reconnectTimeoutId=t.setTimeout(function(){p(s)},Math.min(1e3*(Math.pow(2,l)-1),w))))})(o)},250)},lostConnection:function(n){n.pollXhr&&n.pollXhr.abort("lostConnection")},send:function(n,t){i.ajaxSend(n,t)},stop:function(n){t.clearTimeout(n._.pollTimeoutId);t.clearTimeout(n._.reconnectTimeoutId);delete n._.pollTimeoutId;delete n._.reconnectTimeoutId;n.pollXhr&&(n.pollXhr.abort(),n.pollXhr=null,delete n.pollXhr)},abort:function(n,t){i.ajaxAbort(n,t)}}}(_insideGraph.jQuery,window),function(n){function r(n){return n+e}function s(n,t,i){for(var f=n.length,u=[],r=0;r<f;r+=1)n.hasOwnProperty(r)&&(u[r]=t.call(i,n[r],r,n));return u}function h(t){return n.isFunction(t)?null:n.type(t)==="undefined"?null:t}function u(n){for(var t in n)if(n.hasOwnProperty(t))return!0;return!1}function f(n,t){var i=n._.invocationCallbacks,r,f;u(i)&&n.log("Clearing hub invocation callbacks with error: "+t+".");n._.invocationCallbackId=0;delete n._.invocationCallbacks;n._.invocationCallbacks={};for(f in i)r=i[f],r.method.call(r.scope,{E:t})}function i(n,t){return new i.fn.init(n,t)}function t(i,r){var u={qs:null,logging:!1,useDefaultPath:!0};return n.extend(u,r),(!i||u.useDefaultPath)&&(i=(i||"")+"/signalr"),new t.fn.init(i,u)}var e=".hubProxy",o=n.signalR;i.fn=i.prototype={init:function(n,t){this.state={};this.connection=n;this.hubName=t;this._={callbackMap:{}}},constructor:i,hasSubscriptions:function(){return u(this._.callbackMap)},on:function(t,i){var u=this,f=u._.callbackMap;return t=t.toLowerCase(),f[t]||(f[t]={}),f[t][i]=function(n,t){i.apply(u,t)},n(u).bind(r(t),f[t][i]),u},off:function(t,i){var e=this,o=e._.callbackMap,f;return t=t.toLowerCase(),f=o[t],f&&(f[i]?(n(e).unbind(r(t),f[i]),delete f[i],u(f)||delete o[t]):i||(n(e).unbind(r(t)),delete o[t])),e},invoke:function(t){var i=this,r=i.connection,e=n.makeArray(arguments).slice(1),c=s(e,h),f={H:i.hubName,M:t,A:c,I:r._.invocationCallbackId},u=n.Deferred(),l=function(f){var e=i._maximizeHubResponse(f),h,s;n.extend(i.state,e.State);e.Progress?u.notifyWith?u.notifyWith(i,[e.Progress.Data]):r._.progressjQueryVersionLogged||(r.log("A hub method invocation progress update was received but the version of jQuery in use ("+n.prototype.jquery+") does not support progress updates. Upgrade to jQuery 1.7+ to receive progress notifications."),r._.progressjQueryVersionLogged=!0):e.Error?(e.StackTrace&&r.log(e.Error+"\n"+e.StackTrace+"."),h=e.IsHubException?"HubException":"Exception",s=o._.error(e.Error,h),s.data=e.ErrorData,r.log(i.hubName+"."+t+" failed to execute. Error: "+s.message),u.rejectWith(i,[s])):(r.log("Invoked "+i.hubName+"."+t),u.resolveWith(i,[e.Result]))};return r._.invocationCallbacks[r._.invocationCallbackId.toString()]={scope:i,method:l},r._.invocationCallbackId+=1,n.isEmptyObject(i.state)||(f.S=i.state),r.log("Invoking "+i.hubName+"."+t),r.send(f),u.promise()},_maximizeHubResponse:function(n){return{State:n.S,Result:n.R,Progress:n.P?{Id:n.P.I,Data:n.P.D}:null,Id:n.I,IsHubException:n.H,Error:n.E,StackTrace:n.T,ErrorData:n.D}}};i.fn.init.prototype=i.fn;t.fn=t.prototype=n.connection();t.fn.init=function(t,i){var e={qs:null,logging:!1,useDefaultPath:!0},u=this;n.extend(e,i);n.signalR.fn.init.call(u,t,e.qs,e.logging);u.proxies={};u._.invocationCallbackId=0;u._.invocationCallbacks={};u.received(function(t){var f,o,e,i,s,h;t&&(typeof t.P!="undefined"?(e=t.P.I.toString(),i=u._.invocationCallbacks[e],i&&i.method.call(i.scope,t)):typeof t.I!="undefined"?(e=t.I.toString(),i=u._.invocationCallbacks[e],i&&(u._.invocationCallbacks[e]=null,delete u._.invocationCallbacks[e],i.method.call(i.scope,t))):(f=this._maximizeClientHubInvocation(t),u.log("Triggering client hub event '"+f.Method+"' on hub '"+f.Hub+"'."),s=f.Hub.toLowerCase(),h=f.Method.toLowerCase(),o=this.proxies[s],n.extend(o.state,f.State),n(o).triggerHandler(r(h),[f.Args])))});u.error(function(n,t){var i,r;t&&(i=t.I,r=u._.invocationCallbacks[i],r&&(u._.invocationCallbacks[i]=null,delete u._.invocationCallbacks[i],r.method.call(r.scope,{E:n})))});u.reconnecting(function(){u.transport&&u.transport.name==="webSockets"&&f(u,"Connection started reconnecting before invocation result was received.")});u.disconnected(function(){f(u,"Connection was disconnected before invocation result was received.")})};t.fn._maximizeClientHubInvocation=function(n){return{Hub:n.H,Method:n.M,Args:n.A,State:n.S}};t.fn._registerSubscribedHubs=function(){var t=this;t._subscribedToHubs||(t._subscribedToHubs=!0,t.starting(function(){var i=[];n.each(t.proxies,function(n){this.hasSubscriptions()&&(i.push({name:n}),t.log("Client subscribed to hub '"+n+"'."))});i.length===0&&t.log("No hubs have been subscribed to.  The client will not receive data from hubs.  To fix, declare at least one client side function prior to connection start for each hub you wish to subscribe to.");t.data=t.json.stringify(i)}))};t.fn.createHubProxy=function(n){n=n.toLowerCase();var t=this.proxies[n];return t||(t=i(this,n),this.proxies[n]=t),this._registerSubscribedHubs(),t};t.fn.init.prototype=t.fn;n.hubConnection=t}(_insideGraph.jQuery,window),function(n){n.signalR.version="2.2.0"}(_insideGraph.jQuery)_insideGraph.registerModule("realtime",function(){console.log("INSIDE Module 'realtime' started");(function($,window,undefined){"use strict";if(typeof($.signalR)!=="function"){throw new Error("SignalR: SignalR is not loaded. Please ensure jquery.signalR-x.js is referenced before ~/signalr/js.");}
var signalR=$.signalR;function makeProxyCallback(hub,callback){return function(){callback.apply(hub,$.makeArray(arguments));};}
function registerHubProxies(instance,shouldSubscribe){var key,hub,memberKey,memberValue,subscriptionMethod;for(key in instance){if(instance.hasOwnProperty(key)){hub=instance[key];if(!(hub.hubName)){continue;}
if(shouldSubscribe){subscriptionMethod=hub.on;}else{subscriptionMethod=hub.off;}
for(memberKey in hub.client){if(hub.client.hasOwnProperty(memberKey)){memberValue=hub.client[memberKey];if(!$.isFunction(memberValue)){continue;}
subscriptionMethod.call(hub,memberKey,makeProxyCallback(hub,memberValue));}}}}}
$.hubConnection.prototype.createHubProxies=function(){var proxies={};this.starting(function(){registerHubProxies(proxies,true);this._registerSubscribedHubs();}).disconnected(function(){registerHubProxies(proxies,false);});proxies['insideSocialHub']=this.createHubProxy('insideSocialHub');proxies['insideSocialHub'].client={};proxies['insideSocialHub'].server={active:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["active"],$.makeArray(arguments)));},blockVisitor:function(userid,block){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["blockVisitor"],$.makeArray(arguments)));},chat:function(chatid,text){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["chat"],$.makeArray(arguments)));},chatData:function(chatid,data){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["chatData"],$.makeArray(arguments)));},followVisitor:function(userid){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["followVisitor"],$.makeArray(arguments)));},getCurrentView:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["GetCurrentView"],$.makeArray(arguments)));},getUserData:function(userid){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["getUserData"],$.makeArray(arguments)));},inactive:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["inactive"],$.makeArray(arguments)));},keepAlive:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["keepAlive"],$.makeArray(arguments)));},loadChat:function(chatid){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["loadChat"],$.makeArray(arguments)));},reportVisitor:function(userid,reason){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["reportVisitor"],$.makeArray(arguments)));},setNickname:function(nickname){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["setNickname"],$.makeArray(arguments)));},shoutout:function(target,text){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["shoutout"],$.makeArray(arguments)));},startChat:function(text){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["startChat"],$.makeArray(arguments)));},stopChat:function(chatid){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["stopChat"],$.makeArray(arguments)));},unstick:function(stickyid){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["Unstick"],$.makeArray(arguments)));},updateCoBrowseSession:function(data){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["UpdateCoBrowseSession"],$.makeArray(arguments)));},acceptCoBrowseRequest:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["AcceptCoBrowseRequest"],$.makeArray(arguments)));},rejectCoBrowseRequest:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["RejectCoBrowseRequest"],$.makeArray(arguments)));},coBrowseNotSupported:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["CoBrowseNotSupported"],$.makeArray(arguments)));},closeCoBrowseSession:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["CloseCoBrowseSession"],$.makeArray(arguments)));},coBrowseRequestState:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["CoBrowseRequestState"],$.makeArray(arguments)));},coBrowseSendToOperator:function(){return proxies['insideSocialHub'].invoke.apply(proxies['insideSocialHub'],$.merge(["CoBrowseSendToOperator"],$.makeArray(arguments)));}};return proxies;};signalR.hub=$.hubConnection("/signalr",{useDefaultPath:false});$.extend(signalR,signalR.hub.createHubProxies());})(_insideGraph.jQuery,window);(function($,window){$.connection.hub.url=_insideSocialUrl+"signalr";var hub=$.connection.insideSocialHub;var _callback={};var _queueEvents=true;var _eventQueue=[];var _init=[];var _deinit=[];hub.init=function(callback){_init[_init.length]=callback;};hub.deinit=function(callback){_deinit[_deinit.length]=callback;};hub.bind=function(name,callback){if(!_insideLive&&typeof(console)!="undefined"&&typeof(console.log)!="undefined")
console.log("[INSIDE]: bind :: "+ name);if(typeof callback=="function"){if(!_callback[name])
_callback[name]=[];_callback[name][_callback[name].length]=callback;}};hub.unbind=function(name,callback){if(typeof _callback[name]!="undefined"&&typeof callback=="function"){_callback[name].splice(i,1);}};hub.unbindAll=function(name){if(typeof _callback[name]!="undefined"){_callback[name]=[];}};hub.clearAllEvents=function(){_callback={};}
hub.client.doEvent=function(name,data,stickyid){if(_queueEvents&&name!="connected"){_eventQueue.push({"name":name,"data":data,"stickyid":stickyid});return;}
if(!_insideLive&&typeof(console)!="undefined"&&typeof(console.log)!="undefined")
console.log("[INSIDE]: doEvent :: "+ name+" :: "+(typeof(stickyid)!="undefined"?stickyid:"")+" :: ",data);var eventCallbackList=_callback[name];if(eventCallbackList)
for(var i=0;i<eventCallbackList.length;i++)
eventCallbackList[i](data,stickyid);};hub.flushQueue=function(){_queueEvents=false;hub.client.doEvents(_eventQueue);_eventQueue=[];};hub.client.doEvents=function(events){for(var i=0;i<events.length;i++){hub.client.doEvent(events[i].name,events[i].data,events[i].stickyid);}};hub.client.stopInside=function(){$.connection.hub.stop();hub.client.doEvent("disconnected",null);for(var i=0;i<_deinit.length;i++)
_deinit[i]();_deinit=[];};hub.setCookie=function(cName,value){var exdate=new Date();exdate.setDate(exdate.getDate()+ 365*50);var c_value=escape(value)+";expires="+ exdate.toUTCString()+";path=/";document.cookie=cName+"="+ c_value;};hub.stop=function(){if(hub.connection.ajaxDataType=="jsonp"&&_insideGraph.ieVersion==9){hub.connection.ajaxDataType="text";$.connection.hub.stop();hub.connection.ajaxDataType="jsonp";}
else
$.connection.hub.stop();hub.client.doEvent("disconnected",null);};function isNullOrWhiteSpace(str){return(!str||str.length===0||/^\s*$/.test(str))}
hub.ga=function(event)
{if(typeof(event)=="object"&&!isNullOrWhiteSpace(event.category)&&!isNullOrWhiteSpace(event.action))
{if(_insideGraph.hasEvent("googleAnalytics")){hub.bind("ga",function(event){_insideGraph.doEvent("googleAnalytics",event);});}else{if(typeof(ga)=="function")
ga('send','event',event.category,event.action,event.label,event.value,true);if(typeof(_gaq)=="object")
_gaq.push('_trackEvent',event.category,event.action,event.label,event.value,true);if(typeof(google_tag_manager)!="undefined"&&typeof(dataLayer)=="object"){dataLayer.push({"event":"insideEvent","eventCategory":event.category,"eventAction":event.action,"eventLabel":event.label,"eventValue":event.value,"eventNI":true});}}}};hub.bind("ga",hub.ga);hub.starts=0;hub.start=function(){try{if(window.Prototype){delete Array.prototype.toJSON;}}catch(ex){}
if($.connection.hub.state!=$.connection.connectionState.disconnected)
$.connection.hub.stop();if(!_insideLive)
$.connection.hub.logging=true;$.connection.hub.qs={'k':_insideGraph.current.account+":"+ _insideGraph.pid+":"+ _insideGraph.accessKey,'c':_insideGraph.accessCheck};$.connection.hub.start({transport:["webSockets","longPolling"]}).done(hubStarted);};function hubStarted(){if($.inside.starts==0){$(window).on('beforeunload',function(){$.connection.hub.url=_insideSocialUrl+"signalr";if($.connection.hub.state!=$.connection.connectionState.disconnected)
$.connection.hub.stop();});_insideGraph.doEvent("onconnected",true);}
for(var i=0;i<_init.length;i++)
_init[i]();_init=[];$.inside.starts++;}
hub.keepAlive=function(){if(hub.isConnected())
hub.server.keepAlive();};hub.isConnected=function(){if($.connection.hub.state==$.connection.connectionState.connected)
return true;return false;};hub.isEnabled=function(func){var temp=$.inArray(func,_insideGraph.enabled);return typeof(temp)!="undefined"&&temp!=-1;};$.inside=hub;})(_insideGraph.jQuery,window);(function($,window){var _keepAliveTimeout=120000;var _idleTimeout=180000;var _awayTimeout=300000;var _idleNow=false;var _idleTimestamp=null;var _idleTimer=null;var _awayNow=false;var _awayTimestamp=null;var _awayTimer=null;$.inside.bind("poke",function(){_active(true);});var agent=window.navigator.userAgent.toLowerCase();if(agent.indexOf("ipad")!=-1){var lastTime=new Date();setTimeout(checkTime,500);}
var _keepAliveTimer=setTimeout(keepAlive,_keepAliveTimeout);function keepAlive(){$.inside.keepAlive();_keepAliveTimer=setTimeout(keepAlive,_keepAliveTimeout);}
function checkTime(){var dt=(new Date()).valueOf()- lastTime.valueOf();if(dt>300000&&$.connection){$.inside.stop();}
if(dt>1000)
{_idleNow=true;_active();}
lastTime=new Date();setTimeout(checkTime,500);}
function setIdleTimeout(ms){_idleTimeout=ms;_idleTimestamp=new Date().getTime()+ ms;if(_idleTimer!=null)
clearTimeout(_idleTimer);_idleTimer=setTimeout(_makeIdle,ms+ 50);}
function setAwayTimeout(ms){_awayTimeout=ms;_awayTimestamp=new Date().getTime()+ ms;if(_awayTimer!=null)
clearTimeout(_awayTimer);_awayTimer=setTimeout(_makeAway,ms+ 50);}
function _makeIdle(override){if(_idleNow)
return;if(!override)
{var t=new Date().getTime();if(t<_idleTimestamp){_idleTimer=setTimeout(_makeIdle,_idleTimestamp- t+ 50);return;}}
if(_idleTimer!=null)
clearTimeout(_idleTimer);_idleNow=true;if($.inside.isConnected())
$.inside.server.inactive();}
function _makeAway(override)
{if(_awayNow)
return;if(!override)
{var t=new Date().getTime();if(t<_awayTimestamp||override){_awayTimer=setTimeout(_makeAway,_awayTimestamp- t+ 50);return;}}
if(_awayTimer!=null)
clearTimeout(_awayTimer);if(_keepAliveTimer!=null)
clearTimeout(_keepAliveTimer);_awayNow=true;$.inside.stop();}
function _active(data)
{var poke=false;if(typeof(data)==="boolean")
poke=data;if(!poke&&(_idleNow||_awayNow))
if(typeof(window.document.hasFocus)!="undefined"&&!window.document.hasFocus())
return;var t=new Date().getTime();_idleTimestamp=t+ _idleTimeout;_awayTimestamp=t+ _awayTimeout;try{if(_keepAliveTimer==null)
_keepAliveTimer=setTimeout(keepAlive,_keepAliveTimeout);if(_awayNow){setAwayTimeout(_awayTimeout);$.inside.start();}
if(_idleNow){setIdleTimeout(_idleTimeout);if(!_awayNow)
{if(!$.inside.isConnected())
$.inside.start();else{$.inside.server.active().done(function(connected){if(!connected)
$.inside.start();});}}}}catch(err){}
_idleNow=false;_awayNow=false;}
function _onchange(evt)
{evt=evt||window.event;if(evt.type=="blur"||evt.type=="focusout"||evt.type=="pagehide")
_makeIdle(true);else
_active();}
function getDevice(){var userAgent=navigator.userAgent.toLowerCase();$.browser.chrome=/chrome/.test(navigator.userAgent.toLowerCase());var agent=window.navigator.userAgent.toLowerCase();var iPod=(agent.indexOf("ipod")>0);var iPhone=(agent.indexOf("iphone")>0);var iPad=(agent.indexOf("ipad")>0);var android=((agent.indexOf("android")>0)&&(agent.indexOf("mobile")>0));var androidTablet=!android&&(agent.indexOf("android")>0);var webOS=(agent.indexOf("webos")>0);var blackBerry=(agent.indexOf("blackberry")>0);var rimTablet=(agent.indexOf("rimtablet")>0);var device=1;if(iPod||iPhone||android||webOS||blackBerry){device=2;}else if(iPad||rimTablet||androidTablet){device=3;}
return device;}
function _init()
{var doc=$(window.document);doc.mousemove(_active);doc.mouseenter(_active);doc.scroll(_active);doc.keydown(_active);doc.click(_active);doc.dblclick(_active);if(doc.addEventListener){doc.addEventListener("touchstart",_active,false);doc.addEventListener("touchmove",_active,false);}
setIdleTimeout(180000);setAwayTimeout(300000);$(window).focus(_onchange);$(window).blur(_onchange);if(typeof(window.addEventListener)!="undefined"){window.addEventListener("pageshow",_onchange);window.addEventListener("pagehide",_onchange);}}
function _deinit()
{var doc=$(window.document);doc.unbind("mousemove",_active);doc.unbind("mouseenter",_active);doc.unbind("scroll",_active);doc.unbind("keydown",_active);doc.unbind("click",_active);doc.unbind("dblclick",_active);if(doc.removeEventListener){doc.removeEventListener("touchstart",_active,false);doc.removeEventListener("touchmove",_active,false);}
$(window).unbind("focus",_onchange);$(window).unbind("blur",_onchange);if(typeof(window.removeEventListener)!="undefined"){window.removeEventListener("pageshow",_onchange);window.removeEventListener("pagehide",_onchange);}}
$.inside.forceIdle=function(){_makeIdle(true);};$.inside.forceAway=function(){_makeAway(true);};$.inside.init(_init);$.inside.deinit(_deinit);})(_insideGraph.jQuery,window);});if(!Array.prototype.indexOf||[].indexOf("")!=-1){Array.prototype.indexOf=function(obj,start){for(var i=(start||0),j=this.length;i<j;i++){if(this[i]===obj){return i;}}
return-1;}}
var insideFrontInterface={};var insideTween;_insideGraph.registerModule("front",function(){console.log("INSIDE Module 'front' started");var gsConflict=false;if(typeof(window.GreenSockGlobals)!="undefined"){gsConflict=true;var oldGSG=window.GreenSockGlobals;}
insideTween=window.GreenSockGlobals={};(window._gsQueue||(window._gsQueue=[])).push(function(){"use strict";window._gsDefine("TweenMax",["core.Animation","core.SimpleTimeline","TweenLite"],function(t,e,i){var s=[].slice,r=function(t,e,s){i.call(this,t,e,s),this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._dirty=!0,this.render=r.prototype.render},n=1e-10,a=i._internals.isSelector,o=i._internals.isArray,h=r.prototype=i.to({},.1,{}),l=[];r.version="1.11.2",h.constructor=r,h.kill()._gc=!1,r.killTweensOf=r.killDelayedCallsTo=i.killTweensOf,r.getTweensOf=i.getTweensOf,r.ticker=i.ticker,h.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),i.prototype.invalidate.call(this)},h.updateTo=function(t,e){var s,r=this.ratio;e&&this.timeline&&this._startTime<this._timeline._time&&(this._startTime=this._timeline._time,this._uncache(!1),this._gc?this._enabled(!0,!1):this._timeline.insert(this,this._startTime-this._delay));for(s in t)this.vars[s]=t[s];if(this._initted)if(e)this._initted=!1;else if(this._notifyPluginsOfEnabled&&this._firstPT&&i._onPluginEvent("_onDisable",this),this._time/this._duration>.998){var n=this._time;this.render(0,!0,!1),this._initted=!1,this.render(n,!0,!1)}else if(this._time>0){this._initted=!1,this._init();for(var a,o=1/(1-r),h=this._firstPT;h;)a=h.s+h.c,h.c*=o,h.s=a-h.c,h=h._next}return this},h.render=function(t,e,i){this._initted||0===this._duration&&this.vars.repeat&&this.invalidate();var s,r,a,o,h,_,u,p,f=this._dirty?this.totalDuration():this._totalDuration,c=this._time,m=this._totalTime,d=this._cycle,g=this._duration;if(t>=f?(this._totalTime=f,this._cycle=this._repeat,this._yoyo&&0!==(1&this._cycle)?(this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0):(this._time=g,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1),this._reversed||(s=!0,r="onComplete"),0===g&&(p=this._rawPrevTime,(0===t||0>p||p===n)&&p!==t&&(i=!0,p>n&&(r="onReverseComplete")),this._rawPrevTime=p=!e||t?t:n)):1e-7>t?(this._totalTime=this._time=this._cycle=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==m||0===g&&this._rawPrevTime>n)&&(r="onReverseComplete",s=this._reversed),0>t?(this._active=!1,0===g&&(this._rawPrevTime>=0&&(i=!0),this._rawPrevTime=p=!e||t?t:n)):this._initted||(i=!0)):(this._totalTime=this._time=t,0!==this._repeat&&(o=g+this._repeatDelay,this._cycle=this._totalTime/o>>0,0!==this._cycle&&this._cycle===this._totalTime/o&&this._cycle--,this._time=this._totalTime-this._cycle*o,this._yoyo&&0!==(1&this._cycle)&&(this._time=g-this._time),this._time>g?this._time=g:0>this._time&&(this._time=0)),this._easeType?(h=this._time/g,_=this._easeType,u=this._easePower,(1===_||3===_&&h>=.5)&&(h=1-h),3===_&&(h*=2),1===u?h*=h:2===u?h*=h*h:3===u?h*=h*h*h:4===u&&(h*=h*h*h*h),this.ratio=1===_?1-h:2===_?h:.5>this._time/g?h/2:1-h/2):this.ratio=this._ease.getRatio(this._time/g)),c===this._time&&!i&&d===this._cycle)return m!==this._totalTime&&this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||l)),void 0;if(!this._initted){if(this._init(),!this._initted||this._gc)return;this._time&&!s?this.ratio=this._ease.getRatio(this._time/g):s&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._active||!this._paused&&this._time!==c&&t>=0&&(this._active=!0),0===m&&(this._startAt&&(t>=0?this._startAt.render(t,e,i):r||(r="_dummyGS")),this.vars.onStart&&(0!==this._totalTime||0===g)&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||l))),a=this._firstPT;a;)a.f?a.t[a.p](a.c*this.ratio+a.s):a.t[a.p]=a.c*this.ratio+a.s,a=a._next;this._onUpdate&&(0>t&&this._startAt&&this._startTime&&this._startAt.render(t,e,i),e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||l)),this._cycle!==d&&(e||this._gc||this.vars.onRepeat&&this.vars.onRepeat.apply(this.vars.onRepeatScope||this,this.vars.onRepeatParams||l)),r&&(this._gc||(0>t&&this._startAt&&!this._onUpdate&&this._startTime&&this._startAt.render(t,e,i),s&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[r]&&this.vars[r].apply(this.vars[r+"Scope"]||this,this.vars[r+"Params"]||l),0===g&&this._rawPrevTime===n&&p!==n&&(this._rawPrevTime=0)))},r.to=function(t,e,i){return new r(t,e,i)},r.from=function(t,e,i){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,new r(t,e,i)},r.fromTo=function(t,e,i,s){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,new r(t,e,s)},r.staggerTo=r.allTo=function(t,e,n,h,_,u,p){h=h||0;var f,c,m,d,g=n.delay||0,v=[],y=function(){n.onComplete&&n.onComplete.apply(n.onCompleteScope||this,arguments),_.apply(p||this,u||l)};for(o(t)||("string"==typeof t&&(t=i.selector(t)||t),a(t)&&(t=s.call(t,0))),f=t.length,m=0;f>m;m++){c={};for(d in n)c[d]=n[d];c.delay=g,m===f-1&&_&&(c.onComplete=y),v[m]=new r(t[m],e,c),g+=h}return v},r.staggerFrom=r.allFrom=function(t,e,i,s,n,a,o){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,r.staggerTo(t,e,i,s,n,a,o)},r.staggerFromTo=r.allFromTo=function(t,e,i,s,n,a,o,h){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,r.staggerTo(t,e,s,n,a,o,h)},r.delayedCall=function(t,e,i,s,n){return new r(e,0,{delay:t,onComplete:e,onCompleteParams:i,onCompleteScope:s,onReverseComplete:e,onReverseCompleteParams:i,onReverseCompleteScope:s,immediateRender:!1,useFrames:n,overwrite:0})},r.set=function(t,e){return new r(t,0,e)},r.isTweening=function(t){return i.getTweensOf(t,!0).length>0};var _=function(t,e){for(var s=[],r=0,n=t._first;n;)n instanceof i?s[r++]=n:(e&&(s[r++]=n),s=s.concat(_(n,e)),r=s.length),n=n._next;return s},u=r.getAllTweens=function(e){return _(t._rootTimeline,e).concat(_(t._rootFramesTimeline,e))};r.killAll=function(t,i,s,r){null==i&&(i=!0),null==s&&(s=!0);var n,a,o,h=u(0!=r),l=h.length,_=i&&s&&r;for(o=0;l>o;o++)a=h[o],(_||a instanceof e||(n=a.target===a.vars.onComplete)&&s||i&&!n)&&(t?a.totalTime(a.totalDuration()):a._enabled(!1,!1))},r.killChildTweensOf=function(t,e){if(null!=t){var n,h,l,_,u,p=i._tweenLookup;if("string"==typeof t&&(t=i.selector(t)||t),a(t)&&(t=s(t,0)),o(t))for(_=t.length;--_>-1;)r.killChildTweensOf(t[_],e);else{n=[];for(l in p)for(h=p[l].target.parentNode;h;)h===t&&(n=n.concat(p[l].tweens)),h=h.parentNode;for(u=n.length,_=0;u>_;_++)e&&n[_].totalTime(n[_].totalDuration()),n[_]._enabled(!1,!1)}}};var p=function(t,i,s,r){i=i!==!1,s=s!==!1,r=r!==!1;for(var n,a,o=u(r),h=i&&s&&r,l=o.length;--l>-1;)a=o[l],(h||a instanceof e||(n=a.target===a.vars.onComplete)&&s||i&&!n)&&a.paused(t)};return r.pauseAll=function(t,e,i){p(!0,t,e,i)},r.resumeAll=function(t,e,i){p(!1,t,e,i)},r.globalTimeScale=function(e){var s=t._rootTimeline,r=i.ticker.time;return arguments.length?(e=e||n,s._startTime=r-(r-s._startTime)*s._timeScale/e,s=t._rootFramesTimeline,r=i.ticker.frame,s._startTime=r-(r-s._startTime)*s._timeScale/e,s._timeScale=t._rootTimeline._timeScale=e,e):s._timeScale},h.progress=function(t){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-t:t)+this._cycle*(this._duration+this._repeatDelay),!1):this._time/this.duration()},h.totalProgress=function(t){return arguments.length?this.totalTime(this.totalDuration()*t,!1):this._totalTime/this.totalDuration()},h.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),t>this._duration&&(t=this._duration),this._yoyo&&0!==(1&this._cycle)?t=this._duration-t+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(t+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(t,e)):this._time},h.duration=function(e){return arguments.length?t.prototype.duration.call(this,e):this._duration},h.totalDuration=function(t){return arguments.length?-1===this._repeat?this:this.duration((t-this._repeat*this._repeatDelay)/(this._repeat+1)):(this._dirty&&(this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat,this._dirty=!1),this._totalDuration)},h.repeat=function(t){return arguments.length?(this._repeat=t,this._uncache(!0)):this._repeat},h.repeatDelay=function(t){return arguments.length?(this._repeatDelay=t,this._uncache(!0)):this._repeatDelay},h.yoyo=function(t){return arguments.length?(this._yoyo=t,this):this._yoyo},r},!0),window._gsDefine("TimelineLite",["core.Animation","core.SimpleTimeline","TweenLite"],function(t,e,i){var s=function(t){e.call(this,t),this._labels={},this.autoRemoveChildren=this.vars.autoRemoveChildren===!0,this.smoothChildTiming=this.vars.smoothChildTiming===!0,this._sortChildren=!0,this._onUpdate=this.vars.onUpdate;var i,s,r=this.vars;for(s in r)i=r[s],a(i)&&-1!==i.join("").indexOf("{self}")&&(r[s]=this._swapSelfInParams(i));a(r.tweens)&&this.add(r.tweens,0,r.align,r.stagger)},r=1e-10,n=i._internals.isSelector,a=i._internals.isArray,o=[],h=function(t){var e,i={};for(e in t)i[e]=t[e];return i},l=function(t,e,i,s){t._timeline.pause(t._startTime),e&&e.apply(s||t._timeline,i||o)},_=o.slice,u=s.prototype=new e;return s.version="1.11.0",u.constructor=s,u.kill()._gc=!1,u.to=function(t,e,s,r){return e?this.add(new i(t,e,s),r):this.set(t,s,r)},u.from=function(t,e,s,r){return this.add(i.from(t,e,s),r)},u.fromTo=function(t,e,s,r,n){return e?this.add(i.fromTo(t,e,s,r),n):this.set(t,r,n)},u.staggerTo=function(t,e,r,a,o,l,u,p){var f,c=new s({onComplete:l,onCompleteParams:u,onCompleteScope:p});for("string"==typeof t&&(t=i.selector(t)||t),n(t)&&(t=_.call(t,0)),a=a||0,f=0;t.length>f;f++)r.startAt&&(r.startAt=h(r.startAt)),c.to(t[f],e,h(r),f*a);return this.add(c,o)},u.staggerFrom=function(t,e,i,s,r,n,a,o){return i.immediateRender=0!=i.immediateRender,i.runBackwards=!0,this.staggerTo(t,e,i,s,r,n,a,o)},u.staggerFromTo=function(t,e,i,s,r,n,a,o,h){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,this.staggerTo(t,e,s,r,n,a,o,h)},u.call=function(t,e,s,r){return this.add(i.delayedCall(0,t,e,s),r)},u.set=function(t,e,s){return s=this._parseTimeOrLabel(s,0,!0),null==e.immediateRender&&(e.immediateRender=s===this._time&&!this._paused),this.add(new i(t,0,e),s)},s.exportRoot=function(t,e){t=t||{},null==t.smoothChildTiming&&(t.smoothChildTiming=!0);var r,n,a=new s(t),o=a._timeline;for(null==e&&(e=!0),o._remove(a,!0),a._startTime=0,a._rawPrevTime=a._time=a._totalTime=o._time,r=o._first;r;)n=r._next,e&&r instanceof i&&r.target===r.vars.onComplete||a.add(r,r._startTime-r._delay),r=n;return o.add(a,0),a},u.add=function(r,n,o,h){var l,_,u,p,f,c;if("number"!=typeof n&&(n=this._parseTimeOrLabel(n,0,!0,r)),!(r instanceof t)){if(r instanceof Array||r&&r.push&&a(r)){for(o=o||"normal",h=h||0,l=n,_=r.length,u=0;_>u;u++)a(p=r[u])&&(p=new s({tweens:p})),this.add(p,l),"string"!=typeof p&&"function"!=typeof p&&("sequence"===o?l=p._startTime+p.totalDuration()/p._timeScale:"start"===o&&(p._startTime-=p.delay())),l+=h;return this._uncache(!0)}if("string"==typeof r)return this.addLabel(r,n);if("function"!=typeof r)throw"Cannot add "+r+" into the timeline; it is not a tween, timeline, function, or string.";r=i.delayedCall(0,r)}if(e.prototype.add.call(this,r,n),this._gc&&!this._paused&&this._duration<this.duration())for(f=this,c=f.rawTime()>r._startTime;f._gc&&f._timeline;)f._timeline.smoothChildTiming&&c?f.totalTime(f._totalTime,!0):f._enabled(!0,!1),f=f._timeline;return this},u.remove=function(e){if(e instanceof t)return this._remove(e,!1);if(e instanceof Array||e&&e.push&&a(e)){for(var i=e.length;--i>-1;)this.remove(e[i]);return this}return"string"==typeof e?this.removeLabel(e):this.kill(null,e)},u._remove=function(t,i){e.prototype._remove.call(this,t,i);var s=this._last;return s?this._time>s._startTime+s._totalDuration/s._timeScale&&(this._time=this.duration(),this._totalTime=this._totalDuration):this._time=this._totalTime=0,this},u.append=function(t,e){return this.add(t,this._parseTimeOrLabel(null,e,!0,t))},u.insert=u.insertMultiple=function(t,e,i,s){return this.add(t,e||0,i,s)},u.appendMultiple=function(t,e,i,s){return this.add(t,this._parseTimeOrLabel(null,e,!0,t),i,s)},u.addLabel=function(t,e){return this._labels[t]=this._parseTimeOrLabel(e),this},u.addPause=function(t,e,i,s){return this.call(l,["{self}",e,i,s],this,t)},u.removeLabel=function(t){return delete this._labels[t],this},u.getLabelTime=function(t){return null!=this._labels[t]?this._labels[t]:-1},u._parseTimeOrLabel=function(e,i,s,r){var n;if(r instanceof t&&r.timeline===this)this.remove(r);else if(r&&(r instanceof Array||r.push&&a(r)))for(n=r.length;--n>-1;)r[n]instanceof t&&r[n].timeline===this&&this.remove(r[n]);if("string"==typeof i)return this._parseTimeOrLabel(i,s&&"number"==typeof e&&null==this._labels[i]?e-this.duration():0,s);if(i=i||0,"string"!=typeof e||!isNaN(e)&&null==this._labels[e])null==e&&(e=this.duration());else{if(n=e.indexOf("="),-1===n)return null==this._labels[e]?s?this._labels[e]=this.duration()+i:i:this._labels[e]+i;i=parseInt(e.charAt(n-1)+"1",10)*Number(e.substr(n+1)),e=n>1?this._parseTimeOrLabel(e.substr(0,n-1),0,s):this.duration()}return Number(e)+i},u.seek=function(t,e){return this.totalTime("number"==typeof t?t:this._parseTimeOrLabel(t),e!==!1)},u.stop=function(){return this.paused(!0)},u.gotoAndPlay=function(t,e){return this.play(t,e)},u.gotoAndStop=function(t,e){return this.pause(t,e)},u.render=function(t,e,i){this._gc&&this._enabled(!0,!1);var s,n,a,h,l,_=this._dirty?this.totalDuration():this._totalDuration,u=this._time,p=this._startTime,f=this._timeScale,c=this._paused;if(t>=_?(this._totalTime=this._time=_,this._reversed||this._hasPausedChild()||(n=!0,h="onComplete",0===this._duration&&(0===t||0>this._rawPrevTime||this._rawPrevTime===r)&&this._rawPrevTime!==t&&this._first&&(l=!0,this._rawPrevTime>r&&(h="onReverseComplete"))),this._rawPrevTime=this._duration||!e||t?t:r,t=_+1e-6):1e-7>t?(this._totalTime=this._time=0,(0!==u||0===this._duration&&(this._rawPrevTime>r||0>t&&this._rawPrevTime>=0))&&(h="onReverseComplete",n=this._reversed),0>t?(this._active=!1,0===this._duration&&this._rawPrevTime>=0&&this._first&&(l=!0),this._rawPrevTime=t):(this._rawPrevTime=this._duration||!e||t?t:r,t=0,this._initted||(l=!0))):this._totalTime=this._time=this._rawPrevTime=t,this._time!==u&&this._first||i||l){if(this._initted||(this._initted=!0),this._active||!this._paused&&this._time!==u&&t>0&&(this._active=!0),0===u&&this.vars.onStart&&0!==this._time&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||o)),this._time>=u)for(s=this._first;s&&(a=s._next,!this._paused||c);)(s._active||s._startTime<=this._time&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=a;else for(s=this._last;s&&(a=s._prev,!this._paused||c);)(s._active||u>=s._startTime&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=a;this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||o)),h&&(this._gc||(p===this._startTime||f!==this._timeScale)&&(0===this._time||_>=this.totalDuration())&&(n&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[h]&&this.vars[h].apply(this.vars[h+"Scope"]||this,this.vars[h+"Params"]||o)))}},u._hasPausedChild=function(){for(var t=this._first;t;){if(t._paused||t instanceof s&&t._hasPausedChild())return!0;t=t._next}return!1},u.getChildren=function(t,e,s,r){r=r||-9999999999;for(var n=[],a=this._first,o=0;a;)r>a._startTime||(a instanceof i?e!==!1&&(n[o++]=a):(s!==!1&&(n[o++]=a),t!==!1&&(n=n.concat(a.getChildren(!0,e,s)),o=n.length))),a=a._next;return n},u.getTweensOf=function(t,e){for(var s=i.getTweensOf(t),r=s.length,n=[],a=0;--r>-1;)(s[r].timeline===this||e&&this._contains(s[r]))&&(n[a++]=s[r]);return n},u._contains=function(t){for(var e=t.timeline;e;){if(e===this)return!0;e=e.timeline}return!1},u.shiftChildren=function(t,e,i){i=i||0;for(var s,r=this._first,n=this._labels;r;)r._startTime>=i&&(r._startTime+=t),r=r._next;if(e)for(s in n)n[s]>=i&&(n[s]+=t);return this._uncache(!0)},u._kill=function(t,e){if(!t&&!e)return this._enabled(!1,!1);for(var i=e?this.getTweensOf(e):this.getChildren(!0,!0,!1),s=i.length,r=!1;--s>-1;)i[s]._kill(t,e)&&(r=!0);return r},u.clear=function(t){var e=this.getChildren(!1,!0,!0),i=e.length;for(this._time=this._totalTime=0;--i>-1;)e[i]._enabled(!1,!1);return t!==!1&&(this._labels={}),this._uncache(!0)},u.invalidate=function(){for(var t=this._first;t;)t.invalidate(),t=t._next;return this},u._enabled=function(t,i){if(t===this._gc)for(var s=this._first;s;)s._enabled(t,!0),s=s._next;return e.prototype._enabled.call(this,t,i)},u.duration=function(t){return arguments.length?(0!==this.duration()&&0!==t&&this.timeScale(this._duration/t),this):(this._dirty&&this.totalDuration(),this._duration)},u.totalDuration=function(t){if(!arguments.length){if(this._dirty){for(var e,i,s=0,r=this._last,n=999999999999;r;)e=r._prev,r._dirty&&r.totalDuration(),r._startTime>n&&this._sortChildren&&!r._paused?this.add(r,r._startTime-r._delay):n=r._startTime,0>r._startTime&&!r._paused&&(s-=r._startTime,this._timeline.smoothChildTiming&&(this._startTime+=r._startTime/this._timeScale),this.shiftChildren(-r._startTime,!1,-9999999999),n=0),i=r._startTime+r._totalDuration/r._timeScale,i>s&&(s=i),r=e;this._duration=this._totalDuration=s,this._dirty=!1}return this._totalDuration}return 0!==this.totalDuration()&&0!==t&&this.timeScale(this._totalDuration/t),this},u.usesFrames=function(){for(var e=this._timeline;e._timeline;)e=e._timeline;return e===t._rootFramesTimeline},u.rawTime=function(){return this._paused?this._totalTime:(this._timeline.rawTime()-this._startTime)*this._timeScale},s},!0),window._gsDefine("TimelineMax",["TimelineLite","TweenLite","easing.Ease"],function(t,e,i){var s=function(e){t.call(this,e),this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._dirty=!0},r=1e-10,n=[],a=new i(null,null,1,0),o=s.prototype=new t;return o.constructor=s,o.kill()._gc=!1,s.version="1.11.0",o.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),t.prototype.invalidate.call(this)},o.addCallback=function(t,i,s,r){return this.add(e.delayedCall(0,t,s,r),i)},o.removeCallback=function(t,e){if(t)if(null==e)this._kill(null,t);else for(var i=this.getTweensOf(t,!1),s=i.length,r=this._parseTimeOrLabel(e);--s>-1;)i[s]._startTime===r&&i[s]._enabled(!1,!1);return this},o.tweenTo=function(t,i){i=i||{};var s,r,o={ease:a,overwrite:2,useFrames:this.usesFrames(),immediateRender:!1};for(s in i)o[s]=i[s];return o.time=this._parseTimeOrLabel(t),r=new e(this,Math.abs(Number(o.time)-this._time)/this._timeScale||.001,o),o.onStart=function(){r.target.paused(!0),r.vars.time!==r.target.time()&&r.duration(Math.abs(r.vars.time-r.target.time())/r.target._timeScale),i.onStart&&i.onStart.apply(i.onStartScope||r,i.onStartParams||n)},r},o.tweenFromTo=function(t,e,i){i=i||{},t=this._parseTimeOrLabel(t),i.startAt={onComplete:this.seek,onCompleteParams:[t],onCompleteScope:this},i.immediateRender=i.immediateRender!==!1;var s=this.tweenTo(e,i);return s.duration(Math.abs(s.vars.time-t)/this._timeScale||.001)},o.render=function(t,e,i){this._gc&&this._enabled(!0,!1);var s,a,o,h,l,_,u=this._dirty?this.totalDuration():this._totalDuration,p=this._duration,f=this._time,c=this._totalTime,m=this._startTime,d=this._timeScale,g=this._rawPrevTime,v=this._paused,y=this._cycle;if(t>=u?(this._locked||(this._totalTime=u,this._cycle=this._repeat),this._reversed||this._hasPausedChild()||(a=!0,h="onComplete",0===this._duration&&(0===t||0>g||g===r)&&g!==t&&this._first&&(l=!0,g>r&&(h="onReverseComplete"))),this._rawPrevTime=this._duration||!e||t?t:r,this._yoyo&&0!==(1&this._cycle)?this._time=t=0:(this._time=p,t=p+1e-6)):1e-7>t?(this._locked||(this._totalTime=this._cycle=0),this._time=0,(0!==f||0===p&&(g>r||0>t&&g>=0)&&!this._locked)&&(h="onReverseComplete",a=this._reversed),0>t?(this._active=!1,0===p&&g>=0&&this._first&&(l=!0),this._rawPrevTime=t):(this._rawPrevTime=p||!e||t?t:r,t=0,this._initted||(l=!0))):(0===p&&0>g&&(l=!0),this._time=this._rawPrevTime=t,this._locked||(this._totalTime=t,0!==this._repeat&&(_=p+this._repeatDelay,this._cycle=this._totalTime/_>>0,0!==this._cycle&&this._cycle===this._totalTime/_&&this._cycle--,this._time=this._totalTime-this._cycle*_,this._yoyo&&0!==(1&this._cycle)&&(this._time=p-this._time),this._time>p?(this._time=p,t=p+1e-6):0>this._time?this._time=t=0:t=this._time))),this._cycle!==y&&!this._locked){var T=this._yoyo&&0!==(1&y),w=T===(this._yoyo&&0!==(1&this._cycle)),x=this._totalTime,b=this._cycle,P=this._rawPrevTime,S=this._time;if(this._totalTime=y*p,y>this._cycle?T=!T:this._totalTime+=p,this._time=f,this._rawPrevTime=0===p?g-1e-5:g,this._cycle=y,this._locked=!0,f=T?0:p,this.render(f,e,0===p),e||this._gc||this.vars.onRepeat&&this.vars.onRepeat.apply(this.vars.onRepeatScope||this,this.vars.onRepeatParams||n),w&&(f=T?p+1e-6:-1e-6,this.render(f,!0,!1)),this._locked=!1,this._paused&&!v)return;this._time=S,this._totalTime=x,this._cycle=b,this._rawPrevTime=P}if(!(this._time!==f&&this._first||i||l))return c!==this._totalTime&&this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||n)),void 0;if(this._initted||(this._initted=!0),this._active||!this._paused&&this._totalTime!==c&&t>0&&(this._active=!0),0===c&&this.vars.onStart&&0!==this._totalTime&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||n)),this._time>=f)for(s=this._first;s&&(o=s._next,!this._paused||v);)(s._active||s._startTime<=this._time&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=o;else for(s=this._last;s&&(o=s._prev,!this._paused||v);)(s._active||f>=s._startTime&&!s._paused&&!s._gc)&&(s._reversed?s.render((s._dirty?s.totalDuration():s._totalDuration)-(t-s._startTime)*s._timeScale,e,i):s.render((t-s._startTime)*s._timeScale,e,i)),s=o;this._onUpdate&&(e||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||n)),h&&(this._locked||this._gc||(m===this._startTime||d!==this._timeScale)&&(0===this._time||u>=this.totalDuration())&&(a&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[h]&&this.vars[h].apply(this.vars[h+"Scope"]||this,this.vars[h+"Params"]||n)))},o.getActive=function(t,e,i){null==t&&(t=!0),null==e&&(e=!0),null==i&&(i=!1);var s,r,n=[],a=this.getChildren(t,e,i),o=0,h=a.length;for(s=0;h>s;s++)r=a[s],r.isActive()&&(n[o++]=r);return n},o.getLabelAfter=function(t){t||0!==t&&(t=this._time);var e,i=this.getLabelsArray(),s=i.length;for(e=0;s>e;e++)if(i[e].time>t)return i[e].name;return null},o.getLabelBefore=function(t){null==t&&(t=this._time);for(var e=this.getLabelsArray(),i=e.length;--i>-1;)if(t>e[i].time)return e[i].name;return null},o.getLabelsArray=function(){var t,e=[],i=0;for(t in this._labels)e[i++]={time:this._labels[t],name:t};return e.sort(function(t,e){return t.time-e.time}),e},o.progress=function(t){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-t:t)+this._cycle*(this._duration+this._repeatDelay),!1):this._time/this.duration()},o.totalProgress=function(t){return arguments.length?this.totalTime(this.totalDuration()*t,!1):this._totalTime/this.totalDuration()},o.totalDuration=function(e){return arguments.length?-1===this._repeat?this:this.duration((e-this._repeat*this._repeatDelay)/(this._repeat+1)):(this._dirty&&(t.prototype.totalDuration.call(this),this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat),this._totalDuration)},o.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),t>this._duration&&(t=this._duration),this._yoyo&&0!==(1&this._cycle)?t=this._duration-t+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(t+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(t,e)):this._time},o.repeat=function(t){return arguments.length?(this._repeat=t,this._uncache(!0)):this._repeat},o.repeatDelay=function(t){return arguments.length?(this._repeatDelay=t,this._uncache(!0)):this._repeatDelay},o.yoyo=function(t){return arguments.length?(this._yoyo=t,this):this._yoyo},o.currentLabel=function(t){return arguments.length?this.seek(t,!0):this.getLabelBefore(this._time+1e-8)},s},!0),function(){var t=180/Math.PI,e=[],i=[],s=[],r={},n=function(t,e,i,s){this.a=t,this.b=e,this.c=i,this.d=s,this.da=s-t,this.ca=i-t,this.ba=e-t},a=",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,",o=function(t,e,i,s){var r={a:t},n={},a={},o={c:s},h=(t+e)/2,l=(e+i)/2,_=(i+s)/2,u=(h+l)/2,p=(l+_)/2,f=(p-u)/8;return r.b=h+(t-h)/4,n.b=u+f,r.c=n.a=(r.b+n.b)/2,n.c=a.a=(u+p)/2,a.b=p-f,o.b=_+(s-_)/4,a.c=o.a=(a.b+o.b)/2,[r,n,a,o]},h=function(t,r,n,a,h){var l,_,u,p,f,c,m,d,g,v,y,T,w,x=t.length-1,b=0,P=t[0].a;for(l=0;x>l;l++)f=t[b],_=f.a,u=f.d,p=t[b+1].d,h?(y=e[l],T=i[l],w=.25*(T+y)*r/(a?.5:s[l]||.5),c=u-(u-_)*(a?.5*r:0!==y?w/y:0),m=u+(p-u)*(a?.5*r:0!==T?w/T:0),d=u-(c+((m-c)*(3*y/(y+T)+.5)/4||0))):(c=u-.5*(u-_)*r,m=u+.5*(p-u)*r,d=u-(c+m)/2),c+=d,m+=d,f.c=g=c,f.b=0!==l?P:P=f.a+.6*(f.c-f.a),f.da=u-_,f.ca=g-_,f.ba=P-_,n?(v=o(_,P,g,u),t.splice(b,1,v[0],v[1],v[2],v[3]),b+=4):b++,P=m;f=t[b],f.b=P,f.c=P+.4*(f.d-P),f.da=f.d-f.a,f.ca=f.c-f.a,f.ba=P-f.a,n&&(v=o(f.a,P,f.c,f.d),t.splice(b,1,v[0],v[1],v[2],v[3]))},l=function(t,s,r,a){var o,h,l,_,u,p,f=[];if(a)for(t=[a].concat(t),h=t.length;--h>-1;)"string"==typeof(p=t[h][s])&&"="===p.charAt(1)&&(t[h][s]=a[s]+Number(p.charAt(0)+p.substr(2)));if(o=t.length-2,0>o)return f[0]=new n(t[0][s],0,0,t[-1>o?0:1][s]),f;for(h=0;o>h;h++)l=t[h][s],_=t[h+1][s],f[h]=new n(l,0,0,_),r&&(u=t[h+2][s],e[h]=(e[h]||0)+(_-l)*(_-l),i[h]=(i[h]||0)+(u-_)*(u-_));return f[h]=new n(t[h][s],0,0,t[h+1][s]),f},_=function(t,n,o,_,u,p){var f,c,m,d,g,v,y,T,w={},x=[],b=p||t[0];u="string"==typeof u?","+u+",":a,null==n&&(n=1);for(c in t[0])x.push(c);if(t.length>1){for(T=t[t.length-1],y=!0,f=x.length;--f>-1;)if(c=x[f],Math.abs(b[c]-T[c])>.05){y=!1;break}y&&(t=t.concat(),p&&t.unshift(p),t.push(t[1]),p=t[t.length-3])}for(e.length=i.length=s.length=0,f=x.length;--f>-1;)c=x[f],r[c]=-1!==u.indexOf(","+c+","),w[c]=l(t,c,r[c],p);for(f=e.length;--f>-1;)e[f]=Math.sqrt(e[f]),i[f]=Math.sqrt(i[f]);if(!_){for(f=x.length;--f>-1;)if(r[c])for(m=w[x[f]],v=m.length-1,d=0;v>d;d++)g=m[d+1].da/i[d]+m[d].da/e[d],s[d]=(s[d]||0)+g*g;for(f=s.length;--f>-1;)s[f]=Math.sqrt(s[f])}for(f=x.length,d=o?4:1;--f>-1;)c=x[f],m=w[c],h(m,n,o,_,r[c]),y&&(m.splice(0,d),m.splice(m.length-d,d));return w},u=function(t,e,i){e=e||"soft";var s,r,a,o,h,l,_,u,p,f,c,m={},d="cubic"===e?3:2,g="soft"===e,v=[];if(g&&i&&(t=[i].concat(t)),null==t||d+1>t.length)throw"invalid Bezier data";for(p in t[0])v.push(p);for(l=v.length;--l>-1;){for(p=v[l],m[p]=h=[],f=0,u=t.length,_=0;u>_;_++)s=null==i?t[_][p]:"string"==typeof(c=t[_][p])&&"="===c.charAt(1)?i[p]+Number(c.charAt(0)+c.substr(2)):Number(c),g&&_>1&&u-1>_&&(h[f++]=(s+h[f-2])/2),h[f++]=s;for(u=f-d+1,f=0,_=0;u>_;_+=d)s=h[_],r=h[_+1],a=h[_+2],o=2===d?0:h[_+3],h[f++]=c=3===d?new n(s,r,a,o):new n(s,(2*r+s)/3,(2*r+a)/3,a);h.length=f}return m},p=function(t,e,i){for(var s,r,n,a,o,h,l,_,u,p,f,c=1/i,m=t.length;--m>-1;)for(p=t[m],n=p.a,a=p.d-n,o=p.c-n,h=p.b-n,s=r=0,_=1;i>=_;_++)l=c*_,u=1-l,s=r-(r=(l*l*a+3*u*(l*o+u*h))*l),f=m*i+_-1,e[f]=(e[f]||0)+s*s},f=function(t,e){e=e>>0||6;var i,s,r,n,a=[],o=[],h=0,l=0,_=e-1,u=[],f=[];for(i in t)p(t[i],a,e);for(r=a.length,s=0;r>s;s++)h+=Math.sqrt(a[s]),n=s%e,f[n]=h,n===_&&(l+=h,n=s/e>>0,u[n]=f,o[n]=l,h=0,f=[]);return{length:l,lengths:o,segments:u}},c=window._gsDefine.plugin({propName:"bezier",priority:-1,API:2,global:!0,init:function(t,e,i){this._target=t,e instanceof Array&&(e={values:e}),this._func={},this._round={},this._props=[],this._timeRes=null==e.timeResolution?6:parseInt(e.timeResolution,10);var s,r,n,a,o,h=e.values||[],l={},p=h[0],c=e.autoRotate||i.vars.orientToBezier;this._autoRotate=c?c instanceof Array?c:[["x","y","rotation",c===!0?0:Number(c)||0]]:null;for(s in p)this._props.push(s);for(n=this._props.length;--n>-1;)s=this._props[n],this._overwriteProps.push(s),r=this._func[s]="function"==typeof t[s],l[s]=r?t[s.indexOf("set")||"function"!=typeof t["get"+s.substr(3)]?s:"get"+s.substr(3)]():parseFloat(t[s]),o||l[s]!==h[0][s]&&(o=l);if(this._beziers="cubic"!==e.type&&"quadratic"!==e.type&&"soft"!==e.type?_(h,isNaN(e.curviness)?1:e.curviness,!1,"thruBasic"===e.type,e.correlate,o):u(h,e.type,l),this._segCount=this._beziers[s].length,this._timeRes){var m=f(this._beziers,this._timeRes);this._length=m.length,this._lengths=m.lengths,this._segments=m.segments,this._l1=this._li=this._s1=this._si=0,this._l2=this._lengths[0],this._curSeg=this._segments[0],this._s2=this._curSeg[0],this._prec=1/this._curSeg.length}if(c=this._autoRotate)for(c[0]instanceof Array||(this._autoRotate=c=[c]),n=c.length;--n>-1;)for(a=0;3>a;a++)s=c[n][a],this._func[s]="function"==typeof t[s]?t[s.indexOf("set")||"function"!=typeof t["get"+s.substr(3)]?s:"get"+s.substr(3)]:!1;return!0},set:function(e){var i,s,r,n,a,o,h,l,_,u,p=this._segCount,f=this._func,c=this._target;if(this._timeRes){if(_=this._lengths,u=this._curSeg,e*=this._length,r=this._li,e>this._l2&&p-1>r){for(l=p-1;l>r&&e>=(this._l2=_[++r]););this._l1=_[r-1],this._li=r,this._curSeg=u=this._segments[r],this._s2=u[this._s1=this._si=0]}else if(this._l1>e&&r>0){for(;r>0&&(this._l1=_[--r])>=e;);0===r&&this._l1>e?this._l1=0:r++,this._l2=_[r],this._li=r,this._curSeg=u=this._segments[r],this._s1=u[(this._si=u.length-1)-1]||0,this._s2=u[this._si]}if(i=r,e-=this._l1,r=this._si,e>this._s2&&u.length-1>r){for(l=u.length-1;l>r&&e>=(this._s2=u[++r]););this._s1=u[r-1],this._si=r}else if(this._s1>e&&r>0){for(;r>0&&(this._s1=u[--r])>=e;);0===r&&this._s1>e?this._s1=0:r++,this._s2=u[r],this._si=r}o=(r+(e-this._s1)/(this._s2-this._s1))*this._prec}else i=0>e?0:e>=1?p-1:p*e>>0,o=(e-i*(1/p))*p;for(s=1-o,r=this._props.length;--r>-1;)n=this._props[r],a=this._beziers[n][i],h=(o*o*a.da+3*s*(o*a.ca+s*a.ba))*o+a.a,this._round[n]&&(h=h+(h>0?.5:-.5)>>0),f[n]?c[n](h):c[n]=h;if(this._autoRotate){var m,d,g,v,y,T,w,x=this._autoRotate;for(r=x.length;--r>-1;)n=x[r][2],T=x[r][3]||0,w=x[r][4]===!0?1:t,a=this._beziers[x[r][0]],m=this._beziers[x[r][1]],a&&m&&(a=a[i],m=m[i],d=a.a+(a.b-a.a)*o,v=a.b+(a.c-a.b)*o,d+=(v-d)*o,v+=(a.c+(a.d-a.c)*o-v)*o,g=m.a+(m.b-m.a)*o,y=m.b+(m.c-m.b)*o,g+=(y-g)*o,y+=(m.c+(m.d-m.c)*o-y)*o,h=Math.atan2(y-g,v-d)*w+T,f[n]?c[n](h):c[n]=h)}}}),m=c.prototype;c.bezierThrough=_,c.cubicToQuadratic=o,c._autoCSS=!0,c.quadraticToCubic=function(t,e,i){return new n(t,(2*e+t)/3,(2*e+i)/3,i)},c._cssRegister=function(){var t=window._gsDefine.globals.CSSPlugin;if(t){var e=t._internals,i=e._parseToProxy,s=e._setPluginRatio,r=e.CSSPropTween;e._registerComplexSpecialProp("bezier",{parser:function(t,e,n,a,o,h){e instanceof Array&&(e={values:e}),h=new c;var l,_,u,p=e.values,f=p.length-1,m=[],d={};if(0>f)return o;for(l=0;f>=l;l++)u=i(t,p[l],a,o,h,f!==l),m[l]=u.end;for(_ in e)d[_]=e[_];return d.values=m,o=new r(t,"bezier",0,0,u.pt,2),o.data=u,o.plugin=h,o.setRatio=s,0===d.autoRotate&&(d.autoRotate=!0),!d.autoRotate||d.autoRotate instanceof Array||(l=d.autoRotate===!0?0:Number(d.autoRotate),d.autoRotate=null!=u.end.left?[["left","top","rotation",l,!1]]:null!=u.end.x?[["x","y","rotation",l,!1]]:!1),d.autoRotate&&(a._transform||a._enableTransforms(!1),u.autoRotate=a._target._gsTransform),h._onInitTween(u.proxy,d,a._tween),o}})}},m._roundProps=function(t,e){for(var i=this._overwriteProps,s=i.length;--s>-1;)(t[i[s]]||t.bezier||t.bezierThrough)&&(this._round[i[s]]=e)},m._kill=function(t){var e,i,s=this._props;for(e in this._beziers)if(e in t)for(delete this._beziers[e],delete this._func[e],i=s.length;--i>-1;)s[i]===e&&s.splice(i,1);return this._super._kill.call(this,t)}}(),window._gsDefine("plugins.CSSPlugin",["plugins.TweenPlugin","TweenLite"],function(t,e){var i,s,r,n,a=function(){t.call(this,"css"),this._overwriteProps.length=0,this.setRatio=a.prototype.setRatio},o={},h=a.prototype=new t("css");h.constructor=a,a.version="1.11.2",a.API=2,a.defaultTransformPerspective=0,h="px",a.suffixMap={top:h,right:h,bottom:h,left:h,width:h,height:h,fontSize:h,padding:h,margin:h,perspective:h};var l,_,u,p,f,c,m=/(?:\d|\-\d|\.\d|\-\.\d)+/g,d=/(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,g=/(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,v=/[^\d\-\.]/g,y=/(?:\d|\-|\+|=|#|\.)*/g,T=/opacity *= *([^)]*)/,w=/opacity:([^;]*)/,x=/alpha\(opacity *=.+?\)/i,b=/^(rgb|hsl)/,P=/([A-Z])/g,S=/-([a-z])/gi,k=/(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,R=function(t,e){return e.toUpperCase()},A=/(?:Left|Right|Width)/i,C=/(M11|M12|M21|M22)=[\d\-\.e]+/gi,O=/progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,D=/,(?=[^\)]*(?:\(|$))/gi,M=Math.PI/180,I=180/Math.PI,E={},F=document,N=F.createElement("div"),L=F.createElement("img"),X=a._internals={_specialProps:o},z=navigator.userAgent,U=function(){var t,e=z.indexOf("Android"),i=F.createElement("div");return u=-1!==z.indexOf("Safari")&&-1===z.indexOf("Chrome")&&(-1===e||Number(z.substr(e+8,1))>3),f=u&&6>Number(z.substr(z.indexOf("Version/")+8,1)),p=-1!==z.indexOf("Firefox"),/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(z)&&(c=parseFloat(RegExp.$1)),i.innerHTML="<a style='top:1px;opacity:.55;'>a</a>",t=i.getElementsByTagName("a")[0],t?/^0.55/.test(t.style.opacity):!1}(),Y=function(t){return T.test("string"==typeof t?t:(t.currentStyle?t.currentStyle.filter:t.style.filter)||"")?parseFloat(RegExp.$1)/100:1},j=function(t){window.console&&console.log(t)},B="",q="",V=function(t,e){e=e||N;var i,s,r=e.style;if(void 0!==r[t])return t;for(t=t.charAt(0).toUpperCase()+t.substr(1),i=["O","Moz","ms","Ms","Webkit"],s=5;--s>-1&&void 0===r[i[s]+t];);return s>=0?(q=3===s?"ms":i[s],B="-"+q.toLowerCase()+"-",q+t):null},Z=F.defaultView?F.defaultView.getComputedStyle:function(){},G=a.getStyle=function(t,e,i,s,r){var n;return U||"opacity"!==e?(!s&&t.style[e]?n=t.style[e]:(i=i||Z(t,null))?(t=i.getPropertyValue(e.replace(P,"-$1").toLowerCase()),n=t||i.length?t:i[e]):t.currentStyle&&(n=t.currentStyle[e]),null==r||n&&"none"!==n&&"auto"!==n&&"auto auto"!==n?n:r):Y(t)},$=function(t,e,i,s,r){if("px"===s||!s)return i;if("auto"===s||!i)return 0;var n,a=A.test(e),o=t,h=N.style,l=0>i;return l&&(i=-i),"%"===s&&-1!==e.indexOf("border")?n=i/100*(a?t.clientWidth:t.clientHeight):(h.cssText="border:0 solid red;position:"+G(t,"position")+";line-height:0;","%"!==s&&o.appendChild?h[a?"borderLeftWidth":"borderTopWidth"]=i+s:(o=t.parentNode||F.body,h[a?"width":"height"]=i+s),o.appendChild(N),n=parseFloat(N[a?"offsetWidth":"offsetHeight"]),o.removeChild(N),0!==n||r||(n=$(t,e,i,s,!0))),l?-n:n},Q=function(t,e,i){if("absolute"!==G(t,"position",i))return 0;var s="left"===e?"Left":"Top",r=G(t,"margin"+s,i);return t["offset"+s]-($(t,e,parseFloat(r),r.replace(y,""))||0)},W=function(t,e){var i,s,r={};if(e=e||Z(t,null))if(i=e.length)for(;--i>-1;)r[e[i].replace(S,R)]=e.getPropertyValue(e[i]);else for(i in e)r[i]=e[i];else if(e=t.currentStyle||t.style)for(i in e)"string"==typeof i&&void 0!==r[i]&&(r[i.replace(S,R)]=e[i]);return U||(r.opacity=Y(t)),s=be(t,e,!1),r.rotation=s.rotation,r.skewX=s.skewX,r.scaleX=s.scaleX,r.scaleY=s.scaleY,r.x=s.x,r.y=s.y,xe&&(r.z=s.z,r.rotationX=s.rotationX,r.rotationY=s.rotationY,r.scaleZ=s.scaleZ),r.filters&&delete r.filters,r},H=function(t,e,i,s,r){var n,a,o,h={},l=t.style;for(a in i)"cssText"!==a&&"length"!==a&&isNaN(a)&&(e[a]!==(n=i[a])||r&&r[a])&&-1===a.indexOf("Origin")&&("number"==typeof n||"string"==typeof n)&&(h[a]="auto"!==n||"left"!==a&&"top"!==a?""!==n&&"auto"!==n&&"none"!==n||"string"!=typeof e[a]||""===e[a].replace(v,"")?n:0:Q(t,a),void 0!==l[a]&&(o=new ue(l,a,l[a],o)));if(s)for(a in s)"className"!==a&&(h[a]=s[a]);return{difs:h,firstMPT:o}},K={width:["Left","Right"],height:["Top","Bottom"]},J=["marginLeft","marginRight","marginTop","marginBottom"],te=function(t,e,i){var s=parseFloat("width"===e?t.offsetWidth:t.offsetHeight),r=K[e],n=r.length;for(i=i||Z(t,null);--n>-1;)s-=parseFloat(G(t,"padding"+r[n],i,!0))||0,s-=parseFloat(G(t,"border"+r[n]+"Width",i,!0))||0;return s},ee=function(t,e){(null==t||""===t||"auto"===t||"auto auto"===t)&&(t="0 0");var i=t.split(" "),s=-1!==t.indexOf("left")?"0%":-1!==t.indexOf("right")?"100%":i[0],r=-1!==t.indexOf("top")?"0%":-1!==t.indexOf("bottom")?"100%":i[1];return null==r?r="0":"center"===r&&(r="50%"),("center"===s||isNaN(parseFloat(s))&&-1===(s+"").indexOf("="))&&(s="50%"),e&&(e.oxp=-1!==s.indexOf("%"),e.oyp=-1!==r.indexOf("%"),e.oxr="="===s.charAt(1),e.oyr="="===r.charAt(1),e.ox=parseFloat(s.replace(v,"")),e.oy=parseFloat(r.replace(v,""))),s+" "+r+(i.length>2?" "+i[2]:"")},ie=function(t,e){return"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*parseFloat(t.substr(2)):parseFloat(t)-parseFloat(e)},se=function(t,e){return null==t?e:"string"==typeof t&&"="===t.charAt(1)?parseInt(t.charAt(0)+"1",10)*Number(t.substr(2))+e:parseFloat(t)},re=function(t,e,i,s){var r,n,a,o,h=1e-6;return null==t?o=e:"number"==typeof t?o=t:(r=360,n=t.split("_"),a=Number(n[0].replace(v,""))*(-1===t.indexOf("rad")?1:I)-("="===t.charAt(1)?0:e),n.length&&(s&&(s[i]=e+a),-1!==t.indexOf("short")&&(a%=r,a!==a%(r/2)&&(a=0>a?a+r:a-r)),-1!==t.indexOf("_cw")&&0>a?a=(a+9999999999*r)%r-(0|a/r)*r:-1!==t.indexOf("ccw")&&a>0&&(a=(a-9999999999*r)%r-(0|a/r)*r)),o=e+a),h>o&&o>-h&&(o=0),o},ne={aqua:[0,255,255],lime:[0,255,0],silver:[192,192,192],black:[0,0,0],maroon:[128,0,0],teal:[0,128,128],blue:[0,0,255],navy:[0,0,128],white:[255,255,255],fuchsia:[255,0,255],olive:[128,128,0],yellow:[255,255,0],orange:[255,165,0],gray:[128,128,128],purple:[128,0,128],green:[0,128,0],red:[255,0,0],pink:[255,192,203],cyan:[0,255,255],transparent:[255,255,255,0]},ae=function(t,e,i){return t=0>t?t+1:t>1?t-1:t,0|255*(1>6*t?e+6*(i-e)*t:.5>t?i:2>3*t?e+6*(i-e)*(2/3-t):e)+.5},oe=function(t){var e,i,s,r,n,a;return t&&""!==t?"number"==typeof t?[t>>16,255&t>>8,255&t]:(","===t.charAt(t.length-1)&&(t=t.substr(0,t.length-1)),ne[t]?ne[t]:"#"===t.charAt(0)?(4===t.length&&(e=t.charAt(1),i=t.charAt(2),s=t.charAt(3),t="#"+e+e+i+i+s+s),t=parseInt(t.substr(1),16),[t>>16,255&t>>8,255&t]):"hsl"===t.substr(0,3)?(t=t.match(m),r=Number(t[0])%360/360,n=Number(t[1])/100,a=Number(t[2])/100,i=.5>=a?a*(n+1):a+n-a*n,e=2*a-i,t.length>3&&(t[3]=Number(t[3])),t[0]=ae(r+1/3,e,i),t[1]=ae(r,e,i),t[2]=ae(r-1/3,e,i),t):(t=t.match(m)||ne.transparent,t[0]=Number(t[0]),t[1]=Number(t[1]),t[2]=Number(t[2]),t.length>3&&(t[3]=Number(t[3])),t)):ne.black},he="(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#.+?\\b";for(h in ne)he+="|"+h+"\\b";he=RegExp(he+")","gi");var le=function(t,e,i,s){if(null==t)return function(t){return t};var r,n=e?(t.match(he)||[""])[0]:"",a=t.split(n).join("").match(g)||[],o=t.substr(0,t.indexOf(a[0])),h=")"===t.charAt(t.length-1)?")":"",l=-1!==t.indexOf(" ")?" ":",",_=a.length,u=_>0?a[0].replace(m,""):"";return _?r=e?function(t){var e,p,f,c;if("number"==typeof t)t+=u;else if(s&&D.test(t)){for(c=t.replace(D,"|").split("|"),f=0;c.length>f;f++)c[f]=r(c[f]);return c.join(",")}if(e=(t.match(he)||[n])[0],p=t.split(e).join("").match(g)||[],f=p.length,_>f--)for(;_>++f;)p[f]=i?p[0|(f-1)/2]:a[f];return o+p.join(l)+l+e+h+(-1!==t.indexOf("inset")?" inset":"")}:function(t){var e,n,p;if("number"==typeof t)t+=u;else if(s&&D.test(t)){for(n=t.replace(D,"|").split("|"),p=0;n.length>p;p++)n[p]=r(n[p]);return n.join(",")}if(e=t.match(g)||[],p=e.length,_>p--)for(;_>++p;)e[p]=i?e[0|(p-1)/2]:a[p];return o+e.join(l)+h}:function(t){return t}},_e=function(t){return t=t.split(","),function(e,i,s,r,n,a,o){var h,l=(i+"").split(" ");for(o={},h=0;4>h;h++)o[t[h]]=l[h]=l[h]||l[(h-1)/2>>0];return r.parse(e,o,n,a)}},ue=(X._setPluginRatio=function(t){this.plugin.setRatio(t);for(var e,i,s,r,n=this.data,a=n.proxy,o=n.firstMPT,h=1e-6;o;)e=a[o.v],o.r?e=e>0?0|e+.5:0|e-.5:h>e&&e>-h&&(e=0),o.t[o.p]=e,o=o._next;if(n.autoRotate&&(n.autoRotate.rotation=a.rotation),1===t)for(o=n.firstMPT;o;){if(i=o.t,i.type){if(1===i.type){for(r=i.xs0+i.s+i.xs1,s=1;i.l>s;s++)r+=i["xn"+s]+i["xs"+(s+1)];i.e=r}}else i.e=i.s+i.xs0;o=o._next}},function(t,e,i,s,r){this.t=t,this.p=e,this.v=i,this.r=r,s&&(s._prev=this,this._next=s)}),pe=(X._parseToProxy=function(t,e,i,s,r,n){var a,o,h,l,_,u=s,p={},f={},c=i._transform,m=E;for(i._transform=null,E=e,s=_=i.parse(t,e,s,r),E=m,n&&(i._transform=c,u&&(u._prev=null,u._prev&&(u._prev._next=null)));s&&s!==u;){if(1>=s.type&&(o=s.p,f[o]=s.s+s.c,p[o]=s.s,n||(l=new ue(s,"s",o,l,s.r),s.c=0),1===s.type))for(a=s.l;--a>0;)h="xn"+a,o=s.p+"_"+h,f[o]=s.data[h],p[o]=s[h],n||(l=new ue(s,h,o,l,s.rxp[h]));s=s._next}return{proxy:p,end:f,firstMPT:l,pt:_}},X.CSSPropTween=function(t,e,s,r,a,o,h,l,_,u,p){this.t=t,this.p=e,this.s=s,this.c=r,this.n=h||e,t instanceof pe||n.push(this.n),this.r=l,this.type=o||0,_&&(this.pr=_,i=!0),this.b=void 0===u?s:u,this.e=void 0===p?s+r:p,a&&(this._next=a,a._prev=this)}),fe=a.parseComplex=function(t,e,i,s,r,n,a,o,h,_){i=i||n||"",a=new pe(t,e,0,0,a,_?2:1,null,!1,o,i,s),s+="";var u,p,f,c,g,v,y,T,w,x,P,S,k=i.split(", ").join(",").split(" "),R=s.split(", ").join(",").split(" "),A=k.length,C=l!==!1;for((-1!==s.indexOf(",")||-1!==i.indexOf(","))&&(k=k.join(" ").replace(D,", ").split(" "),R=R.join(" ").replace(D,", ").split(" "),A=k.length),A!==R.length&&(k=(n||"").split(" "),A=k.length),a.plugin=h,a.setRatio=_,u=0;A>u;u++)if(c=k[u],g=R[u],T=parseFloat(c),T||0===T)a.appendXtra("",T,ie(g,T),g.replace(d,""),C&&-1!==g.indexOf("px"),!0);else if(r&&("#"===c.charAt(0)||ne[c]||b.test(c)))S=","===g.charAt(g.length-1)?"),":")",c=oe(c),g=oe(g),w=c.length+g.length>6,w&&!U&&0===g[3]?(a["xs"+a.l]+=a.l?" transparent":"transparent",a.e=a.e.split(R[u]).join("transparent")):(U||(w=!1),a.appendXtra(w?"rgba(":"rgb(",c[0],g[0]-c[0],",",!0,!0).appendXtra("",c[1],g[1]-c[1],",",!0).appendXtra("",c[2],g[2]-c[2],w?",":S,!0),w&&(c=4>c.length?1:c[3],a.appendXtra("",c,(4>g.length?1:g[3])-c,S,!1)));else if(v=c.match(m)){if(y=g.match(d),!y||y.length!==v.length)return a;for(f=0,p=0;v.length>p;p++)P=v[p],x=c.indexOf(P,f),a.appendXtra(c.substr(f,x-f),Number(P),ie(y[p],P),"",C&&"px"===c.substr(x+P.length,2),0===p),f=x+P.length;a["xs"+a.l]+=c.substr(f)}else a["xs"+a.l]+=a.l?" "+c:c;if(-1!==s.indexOf("=")&&a.data){for(S=a.xs0+a.data.s,u=1;a.l>u;u++)S+=a["xs"+u]+a.data["xn"+u];a.e=S+a["xs"+u]}return a.l||(a.type=-1,a.xs0=a.e),a.xfirst||a},ce=9;for(h=pe.prototype,h.l=h.pr=0;--ce>0;)h["xn"+ce]=0,h["xs"+ce]="";h.xs0="",h._next=h._prev=h.xfirst=h.data=h.plugin=h.setRatio=h.rxp=null,h.appendXtra=function(t,e,i,s,r,n){var a=this,o=a.l;return a["xs"+o]+=n&&o?" "+t:t||"",i||0===o||a.plugin?(a.l++,a.type=a.setRatio?2:1,a["xs"+a.l]=s||"",o>0?(a.data["xn"+o]=e+i,a.rxp["xn"+o]=r,a["xn"+o]=e,a.plugin||(a.xfirst=new pe(a,"xn"+o,e,i,a.xfirst||a,0,a.n,r,a.pr),a.xfirst.xs0=0),a):(a.data={s:e+i},a.rxp={},a.s=e,a.c=i,a.r=r,a)):(a["xs"+o]+=e+(s||""),a)};var me=function(t,e){e=e||{},this.p=e.prefix?V(t)||t:t,o[t]=o[this.p]=this,this.format=e.formatter||le(e.defaultValue,e.color,e.collapsible,e.multi),e.parser&&(this.parse=e.parser),this.clrs=e.color,this.multi=e.multi,this.keyword=e.keyword,this.dflt=e.defaultValue,this.pr=e.priority||0},de=X._registerComplexSpecialProp=function(t,e,i){"object"!=typeof e&&(e={parser:i});var s,r,n=t.split(","),a=e.defaultValue;for(i=i||[a],s=0;n.length>s;s++)e.prefix=0===s&&e.prefix,e.defaultValue=i[s]||a,r=new me(n[s],e)},ge=function(t){if(!o[t]){var e=t.charAt(0).toUpperCase()+t.substr(1)+"Plugin";de(t,{parser:function(t,i,s,r,n,a,h){var l=(window.GreenSockGlobals||window).com.greensock.plugins[e];return l?(l._cssRegister(),o[s].parse(t,i,s,r,n,a,h)):(j("Error: "+e+" js file not loaded."),n)}})}};h=me.prototype,h.parseComplex=function(t,e,i,s,r,n){var a,o,h,l,_,u,p=this.keyword;if(this.multi&&(D.test(i)||D.test(e)?(o=e.replace(D,"|").split("|"),h=i.replace(D,"|").split("|")):p&&(o=[e],h=[i])),h){for(l=h.length>o.length?h.length:o.length,a=0;l>a;a++)e=o[a]=o[a]||this.dflt,i=h[a]=h[a]||this.dflt,p&&(_=e.indexOf(p),u=i.indexOf(p),_!==u&&(i=-1===u?h:o,i[a]+=" "+p));e=o.join(", "),i=h.join(", ")}return fe(t,this.p,e,i,this.clrs,this.dflt,s,this.pr,r,n)},h.parse=function(t,e,i,s,n,a){return this.parseComplex(t.style,this.format(G(t,this.p,r,!1,this.dflt)),this.format(e),n,a)},a.registerSpecialProp=function(t,e,i){de(t,{parser:function(t,s,r,n,a,o){var h=new pe(t,r,0,0,a,2,r,!1,i);return h.plugin=o,h.setRatio=e(t,s,n._tween,r),h},priority:i})};var ve="scaleX,scaleY,scaleZ,x,y,z,skewX,rotation,rotationX,rotationY,perspective".split(","),ye=V("transform"),Te=B+"transform",we=V("transformOrigin"),xe=null!==V("perspective"),be=function(t,e,i,s){if(t._gsTransform&&i&&!s)return t._gsTransform;var r,n,o,h,l,_,u,p,f,c,m,d,g,v=i?t._gsTransform||{skewY:0}:{skewY:0},y=0>v.scaleX,T=2e-5,w=1e5,x=179.99,b=x*M,P=xe?parseFloat(G(t,we,e,!1,"0 0 0").split(" ")[2])||v.zOrigin||0:0;for(ye?r=G(t,Te,e,!0):t.currentStyle&&(r=t.currentStyle.filter.match(C),r=r&&4===r.length?[r[0].substr(4),Number(r[2].substr(4)),Number(r[1].substr(4)),r[3].substr(4),v.x||0,v.y||0].join(","):""),n=(r||"").match(/(?:\-|\b)[\d\-\.e]+\b/gi)||[],o=n.length;--o>-1;)h=Number(n[o]),n[o]=(l=h-(h|=0))?(0|l*w+(0>l?-.5:.5))/w+h:h;if(16===n.length){var S=n[8],k=n[9],R=n[10],A=n[12],O=n[13],D=n[14];if(v.zOrigin&&(D=-v.zOrigin,A=S*D-n[12],O=k*D-n[13],D=R*D+v.zOrigin-n[14]),!i||s||null==v.rotationX){var E,F,N,L,X,z,U,Y=n[0],j=n[1],B=n[2],q=n[3],V=n[4],Z=n[5],$=n[6],Q=n[7],W=n[11],H=Math.atan2($,R),K=-b>H||H>b;v.rotationX=H*I,H&&(L=Math.cos(-H),X=Math.sin(-H),E=V*L+S*X,F=Z*L+k*X,N=$*L+R*X,S=V*-X+S*L,k=Z*-X+k*L,R=$*-X+R*L,W=Q*-X+W*L,V=E,Z=F,$=N),H=Math.atan2(S,Y),v.rotationY=H*I,H&&(z=-b>H||H>b,L=Math.cos(-H),X=Math.sin(-H),E=Y*L-S*X,F=j*L-k*X,N=B*L-R*X,k=j*X+k*L,R=B*X+R*L,W=q*X+W*L,Y=E,j=F,B=N),H=Math.atan2(j,Z),v.rotation=H*I,H&&(U=-b>H||H>b,L=Math.cos(-H),X=Math.sin(-H),Y=Y*L+V*X,F=j*L+Z*X,Z=j*-X+Z*L,$=B*-X+$*L,j=F),U&&K?v.rotation=v.rotationX=0:U&&z?v.rotation=v.rotationY=0:z&&K&&(v.rotationY=v.rotationX=0),v.scaleX=(0|Math.sqrt(Y*Y+j*j)*w+.5)/w,v.scaleY=(0|Math.sqrt(Z*Z+k*k)*w+.5)/w,v.scaleZ=(0|Math.sqrt($*$+R*R)*w+.5)/w,v.skewX=0,v.perspective=W?1/(0>W?-W:W):0,v.x=A,v.y=O,v.z=D}}else if(!(xe&&!s&&n.length&&v.x===n[4]&&v.y===n[5]&&(v.rotationX||v.rotationY)||void 0!==v.x&&"none"===G(t,"display",e))){var J=n.length>=6,te=J?n[0]:1,ee=n[1]||0,ie=n[2]||0,se=J?n[3]:1;v.x=n[4]||0,v.y=n[5]||0,_=Math.sqrt(te*te+ee*ee),u=Math.sqrt(se*se+ie*ie),p=te||ee?Math.atan2(ee,te)*I:v.rotation||0,f=ie||se?Math.atan2(ie,se)*I+p:v.skewX||0,c=_-Math.abs(v.scaleX||0),m=u-Math.abs(v.scaleY||0),Math.abs(f)>90&&270>Math.abs(f)&&(y?(_*=-1,f+=0>=p?180:-180,p+=0>=p?180:-180):(u*=-1,f+=0>=f?180:-180)),d=(p-v.rotation)%180,g=(f-v.skewX)%180,(void 0===v.skewX||c>T||-T>c||m>T||-T>m||d>-x&&x>d&&false|d*w||g>-x&&x>g&&false|g*w)&&(v.scaleX=_,v.scaleY=u,v.rotation=p,v.skewX=f),xe&&(v.rotationX=v.rotationY=v.z=0,v.perspective=parseFloat(a.defaultTransformPerspective)||0,v.scaleZ=1)}v.zOrigin=P;for(o in v)T>v[o]&&v[o]>-T&&(v[o]=0);return i&&(t._gsTransform=v),v},Pe=function(t){var e,i,s=this.data,r=-s.rotation*M,n=r+s.skewX*M,a=1e5,o=(0|Math.cos(r)*s.scaleX*a)/a,h=(0|Math.sin(r)*s.scaleX*a)/a,l=(0|Math.sin(n)*-s.scaleY*a)/a,_=(0|Math.cos(n)*s.scaleY*a)/a,u=this.t.style,p=this.t.currentStyle;if(p){i=h,h=-l,l=-i,e=p.filter,u.filter="";var f,m,d=this.t.offsetWidth,g=this.t.offsetHeight,v="absolute"!==p.position,w="progid:DXImageTransform.Microsoft.Matrix(M11="+o+", M12="+h+", M21="+l+", M22="+_,x=s.x,b=s.y;if(null!=s.ox&&(f=(s.oxp?.01*d*s.ox:s.ox)-d/2,m=(s.oyp?.01*g*s.oy:s.oy)-g/2,x+=f-(f*o+m*h),b+=m-(f*l+m*_)),v?(f=d/2,m=g/2,w+=", Dx="+(f-(f*o+m*h)+x)+", Dy="+(m-(f*l+m*_)+b)+")"):w+=", sizingMethod='auto expand')",u.filter=-1!==e.indexOf("DXImageTransform.Microsoft.Matrix(")?e.replace(O,w):w+" "+e,(0===t||1===t)&&1===o&&0===h&&0===l&&1===_&&(v&&-1===w.indexOf("Dx=0, Dy=0")||T.test(e)&&100!==parseFloat(RegExp.$1)||-1===e.indexOf("gradient("&&e.indexOf("Alpha"))&&u.removeAttribute("filter")),!v){var P,S,k,R=8>c?1:-1;for(f=s.ieOffsetX||0,m=s.ieOffsetY||0,s.ieOffsetX=Math.round((d-((0>o?-o:o)*d+(0>h?-h:h)*g))/2+x),s.ieOffsetY=Math.round((g-((0>_?-_:_)*g+(0>l?-l:l)*d))/2+b),ce=0;4>ce;ce++)S=J[ce],P=p[S],i=-1!==P.indexOf("px")?parseFloat(P):$(this.t,S,parseFloat(P),P.replace(y,""))||0,k=i!==s[S]?2>ce?-s.ieOffsetX:-s.ieOffsetY:2>ce?f-s.ieOffsetX:m-s.ieOffsetY,u[S]=(s[S]=Math.round(i-k*(0===ce||2===ce?1:R)))+"px"}}},Se=function(){var t,e,i,s,r,n,a,o,h,l,_,u,f,c,m,d,g,v,y,T,w,x,b,P=this.data,S=this.t.style,k=P.rotation*M,R=P.scaleX,A=P.scaleY,C=P.scaleZ,O=P.perspective;if(p){var D=1e-4;D>R&&R>-D&&(R=C=2e-5),D>A&&A>-D&&(A=C=2e-5),!O||P.z||P.rotationX||P.rotationY||(O=0)}if(k||P.skewX)v=Math.cos(k),y=Math.sin(k),t=v,r=y,P.skewX&&(k-=P.skewX*M,v=Math.cos(k),y=Math.sin(k)),e=-y,n=v;else{if(!(P.rotationY||P.rotationX||1!==C||O))return S[ye]="translate3d("+P.x+"px,"+P.y+"px,"+P.z+"px)"+(1!==R||1!==A?" scale("+R+","+A+")":""),void 0;t=n=1,e=r=0}_=1,i=s=a=o=h=l=u=f=c=0,m=O?-1/O:0,d=P.zOrigin,g=1e5,k=P.rotationY*M,k&&(v=Math.cos(k),y=Math.sin(k),h=_*-y,f=m*-y,i=t*y,a=r*y,_*=v,m*=v,t*=v,r*=v),k=P.rotationX*M,k&&(v=Math.cos(k),y=Math.sin(k),T=e*v+i*y,w=n*v+a*y,x=l*v+_*y,b=c*v+m*y,i=e*-y+i*v,a=n*-y+a*v,_=l*-y+_*v,m=c*-y+m*v,e=T,n=w,l=x,c=b),1!==C&&(i*=C,a*=C,_*=C,m*=C),1!==A&&(e*=A,n*=A,l*=A,c*=A),1!==R&&(t*=R,r*=R,h*=R,f*=R),d&&(u-=d,s=i*u,o=a*u,u=_*u+d),s=(T=(s+=P.x)-(s|=0))?(0|T*g+(0>T?-.5:.5))/g+s:s,o=(T=(o+=P.y)-(o|=0))?(0|T*g+(0>T?-.5:.5))/g+o:o,u=(T=(u+=P.z)-(u|=0))?(0|T*g+(0>T?-.5:.5))/g+u:u,S[ye]="matrix3d("+[(0|t*g)/g,(0|r*g)/g,(0|h*g)/g,(0|f*g)/g,(0|e*g)/g,(0|n*g)/g,(0|l*g)/g,(0|c*g)/g,(0|i*g)/g,(0|a*g)/g,(0|_*g)/g,(0|m*g)/g,s,o,u,O?1+-u/O:1].join(",")+")"},ke=function(){var t,e,i,s,r,n,a,o,h,l=this.data,_=this.t,u=_.style;p&&(t=u.top?"top":u.bottom?"bottom":parseFloat(G(_,"top",null,!1))?"bottom":"top",e=G(_,t,null,!1),i=parseFloat(e)||0,s=e.substr((i+"").length)||"px",l._ffFix=!l._ffFix,u[t]=(l._ffFix?i+.05:i-.05)+s),l.rotation||l.skewX?(r=l.rotation*M,n=r-l.skewX*M,a=1e5,o=l.scaleX*a,h=l.scaleY*a,u[ye]="matrix("+(0|Math.cos(r)*o)/a+","+(0|Math.sin(r)*o)/a+","+(0|Math.sin(n)*-h)/a+","+(0|Math.cos(n)*h)/a+","+l.x+","+l.y+")"):u[ye]="matrix("+l.scaleX+",0,0,"+l.scaleY+","+l.x+","+l.y+")"};de("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,transformPerspective,directionalRotation,parseTransform,force3D",{parser:function(t,e,i,s,n,a,o){if(s._transform)return n;var h,l,_,u,p,f,c,m=s._transform=be(t,r,!0,o.parseTransform),d=t.style,g=1e-6,v=ve.length,y=o,T={};if("string"==typeof y.transform&&ye)_=d.cssText,d[ye]=y.transform,d.display="block",h=be(t,null,!1),d.cssText=_;else if("object"==typeof y){if(h={scaleX:se(null!=y.scaleX?y.scaleX:y.scale,m.scaleX),scaleY:se(null!=y.scaleY?y.scaleY:y.scale,m.scaleY),scaleZ:se(null!=y.scaleZ?y.scaleZ:y.scale,m.scaleZ),x:se(y.x,m.x),y:se(y.y,m.y),z:se(y.z,m.z),perspective:se(y.transformPerspective,m.perspective)},c=y.directionalRotation,null!=c)if("object"==typeof c)for(_ in c)y[_]=c[_];else y.rotation=c;h.rotation=re("rotation"in y?y.rotation:"shortRotation"in y?y.shortRotation+"_short":"rotationZ"in y?y.rotationZ:m.rotation,m.rotation,"rotation",T),xe&&(h.rotationX=re("rotationX"in y?y.rotationX:"shortRotationX"in y?y.shortRotationX+"_short":m.rotationX||0,m.rotationX,"rotationX",T),h.rotationY=re("rotationY"in y?y.rotationY:"shortRotationY"in y?y.shortRotationY+"_short":m.rotationY||0,m.rotationY,"rotationY",T)),h.skewX=null==y.skewX?m.skewX:re(y.skewX,m.skewX),h.skewY=null==y.skewY?m.skewY:re(y.skewY,m.skewY),(l=h.skewY-m.skewY)&&(h.skewX+=l,h.rotation+=l)}for(null!=y.force3D&&(m.force3D=y.force3D,f=!0),p=m.force3D||m.z||m.rotationX||m.rotationY||h.z||h.rotationX||h.rotationY||h.perspective,p||null==y.scale||(h.scaleZ=1);--v>-1;)i=ve[v],u=h[i]-m[i],(u>g||-g>u||null!=E[i])&&(f=!0,n=new pe(m,i,m[i],u,n),i in T&&(n.e=T[i]),n.xs0=0,n.plugin=a,s._overwriteProps.push(n.n));return u=y.transformOrigin,(u||xe&&p&&m.zOrigin)&&(ye?(f=!0,i=we,u=(u||G(t,i,r,!1,"50% 50%"))+"",n=new pe(d,i,0,0,n,-1,"transformOrigin"),n.b=d[i],n.plugin=a,xe?(_=m.zOrigin,u=u.split(" "),m.zOrigin=(u.length>2&&(0===_||"0px"!==u[2])?parseFloat(u[2]):_)||0,n.xs0=n.e=d[i]=u[0]+" "+(u[1]||"50%")+" 0px",n=new pe(m,"zOrigin",0,0,n,-1,n.n),n.b=_,n.xs0=n.e=m.zOrigin):n.xs0=n.e=d[i]=u):ee(u+"",m)),f&&(s._transformType=p||3===this._transformType?3:2),n},prefix:!0}),de("boxShadow",{defaultValue:"0px 0px 0px 0px #999",prefix:!0,color:!0,multi:!0,keyword:"inset"}),de("borderRadius",{defaultValue:"0px",parser:function(t,e,i,n,a){e=this.format(e);var o,h,l,_,u,p,f,c,m,d,g,v,y,T,w,x,b=["borderTopLeftRadius","borderTopRightRadius","borderBottomRightRadius","borderBottomLeftRadius"],P=t.style;for(m=parseFloat(t.offsetWidth),d=parseFloat(t.offsetHeight),o=e.split(" "),h=0;b.length>h;h++)this.p.indexOf("border")&&(b[h]=V(b[h])),u=_=G(t,b[h],r,!1,"0px"),-1!==u.indexOf(" ")&&(_=u.split(" "),u=_[0],_=_[1]),p=l=o[h],f=parseFloat(u),v=u.substr((f+"").length),y="="===p.charAt(1),y?(c=parseInt(p.charAt(0)+"1",10),p=p.substr(2),c*=parseFloat(p),g=p.substr((c+"").length-(0>c?1:0))||""):(c=parseFloat(p),g=p.substr((c+"").length)),""===g&&(g=s[i]||v),g!==v&&(T=$(t,"borderLeft",f,v),w=$(t,"borderTop",f,v),"%"===g?(u=100*(T/m)+"%",_=100*(w/d)+"%"):"em"===g?(x=$(t,"borderLeft",1,"em"),u=T/x+"em",_=w/x+"em"):(u=T+"px",_=w+"px"),y&&(p=parseFloat(u)+c+g,l=parseFloat(_)+c+g)),a=fe(P,b[h],u+" "+_,p+" "+l,!1,"0px",a);return a},prefix:!0,formatter:le("0px 0px 0px 0px",!1,!0)}),de("backgroundPosition",{defaultValue:"0 0",parser:function(t,e,i,s,n,a){var o,h,l,_,u,p,f="background-position",m=r||Z(t,null),d=this.format((m?c?m.getPropertyValue(f+"-x")+" "+m.getPropertyValue(f+"-y"):m.getPropertyValue(f):t.currentStyle.backgroundPositionX+" "+t.currentStyle.backgroundPositionY)||"0 0"),g=this.format(e);if(-1!==d.indexOf("%")!=(-1!==g.indexOf("%"))&&(p=G(t,"backgroundImage").replace(k,""),p&&"none"!==p)){for(o=d.split(" "),h=g.split(" "),L.setAttribute("src",p),l=2;--l>-1;)d=o[l],_=-1!==d.indexOf("%"),_!==(-1!==h[l].indexOf("%"))&&(u=0===l?t.offsetWidth-L.width:t.offsetHeight-L.height,o[l]=_?parseFloat(d)/100*u+"px":100*(parseFloat(d)/u)+"%");d=o.join(" ")}return this.parseComplex(t.style,d,g,n,a)},formatter:ee}),de("backgroundSize",{defaultValue:"0 0",formatter:ee}),de("perspective",{defaultValue:"0px",prefix:!0}),de("perspectiveOrigin",{defaultValue:"50% 50%",prefix:!0}),de("transformStyle",{prefix:!0}),de("backfaceVisibility",{prefix:!0}),de("userSelect",{prefix:!0}),de("margin",{parser:_e("marginTop,marginRight,marginBottom,marginLeft")}),de("padding",{parser:_e("paddingTop,paddingRight,paddingBottom,paddingLeft")}),de("clip",{defaultValue:"rect(0px,0px,0px,0px)",parser:function(t,e,i,s,n,a){var o,h,l;return 9>c?(h=t.currentStyle,l=8>c?" ":",",o="rect("+h.clipTop+l+h.clipRight+l+h.clipBottom+l+h.clipLeft+")",e=this.format(e).split(",").join(l)):(o=this.format(G(t,this.p,r,!1,this.dflt)),e=this.format(e)),this.parseComplex(t.style,o,e,n,a)}}),de("textShadow",{defaultValue:"0px 0px 0px #999",color:!0,multi:!0}),de("autoRound,strictUnits",{parser:function(t,e,i,s,r){return r}}),de("border",{defaultValue:"0px solid #000",parser:function(t,e,i,s,n,a){return this.parseComplex(t.style,this.format(G(t,"borderTopWidth",r,!1,"0px")+" "+G(t,"borderTopStyle",r,!1,"solid")+" "+G(t,"borderTopColor",r,!1,"#000")),this.format(e),n,a)},color:!0,formatter:function(t){var e=t.split(" ");return e[0]+" "+(e[1]||"solid")+" "+(t.match(he)||["#000"])[0]}}),de("float,cssFloat,styleFloat",{parser:function(t,e,i,s,r){var n=t.style,a="cssFloat"in n?"cssFloat":"styleFloat";return new pe(n,a,0,0,r,-1,i,!1,0,n[a],e)}});var Re=function(t){var e,i=this.t,s=i.filter||G(this.data,"filter"),r=0|this.s+this.c*t;100===r&&(-1===s.indexOf("atrix(")&&-1===s.indexOf("radient(")&&-1===s.indexOf("oader(")?(i.removeAttribute("filter"),e=!G(this.data,"filter")):(i.filter=s.replace(x,""),e=!0)),e||(this.xn1&&(i.filter=s=s||"alpha(opacity="+r+")"),-1===s.indexOf("opacity")?0===r&&this.xn1||(i.filter=s+" alpha(opacity="+r+")"):i.filter=s.replace(T,"opacity="+r))};de("opacity,alpha,autoAlpha",{defaultValue:"1",parser:function(t,e,i,s,n,a){var o=parseFloat(G(t,"opacity",r,!1,"1")),h=t.style,l="autoAlpha"===i;return"string"==typeof e&&"="===e.charAt(1)&&(e=("-"===e.charAt(0)?-1:1)*parseFloat(e.substr(2))+o),l&&1===o&&"hidden"===G(t,"visibility",r)&&0!==e&&(o=0),U?n=new pe(h,"opacity",o,e-o,n):(n=new pe(h,"opacity",100*o,100*(e-o),n),n.xn1=l?1:0,h.zoom=1,n.type=2,n.b="alpha(opacity="+n.s+")",n.e="alpha(opacity="+(n.s+n.c)+")",n.data=t,n.plugin=a,n.setRatio=Re),l&&(n=new pe(h,"visibility",0,0,n,-1,null,!1,0,0!==o?"inherit":"hidden",0===e?"hidden":"inherit"),n.xs0="inherit",s._overwriteProps.push(n.n),s._overwriteProps.push(i)),n}});var Ae=function(t,e){e&&(t.removeProperty?t.removeProperty(e.replace(P,"-$1").toLowerCase()):t.removeAttribute(e))},Ce=function(t){if(this.t._gsClassPT=this,1===t||0===t){this.t.className=0===t?this.b:this.e;for(var e=this.data,i=this.t.style;e;)e.v?i[e.p]=e.v:Ae(i,e.p),e=e._next;1===t&&this.t._gsClassPT===this&&(this.t._gsClassPT=null)}else this.t.className!==this.e&&(this.t.className=this.e)};de("className",{parser:function(t,e,s,n,a,o,h){var l,_,u,p,f,c=t.className,m=t.style.cssText;if(a=n._classNamePT=new pe(t,s,0,0,a,2),a.setRatio=Ce,a.pr=-11,i=!0,a.b=c,_=W(t,r),u=t._gsClassPT){for(p={},f=u.data;f;)p[f.p]=1,f=f._next;u.setRatio(1)}return t._gsClassPT=a,a.e="="!==e.charAt(1)?e:c.replace(RegExp("\\s*\\b"+e.substr(2)+"\\b"),"")+("+"===e.charAt(0)?" "+e.substr(2):""),n._tween._duration&&(t.className=a.e,l=H(t,_,W(t),h,p),t.className=c,a.data=l.firstMPT,t.style.cssText=m,a=a.xfirst=n.parse(t,l.difs,a,o)),a}});var Oe=function(t){if((1===t||0===t)&&this.data._totalTime===this.data._totalDuration&&"isFromStart"!==this.data.data){var e,i,s,r,n=this.t.style,a=o.transform.parse;if("all"===this.e)n.cssText="",r=!0;else for(e=this.e.split(","),s=e.length;--s>-1;)i=e[s],o[i]&&(o[i].parse===a?r=!0:i="transformOrigin"===i?we:o[i].p),Ae(n,i);r&&(Ae(n,ye),this.t._gsTransform&&delete this.t._gsTransform)}};for(de("clearProps",{parser:function(t,e,s,r,n){return n=new pe(t,s,0,0,n,2),n.setRatio=Oe,n.e=e,n.pr=-10,n.data=r._tween,i=!0,n}}),h="bezier,throwProps,physicsProps,physics2D".split(","),ce=h.length;ce--;)ge(h[ce]);h=a.prototype,h._firstPT=null,h._onInitTween=function(t,e,o){if(!t.nodeType)return!1;this._target=t,this._tween=o,this._vars=e,l=e.autoRound,i=!1,s=e.suffixMap||a.suffixMap,r=Z(t,""),n=this._overwriteProps;var h,p,c,m,d,g,v,y,T,x=t.style;if(_&&""===x.zIndex&&(h=G(t,"zIndex",r),("auto"===h||""===h)&&(x.zIndex=0)),"string"==typeof e&&(m=x.cssText,h=W(t,r),x.cssText=m+";"+e,h=H(t,h,W(t)).difs,!U&&w.test(e)&&(h.opacity=parseFloat(RegExp.$1)),e=h,x.cssText=m),this._firstPT=p=this.parse(t,e,null),this._transformType){for(T=3===this._transformType,ye?u&&(_=!0,""===x.zIndex&&(v=G(t,"zIndex",r),("auto"===v||""===v)&&(x.zIndex=0)),f&&(x.WebkitBackfaceVisibility=this._vars.WebkitBackfaceVisibility||(T?"visible":"hidden"))):x.zoom=1,c=p;c&&c._next;)c=c._next;y=new pe(t,"transform",0,0,null,2),this._linkCSSP(y,null,c),y.setRatio=T&&xe?Se:ye?ke:Pe,y.data=this._transform||be(t,r,!0),n.pop()}if(i){for(;p;){for(g=p._next,c=m;c&&c.pr>p.pr;)c=c._next;(p._prev=c?c._prev:d)?p._prev._next=p:m=p,(p._next=c)?c._prev=p:d=p,p=g}this._firstPT=m}return!0},h.parse=function(t,e,i,n){var a,h,_,u,p,f,c,m,d,g,v=t.style;for(a in e)f=e[a],h=o[a],h?i=h.parse(t,f,a,this,i,n,e):(p=G(t,a,r)+"",d="string"==typeof f,"color"===a||"fill"===a||"stroke"===a||-1!==a.indexOf("Color")||d&&b.test(f)?(d||(f=oe(f),f=(f.length>3?"rgba(":"rgb(")+f.join(",")+")"),i=fe(v,a,p,f,!0,"transparent",i,0,n)):!d||-1===f.indexOf(" ")&&-1===f.indexOf(",")?(_=parseFloat(p),c=_||0===_?p.substr((_+"").length):"",(""===p||"auto"===p)&&("width"===a||"height"===a?(_=te(t,a,r),c="px"):"left"===a||"top"===a?(_=Q(t,a,r),c="px"):(_="opacity"!==a?0:1,c="")),g=d&&"="===f.charAt(1),g?(u=parseInt(f.charAt(0)+"1",10),f=f.substr(2),u*=parseFloat(f),m=f.replace(y,"")):(u=parseFloat(f),m=d?f.substr((u+"").length)||"":""),""===m&&(m=s[a]||c),f=u||0===u?(g?u+_:u)+m:e[a],c!==m&&""!==m&&(u||0===u)&&(_||0===_)&&(_=$(t,a,_,c),"%"===m?(_/=$(t,a,100,"%")/100,_>100&&(_=100),e.strictUnits!==!0&&(p=_+"%")):"em"===m?_/=$(t,a,1,"em"):(u=$(t,a,u,m),m="px"),g&&(u||0===u)&&(f=u+_+m)),g&&(u+=_),!_&&0!==_||!u&&0!==u?void 0!==v[a]&&(f||"NaN"!=f+""&&null!=f)?(i=new pe(v,a,u||_||0,0,i,-1,a,!1,0,p,f),i.xs0="none"!==f||"display"!==a&&-1===a.indexOf("Style")?f:p):j("invalid "+a+" tween value: "+e[a]):(i=new pe(v,a,_,u-_,i,0,a,l!==!1&&("px"===m||"zIndex"===a),0,p,f),i.xs0=m)):i=fe(v,a,p,f,!0,null,i,0,n)),n&&i&&!i.plugin&&(i.plugin=n);return i},h.setRatio=function(t){var e,i,s,r=this._firstPT,n=1e-6;if(1!==t||this._tween._time!==this._tween._duration&&0!==this._tween._time)if(t||this._tween._time!==this._tween._duration&&0!==this._tween._time||this._tween._rawPrevTime===-1e-6)for(;r;){if(e=r.c*t+r.s,r.r?e=e>0?0|e+.5:0|e-.5:n>e&&e>-n&&(e=0),r.type)if(1===r.type)if(s=r.l,2===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2;else if(3===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3;else if(4===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3+r.xn3+r.xs4;else if(5===s)r.t[r.p]=r.xs0+e+r.xs1+r.xn1+r.xs2+r.xn2+r.xs3+r.xn3+r.xs4+r.xn4+r.xs5;else{for(i=r.xs0+e+r.xs1,s=1;r.l>s;s++)i+=r["xn"+s]+r["xs"+(s+1)];r.t[r.p]=i}else-1===r.type?r.t[r.p]=r.xs0:r.setRatio&&r.setRatio(t);else r.t[r.p]=e+r.xs0;r=r._next}else for(;r;)2!==r.type?r.t[r.p]=r.b:r.setRatio(t),r=r._next;else for(;r;)2!==r.type?r.t[r.p]=r.e:r.setRatio(t),r=r._next},h._enableTransforms=function(t){this._transformType=t||3===this._transformType?3:2,this._transform=this._transform||be(this._target,r,!0)},h._linkCSSP=function(t,e,i,s){return t&&(e&&(e._prev=t),t._next&&(t._next._prev=t._prev),t._prev?t._prev._next=t._next:this._firstPT===t&&(this._firstPT=t._next,s=!0),i?i._next=t:s||null!==this._firstPT||(this._firstPT=t),t._next=e,t._prev=i),t},h._kill=function(e){var i,s,r,n=e;if(e.autoAlpha||e.alpha){n={};for(s in e)n[s]=e[s];n.opacity=1,n.autoAlpha&&(n.visibility=1)}return e.className&&(i=this._classNamePT)&&(r=i.xfirst,r&&r._prev?this._linkCSSP(r._prev,i._next,r._prev._prev):r===this._firstPT&&(this._firstPT=i._next),i._next&&this._linkCSSP(i._next,i._next._next,r._prev),this._classNamePT=null),t.prototype._kill.call(this,n)};var De=function(t,e,i){var s,r,n,a;if(t.slice)for(r=t.length;--r>-1;)De(t[r],e,i);else for(s=t.childNodes,r=s.length;--r>-1;)n=s[r],a=n.type,n.style&&(e.push(W(n)),i&&i.push(n)),1!==a&&9!==a&&11!==a||!n.childNodes.length||De(n,e,i)};return a.cascadeTo=function(t,i,s){var r,n,a,o=e.to(t,i,s),h=[o],l=[],_=[],u=[],p=e._internals.reservedProps;for(t=o._targets||o.target,De(t,l,u),o.render(i,!0),De(t,_),o.render(0,!0),o._enabled(!0),r=u.length;--r>-1;)if(n=H(u[r],l[r],_[r]),n.firstMPT){n=n.difs;for(a in s)p[a]&&(n[a]=s[a]);h.push(e.to(u[r],i,n))}return h},t.activate([a]),a},!0),function(){var t=window._gsDefine.plugin({propName:"roundProps",priority:-1,API:2,init:function(t,e,i){return this._tween=i,!0}}),e=t.prototype;e._onInitAllProps=function(){for(var t,e,i,s=this._tween,r=s.vars.roundProps instanceof Array?s.vars.roundProps:s.vars.roundProps.split(","),n=r.length,a={},o=s._propLookup.roundProps;--n>-1;)a[r[n]]=1;for(n=r.length;--n>-1;)for(t=r[n],e=s._firstPT;e;)i=e._next,e.pg?e.t._roundProps(a,!0):e.n===t&&(this._add(e.t,t,e.s,e.c),i&&(i._prev=e._prev),e._prev?e._prev._next=i:s._firstPT===e&&(s._firstPT=i),e._next=e._prev=null,s._propLookup[t]=o),e=i;return!1},e._add=function(t,e,i,s){this._addTween(t,e,i,i+s,e,!0),this._overwriteProps.push(e)}}(),window._gsDefine.plugin({propName:"attr",API:2,init:function(t,e){var i;if("function"!=typeof t.setAttribute)return!1;this._target=t,this._proxy={};for(i in e)this._addTween(this._proxy,i,parseFloat(t.getAttribute(i)),e[i],i)&&this._overwriteProps.push(i);return!0},set:function(t){this._super.setRatio.call(this,t);for(var e,i=this._overwriteProps,s=i.length;--s>-1;)e=i[s],this._target.setAttribute(e,this._proxy[e]+"")}}),window._gsDefine.plugin({propName:"directionalRotation",API:2,init:function(t,e){"object"!=typeof e&&(e={rotation:e}),this.finals={};var i,s,r,n,a,o,h=e.useRadians===!0?2*Math.PI:360,l=1e-6;for(i in e)"useRadians"!==i&&(o=(e[i]+"").split("_"),s=o[0],r=parseFloat("function"!=typeof t[i]?t[i]:t[i.indexOf("set")||"function"!=typeof t["get"+i.substr(3)]?i:"get"+i.substr(3)]()),n=this.finals[i]="string"==typeof s&&"="===s.charAt(1)?r+parseInt(s.charAt(0)+"1",10)*Number(s.substr(2)):Number(s)||0,a=n-r,o.length&&(s=o.join("_"),-1!==s.indexOf("short")&&(a%=h,a!==a%(h/2)&&(a=0>a?a+h:a-h)),-1!==s.indexOf("_cw")&&0>a?a=(a+9999999999*h)%h-(0|a/h)*h:-1!==s.indexOf("ccw")&&a>0&&(a=(a-9999999999*h)%h-(0|a/h)*h)),(a>l||-l>a)&&(this._addTween(t,i,r,r+a,i),this._overwriteProps.push(i)));return!0},set:function(t){var e;if(1!==t)this._super.setRatio.call(this,t);else for(e=this._firstPT;e;)e.f?e.t[e.p](this.finals[e.p]):e.t[e.p]=this.finals[e.p],e=e._next}})._autoCSS=!0,window._gsDefine("easing.Back",["easing.Ease"],function(t){var e,i,s,r=window.GreenSockGlobals||window,n=r.com.greensock,a=2*Math.PI,o=Math.PI/2,h=n._class,l=function(e,i){var s=h("easing."+e,function(){},!0),r=s.prototype=new t;return r.constructor=s,r.getRatio=i,s},_=t.register||function(){},u=function(t,e,i,s){var r=h("easing."+t,{easeOut:new e,easeIn:new i,easeInOut:new s},!0);return _(r,t),r},p=function(t,e,i){this.t=t,this.v=e,i&&(this.next=i,i.prev=this,this.c=i.v-e,this.gap=i.t-t)},f=function(e,i){var s=h("easing."+e,function(t){this._p1=t||0===t?t:1.70158,this._p2=1.525*this._p1},!0),r=s.prototype=new t;return r.constructor=s,r.getRatio=i,r.config=function(t){return new s(t)},s},c=u("Back",f("BackOut",function(t){return(t-=1)*t*((this._p1+1)*t+this._p1)+1}),f("BackIn",function(t){return t*t*((this._p1+1)*t-this._p1)}),f("BackInOut",function(t){return 1>(t*=2)?.5*t*t*((this._p2+1)*t-this._p2):.5*((t-=2)*t*((this._p2+1)*t+this._p2)+2)})),m=h("easing.SlowMo",function(t,e,i){e=e||0===e?e:.7,null==t?t=.7:t>1&&(t=1),this._p=1!==t?e:0,this._p1=(1-t)/2,this._p2=t,this._p3=this._p1+this._p2,this._calcEnd=i===!0},!0),d=m.prototype=new t;return d.constructor=m,d.getRatio=function(t){var e=t+(.5-t)*this._p;return this._p1>t?this._calcEnd?1-(t=1-t/this._p1)*t:e-(t=1-t/this._p1)*t*t*t*e:t>this._p3?this._calcEnd?1-(t=(t-this._p3)/this._p1)*t:e+(t-e)*(t=(t-this._p3)/this._p1)*t*t*t:this._calcEnd?1:e},m.ease=new m(.7,.7),d.config=m.config=function(t,e,i){return new m(t,e,i)},e=h("easing.SteppedEase",function(t){t=t||1,this._p1=1/t,this._p2=t+1},!0),d=e.prototype=new t,d.constructor=e,d.getRatio=function(t){return 0>t?t=0:t>=1&&(t=.999999999),(this._p2*t>>0)*this._p1},d.config=e.config=function(t){return new e(t)},i=h("easing.RoughEase",function(e){e=e||{};for(var i,s,r,n,a,o,h=e.taper||"none",l=[],_=0,u=0|(e.points||20),f=u,c=e.randomize!==!1,m=e.clamp===!0,d=e.template instanceof t?e.template:null,g="number"==typeof e.strength?.4*e.strength:.4;--f>-1;)i=c?Math.random():1/u*f,s=d?d.getRatio(i):i,"none"===h?r=g:"out"===h?(n=1-i,r=n*n*g):"in"===h?r=i*i*g:.5>i?(n=2*i,r=.5*n*n*g):(n=2*(1-i),r=.5*n*n*g),c?s+=Math.random()*r-.5*r:f%2?s+=.5*r:s-=.5*r,m&&(s>1?s=1:0>s&&(s=0)),l[_++]={x:i,y:s};for(l.sort(function(t,e){return t.x-e.x}),o=new p(1,1,null),f=u;--f>-1;)a=l[f],o=new p(a.x,a.y,o);this._prev=new p(0,0,0!==o.t?o:o.next)},!0),d=i.prototype=new t,d.constructor=i,d.getRatio=function(t){var e=this._prev;if(t>e.t){for(;e.next&&t>=e.t;)e=e.next;e=e.prev}else for(;e.prev&&e.t>=t;)e=e.prev;return this._prev=e,e.v+(t-e.t)/e.gap*e.c},d.config=function(t){return new i(t)},i.ease=new i,u("Bounce",l("BounceOut",function(t){return 1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375}),l("BounceIn",function(t){return 1/2.75>(t=1-t)?1-7.5625*t*t:2/2.75>t?1-(7.5625*(t-=1.5/2.75)*t+.75):2.5/2.75>t?1-(7.5625*(t-=2.25/2.75)*t+.9375):1-(7.5625*(t-=2.625/2.75)*t+.984375)}),l("BounceInOut",function(t){var e=.5>t;return t=e?1-2*t:2*t-1,t=1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375,e?.5*(1-t):.5*t+.5})),u("Circ",l("CircOut",function(t){return Math.sqrt(1-(t-=1)*t)}),l("CircIn",function(t){return-(Math.sqrt(1-t*t)-1)}),l("CircInOut",function(t){return 1>(t*=2)?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)})),s=function(e,i,s){var r=h("easing."+e,function(t,e){this._p1=t||1,this._p2=e||s,this._p3=this._p2/a*(Math.asin(1/this._p1)||0)},!0),n=r.prototype=new t;return n.constructor=r,n.getRatio=i,n.config=function(t,e){return new r(t,e)},r},u("Elastic",s("ElasticOut",function(t){return this._p1*Math.pow(2,-10*t)*Math.sin((t-this._p3)*a/this._p2)+1},.3),s("ElasticIn",function(t){return-(this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*a/this._p2))},.3),s("ElasticInOut",function(t){return 1>(t*=2)?-.5*this._p1*Math.pow(2,10*(t-=1))*Math.sin((t-this._p3)*a/this._p2):.5*this._p1*Math.pow(2,-10*(t-=1))*Math.sin((t-this._p3)*a/this._p2)+1},.45)),u("Expo",l("ExpoOut",function(t){return 1-Math.pow(2,-10*t)}),l("ExpoIn",function(t){return Math.pow(2,10*(t-1))-.001}),l("ExpoInOut",function(t){return 1>(t*=2)?.5*Math.pow(2,10*(t-1)):.5*(2-Math.pow(2,-10*(t-1)))})),u("Sine",l("SineOut",function(t){return Math.sin(t*o)}),l("SineIn",function(t){return-Math.cos(t*o)+1}),l("SineInOut",function(t){return-.5*(Math.cos(Math.PI*t)-1)})),h("easing.EaseLookup",{find:function(e){return t.map[e]}},!0),_(r.SlowMo,"SlowMo","ease,"),_(i,"RoughEase","ease,"),_(e,"SteppedEase","ease,"),c},!0)}),function(t){"use strict";var e=t.GreenSockGlobals||t;if(!e.TweenLite){var i,s,r,n,a,o=function(t){var i,s=t.split("."),r=e;for(i=0;s.length>i;i++)r[s[i]]=r=r[s[i]]||{};return r},h=o("com.greensock"),l=1e-10,_=[].slice,u=function(){},p=function(){var t=Object.prototype.toString,e=t.call([]);return function(i){return i instanceof Array||"object"==typeof i&&!!i.push&&t.call(i)===e}}(),f={},c=function(i,s,r,n){this.sc=f[i]?f[i].sc:[],f[i]=this,this.gsClass=null,this.func=r;var a=[];this.check=function(h){for(var l,_,u,p,m=s.length,d=m;--m>-1;)(l=f[s[m]]||new c(s[m],[])).gsClass?(a[m]=l.gsClass,d--):h&&l.sc.push(this);if(0===d&&r)for(_=("com.greensock."+i).split("."),u=_.pop(),p=o(_.join("."))[u]=this.gsClass=r.apply(r,a),n&&(e[u]=p,"function"==typeof define&&define.amd?define((t.GreenSockAMDPath?t.GreenSockAMDPath+"/":"")+i.split(".").join("/"),[],function(){return p}):"undefined"!=typeof module&&module.exports&&(module.exports=p)),m=0;this.sc.length>m;m++)this.sc[m].check()},this.check(!0)},m=t._gsDefine=function(t,e,i,s){return new c(t,e,i,s)},d=h._class=function(t,e,i){return e=e||function(){},m(t,[],function(){return e},i),e};m.globals=e;var g=[0,0,1,1],v=[],y=d("easing.Ease",function(t,e,i,s){this._func=t,this._type=i||0,this._power=s||0,this._params=e?g.concat(e):g},!0),T=y.map={},w=y.register=function(t,e,i,s){for(var r,n,a,o,l=e.split(","),_=l.length,u=(i||"easeIn,easeOut,easeInOut").split(",");--_>-1;)for(n=l[_],r=s?d("easing."+n,null,!0):h.easing[n]||{},a=u.length;--a>-1;)o=u[a],T[n+"."+o]=T[o+n]=r[o]=t.getRatio?t:t[o]||new t};for(r=y.prototype,r._calcEnd=!1,r.getRatio=function(t){if(this._func)return this._params[0]=t,this._func.apply(null,this._params);var e=this._type,i=this._power,s=1===e?1-t:2===e?t:.5>t?2*t:2*(1-t);return 1===i?s*=s:2===i?s*=s*s:3===i?s*=s*s*s:4===i&&(s*=s*s*s*s),1===e?1-s:2===e?s:.5>t?s/2:1-s/2},i=["Linear","Quad","Cubic","Quart","Quint,Strong"],s=i.length;--s>-1;)r=i[s]+",Power"+s,w(new y(null,null,1,s),r,"easeOut",!0),w(new y(null,null,2,s),r,"easeIn"+(0===s?",easeNone":"")),w(new y(null,null,3,s),r,"easeInOut");T.linear=h.easing.Linear.easeIn,T.swing=h.easing.Quad.easeInOut;var x=d("events.EventDispatcher",function(t){this._listeners={},this._eventTarget=t||this});r=x.prototype,r.addEventListener=function(t,e,i,s,r){r=r||0;var o,h,l=this._listeners[t],_=0;for(null==l&&(this._listeners[t]=l=[]),h=l.length;--h>-1;)o=l[h],o.c===e&&o.s===i?l.splice(h,1):0===_&&r>o.pr&&(_=h+1);l.splice(_,0,{c:e,s:i,up:s,pr:r}),this!==n||a||n.wake()},r.removeEventListener=function(t,e){var i,s=this._listeners[t];if(s)for(i=s.length;--i>-1;)if(s[i].c===e)return s.splice(i,1),void 0},r.dispatchEvent=function(t){var e,i,s,r=this._listeners[t];if(r)for(e=r.length,i=this._eventTarget;--e>-1;)s=r[e],s.up?s.c.call(s.s||i,{type:t,target:i}):s.c.call(s.s||i)};var b=t.requestAnimationFrame,P=t.cancelAnimationFrame,S=Date.now||function(){return(new Date).getTime()},k=S();for(i=["ms","moz","webkit","o"],s=i.length;--s>-1&&!b;)b=t[i[s]+"RequestAnimationFrame"],P=t[i[s]+"CancelAnimationFrame"]||t[i[s]+"CancelRequestAnimationFrame"];d("Ticker",function(t,e){var i,s,r,o,h,l=this,_=S(),p=e!==!1&&b,f=function(t){k=S(),l.time=(k-_)/1e3;var e,n=l.time-h;(!i||n>0||t===!0)&&(l.frame++,h+=n+(n>=o?.004:o-n),e=!0),t!==!0&&(r=s(f)),e&&l.dispatchEvent("tick")};x.call(l),l.time=l.frame=0,l.tick=function(){f(!0)},l.sleep=function(){null!=r&&(p&&P?P(r):clearTimeout(r),s=u,r=null,l===n&&(a=!1))},l.wake=function(){null!==r&&l.sleep(),s=0===i?u:p&&b?b:function(t){return setTimeout(t,0|1e3*(h-l.time)+1)},l===n&&(a=!0),f(2)},l.fps=function(t){return arguments.length?(i=t,o=1/(i||60),h=this.time+o,l.wake(),void 0):i},l.useRAF=function(t){return arguments.length?(l.sleep(),p=t,l.fps(i),void 0):p},l.fps(t),setTimeout(function(){p&&(!r||5>l.frame)&&l.useRAF(!1)},1500)}),r=h.Ticker.prototype=new h.events.EventDispatcher,r.constructor=h.Ticker;var R=d("core.Animation",function(t,e){if(this.vars=e=e||{},this._duration=this._totalDuration=t||0,this._delay=Number(e.delay)||0,this._timeScale=1,this._active=e.immediateRender===!0,this.data=e.data,this._reversed=e.reversed===!0,U){a||n.wake();var i=this.vars.useFrames?z:U;i.add(this,i._time),this.vars.paused&&this.paused(!0)}});n=R.ticker=new h.Ticker,r=R.prototype,r._dirty=r._gc=r._initted=r._paused=!1,r._totalTime=r._time=0,r._rawPrevTime=-1,r._next=r._last=r._onUpdate=r._timeline=r.timeline=null,r._paused=!1;var A=function(){a&&S()-k>2e3&&n.wake(),setTimeout(A,2e3)};A(),r.play=function(t,e){return arguments.length&&this.seek(t,e),this.reversed(!1).paused(!1)},r.pause=function(t,e){return arguments.length&&this.seek(t,e),this.paused(!0)},r.resume=function(t,e){return arguments.length&&this.seek(t,e),this.paused(!1)},r.seek=function(t,e){return this.totalTime(Number(t),e!==!1)},r.restart=function(t,e){return this.reversed(!1).paused(!1).totalTime(t?-this._delay:0,e!==!1,!0)},r.reverse=function(t,e){return arguments.length&&this.seek(t||this.totalDuration(),e),this.reversed(!0).paused(!1)},r.render=function(){},r.invalidate=function(){return this},r.isActive=function(){var t,e=this._timeline,i=this._startTime;return!e||!this._gc&&!this._paused&&e.isActive()&&(t=e.rawTime())>=i&&i+this.totalDuration()/this._timeScale>t},r._enabled=function(t,e){return a||n.wake(),this._gc=!t,this._active=this.isActive(),e!==!0&&(t&&!this.timeline?this._timeline.add(this,this._startTime-this._delay):!t&&this.timeline&&this._timeline._remove(this,!0)),!1},r._kill=function(){return this._enabled(!1,!1)},r.kill=function(t,e){return this._kill(t,e),this},r._uncache=function(t){for(var e=t?this:this.timeline;e;)e._dirty=!0,e=e.timeline;return this},r._swapSelfInParams=function(t){for(var e=t.length,i=t.concat();--e>-1;)"{self}"===t[e]&&(i[e]=this);return i},r.eventCallback=function(t,e,i,s){if("on"===(t||"").substr(0,2)){var r=this.vars;if(1===arguments.length)return r[t];null==e?delete r[t]:(r[t]=e,r[t+"Params"]=p(i)&&-1!==i.join("").indexOf("{self}")?this._swapSelfInParams(i):i,r[t+"Scope"]=s),"onUpdate"===t&&(this._onUpdate=e)}return this},r.delay=function(t){return arguments.length?(this._timeline.smoothChildTiming&&this.startTime(this._startTime+t-this._delay),this._delay=t,this):this._delay},r.duration=function(t){return arguments.length?(this._duration=this._totalDuration=t,this._uncache(!0),this._timeline.smoothChildTiming&&this._time>0&&this._time<this._duration&&0!==t&&this.totalTime(this._totalTime*(t/this._duration),!0),this):(this._dirty=!1,this._duration)},r.totalDuration=function(t){return this._dirty=!1,arguments.length?this.duration(t):this._totalDuration},r.time=function(t,e){return arguments.length?(this._dirty&&this.totalDuration(),this.totalTime(t>this._duration?this._duration:t,e)):this._time},r.totalTime=function(t,e,i){if(a||n.wake(),!arguments.length)return this._totalTime;if(this._timeline){if(0>t&&!i&&(t+=this.totalDuration()),this._timeline.smoothChildTiming){this._dirty&&this.totalDuration();var s=this._totalDuration,r=this._timeline;if(t>s&&!i&&(t=s),this._startTime=(this._paused?this._pauseTime:r._time)-(this._reversed?s-t:t)/this._timeScale,r._dirty||this._uncache(!1),r._timeline)for(;r._timeline;)r._timeline._time!==(r._startTime+r._totalTime)/r._timeScale&&r.totalTime(r._totalTime,!0),r=r._timeline}this._gc&&this._enabled(!0,!1),(this._totalTime!==t||0===this._duration)&&this.render(t,e,!1)}return this},r.progress=r.totalProgress=function(t,e){return arguments.length?this.totalTime(this.duration()*t,e):this._time/this.duration()},r.startTime=function(t){return arguments.length?(t!==this._startTime&&(this._startTime=t,this.timeline&&this.timeline._sortChildren&&this.timeline.add(this,t-this._delay)),this):this._startTime},r.timeScale=function(t){if(!arguments.length)return this._timeScale;if(t=t||l,this._timeline&&this._timeline.smoothChildTiming){var e=this._pauseTime,i=e||0===e?e:this._timeline.totalTime();this._startTime=i-(i-this._startTime)*this._timeScale/t}return this._timeScale=t,this._uncache(!1)},r.reversed=function(t){return arguments.length?(t!=this._reversed&&(this._reversed=t,this.totalTime(this._totalTime,!0)),this):this._reversed},r.paused=function(t){if(!arguments.length)return this._paused;if(t!=this._paused&&this._timeline){a||t||n.wake();var e=this._timeline,i=e.rawTime(),s=i-this._pauseTime;!t&&e.smoothChildTiming&&(this._startTime+=s,this._uncache(!1)),this._pauseTime=t?i:null,this._paused=t,this._active=this.isActive(),!t&&0!==s&&this._initted&&this.duration()&&this.render(e.smoothChildTiming?this._totalTime:(i-this._startTime)/this._timeScale,!0,!0)}return this._gc&&!t&&this._enabled(!0,!1),this};var C=d("core.SimpleTimeline",function(t){R.call(this,0,t),this.autoRemoveChildren=this.smoothChildTiming=!0});r=C.prototype=new R,r.constructor=C,r.kill()._gc=!1,r._first=r._last=null,r._sortChildren=!1,r.add=r.insert=function(t,e){var i,s;if(t._startTime=Number(e||0)+t._delay,t._paused&&this!==t._timeline&&(t._pauseTime=t._startTime+(this.rawTime()-t._startTime)/t._timeScale),t.timeline&&t.timeline._remove(t,!0),t.timeline=t._timeline=this,t._gc&&t._enabled(!0,!0),i=this._last,this._sortChildren)for(s=t._startTime;i&&i._startTime>s;)i=i._prev;return i?(t._next=i._next,i._next=t):(t._next=this._first,this._first=t),t._next?t._next._prev=t:this._last=t,t._prev=i,this._timeline&&this._uncache(!0),this},r._remove=function(t,e){return t.timeline===this&&(e||t._enabled(!1,!0),t.timeline=null,t._prev?t._prev._next=t._next:this._first===t&&(this._first=t._next),t._next?t._next._prev=t._prev:this._last===t&&(this._last=t._prev),this._timeline&&this._uncache(!0)),this},r.render=function(t,e,i){var s,r=this._first;for(this._totalTime=this._time=this._rawPrevTime=t;r;)s=r._next,(r._active||t>=r._startTime&&!r._paused)&&(r._reversed?r.render((r._dirty?r.totalDuration():r._totalDuration)-(t-r._startTime)*r._timeScale,e,i):r.render((t-r._startTime)*r._timeScale,e,i)),r=s},r.rawTime=function(){return a||n.wake(),this._totalTime};var O=d("TweenLite",function(e,i,s){if(R.call(this,i,s),this.render=O.prototype.render,null==e)throw"Cannot tween a null target.";this.target=e="string"!=typeof e?e:O.selector(e)||e;var r,n,a,o=e.jquery||e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType),h=this.vars.overwrite;if(this._overwrite=h=null==h?X[O.defaultOverwrite]:"number"==typeof h?h>>0:X[h],(o||e instanceof Array||e.push&&p(e))&&"number"!=typeof e[0])for(this._targets=a=_.call(e,0),this._propLookup=[],this._siblings=[],r=0;a.length>r;r++)n=a[r],n?"string"!=typeof n?n.length&&n!==t&&n[0]&&(n[0]===t||n[0].nodeType&&n[0].style&&!n.nodeType)?(a.splice(r--,1),this._targets=a=a.concat(_.call(n,0))):(this._siblings[r]=Y(n,this,!1),1===h&&this._siblings[r].length>1&&j(n,this,null,1,this._siblings[r])):(n=a[r--]=O.selector(n),"string"==typeof n&&a.splice(r+1,1)):a.splice(r--,1);else this._propLookup={},this._siblings=Y(e,this,!1),1===h&&this._siblings.length>1&&j(e,this,null,1,this._siblings);(this.vars.immediateRender||0===i&&0===this._delay&&this.vars.immediateRender!==!1)&&this.render(-this._delay,!1,!0)},!0),D=function(e){return e.length&&e!==t&&e[0]&&(e[0]===t||e[0].nodeType&&e[0].style&&!e.nodeType)},M=function(t,e){var i,s={};for(i in t)L[i]||i in e&&"x"!==i&&"y"!==i&&"width"!==i&&"height"!==i&&"className"!==i&&"border"!==i||!(!E[i]||E[i]&&E[i]._autoCSS)||(s[i]=t[i],delete t[i]);t.css=s};r=O.prototype=new R,r.constructor=O,r.kill()._gc=!1,r.ratio=0,r._firstPT=r._targets=r._overwrittenProps=r._startAt=null,r._notifyPluginsOfEnabled=!1,O.version="1.11.2",O.defaultEase=r._ease=new y(null,null,1,1),O.defaultOverwrite="auto",O.ticker=n,O.autoSleep=!0,O.selector=t.$||t.jQuery||function(e){return t.$?(O.selector=t.$,t.$(e)):t.document?t.document.getElementById("#"===e.charAt(0)?e.substr(1):e):e};var I=O._internals={isArray:p,isSelector:D},E=O._plugins={},F=O._tweenLookup={},N=0,L=I.reservedProps={ease:1,delay:1,overwrite:1,onComplete:1,onCompleteParams:1,onCompleteScope:1,useFrames:1,runBackwards:1,startAt:1,onUpdate:1,onUpdateParams:1,onUpdateScope:1,onStart:1,onStartParams:1,onStartScope:1,onReverseComplete:1,onReverseCompleteParams:1,onReverseCompleteScope:1,onRepeat:1,onRepeatParams:1,onRepeatScope:1,easeParams:1,yoyo:1,immediateRender:1,repeat:1,repeatDelay:1,data:1,paused:1,reversed:1,autoCSS:1},X={none:0,all:1,auto:2,concurrent:3,allOnStart:4,preexisting:5,"true":1,"false":0},z=R._rootFramesTimeline=new C,U=R._rootTimeline=new C;U._startTime=n.time,z._startTime=n.frame,U._active=z._active=!0,R._updateRoot=function(){if(U.render((n.time-U._startTime)*U._timeScale,!1,!1),z.render((n.frame-z._startTime)*z._timeScale,!1,!1),!(n.frame%120)){var t,e,i;for(i in F){for(e=F[i].tweens,t=e.length;--t>-1;)e[t]._gc&&e.splice(t,1);0===e.length&&delete F[i]}if(i=U._first,(!i||i._paused)&&O.autoSleep&&!z._first&&1===n._listeners.tick.length){for(;i&&i._paused;)i=i._next;i||n.sleep()}}},n.addEventListener("tick",R._updateRoot);var Y=function(t,e,i){var s,r,n=t._gsTweenID;if(F[n||(t._gsTweenID=n="t"+N++)]||(F[n]={target:t,tweens:[]}),e&&(s=F[n].tweens,s[r=s.length]=e,i))for(;--r>-1;)s[r]===e&&s.splice(r,1);return F[n].tweens},j=function(t,e,i,s,r){var n,a,o,h;if(1===s||s>=4){for(h=r.length,n=0;h>n;n++)if((o=r[n])!==e)o._gc||o._enabled(!1,!1)&&(a=!0);else if(5===s)break;return a}var _,u=e._startTime+l,p=[],f=0,c=0===e._duration;for(n=r.length;--n>-1;)(o=r[n])===e||o._gc||o._paused||(o._timeline!==e._timeline?(_=_||B(e,0,c),0===B(o,_,c)&&(p[f++]=o)):u>=o._startTime&&o._startTime+o.totalDuration()/o._timeScale+l>u&&((c||!o._initted)&&2e-10>=u-o._startTime||(p[f++]=o)));for(n=f;--n>-1;)o=p[n],2===s&&o._kill(i,t)&&(a=!0),(2!==s||!o._firstPT&&o._initted)&&o._enabled(!1,!1)&&(a=!0);return a},B=function(t,e,i){for(var s=t._timeline,r=s._timeScale,n=t._startTime;s._timeline;){if(n+=s._startTime,r*=s._timeScale,s._paused)return-100;s=s._timeline}return n/=r,n>e?n-e:i&&n===e||!t._initted&&2*l>n-e?l:(n+=t.totalDuration()/t._timeScale/r)>e+l?0:n-e-l};r._init=function(){var t,e,i,s,r=this.vars,n=this._overwrittenProps,a=this._duration,o=r.immediateRender,h=r.ease;if(r.startAt){if(this._startAt&&this._startAt.render(-1,!0),r.startAt.overwrite=0,r.startAt.immediateRender=!0,this._startAt=O.to(this.target,0,r.startAt),o)if(this._time>0)this._startAt=null;else if(0!==a)return}else if(r.runBackwards&&0!==a)if(this._startAt)this._startAt.render(-1,!0),this._startAt=null;else{i={};for(s in r)L[s]&&"autoCSS"!==s||(i[s]=r[s]);if(i.overwrite=0,i.data="isFromStart",this._startAt=O.to(this.target,0,i),r.immediateRender){if(0===this._time)return}else this._startAt.render(-1,!0)}if(this._ease=h?h instanceof y?r.easeParams instanceof Array?h.config.apply(h,r.easeParams):h:"function"==typeof h?new y(h,r.easeParams):T[h]||O.defaultEase:O.defaultEase,this._easeType=this._ease._type,this._easePower=this._ease._power,this._firstPT=null,this._targets)for(t=this._targets.length;--t>-1;)this._initProps(this._targets[t],this._propLookup[t]={},this._siblings[t],n?n[t]:null)&&(e=!0);else e=this._initProps(this.target,this._propLookup,this._siblings,n);if(e&&O._onPluginEvent("_onInitAllProps",this),n&&(this._firstPT||"function"!=typeof this.target&&this._enabled(!1,!1)),r.runBackwards)for(i=this._firstPT;i;)i.s+=i.c,i.c=-i.c,i=i._next;this._onUpdate=r.onUpdate,this._initted=!0},r._initProps=function(e,i,s,r){var n,a,o,h,l,_;if(null==e)return!1;this.vars.css||e.style&&e!==t&&e.nodeType&&E.css&&this.vars.autoCSS!==!1&&M(this.vars,e);for(n in this.vars){if(_=this.vars[n],L[n])_&&(_ instanceof Array||_.push&&p(_))&&-1!==_.join("").indexOf("{self}")&&(this.vars[n]=_=this._swapSelfInParams(_,this));else if(E[n]&&(h=new E[n])._onInitTween(e,this.vars[n],this)){for(this._firstPT=l={_next:this._firstPT,t:h,p:"setRatio",s:0,c:1,f:!0,n:n,pg:!0,pr:h._priority},a=h._overwriteProps.length;--a>-1;)i[h._overwriteProps[a]]=this._firstPT;(h._priority||h._onInitAllProps)&&(o=!0),(h._onDisable||h._onEnable)&&(this._notifyPluginsOfEnabled=!0)}else this._firstPT=i[n]=l={_next:this._firstPT,t:e,p:n,f:"function"==typeof e[n],n:n,pg:!1,pr:0},l.s=l.f?e[n.indexOf("set")||"function"!=typeof e["get"+n.substr(3)]?n:"get"+n.substr(3)]():parseFloat(e[n]),l.c="string"==typeof _&&"="===_.charAt(1)?parseInt(_.charAt(0)+"1",10)*Number(_.substr(2)):Number(_)-l.s||0;l&&l._next&&(l._next._prev=l)}return r&&this._kill(r,e)?this._initProps(e,i,s,r):this._overwrite>1&&this._firstPT&&s.length>1&&j(e,this,i,this._overwrite,s)?(this._kill(i,e),this._initProps(e,i,s,r)):o},r.render=function(t,e,i){var s,r,n,a,o=this._time,h=this._duration;if(t>=h)this._totalTime=this._time=h,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1,this._reversed||(s=!0,r="onComplete"),0===h&&(a=this._rawPrevTime,(0===t||0>a||a===l)&&a!==t&&(i=!0,a>l&&(r="onReverseComplete")),this._rawPrevTime=a=!e||t?t:l);else if(1e-7>t)this._totalTime=this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==o||0===h&&this._rawPrevTime>l)&&(r="onReverseComplete",s=this._reversed),0>t?(this._active=!1,0===h&&(this._rawPrevTime>=0&&(i=!0),this._rawPrevTime=a=!e||t?t:l)):this._initted||(i=!0);else if(this._totalTime=this._time=t,this._easeType){var _=t/h,u=this._easeType,p=this._easePower;(1===u||3===u&&_>=.5)&&(_=1-_),3===u&&(_*=2),1===p?_*=_:2===p?_*=_*_:3===p?_*=_*_*_:4===p&&(_*=_*_*_*_),this.ratio=1===u?1-_:2===u?_:.5>t/h?_/2:1-_/2}else this.ratio=this._ease.getRatio(t/h);if(this._time!==o||i){if(!this._initted){if(this._init(),!this._initted||this._gc)return;this._time&&!s?this.ratio=this._ease.getRatio(this._time/h):s&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._active||!this._paused&&this._time!==o&&t>=0&&(this._active=!0),0===o&&(this._startAt&&(t>=0?this._startAt.render(t,e,i):r||(r="_dummyGS")),this.vars.onStart&&(0!==this._time||0===h)&&(e||this.vars.onStart.apply(this.vars.onStartScope||this,this.vars.onStartParams||v))),n=this._firstPT;n;)n.f?n.t[n.p](n.c*this.ratio+n.s):n.t[n.p]=n.c*this.ratio+n.s,n=n._next;this._onUpdate&&(0>t&&this._startAt&&this._startTime&&this._startAt.render(t,e,i),e||i&&0===this._time&&0===o||this._onUpdate.apply(this.vars.onUpdateScope||this,this.vars.onUpdateParams||v)),r&&(this._gc||(0>t&&this._startAt&&!this._onUpdate&&this._startTime&&this._startAt.render(t,e,i),s&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!e&&this.vars[r]&&this.vars[r].apply(this.vars[r+"Scope"]||this,this.vars[r+"Params"]||v),0===h&&this._rawPrevTime===l&&a!==l&&(this._rawPrevTime=0)))}},r._kill=function(t,e){if("all"===t&&(t=null),null==t&&(null==e||e===this.target))return this._enabled(!1,!1);e="string"!=typeof e?e||this._targets||this.target:O.selector(e)||e;var i,s,r,n,a,o,h,l;if((p(e)||D(e))&&"number"!=typeof e[0])for(i=e.length;--i>-1;)this._kill(t,e[i])&&(o=!0);else{if(this._targets){for(i=this._targets.length;--i>-1;)if(e===this._targets[i]){a=this._propLookup[i]||{},this._overwrittenProps=this._overwrittenProps||[],s=this._overwrittenProps[i]=t?this._overwrittenProps[i]||{}:"all";break}}else{if(e!==this.target)return!1;a=this._propLookup,s=this._overwrittenProps=t?this._overwrittenProps||{}:"all"}if(a){h=t||a,l=t!==s&&"all"!==s&&t!==a&&("object"!=typeof t||!t._tempKill);for(r in h)(n=a[r])&&(n.pg&&n.t._kill(h)&&(o=!0),n.pg&&0!==n.t._overwriteProps.length||(n._prev?n._prev._next=n._next:n===this._firstPT&&(this._firstPT=n._next),n._next&&(n._next._prev=n._prev),n._next=n._prev=null),delete a[r]),l&&(s[r]=1);!this._firstPT&&this._initted&&this._enabled(!1,!1)}}return o},r.invalidate=function(){return this._notifyPluginsOfEnabled&&O._onPluginEvent("_onDisable",this),this._firstPT=null,this._overwrittenProps=null,this._onUpdate=null,this._startAt=null,this._initted=this._active=this._notifyPluginsOfEnabled=!1,this._propLookup=this._targets?{}:[],this},r._enabled=function(t,e){if(a||n.wake(),t&&this._gc){var i,s=this._targets;if(s)for(i=s.length;--i>-1;)this._siblings[i]=Y(s[i],this,!0);else this._siblings=Y(this.target,this,!0)}return R.prototype._enabled.call(this,t,e),this._notifyPluginsOfEnabled&&this._firstPT?O._onPluginEvent(t?"_onEnable":"_onDisable",this):!1},O.to=function(t,e,i){return new O(t,e,i)},O.from=function(t,e,i){return i.runBackwards=!0,i.immediateRender=0!=i.immediateRender,new O(t,e,i)},O.fromTo=function(t,e,i,s){return s.startAt=i,s.immediateRender=0!=s.immediateRender&&0!=i.immediateRender,new O(t,e,s)},O.delayedCall=function(t,e,i,s,r){return new O(e,0,{delay:t,onComplete:e,onCompleteParams:i,onCompleteScope:s,onReverseComplete:e,onReverseCompleteParams:i,onReverseCompleteScope:s,immediateRender:!1,useFrames:r,overwrite:0})},O.set=function(t,e){return new O(t,0,e)},O.getTweensOf=function(t,e){if(null==t)return[];t="string"!=typeof t?t:O.selector(t)||t;var i,s,r,n;if((p(t)||D(t))&&"number"!=typeof t[0]){for(i=t.length,s=[];--i>-1;)s=s.concat(O.getTweensOf(t[i],e));for(i=s.length;--i>-1;)for(n=s[i],r=i;--r>-1;)n===s[r]&&s.splice(i,1)}else for(s=Y(t).concat(),i=s.length;--i>-1;)(s[i]._gc||e&&!s[i].isActive())&&s.splice(i,1);return s},O.killTweensOf=O.killDelayedCallsTo=function(t,e,i){"object"==typeof e&&(i=e,e=!1);for(var s=O.getTweensOf(t,e),r=s.length;--r>-1;)s[r]._kill(i,t)};var q=d("plugins.TweenPlugin",function(t,e){this._overwriteProps=(t||"").split(","),this._propName=this._overwriteProps[0],this._priority=e||0,this._super=q.prototype},!0);if(r=q.prototype,q.version="1.10.1",q.API=2,r._firstPT=null,r._addTween=function(t,e,i,s,r,n){var a,o;return null!=s&&(a="number"==typeof s||"="!==s.charAt(1)?Number(s)-i:parseInt(s.charAt(0)+"1",10)*Number(s.substr(2)))?(this._firstPT=o={_next:this._firstPT,t:t,p:e,s:i,c:a,f:"function"==typeof t[e],n:r||e,r:n},o._next&&(o._next._prev=o),o):void 0},r.setRatio=function(t){for(var e,i=this._firstPT,s=1e-6;i;)e=i.c*t+i.s,i.r?e=0|e+(e>0?.5:-.5):s>e&&e>-s&&(e=0),i.f?i.t[i.p](e):i.t[i.p]=e,i=i._next},r._kill=function(t){var e,i=this._overwriteProps,s=this._firstPT;if(null!=t[this._propName])this._overwriteProps=[];else for(e=i.length;--e>-1;)null!=t[i[e]]&&i.splice(e,1);for(;s;)null!=t[s.n]&&(s._next&&(s._next._prev=s._prev),s._prev?(s._prev._next=s._next,s._prev=null):this._firstPT===s&&(this._firstPT=s._next)),s=s._next;return!1},r._roundProps=function(t,e){for(var i=this._firstPT;i;)(t[this._propName]||null!=i.n&&t[i.n.split(this._propName+"_").join("")])&&(i.r=e),i=i._next},O._onPluginEvent=function(t,e){var i,s,r,n,a,o=e._firstPT;if("_onInitAllProps"===t){for(;o;){for(a=o._next,s=r;s&&s.pr>o.pr;)s=s._next;(o._prev=s?s._prev:n)?o._prev._next=o:r=o,(o._next=s)?s._prev=o:n=o,o=a}o=e._firstPT=r}for(;o;)o.pg&&"function"==typeof o.t[t]&&o.t[t]()&&(i=!0),o=o._next;return i},q.activate=function(t){for(var e=t.length;--e>-1;)t[e].API===q.API&&(E[(new t[e])._propName]=t[e]);return!0},m.plugin=function(t){if(!(t&&t.propName&&t.init&&t.API))throw"illegal plugin definition.";var e,i=t.propName,s=t.priority||0,r=t.overwriteProps,n={init:"_onInitTween",set:"setRatio",kill:"_kill",round:"_roundProps",initAll:"_onInitAllProps"},a=d("plugins."+i.charAt(0).toUpperCase()+i.substr(1)+"Plugin",function(){q.call(this,i,s),this._overwriteProps=r||[]},t.global===!0),o=a.prototype=new q(i);o.constructor=a,a.API=t.API;for(e in n)"function"==typeof t[e]&&(o[n[e]]=t[e]);return a.version=t.version,q.activate([a]),a},i=t._gsQueue){for(s=0;i.length>s;s++)i[s]();for(r in f)f[r].func||t.console.log("GSAP encountered missing dependency: com.greensock."+r)}a=!1}}(window);if(gsConflict){window.GreenSockGlobals=oldGSG;}
(function(window,$){var jQuery=$;jQuery.easing.jswing=jQuery.easing.swing;jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,a,c,b,d){return jQuery.easing[jQuery.easing.def](e,a,c,b,d)},easeInQuad:function(e,a,c,b,d){return b*(a/=d)*a+ c},easeOutQuad:function(e,a,c,b,d){return-b*(a/=d)*(a- 2)+ c},easeInOutQuad:function(e,a,c,b,d){return 1>(a/=d/2)?b/2*a*a+ c:-b/2*(--a*(a- 2)- 1)+ c},easeInCubic:function(e,a,c,b,d){return b*(a/=d)*a*a+ c},easeOutCubic:function(e,a,c,b,d){return b*((a=a/d- 1)*a*a+ 1)+ c},easeInOutCubic:function(e,a,c,b,d){return 1>(a/=d/2)?b/2*a*a*a+ c:b/2*((a-=2)*a*a+ 2)+ c},easeInQuart:function(e,a,c,b,d){return b*(a/=d)*a*a*a+ c},easeOutQuart:function(e,a,c,b,d){return-b*((a=a/d- 1)*a*a*a- 1)+ c},easeInOutQuart:function(e,a,c,b,d){return 1>(a/=d/2)?b/2*a*a*a*a+ c:-b/2*((a-=2)*a*a*a- 2)+ c},easeInQuint:function(e,a,c,b,d){return b*(a/=d)*a*a*a*a+ c},easeOutQuint:function(e,a,c,b,d){return b*((a=a/d- 1)*a*a*a*a+ 1)+ c},easeInOutQuint:function(e,a,c,b,d){return 1>(a/=d/2)?b/2*a*a*a*a*a+ c:b/2*((a-=2)*a*a*a*a+ 2)+ c},easeInSine:function(e,a,c,b,d){return-b*Math.cos(a/
d*(Math.PI/2))+ b+ c},easeOutSine:function(e,a,c,b,d){return b*Math.sin(a/d*(Math.PI/2))+ c},easeInOutSine:function(e,a,c,b,d){return-b/2*(Math.cos(Math.PI*a/d)- 1)+ c},easeInExpo:function(e,a,c,b,d){return 0==a?c:b*Math.pow(2,10*(a/d- 1))+ c},easeOutExpo:function(e,a,c,b,d){return a==d?c+ b:b*(-Math.pow(2,-10*a/d)+ 1)+ c},easeInOutExpo:function(e,a,c,b,d){return 0==a?c:a==d?c+ b:1>(a/=d/2)?b/2*Math.pow(2,10*(a- 1))+ c:b/2*(-Math.pow(2,-10*--a)+ 2)+ c},easeInCirc:function(e,a,c,b,d){return-b*(Math.sqrt(1-(a/=d)*a)- 1)+ c},easeOutCirc:function(e,a,c,b,d){return b*Math.sqrt(1-(a=a/d- 1)*a)+ c},easeInOutCirc:function(e,a,c,b,d){return 1>(a/=d/2)?-b/2*(Math.sqrt(1- a*a)- 1)+ c:b/2*(Math.sqrt(1-(a-=2)*a)+ 1)+ c},easeInElastic:function(e,a,c,b,d){e=1.70158;var f=0,g=b;if(0==a)return c;if(1==(a/=d))return c+ b;f||(f=.3*d);g<Math.abs(b)?(g=b,e=f/4):e=f/(2*Math.PI)*Math.asin(b/g);return-(g*Math.pow(2,10*(a-=1))*Math.sin(2*(a*d- e)*Math.PI/f))+ c},easeOutElastic:function(e,a,c,b,d){e=1.70158;var f=0,g=b;if(0==a)return c;if(1==(a/=d))return c+ b;f||(f=.3*d);g<Math.abs(b)?(g=b,e=f/4):e=f/(2*Math.PI)*Math.asin(b/g);return g*Math.pow(2,-10*a)*Math.sin(2*(a*d- e)*Math.PI/f)+ b+ c},easeInOutElastic:function(e,a,c,b,d){e=1.70158;var f=0,g=b;if(0==a)return c;if(2==(a/=d/2))return c+ b;f||(f=.3*d*1.5);g<Math.abs(b)?(g=b,e=f/4):e=f/(2*Math.PI)*Math.asin(b/g);return 1>a?-.5*g*Math.pow(2,10*(a-=1))*Math.sin(2*(a*d- e)*Math.PI/f)+ c:g*Math.pow(2,-10*(a-=1))*Math.sin(2*(a*d- e)*Math.PI/f)*.5+ b+ c},easeInBack:function(e,a,c,b,d,f){void 0==f&&(f=1.70158);return b*(a/=d)*a*((f+ 1)*a- f)+ c},easeOutBack:function(e,a,c,b,d,f){void 0==f&&(f=1.70158);return b*((a=a/d- 1)*a*((f+ 1)*a+ f)+ 1)+ c},easeInOutBack:function(e,a,c,b,d,f){void 0==f&&(f=1.70158);return 1>(a/=d/2)?b/2*a*a*(((f*=1.525)+ 1)*a- f)+ c:b/2*((a-=2)*a*(((f*=1.525)+ 1)*a+ f)+ 2)+ c},easeInBounce:function(e,a,c,b,d){return b- jQuery.easing.easeOutBounce(e,d- a,0,b,d)+ c},easeOutBounce:function(e,a,c,b,d){return(a/=d)<1/2.75?7.5625*b*a*a+ c:a<2/2.75?b*(7.5625*(a-=1.5/2.75)*a+.75)+ c:a<2.5/2.75?b*(7.5625*(a-=2.25/2.75)*a+.9375)+ c:b*(7.5625*(a-=2.625/2.75)*a+.984375)+ c},easeInOutBounce:function(e,a,c,b,d){return a<d/2?.5*jQuery.easing.easeInBounce(e,2*a,0,b,d)+ c:.5*jQuery.easing.easeOutBounce(e,2*a- d,0,b,d)+.5*b+ c}});var frontEnd={};frontEnd.starSelector=starSelector;function starSelector(ob){var _this=this;init();var _value=0;var starSelect;var colorStyle="";function init(){var starHTML="";starHTML+="<div class='inside_starSelector'>"
for(var i=0;i<5;i++){starHTML+="<div class='star icon-sstarempty' val='"+(i+ 1)+"'></div>";}
starHTML+="</div>";starSelect=$(starHTML).appendTo($(ob.holder));$(starSelect).find(".star").hover(function(){$(this).addClass("selected").removeClass("icon-sstarempty").addClass("icon-sstarfull");var val=parseInt($(this).attr("val"));setValue(val);},function(){setValue(_value);});$(starSelect).find(".star").click(function(){_value=parseInt($(this).attr("val"));setValue(_value);});}
_this.value=value;function value(val){if(typeof(val)=="undefined"){return _value;}else{_value=val;setValue(val);}}
function setValue(val){starSelect.find(".star").removeClass("selected").removeClass("icon-sstarfull").addClass("icon-sstarempty");starSelect.find(".star").each(function(){if(parseInt($(this).attr("val"))<=val){$(this).addClass("selected").removeClass("icon-sstarempty").addClass("icon-sstarfull");}})}}
var iOSMobile=(navigator.userAgent.match(/(iPhone|iPod)/g)?true:false);var standalone=window.navigator.standalone,userAgent=window.navigator.userAgent.toLowerCase(),safari=/safari/.test(userAgent),ios=/iphone|ipod|ipad/.test(userAgent);if(isLocal()){if(typeof(_insideCDN)=="undefined"){_insideCDN="./";}}else{_insideCDN=_insideCDN||"./";}
var connected=false;imageurl=_insideCDN+"images/";offerurl=_insideCDN+"offers/";if(typeof(_insideProtocol)=="undefined"){_insideProtocol="";}
frontEnd.imageurl=imageurl;frontEnd.offerurl=offerurl;var settings;var insideDisabled=false;var device;var chatEnabled=true;var tabPosition="right";if(isLocal()){$.inside=(function(){});}
function init(){if(window.top!=window){if(!alreadyConnected){bindEvents();setTimeout(init,100);return;}
if(frontEnd.settings.allowIframes!=true){$("#inside_holder").remove();return;}}
showTabs();if($("#inside_holder").length==0){setTimeout(init,1000);return;}
device=getDevice();bindEvents();if(!isLocal()){chatEnabled=$.inside.isEnabled("chat");}
loadCssFile(_insideCDN+"/ig.css?dev="+ device+"&_"+ _insideScriptVersion);$("#inside_tabs").css("top",-getViewPortSize().height- $("#inside_tabs").height()- 300);setTimeout(function(){windowScale(500);},1000);IE11Quirks=isIE11Quirks();if(IE11Quirks){$(window).scroll(IE11ScrollFix);}}
var IE11Quirks=false;function isIE11Quirks(){var isIE11=!!navigator.userAgent.match(/Trident\/7/);var mode=document.documentMode;return(isIE11&&mode==5);}
function IE11ScrollFix(){viewport=getViewPortSize();$("#inside_holder").css("top",$(window).scrollTop()+"px");$(".inside_chatPane").css("top",viewport.height*0.5- $(".inside_chatPane").height()*0.5);}
frontEnd.showTabs=showTabs();function showTabs(){if($("#inside_holder").length>0){return;}
$("<div id='inside_holder'></div>").appendTo("body");$("#inside_holder").attr("pagetitle",$("title").text());$("#inside_holder").attr("pageurl",document.location.href);$("<div id='inside_tabs'></div>").appendTo("#inside_holder");if(getDevice()==2){$("#inside_holder").addClass("mobile-device");}
tabMouseDown=false;var mousex=0;var mousey=0;var count=0;$("#inside_tabs").unbind("mousedown");$("#inside_tabs").unbind("touchstart");$("#inside_tabs").bind("mousedown",mouseDown);$("#inside_tabs").bind("touchstart",mouseDown);function mouseDown(e){tabMouseDown=true;count=0;var xmove=0;mousex=e.clientX;mousey=e.clientY- parseInt($("#inside_tabs").css("margin-top"));if(typeof(e.originalEvent.touches)!="undefined"){mousey=e.originalEvent.touches[0].clientY- parseInt($("#inside_tabs").css("margin-top"));mousex=e.originalEvent.touches[0].clientX;}
$("#inside_tabs").bind("mousemove",mouseMove);$("#inside_tabs").bind("touchmove",mouseMove)
function mouseMove(e){var clientY=e.clientY;var clientX=e.clientX;if(typeof(e.originalEvent.touches)!="undefined"){clientY=e.originalEvent.touches[0].clientY;clientX=e.originalEvent.touches[0].clientX;}
$("#inside_tabs").css("margin-top",clientY- mousey+"px");xmove=clientX- mousex;count++;e.preventDefault();}
$("#inside_tabs").unbind("mouseup");$("#inside_tabs").unbind("touchend");$("#inside_tabs").bind("mouseup",mouseUp);$("#inside_tabs").bind("touchend",mouseUp);function mouseUp(e){if(count>20){insideChat.preventTabClick=true;setTimeout(function(){insideChat.preventTabClick=false;},100);}
if(device!=1){if(tabPosition.indexOf("right")!=-1&&xmove>20){autohideTabs(100);}
if(tabPosition.indexOf("left")!=-1&&xmove<-20){autohideTabs(100);}}}}
$("#inside_tabs").bind("mouseout",function(){$("#inside_tabs").unbind("mousemove");});$(window).bind("mouseup",windowMouseUp);$(window).bind("touchend",windowMouseUp);function windowMouseUp(e){$("#inside_tabs").unbind("mousemove");tabMouseDown=false;}}
frontEnd.getDevice=getDevice;function getDevice(){if(isLocal()){return 2;}
var userAgent=navigator.userAgent.toLowerCase();var agent=window.navigator.userAgent.toLowerCase();var iPod=(agent.indexOf("ipod")>0);var iPhone=(agent.indexOf("iphone")>0);var iPad=(agent.indexOf("ipad")>0);var android=((agent.indexOf("android")>0)&&(agent.indexOf("mobile")>0));var androidTablet=!android&&(agent.indexOf("android")>0);var webOS=(agent.indexOf("webos")>0);var blackBerry=(agent.indexOf("blackberry")>0);var rimTablet=(agent.indexOf("rimtablet")>0);var device=1;if(iPod||iPhone||android||webOS||blackBerry){device=2;}else if(iPad||rimTablet||androidTablet){device=3;}
return device;}
var eventsBound=false;function bindEvents(){if(eventsBound){return;}
eventsBound=true;$(window).resize(windowScale);$(window).bind('gestureend',windowScale);if(ios){$(document).on('focusin','input',function(){if((tabPosition=="bottomright"||tabPosition=="bottomleft")){$("#inside_tabs").css("top",660+"px");}else if((tabPosition=="topright"||tabPosition=="topleft")){$("#inside_tabs").css("top",top+ $(document).scrollTop()+"px");}});}
if(ios&&!standalone&&safari){$(document).scroll(function(){if($('input').is(":focus")){if((tabPosition=="bottomright"||tabPosition=="bottomleft")){$("#inside_tabs").css("top",660+ $(document).scrollTop()+"px");}else if((tabPosition=="right"||tabPosition=="left")){$("#inside_tabs").css("top",535+ $(document).scrollTop()+"px");}else if((tabPosition=="topright"||tabPosition=="topleft")){$("#inside_tabs").css("top",top+ $(document).scrollTop()+"px");}else{$("#inside_tabs").css("top",top+"px");}}});}
if(typeof $.inside!="object")
return;$.inside.bind("connected",connectedToInside);$.inside.bind("disconnected",disconnected);$.inside.bind("updateUsers",updateUsers);$.inside.bind("removeUsers",removeUsers);$.inside.bind("insideAction",insideAction);$.inside.bind("unstick",unstick);$.inside.bind("coBrowse",coBrowseCommandHandler);$.inside.start();}
function checkForCoBrowseRequestState(){$.inside.server.coBrowseRequestState().done(function(state){if(state=="Accept"){coBrowseCommandHandler("/load")}else if(state=="Request"&&$(".inside_cobrowseRequest").length==0){insideChat.setCoBrowseHtml("request");}});}
function coBrowseCommandHandler(command){_insideGraph.initModule("cobrowse");if(command=="/load"){frontEnd.Load();}else if(command=="/open"){frontEnd.Open();}}
function unstick(stickyid){}
function insideAction(data,stickyid){data.stickyid=stickyid;if(data.type=="visitornotify"){visitorNotify(data);}else if(data.type=="survey"){data.settings.message="<div id='insideSurveyHolder'></div>";showSurveyPopup(data);}else if(data.type=="chatsurvey"){data.settings.message="<div id='insideSurveyHolder'></div>";if($.inside.front.settings.showPostChatSurveyInChat){data.settings.useContainer=true;data.settings.containerid="#inside_chatWindow, #inside_chatPane_custom_chatHolder";}
if($.inside.front.settings.postChatSurvey&&$.inside.front.settings.postChatSurvey!=""){if($.inside.front.settings.postChatSurvey=="disable"){return;}else{data.settings.surveyid=$.inside.front.settings.postChatSurvey;}}
data.settings.position="6";if($("#inside_chatWindow, #inside_chatPane_custom_chatHolder").length>0){insideChat.clearChatGrouping();showSurveyPopup(data);}else{chatSurveyQueue.push(data);frontEnd.chatSurveyQueue=chatSurveyQueue;checkChatSurveyQueue();}}else if(data.type=="jointhecall"){insideChat.clickToCallSettings=data.settings.clickToCallSettings||{};insideChat.clickToCallSettings.showJoinTheCallIcon=data.settings.showJoinTheCallIcon;setTimeout(function(){insideChat.canJoinTheCall=true;insideChat.setupJoinTheCallTag(insideChat.clickToCallSettings);},insideChat?0:5000);}else if(data.type=="clicktocall"){insideFrontInterface.onClickToCallChatTabImageLoad=onClickToCallChatTabImageLoad;insideFrontInterface.onClickToCallChatTabImageClick=onClickToCallChatTabImageClick;insideFrontInterface.showClickToCallRequestPopup=showClickToCallRequestPopup;clickToCallScript=data.settings.script;if(typeof(data.settings.clickToCallSettings)!="undefined"&&(data.settings.clickToCallSettings.script||"")!="")
clickToCallScript=data.settings.clickToCallSettings.script;setupClickToCallTag(data.settings.clickToCallSettings,frontEnd.settings);}}//#region ClickToCall
var clickToCallSettings;var clickToCallScript;var isClickToCallRequestSent=false;function setupClickToCallTag(settings,frontEndSettings){if($("#inside_clickToCallChatTab").length){return;}
clickToCallSettings=settings;insideChat.clickToCallSettings=clickToCallSettings;var src=clickToCallSettings&&clickToCallSettings.clickToCallTab&&clickToCallSettings.clickToCallTab.customTabImage?_insideCDN+"/custom/"+ clickToCallSettings.clickToCallTab.customTabImage:imageurl+"click-to-call-tab.png";var html="<div id='inside_clickToCallChatTab' tabindex='2' class='noselect' ondragstart='return false;' ondrop='return false;'>"+"<img id='inside_clickToCallImage' src='"+ src+"' onload='insideFrontInterface.onClickToCallChatTabImageLoad(this);' />"+"</div>";var $html=$(html);$(html).find("#inside_clickToCallChatTab").hide();$html.appendTo("#inside_tabs");setClickToCallTag(true);$("#inside_clickToCallChatTab").click(onClickToCallChatTabImageClick);if(clickToCallSettings&&clickToCallSettings.clickToCallTab&&clickToCallSettings.clickToCallTab.hideAmount){$("#inside_clickToCallChatTab").attr("autohide",clickToCallSettings.clickToCallTab.hideAmount);}
try{_insideGraph.doEvent("c2cavailable",true);}catch(e){}}
function setClickToCallTag(show){if($("#inside_clickToCallChatTab").length===0){return;}
if(show){$("#inside_clickToCallChatTab").show();}else{$("#inside_clickToCallChatTab").hide();}}
function onClickToCallChatTabImageLoad(arg){if(arg.complete){var hideAmount=30;if($.inside.front.settings.autoHideAmount){hideAmount=parseInt($.inside.front.settings.autoHideAmount);}
hideAmount=30;setTimeout(function(){$("#inside_tabs").show();},500);$("#inside_clickToCallChatTab").width(arg.width);positionTabs();if(typeof(frontEnd.settings.autohideTabs)!="undefined"&&frontEnd.settings.autohideTabs==true){if(device==2&&frontEnd.settings.autohideOnMobile==false){}else{if(frontEnd.settings.autoHideTabOnLoad!=true){setTimeout(autohideTabs,3000);}else{autohideTabs(0);}}}}}
function isValidClickToCallRequestPopupData(){var firstname=$(".inside_clicktocall_firstname input").val().trim();var phonenum=$(".inside_clicktocall_phone_num input").val().trim();return firstname&&phonenum&&phonenum.length>3&&phonenum.match(/^\d+$/);}
function onClickToCallChatTabImageClick(args){if(insideChat.preventTabClick){insideChat.preventTabClick=false;return;}
if(parseInt($("#inside_tabs").css("right"))<-30){return;}
if(!isClickToCallRequestSent){showClickToCallRequestPopup();}else{showClickToCallConnectingPopup(true);}}
function showClickToCallRequestPopup(){$("#inside_holder .inside_clicktocall_request").hide();$("#inside_holder .inside_clicktocall_connecting").hide();if($("#inside_holder .inside_clicktocall_request").length===0){var html="<div class='inside_clicktocall_request'>"+"<div class='inside_clicktocall_header'>"+"<table>"+"<tr>"+"<td class='inside_clicktocall_txt0'></td>"+"<td class='inside_clicktocall_close'><span class='fonticon icon-hclose'></span></td>"+"</tr>"+"</table>"+"</div>"+"<div class='inside_clicktocall_body'>"+"<div class='inside_clicktocall_txt1'></div>"+"<p><br/>"+"<div class='inside_clicktocall_txt2'></div>"+"<p><br/>"+"<table class='inside_clicktocall_name'>"+"<tr>"+"<td>First Name</td>"+"<td>Last Name</td>"+"</tr>"+"<tr>"+"<td class='inside_clicktocall_firstname'><input></td>"+"<td class='inside_clicktocall_lastname'><input></td>"+"</tr>"+"</table>"+"<p><br/>"+"<table class='inside_clicktocall_phone'>"+"<tr>"+"<td class='inside_clicktocall_phone_num_lbl' colspan='2'>Your Number</td>"+"<td class='inside_clicktocall_phone_ext_lbl'>Ext</td>"+"</tr>"+"<tr>"+"<td class='inside_clicktocall_phone_code'><input></td>"+"<td class='inside_clicktocall_phone_num'><input></td>"+"<td class='inside_clicktocall_phone_ext'><input></td>"+"</tr>"+"</table>"+"<div class='inside_clicktocall_form_error'>&nbsp;</div>"+"<button class='inside_clicktocall_btn'></button>"+"</div>"+"</div>";var $html=$(html);$(html).find(".inside_clicktocall_request").hide();$html.appendTo("#inside_holder");}
var settings=clickToCallSettings&&clickToCallSettings.requestForm?clickToCallSettings.requestForm:{};var title=settings.title||"HAVE US CALL YOU BACK";var header=settings.heading||"Talk to an Expert now";var buttonlabel=settings.buttonlabel||"CALL ME NOW";var text="";if(firstname||lastname||phonenum){if(settings.text2){text=settings.text2;}
else{text="Please verify your name and phone number.";}}else{if(settings.text1){text=settings.text1;}
else{text="Please enter your name and phone number.";}}
$("#inside_holder .inside_clicktocall_request .inside_clicktocall_txt0").html(title);$("#inside_holder .inside_clicktocall_request .inside_clicktocall_txt1").html(header);$("#inside_holder .inside_clicktocall_request .inside_clicktocall_txt2").html(text);$("#inside_holder .inside_clicktocall_request .inside_clicktocall_btn").html(buttonlabel);var visitorDetails=_insideGraph.getVisitorDetails()||{};var firstname=visitorDetails.firstname||"";var lastname=visitorDetails.lastname||"";var phone=visitorDetails.phone||"";var phonecode="";var phonenum="";var phoneext="";if(phone.length>10){phonecode=phone.substr(0,3);phonenum=phone.substr(3,7);phoneext=phone.substr(10);}else if(phone.length>7){phonecode=phone.substr(0,3);phonenum=phone.substr(3);}else{phonenum=phone;}
$("#inside_holder .inside_clicktocall_request .inside_clicktocall_firstname input").val(firstname);$("#inside_holder .inside_clicktocall_request .inside_clicktocall_lastname input").val(lastname);$("#inside_holder .inside_clicktocall_request .inside_clicktocall_phone_code input").val(phonecode);$("#inside_holder .inside_clicktocall_request .inside_clicktocall_phone_num input").val(phonenum);$("#inside_holder .inside_clicktocall_request .inside_clicktocall_phone_ext input").val(phoneext);positionClickToCall();$(window).off("resize.inside_clicktocall_request").on("resize.inside_clicktocall_request",(function(){positionClickToCall();}));$("#inside_holder .inside_clicktocall_request .inside_clicktocall_btn").off("click").on("click",function(e){if(!isValidClickToCallRequestPopupData()){$(".inside_clicktocall_form_error").text("Invalid name or phone number!");setTimeout(function(){$(".inside_clicktocall_form_error").html("&nbsp;");},5000);return;}
var args={script:clickToCallScript,firstname:$(".inside_clicktocall_firstname input").val().trim(),lastname:$(".inside_clicktocall_lastname input").val().trim(),phonecode:$(".inside_clicktocall_phone_code input").val().trim(),phonenum:$(".inside_clicktocall_phone_num input").val().trim(),phoneext:$(".inside_clicktocall_phone_ext input").val().trim()};function onSuccess(e){if(e&&(e.status!="Success"||!e.data)){return onError(e);}
showClickToCallConnectingPopup(true);isClickToCallRequestSent=true;}
function onError(e){showClickToCallConnectingPopup(false);}
$("#inside_holder .inside_clicktocall_request").hide();showClickToCallConnectingPopup();insideAPI.call("clicktocall",args,onSuccess,onerror);});$("#inside_holder .inside_clicktocall_request .inside_clicktocall_close").off("click").on("click",function(e){$("#inside_holder .inside_clicktocall_request").hide();$("#inside_clickToCallChatTab").show();windowScale();});if(!isClickToCallRequestSent){$("#inside_clickToCallChatTab").hide();$("#inside_holder .inside_clicktocall_request").show();}else{showClickToCallConnectingPopup(true);}}
function positionClickToCall(){viewport=getViewPortSize();var top=(viewport.height- $("#inside_holder .inside_clicktocall_request").height())*0.5;$("#inside_holder .inside_clicktocall_request").css({position:'absolute',top:top,left:$(window).width()-($("#inside_holder .inside_clicktocall_request").width()+ 40)});}
function showClickToCallConnectingPopup(success){if($("#inside_holder .inside_clicktocall_connecting").length===0){var html="<div class='inside_clicktocall_connecting'>"+"<div class='inside_clicktocall_header'>"+"<table>"+"<tr>"+"<td class='inside_clicktocall_txt0'></td>"+"<td class='inside_clicktocall_close'>X</td>"+"</tr>"+"</table>"+"</div>"+"<div class='inside_clicktocall_body'>"+"<div class='inside_clicktocall_txt3'></div>"+"<div>"+"</div>";var $html=$(html);$(html).find(".inside_clicktocall_connecting").hide();$html.appendTo("#inside_holder");}
var settings=clickToCallSettings&&clickToCallSettings.connectingForm?clickToCallSettings.connectingForm:{};var title=settings.title||"CONNECTING YOU";var text="";if(success===true){if(settings.text1){text=settings.text1;}
else{text="Thanks, please stand by. An expert will be calling you soon.";}}else if(success==false){if(settings.text2){text=settings.text2;}
else{text="Sorry, we could not place your request. Please try later.";}}else{text="Please wait, while we place your request.";}
$("#inside_holder .inside_clicktocall_connecting .inside_clicktocall_txt0").html(title);$("#inside_holder .inside_clicktocall_connecting .inside_clicktocall_txt3").html(text);viewport=getViewPortSize();$("#inside_holder .inside_clicktocall_connecting").css({position:'fixed',top:viewport.height/2- $("#inside_holder .inside_clicktocall_connecting").height()/2,left:viewport.width-($("#inside_holder .inside_clicktocall_connecting").width()+ 40)});$(window).off("resize.inside_clicktocall_connecting").on("resize.inside_clicktocall_connecting",(function(){viewport=getViewPortSize();$("#inside_holder .inside_clicktocall_connecting").css({position:'fixed',top:viewport.height/2- $("#inside_holder .inside_clicktocall_connecting").height()/2,left:viewport.width-($("#inside_holder .inside_clicktocall_connecting").width()+ 40)});}));$("#inside_holder .inside_clicktocall_connecting .inside_clicktocall_close").off("click").on("click",function(e){$("#inside_holder .inside_clicktocall_connecting").hide();$("#inside_clickToCallChatTab").show();});$("#inside_clickToCallChatTab").hide();$("#inside_holder .inside_clicktocall_connecting").show();}
var clickToCallAutoHideTimeout;function startAutoHideClickToCallTab(){$("#inside_clickToCallChatTab").bind("mouseenter",hoverShowClickToCallTab);$("#inside_clickToCallChatTab").bind("mouseleave",function(){clickToCallAutoHideTimeout=setTimeout(autohideClickToCallTab,100);});autohideClickToCallTab();}
function hoverShowClickToCallTab(){clearTimeout(autoHideTimeout);if(tabPosition=="left"||tabPosition=="topleft"||tabPosition=="bottomleft"){$("#inside_clickToCallChatTab").stop(true).animate({left:0},500);}else{$("#inside_clickToCallChatTab").stop(true).animate({right:0},500);}}
frontEnd.autohideClickToCallTab=autohideClickToCallTab;function autohideClickToCallTab(time){if(typeof(time)=="undefined"){time=1000;}
var hideAmount=20;if(typeof(frontEnd.settings.autoHideAmount)!="undefined"&&!isNaN(frontEnd.settings.autoHideAmount)){hideAmount=parseInt(frontEnd.settings.autoHideAmount);}
$("#inside_clickToCallChatTab").show();var tabWidth=$("#inside_clickToCallChatTab").width();if(tabPosition=="left"||tabPosition=="topleft"||tabPosition=="bottomleft"){$("#inside_clickToCallChatTab").animate({left:-tabWidth+ hideAmount},time);}else{if(time!=0){$("#inside_clickToCallChatTab").animate({right:-tabWidth+ hideAmount},time);}else{$("#inside_clickToCallChatTab").show();setTimeout(function(){$("#inside_clickToCallChatTab").css({right:-tabWidth+ hideAmount});},0);}}
if(insideChat.chatPaneOpen){$("#inside_clickToCallChatTab").hide();}}//#endregion ClickToCall
var chatSurveyQueue=[];frontEnd.chatSurveyQueue=chatSurveyQueue;function checkChatSurveyQueue(){if(typeof(insideFrontInterface)=="undefined"||typeof(insideFrontInterface.openChatPane)=="undefined"){setTimeout(checkChatSurveyQueue,1000);return;}
if(chatSurveyQueue.length>0){if(chatSurveyQueue[0].settings.containerid=="#inside_chatWindow, #inside_chatPane_custom_chatHolder"){insideFrontInterface.openChatPane(true);if($("#inside_chatWindow, #inside_chatPane_custom_chatHolder").length>0){insideChat.clearChatGrouping();for(var i=0;i<chatSurveyQueue.length;i++){showSurveyPopup(chatSurveyQueue[i]);}}else{setTimeout(checkChatSurveyQueue,1000);}}else{for(var i=0;i<chatSurveyQueue.length;i++){showSurveyPopup(chatSurveyQueue[i]);}}}}
function showSurveyPopup(data){if($("#insideSurveyHolder").length>0){return;}
if(typeof(data.settings.bg)=="undefined"||data.settings.bg==""){data.settings.bg="#ffffff";}
var popup=visitorNotify(data);_insideGraph.initModule("survey");showSurvey(data,popup);}
function showSurvey(data,popup){var surveyInChat=data.settings.containerid=="#inside_chatWindow, #inside_chatPane_custom_chatHolder";if((surveyInChat&&insideFrontInterface.chatReady!=true)||(surveyInChat&&$("#inside_chatWindow, #inside_chatPane_custom_chatHolder").length==0)){setTimeout(function(){insideFrontInterface.openChatPane(true);},1000);setTimeout(function(){showSurvey(data);},1500);return;}
insideSurvey.showSurvey(data.settings.surveyid,"insideSurveyHolder",function(){if(data.type=="survey"&&typeof(data.settings.headercolour)!="undefined"&&data.settings.headercolour!=""){popup.find(".inside_siteLogo").css("background",data.settings.headercolour);if(getBrightnessOfColour(data.settings.headercolour)<120){popup.find(".inside_closeButton").css("color","white");}}else{if(typeof(frontEnd.settings.headerColour)!="undefined"&&frontEnd.settings.headerColour!=null&&frontEnd.settings.headerColour!=""){popup.find(".inside_siteLogo").css("background",frontEnd.settings.headerColour);}
if(data.type=="survey"&&typeof(frontEnd.settings.iconColour)!="undefined"&&frontEnd.settings.iconColour!=""){popup.find(".inside_closeButton").css("color",frontEnd.settings.iconColour);}}
if(popup.length==0){return;}
popup[0].ready=true;slideInPopup(popup);if(data.type=="chatsurvey"){if(data.settings.containerid=="#inside_chatWindow, #inside_chatPane_custom_chatHolder"){insideChat.scrollChatToBottom();insideFrontInterface.openChatPane(true);}else{insideChat.closeChatPane();}}},data);if(typeof(data.settings.headercolour)!="undefined"&&data.settings.headercolour!=""){var styleString="";if(data.settings.headercolour!="#ffffff"&&data.settings.headercolour!="white"){styleString+=".inside_visitorNotify.survey{border-bottom: 6px solid "+ data.settings.headercolour+"}";}else{styleString+=".inside_visitorNotify.survey{border-bottom: 1px solid #cccccc}";}
if(getBrightnessOfColour(data.settings.headercolour)>150){styleString+="#insideSurveyHolder p.insideSurveyQuestion{ color: black; }";}
addStyleString(styleString,"insideSurvey");}}
function getBrightnessOfColour(colourString){var rgb=[];if(colourString.indexOf("#")==0){rgb.push(parseInt(colourString.substr(1,2),16));rgb.push(parseInt(colourString.substr(3,2),16));rgb.push(parseInt(colourString.substr(5,2),16));}else{var rgba=colourString.split("(")[1].split(")")[0].split(",");rgb.push(parseInt(rgba[0],16));rgb.push(parseInt(rgba[1],16));rgb.push(parseInt(rgba[2],16));}
return 0.213*rgb[0]+ 0.715*rgb[1]+ 0.072*rgb[2];}
frontEnd.users={};function updateUsers(data){for(var i=0;i<data.length;i++){if(typeof(frontEnd.users[data[i].id])!="undefined"&&frontEnd.users[data[i].id]!=null){$(frontEnd.users[data[i].id]).extend(data[i]);}else{frontEnd.users[data[i].id]=data[i];}}}
function removeUsers(arr){for(var i=0;i<arr.length;i++){delete frontEnd.users[arr[i]];}}
frontEnd.bindEvent=bindEvent;function bindEvent(eventName,callback){$.inside.bind(eventName,callback);}
var assistantData=[];frontEnd.settings={};var alreadyConnected=false;var altChatSettingsSet=false;var savedAltChat;function connectedToInside(data){checkForCoBrowseRequestState();if(alreadyConnected){frontEnd.settingsData=data;frontEnd.users=[];if(typeof(frontEnd.settingsData.users)!="undefined"&&frontEnd.settingsData.users.length>0){updateUsers($.inside.front.settingsData.users);}
return;}
alreadyConnected=true;if(typeof(data.settings)!="undefined"&&data.settings!=null)
frontEnd.settings=data.settings;frontEnd.settingsData=data;frontEnd.nickname=data.nickname;if(typeof(frontEnd.settings.insideCustomCSSFile)!="undefined"){loadCssFile(_insideCDN+ frontEnd.settings.insideCustomCSSFile,windowScale);}
if(altChatSettingsSet==true){$.extend(frontEnd.settings,savedAltChat.chatTab);$.extend(frontEnd.settings,savedAltChat.chatPane);$.extend(frontEnd.settings.offlineChat,savedAltChat.offline);$.inside.front.settingsData.settings=frontEnd.settings;frontEnd.settings.useCustomChatPane=(savedAltChat.chatPane.useCustomChatPane==true);}else{if(typeof(data.chatSettings)!="undefined"&&data.chatSettings!=null){savedAltChat=data.chatSettings;altChatSettingsSet=true;$.extend(frontEnd.settings,data.chatSettings.chatTab);$.extend(frontEnd.settings,data.chatSettings.chatPane);$.extend(frontEnd.settings.offlineChat,data.chatSettings.offline);frontEnd.settings.useCustomChatPane=(data.chatSettings.chatPane.useCustomChatPane==true);}}
if(window.top!=window){if(frontEnd.settings.allowIframes!=true){$("#inside_holder").remove();return;}}
setTabPosition();if(typeof(frontEnd.settings.autohideTabs)!="undefined"&&frontEnd.settings.autohideTabs==true){if(device==2&&frontEnd.settings.autohideOnMobile!=true){}else{startautohideTabs();}}
if(typeof(frontEnd.settings.selectedTheme)!="undefined"){setColourTheme(frontEnd.settings.selectedTheme);}
if(typeof(frontEnd.settingsData.users)!="undefined"&&frontEnd.settingsData.users.length>0){updateUsers($.inside.front.settingsData.users);}
if(frontEnd.settings.chatEnabled==false){chatEnabled=false;}
if(chatEnabled){loadInsideChat();}
if(!isLocal()){setTimeout(function(){$.inside.flushQueue();},1000);}}
function setTabPosition(){if(device==2&&typeof(frontEnd.settings.mobileTabPosition)!="undefined"){tabPosition=frontEnd.settings.mobileTabPosition;}else if(typeof(frontEnd.settings.tabPosition)!="undefined"){tabPosition=frontEnd.settings.tabPosition;}
tabYScale=0.5;if(tabPosition.indexOf("right")!=-1){$("#inside_tabs").css("right","0px");}else{$("#inside_tabs").css("right","none");}
if(tabPosition=="left"){$("#inside_tabs").css("left","0px");$("#inside_tabs").css("text-align","left");tabYScale=0.5;}
if(tabPosition=="topleft"){$("#inside_tabs").css("left","0px");$("#inside_tabs").css("text-align","left");tabYScale=0.0;}
if(tabPosition=="bottomleft"){$("#inside_tabs").css("left","0px");$("#inside_tabs").css("text-align","left");tabYScale=1.0;}
if(tabPosition=="topright"){tabYScale=0.0;}
if(tabPosition=="bottomright"){tabYScale=1.0;}}
function unquoteAttr(s,preserveCR){return(''+ s).split('&amp;').join("&").replace(/&apos;/g,'').replace(/&quot;/g,'"').replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&#10;/g,"\r\n").replace(/&#39;/g,'"').replace(/&nbsp;/g,' ').replace(/&#13;/g,"\r\n");}
var dayNames=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];var dayNamesShort=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];var dayNamesMin=['Su','Mo','Tu','We','Th','Fr','Sa'];function checkMacros(str){var d=new Date();str=str.split("{dayofweek}").join(dayNames[d.getDay()]);return str;}
function loadFacebookSDK(){(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}
js=d.createElement(s);js.id=id;js.src='//connect.facebook.net/en_US/sdk.js';fjs.parentNode.insertBefore(js,fjs)}(document,'script','facebook-jssdk'));}
window.fbAppId=window.fbAppId||"";window.fbQueue=window.fbQueue||[];window.fbReady=window.fbReady||false;window.fbAsyncInit=window.fbAsyncInit||function(){FB.init({appId:window.fbAppId,xfbml:false,version:'v2.5'});window.fbReady=true;window.processFbQueue();};window.processFbQueue=window.processFbQueue||function(){if(!window.fbReady){return;}
if(window.fbQueue.length===0){return;}
fbQueue.shift().call(window);setTimeout(window.processFbQueue,0);};frontEnd.visitorNotify=visitorNotify;function visitorNotify(data){if(data.settings.gatrack==true){_insideGraph.jQuery.inside.ga({category:"Inside : Popup",action:"View",value:data.eventId,label:data.settings.galabel});}
if($(".inside_visitorNotify[position='"+ data.settings.position+"']").length>0){if(typeof(data.settings.useContainer)=="undefined"||!data.settings.useContainer){return;}}
var popup=$("<div class='inside_visitorNotify'><div class='inside_closeButton fonticon icon-hclose'></div></div>").appendTo("#inside_holder");if(typeof(data.settings.header)!="undefined"&&data.settings.header!=""){popup.append("<div class='inside_visitorNotify_header'>"+ escapeHtml(data.settings.header)+"</div>");}
if(data.type=="survey"){popup.find(".inside_closeButton").hide().delay(10000).fadeIn();}
if(typeof(data.stickyid)!="undefined"&&data.stickyid>0){popup.attr("stickyid",data.stickyid);}
popup.find(".inside_closeButton").click(function(){if(false&&popup.hasClass("survey")){if(popup[0].minimised){popup.find(".inside_notify_content_message").show();popup.find(".inside_minimised_text").remove();popup[0].minimised=false;popup.removeClass("minimised");slideInPopup(popup);}else{popup.find(".inside_notify_content_message").hide();popup[0].minimised=true;popup.addClass("minimised");popup.find(".inside_minimised_text").remove();$("<div class='inside_minimised_text'>survey</div>").prependTo(popup.find(".inside_notify_content")).css("background",popup.find(".inside_notify_content").css("background"));$(".inside_windowBlack").remove();scalePopups();}}else{if(popup.attr("stickyid")!=null){$.inside.server.unstick(popup.attr("stickyid"));}
popup.remove();}
$("#inside_holder .inside_windowBlack").remove();});popup[0].data=data;viewport=getViewPortSize();popup.attr("position",data.settings.position);if(data.settings.position=="1"||data.settings.position=="4"||data.settings.position=="7"){popup.css("left","10px");}else if(data.settings.position=="3"||data.settings.position=="6"||data.settings.position=="9"){popup.css("left","auto");popup.css("right","10px");}else{popup.css("left","10px");}
scalePopups();var popupMainContent=$("<div class='inside_notify_content'></div>").appendTo(popup);if(typeof(data.settings.content)!="undefined"&&data.settings.content=="iframe"){var w=300;var h=300;if(data.settings.iframeWidth!=""){w=data.settings.iframeWidth;}
if(data.settings.iframeHeight!=""){h=data.settings.iframeHeight;}
var iframeScroll="no";if(data.settings.iframeScroll)
iframeScroll="yes";popupMainContent.append("<div class='inside_notify_content_message'><iframe class='inside_popup_iframe' scrolling='"+ iframeScroll+"' src='"+ data.settings.iframe+"' width='"+ w+"' height='"+ h+"' scrollable='no'></iframe></div>");}else if(typeof(data.settings.content)!="undefined"&&data.settings.content=="fbchat"){var fbAppId=data.fbAppId;if(!fbAppId){popup.remove();return;}
window.fbAppId=fbAppId;var color=data.settings.fbBtnColor||"blue";var size=data.settings.fbBtnSize||"large";popup.addClass("fb-chat");popup.hide();popup[0].ready=false;var processPopup=function(){var content="<div class='fb-messengermessageus' color='"+ color+"' size='"+ size+"' messenger_app_id='"+ fbAppId+"'></div>";popupMainContent.empty().append(content);FB.XFBML.parse(popup[0],function(){popup[0].ready=true;scalePopups();slideInPopup(popup);});};window.fbQueue.push(processPopup);loadFacebookSDK();window.processFbQueue();}else if(typeof(data.settings.content)!="undefined"&&data.settings.content=="fbretail"){var fbAppId=data.fbAppId;if(!fbAppId){popup.remove();return;}
window.fbAppId=fbAppId;var inAcc=data.inAcc;if(!inAcc){popup.remove();return;}
var inMid=data.inMid;if(!inMid){popup.remove();return;}
var fbToken=data.fbToken;popup.addClass("fb-retail");popup.hide();popup[0].ready=false;var processPopup=function(){function processPopup2(response){popup.hide();popup[0].ready=false;var loggedIn=response.status==='connected'||response.status==='not_authorized';var content=!loggedIn?"<table style='padding:0px 10px 0px 10px; text-align:left;'><tr><td><div class='fb-login-button' data-max-rows='1' data-size='large' data-show-faces='false' data-auto-logout-link='false'></div><//td><td>Please log in to recieve<br/>retail notifications.</td></tr></table>":!fbToken?"<div class='fb-messengerbusinesslink' messenger_app_id='"+ fbAppId+"' state='acc:"+ inAcc+";mid:"+ inMid+"'></div>":"<div class='fb-messengertoggle' messenger_app_id='"+ fbAppId+"' token='"+ fbToken+"'></div>";popupMainContent.empty().append(content);FB.XFBML.parse(popup[0],function(){popup[0].ready=true;scalePopups();slideInPopup(popup);});}
FB.getLoginStatus(function(response){processPopup2(response);FB.Event.subscribe('auth.statusChange',function(response){processPopup2(response);});});};window.fbQueue.push(processPopup);loadFacebookSDK();window.processFbQueue();}else{if(typeof(data.settings.message)!="undefined"&&data.settings.message!=""){data.settings.message=data.settings.message.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi," ");data.settings.message=checkMacros(data.settings.message);popupMainContent.append("<div class='inside_notify_content_message'>"+ data.settings.message+"</div>");}}
if(typeof(data.settings.footer)!="undefined"&&data.settings.footer!=null&&data.settings.footer!=""){popup.append("<div class='inside_visitorNotify_footer'>"+ escapeHtml(data.settings.footer)+"</div>");}
if(typeof(data.settings.headercolour)!="undefined"&&data.settings.headercolour!=""){popup.find(".inside_visitorNotify_header").css("background",data.settings.headercolour);popup.find(".inside_visitorNotify_footer").css("background",data.settings.headercolour);if(popup.find(".inside_visitorNotify_header").length>0){if(data.settings.headercolour=="rgba(0, 0, 0, 0)"||getBrightnessOfColour(data.settings.headercolour)>150){popup.find(".inside_visitorNotify_header").css("color","black");popup.find(".inside_visitorNotify_footer").css("color","black");}else{popup.find(".inside_closeButton").css("color","white");}}}else{if(typeof(data.settings.header)!="undefined"&&data.settings.header!=""){popup.find(".inside_closeButton").removeClass("uni").css("color","white");}else{popup.find(".inside_closeButton").addClass("uni");}}
if(popup.find(".inside_visitorNotify_header").length==0){if(typeof(data.settings.bg)!="undefined"&&data.settings.bg!=""){if(data.settings.bg=="rgba(0, 0, 0, 0)"||getBrightnessOfColour(data.settings.bg)>150){popup.find(".inside_closeButton").css("color","black");}else{popup.find(".inside_closeButton").css("color","white");}}else{popup.find(".inside_closeButton").css("color","uni");}
if(data.type!="survey"&&data.type!="chatsurvey"){popup.find(".inside_notify_content_message").css("margin-right","20px");}
if(data.settings.content=="iframe"){if(data.settings.iframeCloseOverFrame){popup.find(".inside_notify_content_message").css("margin-right","0px");}else{popup.find(".inside_notify_content_message").css("margin-right","20px");}
popup.find(".inside_notify_content_message").css("padding","0px");}}
if(typeof(data.settings.bg)!="undefined"&&data.settings.bg!=""){popup.css("background",data.settings.bg);if(getBrightnessOfColour(data.settings.bg)<120){popup.find(".inside_notify_content_message").css("color","white");}}
if(typeof(data.settings.border)!="undefined"&&!data.settings.border){popup.css("box-shadow","none");popup.css("-moz-box-shadow","none");popup.css("-webkit-box-shadow","none");popup[0].margin=0;}else{if(device!=2){popup[0].margin=10;}else{popup[0].margin=0;}}
if(typeof(data.settings.link)!="undefined"&&data.settings.link!=""&&data.settings.link!="none"){if(data.settings.link=="openChat"){popup.css("cursor","pointer");popup.click(function(){if($("#inside_liveChatTab").length>0){$("#inside_liveChatTab").click();}
insideAPI.call("visitoraction",{type:"event",id:data.eventId,action:"click"},null,null,false);});}else{var link=$("<a href='"+ data.settings.link+"' />");link.click(function(e){$.inside.server.unstick(popup.attr("stickyid"));insideAPI.call("visitoraction",{type:"event",id:data.eventId,action:"click"},null,null,false);})
if(data.settings.gatrack==true){link.click(function(e){$.inside.ga({category:"Inside : Popup",action:"Click",value:data.eventId,label:data.settings.galabel});});}
popup.contents(":not('.inside_closeButton')").appendTo(link);popup.append(link);}}else{if(data.type!="survey"&&data.type!="chatsurvey"){popup.click(function(){insideAPI.call("visitoraction",{type:"event",id:data.eventId,action:"click"},null,null,false);return;var sel=getSelectionText();if(sel==""){insideAPI.call("visitoraction",{type:"event",id:data.eventId,action:"click"},null,null,false);if(popup.attr("stickyid")!=null)
$.inside.server.unstick(popup.attr("stickyid"));popup.remove();$(".inside_windowBlack").remove();}});}}
if(typeof(data.settings.useContainer)!="undefined"&&data.settings.useContainer){var container;if(typeof(data.settings.containerid)!="undefined"&&data.settings.containerid!=""){if(data.settings.containerid.indexOf(".")==0||data.settings.containerid.indexOf("#")==0){container=$(data.settings.containerid);}else{container=$("#"+ data.settings.containerid);}}else{container=$(".inside_notify_container");}
if(container.length==0){popup.remove();return;}
if(data.settings.replaceContainer==true){container.empty();}
popup.appendTo(container).css({position:"initial",top:0,left:0});popup.removeClass("position");popup.addClass("embed");popup.find(".inside_closeButton").remove();}
if(typeof(data.settings.image)!="undefined"&&data.settings.image!=""){var img;var image=data.settings.image;var usingHighDPI=false;if(typeof(data.settings.imageHighDPI)!="undefined"&&data.settings.imageHighDPI!=""){var pr=1;try{pr=window.devicePixelRatio;}catch(e){}
if(pr>=1){image=data.settings.imageHighDPI;usingHighDPI=true;}}
if(image.indexOf("http")=="0"){img=$("<img src='"+ image+"' />").appendTo(popupMainContent);}else{img=$("<img src='"+ _insideCDN+"/custom/"+ image+"' />").appendTo(popupMainContent);}
if(usingHighDPI){img.css("max-width",data.settings.imageNormalDPIWidth);}
positionPopupOffScreen(popup);popup.hide();popup[0].ready=false;img.load(function(){popup[0].ready=true;popup.show();setTimeout(function(){positionPopupOffScreen(popup);scalePopups();slideInPopup(popup);},0);if(usingHighDPI){scaleImageByPixelRatio(img);}});}else{positionPopupOffScreen(popup);if(data.type=="survey"){popup.addClass("survey");popup[0].ready=false;}else if(data.type=="visitornotify"&&(data.settings.content=="fbchat"||data.settings.content=="fbretail")){popup[0].ready=false;}else{if($(popup).find("img").length==0){slideInPopup(popup);}else{popup.find("img").each(function(){$(this).load(function(){positionPopupOffScreen(popup);slideInPopup(popup);});if($(this).attr("src")==""){slideInPopup(popup);}})}}}
if(typeof(data.settings.popupCountdown)!="undefined"){setTimeout(function(){popup.fadeOut(500,function(){$(this).remove();$(".inside_windowBlack").remove();})},parseInt(data.settings.popupCountdown)*1000);}
if(typeof(data.settings.showCountdown)!="undefined"&&data.settings.showCountdown==true){$("<div class='inside_popup_countdown'>"+ data.settings.popupCountdown+"</div>").appendTo(popup);var countdownInt=setInterval(function(){var cd=popup.find(".inside_popup_countdown");cd.text((parseInt(cd.text())- 1));},1000)}
return popup;}
function getSelectionText(){var text="";if(window.getSelection){text=window.getSelection().toString();}else if(document.selection&&document.selection.type!="Control"){text=document.selection.createRange().text;}
return text;}
function scaleImageByPixelRatio(img){var pr=1;try{pr=window.devicePixelRatio;}catch(e){}
if(device==2&&pr>1){var viewport=getViewPortSize();var w=$(img).width();w=w*(1/pr);if(device==2){if(w<viewport.width*0.3){w=viewport.width*0.3;}}
img.css("width",w+"px");}}
function roundToPlaces(num,places){places=places<0?0:places;var mult=Math.pow(10,places);return Math.round(num*mult)/ mult}
function slideInPopup(popup){var viewport=getViewPortSize();var data=popup[0].data;var margin=popup[0].margin;var easing="easeOutQuad";popup.show();positionPopupOffScreen(popup);popup[0].shown=false;setTimeout(function(){setTimeout(function(){popup[0].shown=true;},1000);if(data.settings.position=="8"||data.settings.position=="2"||data.settings.position=="5"){popup.css("left",(viewport.width- popup.outerWidth())*0.5);}
if(data.settings.position=="1"||data.settings.position=="2"||data.settings.position=="3"){popup.css("top",-popup.outerHeight()+"px").animate({"top":margin},{duration:500,easing:easing});}else if(data.settings.position=="7"||data.settings.position=="8"||data.settings.position=="9"){popup.css("top",viewport.height+"px").animate({"top":viewport.height- popup.outerHeight()- margin},{duration:500,easing:easing});}else if(data.settings.position=="4"){popup.css("left",-popup.outerWidth()+"px").animate({"left":margin},{duration:500,easing:easing});}else if(data.settings.position=="6"){popup.css("right",-popup.outerWidth()+"px").animate({"right":margin},{duration:500,easing:easing});}else{popup.css("top",(viewport.height- popup.outerHeight())*0.5+"px");popup.css("left","0px");popup.css("left",(viewport.width- popup.outerWidth())*0.5+"px");scalePopups();popup.hide().fadeIn();}
if(typeof(data.settings.windowcover)!="undefined"&&data.settings.windowcover){var alpha=0.75;if(typeof(data.settings.windowcoveralpha)!="undefined"){alpha=data.settings.windowcoveralpha;}
addWindowBlack(alpha,data.settings.windowcovercolour,function(){popup.remove();});}},1000)}
function positionPopupOffScreen(popup){var data=popup[0].data;var pos=data.settings.position;if(pos=="4"||pos=="5"||pos=="6"){popup.css("top",(viewport.height- popup.height())*0.5+"px");}
if(pos=="5"){popup.css("left",(viewport.width)+"px");}
if(pos=="2"||pos=="8"){popup.css("left",((viewport.width- popup.width())*0.5)+"px");}
if(pos=="1"||pos=="2"||pos=="3"){popup.css("top",-popup.outerHeight()+"px");}else if(pos=="7"||pos=="8"||pos=="9"){popup.css("top",viewport.height+"px");}else if(pos=="4"){popup.css("left",-popup.width()+"px");}else if(pos=="6"){popup.css("right",-popup.width()+"px");}else{popup.css("top",(viewport.height- popup.height())*0.5+"px");popup.css("left",(viewport.width)+"px");}
if(device==2){if(pos=="2"||pos=="5"||pos=="8"){if(viewport.width<500){popup.css("left","0px");}else{popup.css("left",(viewport.width)+"px");}}}}
function scalePopups(){var viewport=getViewPortSize();$(".inside_visitorNotify").not(".embed").each(function(){if($(this)[0].shown!=true){return;}
if(typeof($(this)[0].ready)!="undefined"&&$(this)[0].ready==false){return;}
var pos=$(this).attr("position");var margin=$(this)[0].margin;if(pos=="1"||pos=="2"||pos=="3"){$(this).css("top","10px");}
if(pos=="4"||pos=="5"||pos=="6"){$(this).css("top",(viewport.height- $(this).outerHeight())*0.5- margin+"px");}
if(pos=="7"||pos=="8"||pos=="9"){$(this).css("top",(viewport.height- $(this).outerHeight()- margin)*getViewportScale()+"px");}
if(pos=="2"||pos=="5"||pos=="8"){if(device==2||viewport.width<500&&(!$(this)[0].positioned||$(this)[0].positioned[0]!=viewport.width)){$(this).css("left","0px");var popup=this;setTimeout(function(){$(popup).css("opacity","1");$(popup).css("left",(viewport.width- $(popup).outerWidth())*0.5+"px");},0);}else{$(this).css("left",(viewport.width- $(this).outerWidth())*0.5+"px");}}
$(this)[0].positioned=[viewport.width,viewport.height];});}
function disconnected(){alreadyConnected=false;}
var scaleinit=false;var chatPaneMode="";var tabYScale=0.5;var viewport;frontEnd.windowScale=windowScale;function windowScale(animTime){if(typeof(animTime)=="undefined"){animTime=0;}
viewport=getViewPortSize();var top=viewport.height*tabYScale;if(device==3&&(navigator.platform.indexOf("iPhone")!=-1||navigator.platform.indexOf("iPad")!=-1)){top=viewport.height*tabYScale*getViewportScale();}
if(tabPosition=="left"||tabPosition=="right"){top-=$("#inside_tabs").height()*0.5;}
if(tabPosition=="bottomright"||tabPosition=="bottomleft"){top-=$("#inside_tabs").height();if($("#inside_tabs").is(":visible")&&!$("#inside_tabs").is(':empty')&&$("#inside_tabs").height()==0){setTimeout(windowScale,250);}}
if(animTime>0){$("#inside_tabs").stop(true).animate({top:top},animTime);}else{$("#inside_tabs").stop(true);if(!(ios&&!standalone&&safari&&$('input').is(":focus"))){$("#inside_tabs").css("top",top+"px");}}
$("#inside_holder").css("transform","translateZ(0px)");scalePopups();if(IE11Quirks){IE11ScrollFix();}}
insideFrontInterface.positionTabs=positionTabs;function positionTabs(){$("#inside_tabs div").each(function(){$(this).css({"left":"","right":""});if(tabPosition.indexOf("right")!=-1){if(frontEnd.settings.autohideTabs&&frontEnd.settings.autoHideTabOnLoad==true&&!(device==2&&frontEnd.settings.autohideOnMobile==false)){hideAmount=20;if(typeof(frontEnd.settings.autoHideAmount)!="undefined"&&!isNaN(frontEnd.settings.autoHideAmount)){hideAmount=parseInt(frontEnd.settings.autoHideAmount);}
$(this).css("right",hideAmount+"px");}else{if($(this).find("img").length>0)
$(this).css("right",$(this).find("img")[0].width);}}else{$(this).css("left",0);}})}
function updateOrientation(){this.orientation=window.orientation;if(this.orientation===undefined){if(document.documentElement.clientWidth>document.documentElement.clientHeight)this.orientation='landscape';else this.orientation='portrait';}
else if(this.orientation===0||this.orientation===180)this.orientation='portrait';else this.orientation='landscape';};frontEnd.getViewportScale=getViewportScale;function getViewportScale(){this.viewportScale=1;return this.viewportScale;if(device==1){return this.viewportScale;}
var viewportWidth=document.documentElement.clientWidth;this.screenWidth=screen.width;if(this.orientation==='portrait'){if(screen.width>screen.height)this.screenWidth=screen.height;}
else{if(screen.width<screen.height)this.screenWidth=screen.height;}
this.viewportScale=this.screenWidth/window.innerWidth;return this.viewportScale;}
var autoHideTimeout;function startautohideTabs(){$("#inside_tabs").off("mouseenter");$("#inside_tabs").off("mouseleave");$("#inside_tabs").on("touchmove","> div",autohideTabs);$("#inside_tabs").on("mouseenter","> div",hoverShowTabs);$("#inside_tabs").on("mouseleave","> div",function(){autoHideTimeout=setTimeout(autohideTabs,100);});$("#inside_tabs").off("load").on("load","img",autohideTabs);if(frontEnd.settings.autoHideTabOnLoad!=true){positionTabs();setTimeout(autohideTabs,3000);}else{autohideTabs(0);}}
frontEnd.autohideTabs=autohideTabs;function autohideTabs(time){$("#inside_tabs").show();if(typeof(time)=="undefined"){time=1000;}
var hideAmount=20;if(typeof(frontEnd.settings.autoHideAmount)!="undefined"&&!isNaN(frontEnd.settings.autoHideAmount)){hideAmount=parseInt(frontEnd.settings.autoHideAmount);}
$("#inside_tabs div").each(function(){if($(this).attr("autohide")){hideAmount=parseInt($(this).attr("autohide"));}
var tabImage=$(this).find("img");var tabWidth=tabImage.width();if(tabPosition=="left"||tabPosition=="topleft"||tabPosition=="bottomleft"){$(this).stop(true,true).animate({left:-tabWidth+ hideAmount},time);}else{if(time!=0){$(this).stop(true,true).animate({right:hideAmount},time);}else{$(this).stop(true,true).css({right:hideAmount});}}
$(this).attr("tabHidden","true");})
if(insideChat.chatPaneOpen||(insideFrontInterface.getAvailableAssistants&&insideFrontInterface.getAvailableAssistants().length==0)){}}
insideFrontInterface.hoverShowTabs=hoverShowTabs;function hoverShowTabs(_tab){if(typeof(_tab.currentTarget)=="undefined"){tab=_tab;}else{tab=$(this);}
clearTimeout(autoHideTimeout);if(tabPosition=="left"||tabPosition=="topleft"||tabPosition=="bottomleft"){tab.css("right","");tab.stop(true).animate({left:0},250);}else{tab.css("left","");tab.stop(true).animate({right:tab.width()},250);}
tab.removeAttr("tabHidden","true");}
frontEnd.getViewPortSize=getViewPortSize;function getViewPortSize(){updateOrientation();var viewportwidth;var viewportheight;if(typeof window.innerWidth!='undefined'){viewportwidth=window.innerWidth,viewportheight=window.innerHeight}
else if(typeof document.documentElement!='undefined'&&typeof document.documentElement.clientWidth!='undefined'&&document.documentElement.clientWidth!=0){viewportwidth=document.documentElement.clientWidth,viewportheight=document.documentElement.clientHeight}
else{viewportwidth=document.getElementsByTagName('body')[0].clientWidth,viewportheight=document.getElementsByTagName('body')[0].clientHeight}
if(device!=1){var zoomLevel=document.documentElement.clientWidth/window.innerWidth;viewportheight=viewportheight/zoomLevel;}
return{width:viewportwidth,height:viewportheight};}
frontEnd.setCookie=setCookie;frontEnd.getCookie=getCookie;function getCookie(c_name){var i,x,y,ARRcookies=document.cookie.split(";");for(i=0;i<ARRcookies.length;i++){x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+ 1);x=x.replace(/^\s+|\s+$/g,"");if(x==c_name){return unescape(y);}}}
function setCookie(c_name,value,exdays){var exdate=new Date();exdate.setDate(exdate.getDate()+ exdays);var c_value=escape(value)+((exdays==null)?"":"; expires="+ exdate.toUTCString());document.cookie=c_name+"="+ c_value;}
function trace(){var arr=Array.prototype.slice.call(arguments);if(typeof(console)!="undefined"){console.log(arr.join(","));}}
frontEnd.loadCssFile=loadCssFile;function loadCssFile(url,callback){_insideGraph.loadCSS(url,callback);}
function loadInsideChat(callback){_insideGraph.initModule("chat");if(typeof(callback)=="function")
callback();}
function insideChatLoaded(){}
frontEnd.loadIntlTelInputPlugin=loadIntlTelInputPlugin;function loadIntlTelInputPlugin(callback){loadCssFile(_insideCDN+"/css/intlTelInput.css?"+ _insideScriptVersion);}
frontEnd.setColourTheme=setColourTheme;function setColourTheme(theme){if(typeof(theme)=="undefined"){return;}
theme=parseInt(theme);if(theme==1){loadCssFile(_insideCDN+"/css/inside.front.theme_neutral.css?"+ _insideScriptVersion);}
if(theme==2){loadCssFile(_insideCDN+"/css/inside.front.theme_dark.css?"+ _insideScriptVersion);}
if(theme==3){loadCssFile(_insideCDN+"/css/inside.front.theme_cream.css?"+ _insideScriptVersion);}}
frontEnd.loadJS=loadJS;function loadJS(src,instance,onload){var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('src',src);if(onload)loadCallback(instance,onload);var head=document.getElementsByTagName('head')[0];if(head){head.appendChild(s);}else{document.body.appendChild(s);}}
function loadCallback(check,callback){var interval=setInterval(function(){if(eval(check)){clearInterval(interval);callback();}},10);}
frontEnd.addWindowBlack=addWindowBlack;function addWindowBlack(opacity,colour,closeCallback){if(typeof(opacity)=="undefined"||opacity==null){opacity=0.75;}
$(".inside_windowBlack").remove();var viewport=getViewPortSize();var $windowBlack=$("<div class='inside_windowBlack'><div>").appendTo("#inside_holder");$windowBlack.width(viewport.width+"px");$windowBlack.height(viewport.height+"px");$windowBlack.css("opacity",opacity);if(typeof(colour)!="undefined"&&typeof(colour)!=null){$windowBlack.css("background",colour)}
$windowBlack.show();$windowBlack.click(function(){$windowBlack.remove();if(typeof(closeCallback)=="function"){closeCallback();}});}
frontEnd.addStyleString=addStyleString;function addStyleString(str,id){if(typeof(id)!="undefined"){$("style#"+ id).remove();}
var styleNode=$("<style>"+ str+"</style>").appendTo("body");if(typeof(id)!="undefined"){styleNode.attr("id",id);}}
function escapeHtml(str){var div=document.createElement('div');div.appendChild(document.createTextNode(str));return div.innerHTML;};insideFrontInterface.closePopups=closePopups;function closePopups(){$(".inside_windowBlack").remove();$(".inside_visitorNotify").remove();}
$.inside.front=frontEnd;$(document).ready(init);})(window,isLocal()?$:_insideGraph.jQuery)});function isLocal(){return window.location.href.indexOf("file://")==0;}
var insideAPI={};(function(window,$){$.xhrPool=[];$.xhrPool.abortAll=function(){$(this).each(function(idx,jqXHR){jqXHR.abort();});$.xhrPool=[];};$.ajaxSetup({beforeSend:function(jqXHR){$.xhrPool.push(jqXHR);},complete:function(jqXHR){var index=$.xhrPool.indexOf(jqXHR);if(index>-1){$.xhrPool.splice(index,1);}}});function call(callName,args,success,error,async,type,dataType,contentType){if(typeof(async)=="undefined"){async=true;}
if(typeof(type)=="undefined"){type="POST";}
if(typeof(dataType)=="undefined"){dataType="json";}
if(typeof(contentType)=="undefined"){contentType="application/json";}
var urlVariables="";if(typeof(_insideGraph)!="undefined"){var accesskey=_insideGraph.current.account+":"+ _insideGraph.pid+":"+ _insideGraph.accessKey;var accesscheck=_insideGraph.accessCheck;args.accesskey=accesskey;args.accesscheck=accesscheck;var ob={};ob.accesskey=accesskey;ob.accesscheck=accesscheck;urlVariables=$.param(ob);}
var urlpath;if(typeof(_insideSocialUrl)!="undefined"){urlpath=_insideSocialUrl;}else{urlpath=_insideLive;}
var _preIE10=(function(){if(navigator.appVersion.indexOf("MSIE")!=-1){var v=parseFloat(navigator.appVersion.split("MSIE")[1]);if(v<10)
return true;}
return false;})();if(_preIE10&&'XDomainRequest'in window&&window.XDomainRequest!==null){var xdr=new XDomainRequest();xdr.open('post',urlpath+"api/api_call.aspx?call="+ callName+((urlVariables!="")?"&"+urlVariables:""));xdr.onload=function(){var dom=new ActiveXObject('Microsoft.XMLDOM');var response=$.parseJSON(xdr.responseText);dom.async=async;if(response==null||typeof(response)=='undefined'){response=$.parseJSON(data.firstChild.textContent);}
if(typeof(success)=="function"){success(response);}};xdr.onerror=function(){_result=false;};args=JSON.stringify(args);xdr.onprogress=function(){};xdr.ontimeout=function(){};setTimeout(function(){xdr.send(args);},0);return xdr;}
else if(navigator.userAgent.indexOf('MSIE')!=-1&&parseInt(navigator.userAgent.match(/MSIE ([\d.]+)/)[1],10)<8){return false;}
else{if(type=="POST"){args=JSON.stringify(args);}
var headers={};if(typeof(_insideCsrfToken)!="undefined"&&_insideCsrfToken!=null)
headers['RequestVerificationToken']=_insideCsrfToken;var apiCall=$.ajax({type:type,url:urlpath+"api/api_call.aspx?call="+callName+((urlVariables!="")?"&"+urlVariables:""),dataType:dataType,data:args,contentType:contentType,async:async,headers:headers,success:function(response){if(!_insideIsLive){if(typeof($.inside)!="undefined"&&$.inside.logging==true){try{console.info("API CALL '"+ callName+"' success");console.info("API args: ",args);console.info(response);}catch(e){console.log("API CALL '"+ callName+"' success");console.log("API args: ",args);console.log(response);}}}
if(typeof(success)=="function"){success(response);}},error:function(response){if(!_insideIsLive){console.log(response);}
if(typeof(error)=="function"){try{console.log("API CALL '"+ callName+"' error");console.log("API args: ",urlVariables)
error(response);}catch(e){}}}});return apiCall;}}
function post(call,data,success,error){if(typeof(error)=="undefined"){error=null;}
return $.ajax({type:"post",url:_insideLive+ call,dataType:"json",data:JSON.stringify(data),contentType:"application/json",async:true,success:success,error:error});}
insideAPI.call=call;insideAPI.post=post;})(window,typeof(_insideGraph)!="undefined"?_insideGraph.jQuery:$)
var insideChat={};_insideGraph.registerModule("chat",function(){console.log("INSIDE Module 'chat' started");(function(window,$){var jQuery=$;var language="en";var useMinimiseButton=false;function placeholderIsSupported(){var test=document.createElement('input');return('placeholder'in test);}
function escapeHtml(str){var div=document.createElement('div');div.appendChild(document.createTextNode(str));return div.innerHTML;}
function buildTag(tag,attribs,html){var elem=$(document.createElement(tag));for(var key in attribs){elem.attr(key,attribs[key]);}
if(html)
return elem[0].outerHTML;return elem;}
var storeImageShown=false;var checkMessageReceived=true;var connected=false;var displaytype=2;var startOffline=true;var chatInProgress=false;var activeChat=false;insideFrontInterface.activeChat=true;var thisUserId="";insideChat.thisUserId=thisUserId;var alertInterval;var availableAssistants=[];var userList=[];var userArray=[];insideChat._hubLoaded=false;var settings={};var currentChatId="";insideFrontInterface.currentChatId="";var addedPaddingDiv=false;var types=["user_blue","assist","user_green","user_orange","user_purple"];var descs=[];descs.push("This is another user on the site");descs.push("Site Assistant");descs.push("This user is on the same page as you.");var currentTip=0;var initialText="Type here to chat with an assistant";var addedMessages=[];var insideDisabled=false;var insideStoreHidden=false;var typing=false;var device;var prevWindowScroll=0;var customimageurl=_insideCDN+"custom/";var chatPanePosition="middle";var useCustomChatPane=false;var tabPosition="right";var iOSMobile=(navigator.userAgent.match(/(iPhone|iPod)/g)?true:false);var iOSChrome=iOSMobile&&(navigator.userAgent.match('CriOS')?true:false);var iOSSafari=!iOSChrome&&(navigator.userAgent.match('Version')?true:false);if(iOSMobile){$("#inside_holder").addClass("iOSMobile");}
if(iOSChrome){$("#inside_holder").addClass("iOSChrome");}
if(iOSSafari){$("#inside_holder").addClass("iOSSafari");}
var standalone=window.navigator.standalone,userAgent=window.navigator.userAgent.toLowerCase(),safari=/safari/.test(userAgent),ios=/iphone|ipod|ipad/.test(userAgent);insideChat.init=init;function init(){insideFrontInterface.chatReady=true;device=$.inside.front.getDevice();if(typeof(socialCommerce)!="undefined"){socialCommerce.init(j);}
device=device;connectedToInside($.inside.front.settingsData);if(isLocal()){startInsideChat();}
setTimeout(setIE7Quirks,1000);bindInsideEvents();setSetStyle();scaleChatInput();if(availableAssistants.length>0){setOnlineChatTab();if(isLocal()){setTimeout(function(){$("#inside_liveChatTab").show();},200);}
$("#inside_tabs").animate({right:0},500);setTimeout($.inside.front.windowScale,0);}else{if(isLocal()){settings.offlineChat={enabled:true};}
setOfflineChatTab();}
if(settings==null)settings={};setTimeout(function(){getAssistants();if(typeof($.inside.front.settingsData.offers)!="undefined"&&$.inside.front.settingsData.offers.length>0){offerReceived($.inside.front.settingsData.offers);}
if(typeof($.inside.front.settingsData.chats)!="undefined"&&$.inside.front.settingsData.chats.length>0){chatReceived($.inside.front.settingsData.chats,"connected");}},1)
if(isLocal()){setTimeout(function(){$("#inside_chatInput").prop("disabled",false);},2);}
$('.inside_chatPane').on('hover',function(){$(this).css('overflow','hidden').height();});hideInsideStore();var supportsOrientationChange="onorientationchange"in window,orientationEvent=supportsOrientationChange?"orientationchange":"resize";try{window.addEventListener(orientationEvent,orientationChange,false);}catch(e){}
if(device==2){$(window).bind('hashchange',hashChange);}}
var chatTabMode="offline";function setChatTab(t){if(availableAssistants.length>0){if((typeof(t)!="undefined"&&!t)||!startOffline){checkMessageReceived=false;startOffline=false;}
else{checkMessageReceived=true;}
setOnlineChatTab();setJoinTheCallTag(false);$("#inside_clickToCallChatTab").show();}else{setOfflineChatTab();setJoinTheCallTag(true);}
setTimeout($.inside.front.windowScale,1000);}
var chatTabImageSet=false;var hideChatTab=false;var showTabForActive=false;insideChat.setOnlineChatTab=setOnlineChatTab;function setOnlineChatTab(t){if(useMinimiseButton){if(sessionStorage.getItem("insideChatClosed")=="True"){return;}}
if($("#inside_liveChatTab").length==0){addChatTab();$("#inside_liveChatTab").hide();}
if($(".inside_chatPane, #inside_chatPane_custom").is(":visible")){$("#inside_liveChatTab").hide();}
$("#inside_liveChatTab").removeClass("inside_chat_offline").addClass("inside_chat_online");chatTabMode="online";if(typeof(settings)!="undefined"&&settings!=null){if(device!=2){if(typeof(settings.customTabImage)!="undefined"&&settings.customTabImage!=""){$("#inside_liveChatTab .inside_chatTabImage").attr("src",customimageurl+ settings.customTabImage);}else{$("#inside_liveChatTab .inside_chatTabImage").attr("src",$.inside.front.imageurl+"livechaticon.png");}}else{if(typeof(settings.customMobileTabImage)!="undefined"&&settings.customMobileTabImage!=""){$("#inside_liveChatTab .inside_chatTabImage").attr("src",customimageurl+ settings.customMobileTabImage);}else{$("#inside_liveChatTab .inside_chatTabImage").attr("src",$.inside.front.imageurl+"livechaticon_mobile.png");}}}else{$("#inside_liveChatTab .inside_chatTabImage").attr("src",$.inside.front.imageurl+"livechaticon.png");}
if(typeof(settings)!="undefined"&&settings!=null&&typeof(settings.hideChatTab)!="undefined"&&settings.hideChatTab){hideChatTab=isTabHiddenOnThisDevice();if(hideChatTab)
$("#inside_liveChatTab").hide();}else{$("#inside_liveChatTab").show().css("opacity",1);}
if(typeof(settings)!="undefined"&&settings!=null&&typeof settings.hideChatTabOnDevices!="undefined"&&settings.hideChatTabOnDevices!=null){hideChatTab=false;if(settings.hideChatTabOnDevices.indexOf(device.toString())!=-1){hideChatTab=true;}}
if(typeof(settings)!="undefined"&&settings!=null&&typeof(settings.showTabForActive)!="undefined"&&settings.showTabForActive==true){showTabForActive=true;if(messageReceived){if(!chatPaneOpen){$("#inside_liveChatTab").show();insideFrontInterface.positionTabs();}
checkMessageReceived=false;}
if(messageReceived&&!checkMessageReceived&&$(".inside_chatPane, #inside_chatPane_custom").is(":visible")&&typeof(t)!="undefined"&&!t){$("#inside_liveChatTab").click(chatTabClick).hide();checkMessageReceived=false;}}
$("#inside_liveChatTab img").css("width","auto");$("#inside_liveChatTab").css("width","auto");scaleChatTab();if(settings.autohideTabs==true){}else{insideFrontInterface.positionTabs();}
if(chatPaneOpen){$("#inside_liveChatTab").hide();}}
function setInsideTab(){if(availableAssistants.length==0){setOfflineChatTab();}else{setOnlineChatTab();}}
function setOfflineChatTab(){chatTabMode="offline";$("#inside_liveChatTab").removeClass("inside_chat_online").addClass("inside_chat_offline");if(activeChat==false&&$(".inside_dept_selector.selected").length==0){closeChatPane();if(!hideChatTab&&settings&&settings.offlineChat&&settings.offlineChat.enabled&&(!settings.offlineChat.hideOfflineChatTab||!isOfflineTabHiddenOnThisDevice())){$("#inside_liveChatTab").show();}}
if(settings!=null&&settings&&settings.offlineChat&&settings.offlineChat.enabled==true&&(!settings.offlineChat.hideOfflineChatTab||!isOfflineTabHiddenOnThisDevice())){if($("#inside_liveChatTab").length==0){addChatTab();}
if(!$(".inside_chatPane, #inside_chatPane_custom").is(":visible")){$("#inside_liveChatTab").show();}
if(device!=2){if(typeof(settings.offlineChat.chatTab)!="undefined"&&settings.offlineChat.chatTab!=null&&settings.offlineChat.chatTab!=""){$("#inside_liveChatTab .inside_chatTabImage").attr("src",customimageurl+"/"+ settings.offlineChat.chatTab);}else{if(tabPosition=="topright"||tabPosition=="right"||tabPosition=="bottomright"){$("#inside_liveChatTab .inside_chatTabImage").attr("src",imageurl+"livechat_offline.png");}else{$("#inside_liveChatTab .inside_chatTabImage").attr("src",imageurl+"livechat_offline_left.png");}}}else{if(typeof(settings.offlineChat.chatTabMobile)!="undefined"&&settings.offlineChat.chatTabMobile!=null&&settings.offlineChat.chatTabMobile!=""){$("#inside_liveChatTab .inside_chatTabImage").attr("src",customimageurl+"/"+ settings.offlineChat.chatTabMobile);}else{$("#inside_liveChatTab .inside_chatTabImage").attr("src",imageurl+"livechaticon_mobile.png");}}}else{if(!chatInProgress){}
if(activeChat==false){$("#inside_holder #inside_liveChatTab").hide();}}
$("#inside_liveChatTab img").css("width","auto");$("#inside_liveChatTab").css("width","auto");setTimeout(scaleChatTab,0);if(!settings.autohideTabs){insideFrontInterface.positionTabs();}
if(activeChat&&settings.autohideTabs==true){setTimeout(function(){$.inside.front.autohideTabs(0);},1);setTimeout(function(){$.inside.front.autohideTabs(0);},250);}}
function setSetStyle(){$.fn.style=function(styleName,value,priority){var node=this.get(0);if(typeof node=='undefined'){return;}
var style=this.get(0).style;if(typeof styleName!='undefined'){if(typeof value!='undefined'){var priority=typeof priority!='undefined'?priority:'';style.setProperty(styleName,value,priority);}else{return style.getPropertyValue(styleName);}}else{return style;}}}
function isIE5Quirks(){return navigator.userAgent.indexOf("MSIE")!=-1&&typeof(document.documentMode)!="undefined"&&document.documentMode=="5";}
function isIE7Standards(){return navigator.userAgent.indexOf("MSIE")!=-1&&typeof(document.documentMode)!="undefined"&&document.documentMode=="7";}
function setIE7Quirks(){if(isIE7Standards()){var viewport=$.inside.front.getViewPortSize();$("#inside_holder").css("top","");$("#inside_offerTab").css("text-align","left");if(viewport.height>500){$("#inside_chatbottom").css("top","365px");}else{$("#inside_chatbottom").css("top","205px");}
$(".inside_chatMessageUser, .chatMessageAssist").css("padding","0px");$(".assistMessageFooter").css("bottom","-5px");$(".messageText").each(function(){$(this).css("margin-left","5px");$(this).css("position","relative");});}}
function updateCountdowns(){$(".insideOfferCountDown").each(function(){$(this).html(showTimeRemaining(new Date($(this).attr("date"))));if(parseInt($(this).html())<0){$(this).html("*OFFER EXPIRED*");$(this).attr("class","insideOfferCountDownExpired");if(typeof($(this).attr("offerid")!="undefined")){$(".inside_offerImage[offerid='"+ $(this).attr("offerid")+"']").remove();removeOffer($(this).attr("offerid"));}}});setTimeout(updateCountdowns,1000);}
function bindInsideEvents(){if(typeof $.inside!="object")
return;$.inside.bind("updateUsers",updateUsers);$.inside.bind("removeUsers",removeUsers);$.inside.unbindAll("chat");$.inside.bind("chat",chatReceived);$.inside.bind("chatdata",chatDataReceived);$.inside.bind("offer",offerReceived);$.inside.bind("queuePos",queuePos);$.inside.bind("connected",connectedToInside);$.inside.bind("disconnected",disconnected);$.inside.bind("error",errorReceived);$.inside.bind("coBrowse",coBrowseHandShakeHandler);}
function coBrowseHandShakeHandler(state){switch(state){case"/request":setCoBrowseHtml("request");break;case"/dispose":setCoBrowseHtml("dispose");break;default:}}
var cobrowseRequestCount=0;var cobrowseMessageCount=0;insideChat.setCoBrowseHtml=setCoBrowseHtml;function setCoBrowseHtml(state){var requestText="The agent is requesting a</br>Co-Browse Session</br>with you.";var requestYesText="Ok";var requestNoText="Decline";if(typeof $.inside.front.settingsData.cobrowse!="undefined"){var cbSettings=$.inside.front.settingsData.cobrowse;if(typeof cbSettings.requestText!="undefined"&&cbSettings.requestText!="")
requestText=cbSettings.requestText;if(typeof cbSettings.requestYesText!="undefined"&&cbSettings.requestYesText!="")
requestYesText=cbSettings.requestYesText;if(typeof cbSettings.requestNoText!="undefined"&&cbSettings.requestNoText!="")
requestNoText=cbSettings.requestNoText;}
switch(state){case"request":$(".inside_cobrowseRequest").parent().remove();if(!('MutationObserver'in window)){$.inside.server.coBrowseNotSupported();return;}
var html="<div class='inside_cobrowseRequest'>"+"<div class='inside_cobrowseRequestMsg'>"+ requestText+"</div>"+"<button class='inside_cobrowseAcceptBtn'>"+ requestYesText+"</button>"+"<button class='inside_cobrowseRejectBtn'>"+ requestNoText+"</button>"+"</div>";insideFrontInterface.openChatPane();showSystemMessage(safeHTML(html));windowScale();$(".inside_cobrowseAcceptBtn").click(function(){$.inside.server.acceptCoBrowseRequest().done(function(userid){if(userid){var hostName=window.location.hostname;var siteNameFragments=hostName.split(".");var siteName=siteNameFragments[1];var domain=siteNameFragments.slice(1,siteNameFragments.length).join(".");var cbCookieUser="Inside_cobrowse_userid";var cbCookieAgent="Inside_cobrowse_agent_"+ siteName;var cbCookieActiveTab="Inside_cobrowse_"+ siteName;setCBCookie(cbCookieUser,userid,domain);var currentAgentName="";if(typeof(availableAssistants)!="undefined"&&availableAssistants.length>0)
currentAgentName=availableAssistants[0].name;setCBCookie(cbCookieAgent,currentAgentName,domain);setCBCookie(cbCookieActiveTab,$.connection.hub.id,domain);setCoBrowseHtml("accept");insideChat.closeChatPane();}});});$(".inside_cobrowseRejectBtn").click(function(){$.inside.server.rejectCoBrowseRequest().done(function(){setCoBrowseHtml("reject");});});break;case"reject":$(".inside_cobrowseRequest").parent().remove();break;case"accept":$(".inside_cobrowseRequest").parent().remove();break;case"dispose":$(".inside_cobrowseRequest").parent().remove();break;}}
function setCBCookie(cname,cvalue,domain){var d=new Date();d.setTime(d.getTime()+(1*24*60*60*1000));var expires="expires="+ d.toUTCString();document.cookie=cname+"="+ cvalue+"; "+ expires+";domain="+ domain+";path=/";}
function queuePos(response){            if (response.pos == 0) {
                return;
            }
            $(".inside_systemMessage.queue").remove();
            if (settings.queueMessage == "") {
                return;
            } else if (typeof (settings.queueMessage) != "undefined") {
                var msg = settings.queueMessage;
                if (msg.indexOf("{position}") != -1)
                    msg = msg.split("{position}").join(response.pos);
                if (msg.indexOf("{time}") != -1) {
                    var time = Math.round(response.time / 60);
                    msg = msg.split("{time}").join(time <= 1 ? "1 minute" : time + " minutes");
                }
            } else {
                msg = "You are now position " + response.pos + " in the queue.";//" with an estimated waiting time of " + response.time + "mins";
            }
            showSystemMessage(msg, false, "queue");
        }

        function errorReceived(data) {
            if (data.message == "No assistants available") {
                showSystemMessage("Error: " + data.message);
                disableChat();
            }
        }

        function isTabHiddenOnThisDevice() {
            hideChatTab = false;
            if (typeof settings.hideChatTabOnDevices != "undefined" && settings.hideChatTabOnDevices != null) {
                if (settings.hideChatTabOnDevices.indexOf(device.toString()) != -1) {
                    hideChatTab = true;
                }
            } else {
                if (settings.hideChatTab == true) {
                    //not specified per device
                    hideChatTab = true;
                }
            }
            return hideChatTab;
        }

        function isOfflineTabHiddenOnThisDevice() {
            if (typeof settings.offlineChat.hideOfflineChatTabOnDevices != "undefined" && settings.offlineChat.hideOfflineChatTabOnDevices != null) {
                if (settings.offlineChat.hideOfflineChatTabOnDevices.indexOf(device.toString()) != -1) {
                    return true;
                }
            } else {
                if (settings.offlineChat.hideOfflineChatTab == true) {
                    //not specified per device
                    return true;
                }
            }
            return false;
        }

        function getCurrentAssistantName() {
            for (var i = 0; i < availableAssistants.length; i++) {
                if (availableAssistants[i].id.indexOf("assistant:") == 0)
                    return availableAssistants[i].name;
            }
            return "";
        }

        //gets current available assistants from $.inside.front
        function getAssistants() {
            var prevAssist = availableAssistants.length;
            availableAssistants = [];
            for (var userid in $.inside.front.users) {
                var user = $.inside.front.users[userid];
                if (typeof (user) != "function" && typeof (user) != "string")
                    availableAssistants.push(user);
            }
            /*
            if(prevAssist==0 && availableAssistants.length > 0) {
                showStoreImage(availableAssistants[0]);
            }
            */

            if (availableAssistants.length == 0) {
                if (chatTabMode == "online") {
                    setChatTab();
                }
                try {
                    _insideGraph.doEvent("chatavailable", false);
                } catch (ex) { }
                $("#inside_holder .assist_close").hide();
                disableChat();

                $("#inside_chatInputHolderTable").addClass("disabled");
                if (!chatInProgress) {
                    if (settings == null || typeof (settings.offlineChat) == "undefined" || !settings.offlineChat.enabled) {
                        $("#inside_holder #inside_liveChatTab").hide();
                    }
                }
            } else {
                if (chatTabMode == "offline") {
                    setChatTab();
                }
                    //for chat tab hiding
                else {
                    startOffline = false;
                    setChatTab(false);
                }
                $("#inside_chatInputHolderTable").removeClass("disabled");
                currentInsiderName = getCurrentAssistantName();
                $("#inside_holder").show();
                if (!$("#inside_holder .chatPane").is(":visible")) {
                    if (typeof (settings) != "undefined" && settings != null && settings.hideChatTab == true) {
                        hideChatTab = isTabHiddenOnThisDevice();
                        if (!$(".inside_chatPane, #inside_chatPane_custom").is(":visible")) {
                            if (!hideChatTab) {
                                $("#inside_liveChatTab").show();
                            }
                        }
                    }
                }
                try {
                    _insideGraph.doEvent("chatavailable", true);
                } catch (ex) { }

                initialText = getChatPlaceholder();

                if (settings != null && settings.forceDept == true && !deptChosen && !chatInProgress && $(".inside_dept_selector").length > 0) {
                    disableChat(translate("Please select a department"));
                } else {
                    enableChat();
                }

                $("#inside_chatInput").prop("disabled", false);

                //show the chat icon
                $("#inside_holder").show();
                //if(!$(".inside_chatPane").is(":visible")){
                if (typeof (disableInsideChat) == "undefined") {
                    if (!$(".inside_chatPane").is(":visible")) {
                        //$("#inside_liveChatTab").show();
                        $("#inside_chatInput").prop("disabled", false);
                        $("#inside_chatInput").css("color", "");
                        $("#inside_chatInput").val("");
                    }
                }
                //}

            }
            showChatHeaderText();

            if (assistantEvents.length > 0) {
                for (var i = 0; i < assistantEvents.length; i++) {
                    assistantEvents[i](availableAssistants);
                }
            }
        }

        function getChatPlaceholder() {
            var placeholder = translate("Ask your question here...");
            if (settings != null && typeof (settings.chatInputPlaceholder) != "undefined" && settings.chatInputPlaceholder != null && settings.chatInputPlaceholder != "") {
                $("#inside_chatInput, #inside_chatPane_custom_input").attr("placeholder", settings.chatInputPlaceholder);
                updatePlaceholder();
                placeholder = settings.chatInputPlaceholder;
            } else {
                if (availableAssistants.length == 1) {
                    placeholder = translate("Type something here...");//here to chat with " + availableAssistants[0].name;
                    var assistantName = getCurrentAssistantName();
                    if (settings != null && settings.showOperatorName == true && assistantName != "") {
                        placeholder = translate("Type here to chat with " + assistantName);
                    }
                } else {
                    //multiple assistants to chat to, do not a specific name.
                    if (settings != null && typeof (settings.insiderDescription) != "undefined" && settings.insiderDescription != "") {
                        placeholder = translate("Type here to chat with " + (beginsWithVowel(settings.insiderDescription) ? "an " : "a ") + settings.insiderDescription);
                    } else {
                        placeholder = translate("Type here to chat with an assistant");
                    }
                }
            }
            return placeholder;
        }

        function showChatHeaderText() {
            /*
            if(availableAssistants.length == 1){
                $("#inside_chat_status").html("<span class='insiderNameHolder'>Your " + insiderDescription + " is <span class='insiderName'>" + currentInsiderName + "</span></span>");
            } else {
                $("#inside_chat_status").html("<span class='insiderNameHolder'>There are " + (availableAssistants.length==0?"no":availableAssistants.length) + " " + insiderDescriptionPlural + " available</span>");
            }
            */
            if (availableAssistants.length == 0) {
                $("#inside_chat_status").html("<div class='inside_chatstatus'><img src='" + imageurl + "offlinelight.png' /> " + translate("Status: OFFLINE") + "</div>");
            } else {
                showLiveStatus();
            }
        }

        function showLiveStatus() {
            $("#inside_chat_status").html("<div class='inside_chatstatus'><img src='" + imageurl + "livelight.png' /> " + translate("Status: ONLINE") + "</div>");
        }

        function updateUsers(data) {
            getAssistants();
            if (unshownMessages == true && availableAssistants.length > 0) {
                unshownMessages = false;
                animateOpenChatPane();
            }
        }

        function removeUsers(arr) {
            for (var i = 0; i < arr.length; i++) {
                var id = arr[i];
                var index = userList.indexOf(id);
                userList.splice(index, 1);
                userArray.splice(index, 1);

                $("div[userid='" + id + "']").remove();
            }
            getAssistants();

        }

        var reactive = false;

        this.connectedToInside = connectedToInside;
        function connectedToInside(data) {
            if (typeof (data) != "undefined" && data != null) {
                settings = $.inside.front.settingsData.settings;

            }
            if (typeof (settings) == "undefined" || settings == null) {
                settings = {};
            }

            if (data.reactive == true) {
                reactive = true;
            }
            getAssistants();

            if (chatInProgress) {
                //load the current conversation
                reloadConversation();
            }
            if (settings.language && settings.language != "") {
                language = settings.language;
            }

            if (settings.language && settings.language != "") {
                language = settings.language;
            }

            insideChat.data = data;
            insideChat.userid = data.userid;

            setTimeout(function () {
                /*
                if(settings == null){
                    return;
                }
                */
                if (typeof (settings) != "undefined" && settings != null && !$.isEmptyObject(settings)) {
                    if (device == 2 && typeof (settings.mobileTabPosition) != "undefined") {
                        setChatTabPosition(settings.mobileTabPosition);
                    } else if (settings.tabPosition != null) {
                        setChatTabPosition(settings.tabPosition);
                    }
                    if (settings.chatPanePosition != null) {
                        chatPanePosition = settings.chatPanePosition;
                    }
                    if (settings.useCustomChatPane) {
                        useCustomChatPane = settings.useCustomChatPane;
                    }
                }

                startInsideChat();
                setChatTab();

                showQueuedChats();

                if (data.queuePos) {
                    queuePos(data.queuePos);
                }

            }, 250);


        }

        function setupChatPane() {
            if (typeof (settings) != "undefined" && typeof (settings.showChatHeader) != "undefined" && settings.showChatHeader == false) {
                hideChatHeader();
            }
            if (typeof (settings.showStore) != "undefined" && settings.showStore == false || device == 2) {
                setTimeout(hideInsideStore, 0);
            }
            if (typeof (settings.insiderDescription) != "undefined" && settings.insiderDescription != "") {
                insiderDescription = settings.insiderDescription;
                if (typeof (settings.insiderDescriptionPlural) != "undefined") {
                    insiderDescriptionPlural = settings.insiderDescriptionPlural;
                } else {
                    insiderDescriptionPlural = insiderDescription + "s";
                }
                showChatHeaderText();
            }

            if (typeof (settings.siteLogo) != "undefined" && settings.siteLogo != "" && settings.siteLogo != null) {
                $("#inside_siteLogo, #inside_chatPane_custom_chatHeader").html("<img src='" + _insideCDN + settings.siteLogo + "' id='inside_chat_siteLogo' />");
                $("#inside_chat_siteLogo").load(positionChatPane);
            } else {
                $("#inside_siteLogo").html("");
            }

            if (settings.useCustomChatPane == true && typeof (settings.customCSS) != "undefined") {
                $.inside.front.addStyleString(settings.customCSS);
            }

            if (typeof (settings.useCustomChatPane) != "undefined" && settings.useCustomChatPane && typeof (settings.customChatPaneCSSFile) != "undefined" && settings.customCSSStyle != "default") {
                var version = Math.random();
                if (typeof (settings.version) != "undefined") {
                    version = settings.version;
                }
                $.inside.front.loadCssFile(_insideCDN + settings.customChatPaneCSSFile + "?" + version, windowScale);
            }

            if (typeof (settings.chatInputPlaceholder) != "undefined") {
                $('#inside_chatInput, #inside_chatPane_custom_input').val("");
                $("#inside_chatInput, #inside_chatPane_custom_input").attr("placeholder", settings.chatInputPlaceholder);
                updatePlaceholder();
                initialText = settings.chatInputPlaceholder;
            }

            //show options
            if (device != 2) {

                if (isIE5Quirks()) {
                    //font icons not supported
                    $(".inside_closeCross").removeClass("fonticon").removeClass("icon-hclose");
                } else {
                    var iconsHtml = "<div id='inside_option_icons'>" +
                                        "<span id='inside_joinTheCallIcon' title='" + translate("Join the Call") + "' class='icon-hphone'></span>" +
                                        '<span class=\'icon-hprint\' id=\'inside_printIcon\' title="' + translate("Print transcript") + '"></span>' +
                                        "<span id='inside_emailIcon' title='" + translate("Email transcript") + "' class='icon-hemail' ></span>" +
                                    "</div>";

                    $(iconsHtml).appendTo("#inside_siteLogo, #inside_chatPane_custom_chatHeader");
                    if (useMinimiseButton) {
                        $("#inside_option_icons").addClass("morePadding");
                    }           
                }

                $("#inside_joinTheCallIcon").unbind("click").click(function () {
                    $.when(getVisitorCobrowsePasscode())
                    .done(function (response) {
                        var msg = insideChat.clickToCallSettings.joinTheCallMsg || "Join the Call code is:";
                        showSystemMessage(msg + " <b>" + response.data + "</b>", false);
                    })
                    .fail(function (response) {
                        var msg = settings.joinTheCallFailMsg || "Could not get Join the Call code!";
                        showSystemMessage(msg, false);
                    });
                });
                $("#inside_printIcon").click(getChatTranscript);
                $("#inside_emailIcon").click(emailTranscript);

                if (settings.print != true) {
                    $("#inside_printIcon").hide();
                }
                if (settings.email != true) {
                    $("#inside_emailIcon").hide();
                }
                if (settings.showJoinTheCallIcon != true) {
                    $("#inside_joinTheCallIcon").hide();
                }
                if (settings.email != true && settings.print != true && settings.showJoinTheCallIcon != true) {
                    $("#inside_siteLogo, #inside_chatPane_custom_chatHeader").css("padding-right", "14px");
                }

            } else {
                $("#inside_siteLogo, #inside_chatPane_custom_chatHeader").css("padding-right", "14px");
            }

            if (useMinimiseButton) {
                $("#inside_siteLogo, #inside_chatPane_custom_chatHeader").addClass("morePadding");
            }

            if (device == 2) {
                $("#inside_chatPane_custom .inside_chatPane").addClass("inside_mobile");
            }
            if (device == 3) {
                $("#inside_chatPane_custom .inside_chatPane").addClass("inside_tablet");
            }

            if (!placeholderIsSupported()) {
                $('#inside_chatInput, #inside_chatPane_custom_input').focus(function () {

                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                        input.removeClass('placeholder');
                    }
                }).blur(function () {
                    var input = $(this);
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.addClass('placeholder');
                        input.val(input.attr('placeholder'));
                        scaleChatInput();
                    }
                }).blur();
            }

            if (typeof (settings.iconColour) != "undefined") {
                $("#inside_chatPane_custom_chatHeader [class*='icon-'], #inside_chatPaneHeader [class*='icon-'], .inside_chatPane .icon-hclose, #inside_chatPane_custom .icon-hclose, .inside_minimise").css("color", settings.iconColour);
            }
            if (typeof (settings.headerColour) != "undefined") {
                $("#inside_chatPaneHeader, #insideLink, #inside_chat_footer").css("background", settings.headerColour);
                if (typeof settings.statusBarColour != "undefined") {
                    $("#inside_chat_footer, #insideLink, #inside_chat_links, #inside_chatPane_custom_footer").css("background", settings.headerColour);
                }
            }
            if (typeof (settings.iconColour) != "undefined" && typeof (settings.headerColour) != "undefined") {
                $("#inside_chat_footer div, #inside_chat_footer a").css("color", settings.iconColour);
            }
            if (typeof (settings.footerColour) != "undefined") {
                $("#inside_chatInputTableHolder, #inside_chatInputHolderTable").css("background", settings.footerColour);
            }
            if (typeof (settings.statusBarColour) != "undefined") {
                $("#inside_chat_footer, #insideLink, #inside_chat_links, #inside_chatPane_custom_footer").css("background", settings.statusBarColour).css("border", "none");
            }
            if (typeof (settings.statusBarFontColour) != "undefined") {
                $("#inside_chat_status .inside_chatstatus, #inside_chat_status").css("color", settings.statusBarFontColour);
            }
            if (typeof (settings.chatBgColour) != "undefined") {
                $(".inside_glass, #inside_chatPane_custom").css("background", settings.chatBgColour).css("opacity", 1);
            }
            if (typeof (settings.chatBorderColour) != "undefined") {
                $(".inside_chatPane, #inside_chatPane_custom").css("border-color", settings.chatBorderColour);
            }
            var cssString = "";
            if (typeof (settings.messageBgColour) != "undefined") {
                //add style for msg
                cssString += "#inside_holder .inside_chatMessageAssist, #inside_holder .inside_chatMessageUser  { background: " + settings.messageBgColour + " }";
                cssString += "#inside_holder .inside_chatMessageAssist.swap:after { border-bottom-color: " + settings.messageBgColour + ";}";
                cssString += "#inside_holder .inside_chatMessageAssist:not(.swap):after { border-left-color: " + settings.messageBgColour + ";}";
                cssString += "#inside_holder .inside_chatMessageUser.swap:after { border-left-color: " + settings.messageBgColour + ";}";
                cssString += "#inside_holder .inside_chatMessageUser:not(.swap):after { border-bottom-color: " + settings.messageBgColour + ";}";
            }
            if (typeof (settings.messageFontColour) != "undefined") {
                cssString += "#inside_holder .inside_chatMessageAssist .inside_messageText, #inside_holder .inside_chatMessageUser .inside_messageText { color: " + settings.messageFontColour + " }";
            }
            if (typeof (settings.visitorMessageBgColour) != "undefined") {
                cssString += "#inside_holder .inside_chatMessageUser  { background: " + settings.visitorMessageBgColour + " }";
                cssString += "#inside_holder .inside_chatMessageUser.swap:after { border-left-color: " + settings.visitorMessageBgColour + ";}";
                cssString += "#inside_holder .inside_chatMessageUser:not(.swap):after { border-bottom-color: " + settings.visitorMessageBgColour + ";}";
            }
            if (typeof (settings.visitorMessageFontColour) != "undefined") {
                cssString += "#inside_holder .inside_chatMessageUser .inside_messageText { color: " + settings.visitorMessageFontColour + " }";
            }

            if (typeof (settings.messageBorderRadius) != "undefined") {
                cssString += "#inside_holder .inside_chatMessageUser, #inside_holder .inside_chatMessageAssist { border-radius: " + settings.messageBorderRadius + "px; }"
            }
            if (typeof (settings.buttonRadius) != "undefined") {
                cssString += "#inside_holder .inside_adminButton, #inside_holder #inside_chatSendButton, #inside_holder .insideSubmitButton { border-radius: " + settings.buttonRadius + "px; }"
            }
            if (typeof (settings.buttonSize) != "undefined") {
                var w = parseInt(settings.buttonSize.split(",")[0]);
                var h;
                if (settings.buttonSize.split(",").length > 1) {
                    h = parseInt(settings.buttonSize.split(",")[1]);
                }
                cssString += "#inside_holder #inside_chatSendButton { padding: " + 0 + "px; }";
                cssString += "#inside_holder #inside_chatSendButton { width: " + w + "px; }";
                if (h != null) {
                    cssString += "#inside_holder #inside_chatSendButton { height: " + h + "px; }";
                    cssString += "#inside_holder #inside_chatSendButton { line-height: " + (h - 30) + "px; }";
                }
            }
            if (typeof (settings.messageBorderColour) != "undefined") {
                cssString += ".inside_chatMessageUser, .inside_chatMessageAssist {border-color:" + settings.messageBorderColour + "}";
                cssString += ".inside_chatMessageAssist.swap:before {border-bottom-color:" + settings.messageBorderColour + "}";
                cssString += ".inside_chatMessageAssist:not(.swap):before {border-left-color:" + settings.messageBorderColour + "}";
                cssString += ".inside_chatMessageUser.swap:before {border-left-color:" + settings.messageBorderColour + "}";
                cssString += ".inside_chatMessageUser:not(.swap):before {border-bottom-color:" + settings.messageBorderColour + "}";
            }
            if (typeof (settings.visitorMessageBorderColour) != "undefined") {
                cssString += ".inside_chatMessageUser {border-color:" + settings.visitorMessageBorderColour + "}";
                cssString += ".inside_chatMessageUser.swap:before {border-left-color:" + settings.visitorMessageBorderColour + "}";
                cssString += ".inside_chatMessageUser:not(.swap):before {border-bottom-color:" + settings.visitorMessageBorderColour + "}";
            }


            if (typeof (settings.buttonColour) != "undefined") {
                cssString += "#inside_holder .insideSubmitButton {background:" + settings.buttonColour + "}";
                $("#inside_chatSendButton, #inside_send_custom").css("background", settings.buttonColour);
            }
            if (typeof (settings.buttonFontColour) != "undefined") {
                $("#inside_chatSendButton, #inside_send_custom, #inside_holder .insideSubmitButton").css("color", settings.buttonFontColour);
            }

            if (cssString != "") {
                $.inside.front.addStyleString(cssString);
            }

        }

        function reloadConversation() {

            insideAPI.call("GetCurrentChat", {}, function (response) {
                clearChats();
                var messages = response.data;
                if (typeof (messages) != "undefined" && messages != null) {
                    for (var i = 0; i < messages.length; i++) {
                        showMessage(messages[i]);
                    }
                }
            });

        }

        function clearChats() {
            addedMessages = [];
            lastChatDrawn = null;
            lastChatDrawnFrom = null;

            $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").empty();
        }

        function loadData(url, dataType, callback) {
            if (typeof (dataType) == "undefined" || dataType == null) {
                dataType = "text";
            }
            $.ajax({
                type: "GET",
                url: url,
                dataType: dataType,
                success: callback,
                error: function (error) { }
            });
        }

        function getDPI() {
            var testDiv = $("<div id='testdiv' style='height: 1in; left: -100%; position: absolute; top: -100%; width: 1in;'></div>").appendTo("body");
            dpi_x = testDiv[0].offsetWidth;
            dpi_y = testDiv[0].offsetHeight;
            //console.log("dpi = " +dpi_x + ", " + dpi_y );
            testDiv.remove();
            return dpi_x;
        }

        function disconnected() {
            availableAssistants = [];
            if (typeof ($.inside.front) != "undefined" && $.inside.front != null && typeof ($.inside.front.users) != "undefined")
                $.inside.front.users = {};
            $("#inside_liveChatTab").hide();
            closeChatPane();
        }

        var visibleElementsHidden;
        var prevScrollPosition = 0;
        var mobileScrollPosBeforeChatOpened = -1;

        var chatPaneAdded = false;
        function addChatPane() {
            chatPaneAdded = true;
            //custom chat pane
            if (settings.useCustomChatPane) {
                useCustomChatPane = settings.useCustomChatPane;
            }

            if (useCustomChatPane) {
                if ($("#inside_chatPane_custom").length > 0) {
                    $("#inside_chatPane_custom_chatHolder").empty();
                    return;
                }
                var showStatus = true;
                if (typeof (settings.customChatPaneStatus) != "undefined" && !settings.customChatPaneStatus) {
                    showStatus = false;
                }
                $("#inside_holder").append("<div id='inside_chatPane_custom'></div>");
                var customChatPaneHTML = "";
                if (useMinimiseButton) {
                    customChatPaneHTML += "<div class='inside_minimise' title='" + translate("Minimise Chat Pane") + "'>−</div>";
                }
                customChatPaneHTML += "<div class='inside_closeCross fonticon icon-hclose' title='" + translate("Close Chat Pane") + "'></div>";
                customChatPaneHTML += "<div id='inside_chatPane_custom_chatHeader'></div>";
                customChatPaneHTML += "<div id='inside_chatPane_custom_chatHolderCell'><div id='inside_chatPane_custom_chatHolder'></div></div>";
                if (showStatus) {
                    customChatPaneHTML += "<div id='inside_chat_footer'><div id='inside_chat_status'></div></div>";
                }
                customChatPaneHTML += "<div id='inside_chatPane_custom_footer'><textarea id='inside_chatPane_custom_input' placeholder='Ask your question here...'></textarea><input type='button' id='inside_send_custom' value='send' />";

                if (!showStatus) {
                    //customChatPaneHTML += "<div id='inside_custom_chat_link' title='Chat software by Inside&trade;'><a href='http://www.inside.tm?ref=insidechat' target='_new'><img src='" + _insideCDN + "/images/insidelink_grey.png' /></a>";
                }
                customChatPaneHTML += "</div>";

                $("#inside_chatPane_custom").append(customChatPaneHTML).hide();
            } else {
                if ($(".inside_chatPane").length > 0) {
                    $("#inside_chatWindow").empty();
                    return;
                }
                $("#inside_holder").append("<div class='inside_chatPane'><div class='inside_glass'></div><div class='inside_closeCross fonticon icon-hclose' title='" + translate("Close Chat Pane") + "'></div></div><div class='inside_insideToolTip'></div>");
                if (useMinimiseButton) {
                    $("<div class='inside_minimise' title='" + translate("Minimise Chat Pane") + "'>−</div>").insertBefore(".inside_closeCross");
                }

                var chatHTML = "";
                chatHTML += "<div id='inside_chatHeaderAndWindowHolder'><div id='inside_chatWindow'></div><div id='inside_chatbottom'></div></div></td>";
                chatHTML += "<div id='inside_chatPaneHeader'></div>";
                chatHTML += "<div id='insideOfferHolder'><div id='inside_offerSlider'><div id='inside_chat_top_white' ></div><div id='inside_offerImage'></div><div id='inside_offerImageFooter'></div><div id='inside_offerTab'><div id='inside_offerTab_arrow'><img src='" + imageurl + "pulldown_arrow.png' /></div><div id='inside_offerTab_background'></div></div></div></div>";
                //chatHTML += "<table class='inside_chatTable'><tr><td class='inside_chatCell'><div id='insideOfferDots'></div>";

                chatHTML += "</tr><tr><td class='noBorderCell' id='inside_chatHolderCell'></tr></table>";
                //chatHTML += "<div id='inside_frontStoreImage'></div>";
                //chatHTML += "<div id='inside_frontStoreAssistant'></div>";
                $(".inside_chatPane").append(chatHTML).hide();
            }

            //add the inside platform link
            //$("<div id='inside_questionMark' title='" + translate("About") + "'>?</div>").appendTo("#inside_chatHeaderAndWindowHolder, #inside_chatPane_custom_chatHolderCell");
            if(settings.showInsidePlatformLink != false)
                $("<div id='inside_platform_link'><a target='new' href='http://www.insideyourbusiness.com'>" + translate("Customer Engagement by INSIDE") + "<sup>&reg;</sup></a></div>").appendTo("#inside_chatHeaderAndWindowHolder, #inside_chatPane_custom_chatHolderCell");
                        
            if (typeof (settings.defaultCustomCSS) != "undefined") {
                var version = Math.random();
                if (typeof (settings.version) != "undefined") {
                    version = settings.version;
                }
                $.inside.front.loadCssFile(_insideCDN + settings.defaultCustomCSS + "?" + version, windowScale);
            }

            var insideChatStatusHtml = "";
            if (settings.statusBarEnabled) {
                insideChatStatusHtml = "<div id='inside_chat_footer'><div id='inside_chat_status'></div></div>";
            }
            //insideChatStatusHtml += "<div id='inside_chat_moreOptions'>...<div id='inside_chat_moreOptions_menu'><div>Options</div><div id='inside_printTranscript' class='inside_moreOption'>Print Chat Transcript</div><div id='inside_emailTranscript' class='inside_moreOption'>Email Chat Transcript</div></div></div>";

            $(".inside_chatPane").append(insideChatStatusHtml + "<div id='inside_chatInputTableHolder'><table id='inside_chatInputHolderTable' style='cellpadding: 0px;'><tr style='height: 50px;'><td class='noBorderCell'><textarea id='inside_chatInput' alt='Live chat input box' maxlength='500' placeholder='" + translate("Type something here...") + "'></textarea></td><td> <input type='button' value='" + translate("Send") + "' class='inside_adminButton' id='inside_chatSendButton' /></td></tr></table></div>");
            $("<div class='inside_chaticon'></div>").appendTo(".chatPane");

            //add the large offer window
            $("<div id='inside_offerLargeWindow'><div id='inside_offerLargeContent'><div class='inside_closeCross fonticon'></div><div class='inside_offerPopupContent'></div></div></div>").appendTo("body");
            $("#inside_offerLargeContent .inside_closeCross").bind("touchstart click", closeLargeOfferWindow);

            $(".inside_chatPane").append("<div id='assistantChatImage' style='display: none;'></div>");

            $("#inside_chatSendButton, #inside_send_custom").on("click touchend", sendClick);
            $("#inside_chatSendButton").attr("title", translate("Click to send your message"));
            $('#inside_chatInput, #inside_chatPane_custom_input').bind('keydown', chatKeyDown);
            $('#inside_chatInput, #inside_chatPane_custom_input').keyup(chatKeyPress);

            setInitialText();
            setChatTabPosition(tabPosition);

            if (useMinimiseButton) {
                $(".inside_chatPane .inside_closeCross, #inside_chatPane_custom .inside_closeCross").click(closeAndHideChat);
                $(".inside_chatPane .inside_minimise, #inside_chatPane_custom .inside_minimise").click(closeChatWindow);
            } else {
                $(".inside_chatPane .inside_closeCross, #inside_chatPane_custom .inside_closeCross").click(closeChatWindow);
            }

            $("<div class='inside_blueChat'></div>").appendTo("#inside_holder").click(chatTabClick).hide();

            $("#closeInsideButton").remove();
            //$("#closeInsideButton").click(closeInside);
            //$("#troveWindow .inside_closeCross").click(closeTrove);
            $("#insideLogo").click(linkToInside);
            $("#inside_smallOfferHolder").click(smallOfferClick);

            var slw = "100%";
            $("#inside_chatPaneHeader").html("<table style='width: " + slw + "; height: 50px;'><tr><td id='inside_siteLogo' style='vertical-align: middle;'></td></tr></table>");

            $("#inside_offerImage").append("<div id='insideStoreImage'><div>");

            $("#inside_offerImageFooter").append("<div class='inside_offerFooterDesc'></div>");
            $("#inside_offerTab").append("<div class='inside_offerFooterExpires'></div>");

            $("#inside_offerTab").bind("click", function () {
                if ($("#inside_offerImage").is(":visible")) {
                    closeImagePane();
                } else {
                    openImagePane();
                }
            });

            $("#inside_chatInputHolderTable, #inside_chatPane_custom_footer").click(function () {
                if (availableAssistants.length == 0 && typeof (settings.offlineChat) != "undefined" && settings.offlineChat.enabled) {
                    if ($(this).hasClass("disabled")) {
                        openLeaveMessageForm();
                    }
                }
            });

            if (typeof (settings.submitLabel) != "undefined") {
                $("#inside_send_custom, #inside_chatSendButton").attr("value", settings.submitLabel);
            }

            // logic for iOS mobile device: hide everything except for inside when opening keyboard to interact with chat pane. 
            // This fixes iOS centering the input box
            $("#inside_chatInput, #inside_chatPane_custom_input").focus(function () {
                if (device == 2 && iOSMobile) {
                    scrollChatToBottom();
                    setTimeout(scrollChatToBottom, 500);
                    prevScrollPosition = $(window).scrollTop();

                    $(".inside_chatPane, #inside_chatPane_custom").addClass("inside_inputFocus");
                }
            });

            $("#inside_chatInput, #inside_chatPane_custom_input").bind("blur", function () {

                if (device == 2 && iOSMobile) {
                    $(".inside_chatPane, #inside_chatPane_custom").removeClass("inside_inputFocus");
                    if (visibleElementsHidden != null)
                        visibleElementsHidden.show();
                    //$(window).scrollTop(prevScrollPosition);
                }
                if(device == 2 && !iOSMobile) {
                    setTimeout(positionChatPane, 1000);
                }

            });

            $("#inside_chatInput").bind("keyup", scaleChatInput);

            //$("#inside_getTranscript").click(getChatTranscript);
            $("#inside_printTranscript").click(getChatTranscript);
            $("#inside_emailTranscript").click(emailTranscript);

            //showChatDisclaimer();

            setupChatPane();
        }

        function emailTranscript() {
            var dateFormat = getLocaleDateString();
            var email = window.prompt("Enter your email", "");
            insideAPI.call("EmailSessionChatv2", { email: email, offset: new Date().getTimezoneOffset(), dateformat: dateFormat });
        }

        function getLocaleDateString(){
            var formats = {
               "ar-SA" : "dd/MM/yy", "bg-BG" : "dd.M.yyyy", "ca-ES" : "dd/MM/yyyy", "zh-TW" : "yyyy/M/d", "cs-CZ" : "d.M.yyyy", "da-DK" : "dd-MM-yyyy", "de-DE" : "dd.MM.yyyy", "el-GR" : "d/M/yyyy", "en-US" : "M/d/yyyy", "fi-FI" : "d.M.yyyy", "fr-FR" : "dd/MM/yyyy", "he-IL" : "dd/MM/yyyy", "hu-HU" : "yyyy. MM. dd.", "is-IS" : "d.M.yyyy", "it-IT" : "dd/MM/yyyy", "ja-JP" : "yyyy/MM/dd", "ko-KR" : "yyyy-MM-dd", "nl-NL" : "d-M-yyyy", "nb-NO" : "dd.MM.yyyy", "pl-PL" : "yyyy-MM-dd", "pt-BR" : "d/M/yyyy", "ro-RO" : "dd.MM.yyyy", "ru-RU" : "dd.MM.yyyy", "hr-HR" : "d.M.yyyy", "sk-SK" : "d. M. yyyy", "sq-AL" : "yyyy-MM-dd", "sv-SE" : "yyyy-MM-dd", "th-TH" : "d/M/yyyy", "tr-TR" : "dd.MM.yyyy", "ur-PK" : "dd/MM/yyyy", "id-ID" : "dd/MM/yyyy", "uk-UA" : "dd.MM.yyyy", "be-BY" : "dd.MM.yyyy", "sl-SI" : "d.M.yyyy", "et-EE" : "d.MM.yyyy", "lv-LV" : "yyyy.MM.dd.", "lt-LT" : "yyyy.MM.dd", "fa-IR" : "MM/dd/yyyy", "vi-VN" : "dd/MM/yyyy", "hy-AM" : "dd.MM.yyyy", "az-Latn-AZ" : "dd.MM.yyyy", "eu-ES" : "yyyy/MM/dd", "mk-MK" : "dd.MM.yyyy", "af-ZA" : "yyyy/MM/dd", "ka-GE" : "dd.MM.yyyy",
               "fo-FO" : "dd-MM-yyyy", "hi-IN" : "dd-MM-yyyy", "ms-MY" : "dd/MM/yyyy", "kk-KZ" : "dd.MM.yyyy", "ky-KG" : "dd.MM.yy", "sw-KE" : "M/d/yyyy", "uz-Latn-UZ" : "dd/MM yyyy", "tt-RU" : "dd.MM.yyyy", "pa-IN" : "dd-MM-yy", "gu-IN" : "dd-MM-yy", "ta-IN" : "dd-MM-yyyy", "te-IN" : "dd-MM-yy", "kn-IN" : "dd-MM-yy", "mr-IN" : "dd-MM-yyyy", "sa-IN" : "dd-MM-yyyy", "mn-MN" : "yy.MM.dd", "gl-ES" : "dd/MM/yy", "kok-IN" : "dd-MM-yyyy", "syr-SY" : "dd/MM/yyyy", "dv-MV" : "dd/MM/yy", "ar-IQ" : "dd/MM/yyyy", "zh-CN" : "yyyy/M/d", "de-CH" : "dd.MM.yyyy", "en-GB" : "dd/MM/yyyy", "es-MX" : "dd/MM/yyyy", "fr-BE" : "d/MM/yyyy", "it-CH" : "dd.MM.yyyy", "nl-BE" : "d/MM/yyyy", "nn-NO" : "dd.MM.yyyy", "pt-PT" : "dd-MM-yyyy", "sr-Latn-CS" : "d.M.yyyy", "sv-FI" : "d.M.yyyy", "az-Cyrl-AZ" : "dd.MM.yyyy", "ms-BN" : "dd/MM/yyyy", "uz-Cyrl-UZ" : "dd.MM.yyyy", "ar-EG" : "dd/MM/yyyy", "zh-HK" : "d/M/yyyy", "de-AT" : "dd.MM.yyyy", "en-AU" : "d/MM/yyyy", "es-ES" : "dd/MM/yyyy", "fr-CA" : "yyyy-MM-dd", "sr-Cyrl-CS" : "d.M.yyyy", "ar-LY" : "dd/MM/yyyy",
               "zh-SG" : "d/M/yyyy", "de-LU" : "dd.MM.yyyy", "en-CA" : "dd/MM/yyyy", "es-GT" : "dd/MM/yyyy", "fr-CH" : "dd.MM.yyyy", "ar-DZ" : "dd-MM-yyyy", "zh-MO" : "d/M/yyyy", "de-LI" : "dd.MM.yyyy", "en-NZ" : "d/MM/yyyy", "es-CR" : "dd/MM/yyyy", "fr-LU" : "dd/MM/yyyy", "ar-MA" : "dd-MM-yyyy", "en-IE" : "dd/MM/yyyy", "es-PA" : "MM/dd/yyyy", "fr-MC" : "dd/MM/yyyy", "ar-TN" : "dd-MM-yyyy", "en-ZA" : "yyyy/MM/dd", "es-DO" : "dd/MM/yyyy", "ar-OM" : "dd/MM/yyyy", "en-JM" : "dd/MM/yyyy", "es-VE" : "dd/MM/yyyy", "ar-YE" : "dd/MM/yyyy", "en-029" : "MM/dd/yyyy", "es-CO" : "dd/MM/yyyy", "ar-SY" : "dd/MM/yyyy", "en-BZ" : "dd/MM/yyyy", "es-PE" : "dd/MM/yyyy", "ar-JO" : "dd/MM/yyyy", "en-TT" : "dd/MM/yyyy", "es-AR" : "dd/MM/yyyy", "ar-LB" : "dd/MM/yyyy", "en-ZW" : "M/d/yyyy", "es-EC" : "dd/MM/yyyy", "ar-KW" : "dd/MM/yyyy", "en-PH" : "M/d/yyyy", "es-CL" : "dd-MM-yyyy", "ar-AE" : "dd/MM/yyyy", "es-UY" : "dd/MM/yyyy", "ar-BH" : "dd/MM/yyyy", "es-PY" : "dd/MM/yyyy", "ar-QA" : "dd/MM/yyyy", "es-BO" : "dd/MM/yyyy", "es-SV" : "dd/MM/yyyy", "es-HN" : "dd/MM/yyyy", "es-NI" : "dd/MM/yyyy",
               "es-PR" : "dd/MM/yyyy", "am-ET" : "d/M/yyyy", "tzm-Latn-DZ" : "dd-MM-yyyy", "iu-Latn-CA" : "d/MM/yyyy", "sma-NO" : "dd.MM.yyyy", "mn-Mong-CN" : "yyyy/M/d", "gd-GB" : "dd/MM/yyyy", "en-MY" : "d/M/yyyy", "prs-AF" : "dd/MM/yy", "bn-BD" : "dd-MM-yy", "wo-SN" : "dd/MM/yyyy", "rw-RW" : "M/d/yyyy", "qut-GT" : "dd/MM/yyyy", "sah-RU" : "MM.dd.yyyy", "gsw-FR" : "dd/MM/yyyy", "co-FR" : "dd/MM/yyyy", "oc-FR" : "dd/MM/yyyy", "mi-NZ" : "dd/MM/yyyy", "ga-IE" : "dd/MM/yyyy", "se-SE" : "yyyy-MM-dd", "br-FR" : "dd/MM/yyyy", "smn-FI" : "d.M.yyyy", "moh-CA" : "M/d/yyyy", "arn-CL" : "dd-MM-yyyy", "ii-CN" : "yyyy/M/d", "dsb-DE" : "d. M. yyyy", "ig-NG" : "d/M/yyyy", "kl-GL" : "dd-MM-yyyy", "lb-LU" : "dd/MM/yyyy", "ba-RU" : "dd.MM.yy", "nso-ZA" : "yyyy/MM/dd", "quz-BO" : "dd/MM/yyyy", "yo-NG" : "d/M/yyyy", "ha-Latn-NG" : "d/M/yyyy", "fil-PH" : "M/d/yyyy", "ps-AF" : "dd/MM/yy", "fy-NL" : "d-M-yyyy", "ne-NP" : "M/d/yyyy",
               "se-NO" : "dd.MM.yyyy", "iu-Cans-CA" : "d/M/yyyy", "sr-Latn-RS" : "d.M.yyyy", "si-LK" : "yyyy-MM-dd", "sr-Cyrl-RS" : "d.M.yyyy", "lo-LA" : "dd/MM/yyyy", "km-KH" : "yyyy-MM-dd", "cy-GB" : "dd/MM/yyyy", "bo-CN" : "yyyy/M/d", "sms-FI" : "d.M.yyyy", "as-IN" : "dd-MM-yyyy", "ml-IN" : "dd-MM-yy", "en-IN" : "dd-MM-yyyy", "or-IN" : "dd-MM-yy", "bn-IN" : "dd-MM-yy", "tk-TM" : "dd.MM.yy", "bs-Latn-BA" : "d.M.yyyy", "mt-MT" : "dd/MM/yyyy", "sr-Cyrl-ME" : "d.M.yyyy", "se-FI" : "d.M.yyyy", "zu-ZA" : "yyyy/MM/dd", "xh-ZA" : "yyyy/MM/dd", "tn-ZA" : "yyyy/MM/dd", "hsb-DE" : "d. M. yyyy", "bs-Cyrl-BA" : "d.M.yyyy", "tg-Cyrl-TJ" : "dd.MM.yy", "sr-Latn-BA" : "d.M.yyyy", "smj-NO" : "dd.MM.yyyy", "rm-CH" : "dd/MM/yyyy", "smj-SE" : "yyyy-MM-dd", "quz-EC" : "dd/MM/yyyy", "quz-PE" : "dd/MM/yyyy", "hr-BA" : "d.M.yyyy.", "sr-Latn-ME" : "d.M.yyyy", "sma-SE" : "yyyy-MM-dd", "en-SG" : "d/M/yyyy", "ug-CN" : "yyyy-M-d", "sr-Cyrl-BA" : "d.M.yyyy", "es-US" : "M/d/yyyy"
            };

            return formats[navigator.language] || 'dd/MM/yyyy';
        }

        //updated 16/11/16 - this will only print the chats currently showing in the chat pane. Older session chats will be excluded.
        function getChatTranscript(e) {

           /* insideAPI.call("printtranscripttemplates", {}, function (response) {
                //if (response && response.data) { alert(response.data); }
            });
            */
            andPrint = true;
            var w = window.open();
            try {
                w.document.title = translate('Chat Transcript');
            }
            catch (ex) { }

            var dateFormat = getLocaleDateString();
            var chatids = [currentChatId];
            var ob = { offset: new Date().getTimezoneOffset(), dateformat: dateFormat};
            if(settings.restrictPrintConversation == true) {
                ob.chatids = chatids;
            }
            insideAPI.call("PrintSessionChat", ob, function (response) {
                if (response && response.data) {
                    w.focus();

                    setTimeout(function () {
                        w.document.close();
                        w.focus();
                        if (andPrint == true) {
                            w.print();
                        }
                    }, 1000);
                    var data = response.data;
                    if (data.transcriptData && data.transcriptData != "") {
                        var transcriptPage = $(w.document.body).html(data.transcriptData);
                    }
                    else {
                        var chats = data.messagesList;
                        var lastName = "";
                        var lastDate;
                        w.document.write("<!DOCTYPE html><html><title>" + translate("Chat Transcript") + "</title><head><link rel=\"stylesheet\" type=\"text/css\" href=\"" + _insideCDN + "css/inside_chatTranscript.css?v=9\" /></head><body></body></html>");

                        var html = "";
                        html += "<div id='chatTranscript'>"
                        html += '';

                        if (typeof (settings.companyLogo) != "undefined" && settings.companyLogo != "" && settings.companyLogo != null) {
                            html += "<div class='transcriptLogoHolder'><img src='" + _insideCDN + settings.companyLogo + "' /></div><br />";
                        } else if (typeof (settings.siteLogo) != "undefined" && settings.siteLogo != "" && settings.siteLogo != null) {
                            html += "<div class='transcriptLogoHolder'><img src='" + _insideCDN + settings.siteLogo + "' /></div><br />";
                        }

                        if (typeof (chats) == "undefined" || chats.length == 0) {
                            html += "<div>" + translate("No messages were found, or there was an error obtaining the chat transcript.") + "</div>";
                        } else {
                            html += "<div id='chatTranscriptTitle'><h3>" + translate("Chat Transcript") + "</h3>";
                            //var convStarted = getDateTime(Date.fromISO(chats[0].date)).datestr;
                            var convStarted = getDateTime(new Date(chats[0].date)).datestr;
                            if (convStarted == "") {
                                for (var i = 0; i < chats.length; i++) {
                                    // convStarted = getDateTime(Date.fromISO(chats[i].date)).datestr;
                                    convStarted = getDateTime(new Date(chats[i].date)).datestr;
                                    if (convStarted != "") {
                                        break;
                                    }
                                }
                            }
                            html += "<h4>" + translate("Conversation started") + " " + convStarted + "</h4></div>";
                            for (var i = 0; i < chats.length; i++) {
                                if (chats[i].text.indexOf("/") == 0) {
                                    continue;
                                }
                                parseMessage(chats[i]);

                                //var d = Date.fromISO(chats[i].date);
                                var d = new Date(chats[i].date);

                                if (lastName != chats[i].fromname) {
                                    if (i != 0) {
                                        html += "</div>";
                                    }
                                    html += "<div class='chatMessage'>";
                                    html += "<span class='fromName'>" + chats[i].fromname + "</span><br />";
                                    html += "<span class='chatTime'>" + getDateTime(d).str + "</span>";

                                    html += chats[i].text;

                                } else {
                                    if (lastDate == null || d.valueOf() - lastDate.valueOf() > 60000) {
                                        html += "<br /><span class='chatTime'>" + getDateTime(d).str + "</span>";
                                    }
                                    html += "<br />" + chats[i].text;
                                }
                                //lastDate = Date.fromISO(chats[i].date);
                                lastDate = new Date(chats[i].date);
                                lastName = chats[i].fromname;
                            }
                        }
                        html += "</div>";

                        var transcriptPage = $(w.document.body).html(html);
                    }
                    
                    
                }
            });

            
            
                //transcriptPage.find(".transcriptLogoHolder").css("background", $("#inside_chatPane_custom_chatHeader").css("background"));           

        }

        this.getDateTime = getDateTime;
        function getDateTime(d) {
            if (typeof (d) == "number") {
                d = getTime(d).date;
            }
            var days = lang.daysOfWeek[language] || lang.daysOfWeek["en"];
            var dateStr = days[d.getDay()] + " " + d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear();

            var ampm = "am";
            if (d.getHours() >= 12) {
                ampm = "pm";
            }
            var h = d.getHours() % 12;
            if (h == 0) h = 12;
            var timeStr = h + ":" + zeroPad(d.getMinutes()) + " " + "<span class='ampm'>" + ampm + "</span>";
            if (isNaN(h)) {
                timeStr = "";
                dateStr = "";
            }
            if(lang != "en") {
                dateStr = days[d.getDay()] + " " + d.toLocaleDateString();
                timeStr = d.toLocaleTimeString();
            }
            return { datestr: dateStr, str: timeStr, date: d };
        }

        function startInsideChat() {
            if (typeof (disableInsideChat) != "undefined") {
                return;
            }

            if (connected) {
                return;
            }

            connected = true;


            $("<div class='assist_close' title='' style='right:" + 0 + "px; display:none;' userid=''></div>").appendTo("#userHolder").show();
            $("#inside_holder").css("background-image", "none");
            //$(".assist_close").click(assistCloseClick);
            //$(".assist_close").hover(assistHover);
            $(".inside_assist_close").hide();
            $("#inside_holder").show();

            //constrain the dimensions of hover thumbnail
            $("#inside_holder #userInfo #thumbnail").load(function () {
                var img = this;
                setTimeout(function () {
                    if ($(img).width() > 150) {
                        $(img).css("width", "150px");
                    }
                    if ($(img).height() > 120) {
                        $(img).css("width", "");
                        $(img).css("height", "100px");
                    }
                }, 0);
            });

            //setTimeout(showUsers, 0);
            //setInterval(moveUser, 30000);
            //initAlert();

            $(window).bind("gestureend", windowScale);
            $(window).bind("gesturechange", windowScale);

            $(window).resize(windowScale);
            if (device != 2) {
                window.onscroll = windowScroll;
            }
            setTimeout(function () { windowScale(500); }, 0);
            setTimeout(function () { windowScale(0); }, 1000);

            //when mobile:
            if (device == 2) {
                setTimeout(function () {
                    //initialText = "Type here";
                    setInitialText();
                    hideInsideStore();
                    hideChatHeader();
                }, 100);

            }

            updateCountdowns();

            /*
            $(window).bind("scroll", function(){
                viewport = $.inside.front.getViewPortSize();
                if(!addedPaddingDiv && $(".insideBottomPadding").length==0){
                    if(($(window).scrollTop() + viewport.height) == $(document).height()){
                        trace("adding bottom padding");
                        $("<div class='insideBottomPadding' style='height:100px'></div>").appendTo("body");
                        addedPaddingDiv = true;
                    }
                }
            });
            */
        }

        function addChatTab() {
            if (settings && settings.useMinimiseButton == true) {
                useMinimiseButton = true;
            }

            if (useMinimiseButton) {
                if (sessionStorage.getItem("insideChatClosed") == "True") {
                    return;
                }
            }

            if ($("#inside_tabs").length == 0) {
                $.inside.front.showTabs();
            }
            var addTab = true;
            if (hideChatTab && !showTabForActive) {
                addTab = false;
            }
            if (hideChatTab && showTabForActive) {
                addTab = true;
            }
            if (addTab) {
                $("#inside_tabs").hide();
                $("#inside_tabs").prepend("<div id='inside_liveChatTab' tabindex='0'><img class='inside_chatTabImage' onload='insideChat.onChatTabImageLoad(this);' alt='" + translate("Live chat icon, click to open the live chat pane.") + "' title='" + translate("Live chat icon, click to open the live chat pane.") + "' /></div>");
            }

            $("#inside_liveChatTab").hide();
            $("#inside_liveChatTab").on("click enter", chatTabClick);
            $("#inside_liveChatTab").on('keypress', function (e) {
                if (e.which === 13) {
                    $(this).trigger('enter');
                }
            });
            $("#inside_liveChatTab .inside_chatTabImage").load(chatTabImageLoaded);

            if (settings != null && typeof (settings.tabCSSMargins) != "undefined") {
                $(".inside_chatTabImage").css("margin", settings.tabCSSMargins);
            }
            if (device == 2) {
                $("#inside_liveChatTab").on("swiperight", function () {
                    $("#inside_liveChatTab").animate({ left: viewport.width }, 250);
                })
            }
        }

        var greetingMessageShown = false;

        function showGreetingText() {
            if (greetingMessageShown == true) {
                return;
            }
            greetingMessageShown = true;
            var greetingText;
            var prechat = fetchPrechatLocally();
            var defaultWelcomeMessage = prechat ? "Hi " + htmlEncode(prechat.name) + ", (<a href='javascript:;' class='inside-prechat-link'>Not " + htmlEncode(prechat.name) + "? Click here</a>)" : "";

            if (typeof (settings.welcomeMessage) != "undefined" && settings.welcomeMessage != "") {
                if (settings.welcomeMessage.indexOf('{name}') > -1 && settings.preChat && settings.preChat.enabled) {
                    if (prechat) {
                        greetingText = settings.welcomeMessage.replace(/{name}/g, htmlEncode(prechat.name)) + " (<a href='javascript:;' class='inside-prechat-link'>Not " + htmlEncode(prechat.name) + "? Click here</a>)";
                    } else {
                        greetingText = defaultWelcomeMessage;
                    }
                } else {
                    greetingText = settings.welcomeMessage.replace(/{name}/g, "");
                }
            } else {
                if (settings.preChat && settings.preChat.enabled) {
                    greetingText = defaultWelcomeMessage;
                }
            }

            if (!greetingText || (greetingText && greetingText.trim().length === 0)) {
                return;
            } else {
                greetingText = translate(greetingText);
                $(".insideGreetingMessage").remove();
                var assistantName = getCurrentAssistantName();
                if (assistantName == "") {
                    showSystemMessage(greetingText, false, "insideGreetingMessage");
                } else {
                    var messageData = {
                        text: greetingText,
                        id: "-1",
                        age: 0,
                        fromid: "assistant:welcomeGreeting",
                        fromName: "",
                        isGreetingMessage: true
                    };

                    if (lastChatDrawnFrom == messageData.fromid)
                        lastChatDrawnFrom = "";
                    showMessage(messageData);
                }

                $('.inside-prechat-link').bind('click', function () {
                    $(".inside_chatMessageAssist").remove();
                    greetingMessageShown = false;
                    showPrechatForm();
                });
            }
            $("#inside_chatWindow").css("height", "auto");
            $(".inside_previousConversations").prependTo("#inside_chatWindow, #inside_chatPane_custom_chatHolder");
            setTimeout(scrollChatToBottom, 0);
        }

        function chatInputSelect() {
            typing = true;
            windowScale();
            closeImagePane();
            if ($("#inside_chatInput").val() == initialText) {
                $("#inside_chatInput").val("");
            }
            $("#inside_chatInput").css("font-style", "");
            $("#inside_chatInput").css("color", "");

            /*
            if(device == 2) {
                setTimeout(function(){
                    $(".inside_chatPane").css("top", viewport.height-$(".inside_chatPane").height() + "px");
                }, 100);
            }
            */
            windowScale();

        }

        //the chat input will scale based on content.
        function scaleChatInput() {
            if ($("#inside_chatInput").length == 0) {
                return;
            }
            var taHeight = 60;
            try {
                $("#inside_chatInput").style("height", "28px", "important");
            } catch (e) {
                $("#inside_chatInput").css("height", "28px");
            }
            taHeight = $("#inside_chatInput")[0].scrollHeight;
            if (taHeight > 50) {
                taHeight = 50;
            }
            if (taHeight < 28) {
                taHeight = 28;
            }
            try {
                $("#inside_chatInput").style("height", taHeight + "px", "important");
            } catch (e) {
                $("#inside_chatInput").css("height", taHeight + "px");
            }

        }

        function setSendActiveClass() {
            if ($("#inside_chatInput, #inside_chatPane_custom_input").val() != "") {
                $("#inside_chatSendButton, #inside_send_custom").addClass("active");
            } else {
                $("#inside_chatSendButton, #inside_send_custom").removeClass("active");
            }
        }


        var typingTimout;
        var userIsTyping = false;
        function userTyping() {
            if (currentChatId == null || currentChatId == 0) {
                return;
            }
            if (typeof ($.inside) == "undefined" || typeof ($.inside.server) == "undefined" || typeof ($.inside.server.chatData) == "undefined") {
                return;
            }
            if(settings.showTypingText){
                var str = $("#inside_chatInput, #inside_chatPane_custom_input").val();
                if (settings.disable_cc_masking != true) {
                    str = strictReplaceNumbers(str);
                }
                 $.inside.server.chatData(currentChatId, str);
            } else {
                if ((typeof (startedTyping) == "undefined") || ($("#userChatInput").html() == "" && !startedTyping) || !startedTyping) {
                    startedTyping = true;
                    userIsTyping = true;
                    $.inside.server.chatData(currentChatId, "typingOn");
                }
            }

            setSendActiveClass();
            clearTimeout(typingTimout);
            typingTimout = setTimeout(stoppedTyping, 2000);
        }

        function stoppedTyping() {
            if (currentChatId == null || currentChatId == 0) {
                return;
            }
            userIsTyping = false;
            startedTyping = false;
            $.inside.server.chatData(currentChatId, "typingOff");
        }

        function linkToInside() {
            window.open("http://www.inside.tm?ref=insidechat", "insidepopup");
        }

        var scaleinit = false;
        var chatPaneMode = "";
        var viewport;
        var scaleIOSChat = false;

        insideFrontInterface.scale = windowScale;
        function windowScale(timenum) {
            if (typeof (timenum) != "number") {
                timenum = 0;
            }

            viewport = $.inside.front.getViewPortSize();

            setGlassPosition();
            maxChatHeight = $("#inside_chatHeaderAndWindowHolder").height() - $("#inside_chatPaneHeader").height();

            $(".insideTip").each(function () {
                positionTip($(this));
            });
            setIE7Quirks();

            scaleinit = true;

            positionOfflineForm();
            if (iOSMobile && scaleIOSChat && $("#inside_chatInput, #inside_chatPane_custom_input").is(":focus")) {
                var h = 304; //iphone6
                if (screen.availHeight < 600) {
                    h = 234; //iphone5
                }
                if (screen.availHeight > 700) {
                    h = 354; //iphone6plus
                }
                //subtract visible component heights as required.

                insideChat.positionChatPane(0, h);
                $(window).scrollTop(0);
                scrollChatToBottom();
            } else {
                positionChatPane(timenum);
            }
            if (iOSMobile && $("#inside_chatInput, #inside_chatPane_custom_input").is(":focus")) {
                //scroll to bottom?
            }
            if(iOSMobile && iOSChrome) {
                //scroll top top?
            }
            scaleChatTab();

            positionPrechatForm();

            positionJoinTheCallTag();

            if(device == 2){
                setTimeout(positionChatPane, 100);
            }

        }

        insideChat.positionChatPane = positionChatPane;
        function positionChatPane(timenum, height) {
            if(iOSMobile && iOSChrome && $("#inside_chatInput, #inside_chatPane_custom_input").is(":focus")){
                return;
            }
            var $chatPane = $(".inside_chatPane, #inside_chatPane_custom");

            /*
            if(viewport.width < 500){
                if($("#inside_chatPane_custom, .inside_chatPane").parent()[0] != document.body) {
                    $("#inside_chatPane_custom, .inside_chatPane").appendTo("body");
                    
                }
                return;
            }
            */

            if ($chatPane.length == 0) {
                return;
            }

            var totalHeight;
            if (viewport.height < 500 && viewport.height > 400) {
                totalHeight = viewport.height * 0.75 + (viewport.height * (1 - (viewport.height / 500)));
            } else if (viewport.height < 400) {
                totalHeight = viewport.height;
            } else {
                totalHeight = viewport.height * 0.75;
            }

            if (totalHeight > 450) {
                totalHeight = 450;
            }

            //scale the input text based on the chat send button
            $("#inside_chatInput").width($(".inside_chatPane").width() - $("#inside_chatSendButton").outerWidth() - 40);

            if (viewport.width <= 480) {
                totalHeight = viewport.height;
                if (typeof height != "undefined") {
                    totalHeight = height;
                    viewport.height = height;
                }

                $chatPane.outerHeight(totalHeight);
                $chatPane.width(viewport.width);
                var chatHolderHeight = totalHeight - $("#inside_siteLogo, #inside_chatPane_custom_chatHeader").outerHeight() - $("#inside_chatPane_custom_footer").outerHeight();
                if ($("#inside_chat_footer").is(":visible")) {
                    chatHolderHeight -= $("#inside_chat_footer").outerHeight();
                }
                if ($("#customStoreImageToggle").is(":visible")) {
                    chatHolderHeight -= $("#customStoreImageToggle").outerHeight();
                }
                
                //chatHolderHeight -= parseInt($("#inside_chatPane_custom_chatHolder").css("padding-top"));
                //chatHolderHeight -= parseInt($("#inside_chatPane_custom_chatHolder").css("padding-bottom"));
                $("#inside_chatPane_custom_chatHolder").css("height", chatHolderHeight + "px");

                $("#inside_chatPane_custom_input").width(viewport.width - $("#inside_send_custom").outerWidth() - 60);
            } else {
                $("#inside_chatPane_custom_input").width("");
                $chatPane.width("");
                $chatPane.height("");
                $("#inside_chatPane_custom_chatHolder").height("");
            }
            if (typeof height != "undefined") {
                totalHeight = height;
            }

            if (!useCustomChatPane && $("#inside_chatWindow").length > 0) {
                $chatPane.css("height", totalHeight + "px");
                var cw = $("#inside_chatWindow");
                var h = $("#inside_chatWindow")[0].scrollHeight;

                if (h < totalHeight) {
                    $("#inside_chatWindow").css("height", h + "px");
                } else {

                    $("#inside_chatWindow").css("height", totalHeight - $("#inside_chatInputHolderTable").height() - $("#inside_chat_footer").outerHeight() + "px");
                    //$("#inside_chatWindow").css("height", maxChatHeight);
                }

                $("#inside_chatWindow").css("max-height",  $("#inside_chatHeaderAndWindowHolder").height() - $("#inside_chatPaneHeader").height());
                if($("#inside_chatWindow")[0].scrollHeight > 200){
                    //hide the inside link when overlapping.
                    $("#inside_platform_link").hide();
                }

                var bottomHeight = $("#inside_chatInputHolderTable").height();
                if ($(".inside_chatDisclaimer_agree").length > 0) {
                    bottomHeight = $(".inside_chatDisclaimer_agree").outerHeight();
                }

                //$("#inside_chat_footer").css("top", totalHeight - $("#inside_chatInputHolderTable").height() - 20 + "px");
                 $("#inside_chatHeaderAndWindowHolder").css("height", $chatPane.height() - bottomHeight - $("#inside_chat_footer").outerHeight() + "px");

                setPulldownHeight();
            }

            var screenCssPixelRatio = 1;
            if (typeof (window.outerWidth) == "undefined" || typeof (window.innerWidth) == "undefined") {
                screenCssPixelRatio = 1;
            } else {
                screenCssPixelRatio = window.outerWidth / window.innerWidth;
            }
            screenCssPixelRatio = roundToPlaces(screenCssPixelRatio, 1);

            /*
            if($("#pixelratio").length==0) {
                $(".inside_chatPane").prepend("<div id='pixelratio'><h1>" + screenCssPixelRatio + "</h1></div>");
            } else {
                $("#pixelratio").html("<h1>"+screenCssPixelRatio+"</h1>");
            }
            */

            //scaling of chat pane for mobile devices on desktop sites (aka pinch to zoom)
            if (device == 2) {
                //$chatPane.css("transform", "scale3d(" + (1/screenCssPixelRatio) + ", " + (1/screenCssPixelRatio) + ", 1)");
                $chatPane.css("transform", "");
                if (viewport.height > viewport.width) {
                    var scale = Math.round((viewport.width * 0.9) / $chatPane.width());
                } else {
                    scale = Math.round((viewport.height * 0.9) / $chatPane.height());
                }
                $chatPane.css("-webkit-transform", "scale3d(" + scale + ", " + scale + ", 1)");
                $chatPane.css("transform", "scale3d(" + scale + ", " + scale + ", 1)");

            }
            var chatPaneHeight = $chatPane.outerHeight();

            var toppos;

            if (chatPanePosition == "bottom" || device == 2 /*viewport.height*screenCssPixelRatio < chatPaneHeight*/) {
                $chatPane.css("-webkit-transform-origin", "100% 100%");
                $chatPane.css("transform-origin", "100% 100%");
                toppos = viewport.height - chatPaneHeight;
            } else if (chatPanePosition == "top") {
                $chatPane.css("-webkit-transform-origin", "100% 100%");
                $chatPane.css("transform-origin", "100% 0%");
                toppos = 0;
            } else {
                //middle default
                $chatPane.css("-webkit-transform-origin", "100% 100%");
                $chatPane.css("transform-origin", "100% 50%");
                toppos = (viewport.height - chatPaneHeight) * 0.5;

            }

            if($("#inside_option_icons").length>0) {
                $("#inside_siteLogo img, #inside_chatPane_custom_chatHeader img").css("max-width", $("#inside_option_icons").position().left - 20);
            }

            $chatPane.animate({ top: toppos }, timenum);
        }

        insideChat.scrollChatToBottom = scrollChatToBottom;
        function scrollChatToBottom(time) {
            if (typeof ($.prop) != "undefined") {
                if (typeof (time) != "undefined") {
                    $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").animate({ scrollTop: $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").prop("scrollHeight") }, time);
                } else {
                    setTimeout(function () { $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").prop({ scrollTop: $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").prop("scrollHeight") }); }, 0);
                }
            } else {
                setTimeout(function () { $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").attr({ scrollTop: $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").attr("scrollHeight") }); }, 0);
            }
        }

        var troveInterval;
        function notificationClick() {
            closeChatPane();
            $("#tipWindow").hide();
            $(".inside_blueChat").hide();
            $("#troveWindow").show();
            showOffer(0);

            if (!offersArray[currentOffer].viewed) {
                $.inside.server.offerViewed(offersArray[currentOffer].instanceid);
                offersArray[currentOffer].viewed = true;
            }

            stopPulse();
        }

        function updateOfferTimes() {
            if (offersArray.length == 0) {
                return;
            }
            if (getSecondsBetweenDates(offersArray[currentOffer].expiryDate, new Date()) <= 0) {
                //offer has expired
                removeOffer(currentOffer);
                $("#inside_offerLargeContent").hide();
                clearInterval(troveInterval);
                $(".inside_notification").hide();
                return;
            }
            $("#inside_offer" + currentOffer + " .timeLeft").html(showTimeRemaining(offersArray[currentOffer].expiryDate));
            $(".inside_notification").html(offersArray.length);
        }

        function removeOffer(i) {
            $(".inside_offer[id='offer" + i + "']").remove();
            //update the offer dots
            $("#inside_offerDots .dotSelected").remove();
            $(".inside_offerDot[offerid='" + offersArray[i].instanceid + "']").remove();

            if ($("#insideOfferDots span").length > 1) {
                $("#insideOfferDots").show();
            } else {
                $("#insideOfferDots").hide();
            }

            offersArray.splice(i, 1);
            if (offersArray.length > 0) {
                showOffer(0);
            } else {
                if (typeof (availableAssistants[0].images.store) == "undefined" || availableAssistants[0].images.store == "") {
                    $("#insideStoreImage").css("height", "0px");
                    $("#inside_chatWindow").css("height", "");
                    $("#inside_chatWindow").css("padding-top", "20px");
                    $("#inside_offerTab_arrow").hide();
                    $("#inside_offerTab").hide();
                }
                closeTrove();
            }
            //no more offers
            if (offersArray.length == 0) {
                $("#inside_offerImage").slideUp();
                $("#inside_offerImageFooter").slideUp(function () { $("#insideOfferHolder").hide(); });
                $("#insideOfferDots").hide();
            }
        }

        var shiftPress = false;

        function chatKeyDown(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 8) {
                 e.cancelBubble = true;
                if( e.stopPropagation ) e.stopPropagation();
            }
            if (code == 13 && !e.shiftKey) {
                e.preventDefault();
                sendClick();
                return;
            }
        }

        function chatKeyPress(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13 && !e.shiftKey) {
                e.preventDefault();
                //sendClick();
                return;
            } else {
                setTimeout(userTyping, 0);
            }
            setSendActiveClass();
        }

        function sendClick() {

            //don't send if no assistants available
            if ($("#inside_chatInput").prop("disabled") == true || $("#inside_chatPane_custom_input").prop("disabled") == true) {
                return;
            }

            var str = $("#inside_chatInput, #inside_chatPane_custom_input").val();
            if (str == "" || str == initialText) {
                return;
            }
            //clear the form
            $("#inside_chatInput, #inside_chatPane_custom_input").val("");
            str = htmlEntities(str);
            sendChat(str);
            $("#inside_chatInput, #inside_chatPane_custom_input").focus();
            scaleChatInput();

            scrollChatToBottom();
        }

        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }

        function unquoteAttr(s, preserveCR) {
            return ('' + s)
                .split('&amp;').join("&")
                .replace(/&apos;/g, '')
                .replace(/&quot;/g, '"')
                //.replace(/&lt;/g, '<')
                //.replace(/&gt;/g, '>')
                .replace(/&#10;/g, "\r\n")
                .replace(/&#39;/g, "'")
                .replace(/&nbsp;/g, ' ')
                .replace(/&#13;/g, "\r\n");
        }

        function sendChat(str) {
            userIsTyping = false;

            if (device == 2) {
                setTimeout(function () {
                    $("#inside_chatInput").blur();
                    $("#inside_chatSendButton").focus();
                }, 0);
                typing = false;
                windowScale();
            }

            //show chosen department
            if (!chatInProgress && str.indexOf("/dept ") == 0) {
                msg = "You have selected department " + str.substring(6);
                showSystemMessage(msg);
            }
            if (!chatInProgress) {
                if (isSystemMessage(str)) {
                    //do not create a chat to send these.
                    return;
                }
            }

            //detect if a credit card number has been sent
            if (settings.disable_cc_masking != true) {
                str = checkForCreditCardNumbers(str);
            }

            if (!chatInProgress) {
                setTimeout(showChatStartedText, 500);
                currentChatId = 0;
                insideFrontInterface.currentChatId = 0;
            }
            $.inside.server.chat(currentChatId, str).done(function (response) {
                if (response == -1) {
                    //showSystemMessage("Error sending chat! There may be no assistants currently available, please try again later.");
                    //disableChat();
                }
            });
            setSendActiveClass();
        }

        //if the str contains a credit card number/s, replace it with *s.
        function checkForCreditCardNumbers(str) {
            var ccReg = /\b([0-9][ ,-]*){13,19}\b/g; //rough check for 13-19 digit number
            var matches = str.match(ccReg);
            if (matches != null && matches.length > 0) {

                for (var i = 0; i < matches.length; i++) {
                    //603528 matches STAPLES Canada Enterprise Credit Card (issued by Citi Cards Canada)
                    if (settings.strictNumberMasking == true || matches[i].indexOf("603528") == 0 || valid_credit_card(matches[i])) {
                        var val = matches[i];
                        if(settings.pciCreditMasking) {
                            //PCI DSS, the security standard for companies that handle credit cards, defines a number of rules as to how credit cards are handled. One of those rules, 3.3, is defined as follows:
                            //Mask PAN when displayed (the first six and last four digits are the maximum number of digits to be displayed)
                            toMask = val.substring(6);
                            toMask = toMask.substr(0, toMask.length-4);
                            toMask = toMask.replace(/[0-9]/g, "*");
                            stars = "";
                            for(var j=0;j<toMask.length;j++){
                                stars += "*";
                            }
                            pciMask = val.substr(0, 6) + stars + val.substr(val.length-4 ,val.length);
                            str = str.replace(val, pciMask);
                        } else {
                            matches[i] = matches[i].replace(/[0-9]/g, "*");
                            str = str.replace(val, matches[i]);
                        }
                    }
                }

            }

            return str;
        }

        function strictReplaceNumbers(str){
            var ccReg = /\b([0-9][ ,-]*){4,99}\b/g; //rough check for 4-999 digit number
            var matches = str.match(ccReg);
            if (matches != null && matches.length > 0) {
                for (var i = 0; i < matches.length; i++) {
                    var val = matches[i];
                    matches[i] = matches[i].replace(/[0-9]/g, "*");
                    str = str.replace(val, matches[i]);
                }
            }
            return str;
        }

        function valid_credit_card(value) {
            // accept only digits, dashes or spaces
            if (/[^0-9-\s]+/.test(value)) return false;

            // The Luhn Algorithm. It's so pretty.
            var nCheck = 0, nDigit = 0, bEven = false;
            value = value.replace(/\D/g, "");

            for (var n = value.length - 1; n >= 0; n--) {
                var cDigit = value.charAt(n),
                      nDigit = parseInt(cDigit, 10);

                if (bEven) {
                    if ((nDigit *= 2) > 9) nDigit -= 9;
                }

                nCheck += nDigit;
                bEven = !bEven;
            }

            return (nCheck % 10) == 0;
        }

        function showChatStartedText() {
            var assistantName = getCurrentAssistantName() || "";
            if (assistantName == "") {
                return;
            }
            if (typeof (settings.chatStartedMessage) != "undefined" && settings.chatStartedMessage != "") {
                var reg = /{operator}/gi;
                var msg = settings.chatStartedMessage.replace(reg, assistantName);
                $(".inside_chatStartedMessage").remove();
                showSystemMessage(escapeHtml(msg), false, "inside_chatStartedMessage");
            }
            if (typeof (settings.chatStartedText) != "undefined" && settings.chatStartedText != "") {
                $(".inside_chatStartedText").remove();
                $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").append("<div class='inside_chatDisclaimer inside_chatStartedText'>" + escapeHtml(settings.chatStartedText) + "</div>");
                lastChatFrom = "";
                scrollChatToBottom();
                setTimeout(windowScale, 0);
                clearChatGrouping();
            }
        }

        function chatSent(msg) {

        }

        /*
        function moveUser(){
            var i = Math.floor(Math.random() * $(".user_blue_small").length);
            var randLeft = Math.random()*90;
            $($(".user_blue_small")[i]).animate({left: randLeft + "%"}, 4000);
        }
        */

        function beginsWithVowel(word) {
            var isVowel = new RegExp("[a,e,i,o,u]", "g")
            var ch = word.charAt(0);
            return isVowel.test(ch);
        }

        var insiderDescription = "insider";
        var insiderDescriptionPlural = "insiders";
        var currentInsiderName;

        function hashChange() {
            if (document.location.hash.indexOf("insidechat") == -1 && $(".inside_chatPane, #inside_chatPane_custom").is(":visible")) {
                closeChatWindow();
            } else if (document.location.hash.indexOf("insidechat") >= 0 && !$(".inside_chatPane, #inside_chatPane_custom").is(":visible")) {
                //animateOpenChatPane(); //don't open based on hash
            }
        }

        var scaleTimeout;
        function closeChatPane() {
            //if(!chatTabImageSet) {
            //setOnlineChatTab();
            //}
            if (chatPaneOpen == false) {
                return;
            }
            chatPaneOpen = false;
            $("body").removeClass("inside-chat-open");
            insideChat.chatPaneOpen = chatPaneOpen;
            $("#inside_holder").removeClass("mobileChatOpen");
            if (device == 2 && document.location.hash.indexOf("insidechat") >= 0) {
                history.go(-1);
            }
            unbindDocumentClick();

            var time = 250;
            var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
            var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
            var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
            var is_safari = navigator.userAgent.indexOf("Safari") > -1;
            var is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
            if ((is_chrome) && (is_safari)) { is_safari = false; }
            if ((is_chrome) && (is_opera)) { is_chrome = false; }

            if (is_safari) {
                time = 0;
            }

            if (typeof (tabPosition) == "undefined" || tabPosition == null || tabPosition == "" || tabPosition == "topright" || tabPosition == "" || tabPosition == "right" || tabPosition == "bottomright") {
               // $(".inside_chatPane, #inside_chatPane_custom").animate({ right: "-330px" }, { step: function () { setGlassPosition() }, duration: time });
                insideTween.TweenLite.to($(".inside_chatPane, #inside_chatPane_custom"), time/1000, { right: -$(".inside_chatPane, #inside_chatPane_custom").width(), ease: insideTween.Quad.easeOut, onUpdate:function(){setGlassPosition();} });
            } else {
                //$(".inside_chatPane, #inside_chatPane_custom").animate({ right: "-330px" }, { step: function () { setGlassPosition() }, duration: time });
                insideTween.TweenLite.to($(".inside_chatPane, #inside_chatPane_custom"), time/1000, { left: -$(".inside_chatPane, #inside_chatPane_custom").width(), ease: insideTween.Quad.easeOut, onUpdate:function(){setGlassPosition();} });
            }

            setTimeout(function () { $(".inside_chatPane, #inside_chatPane_custom").hide(); }, time);

            clearTimeout(scaleTimeout);
            scaleTimeout = setTimeout(function () { $(window).resize(); }, time);

            // if survey is showing in the chat pane, unstick it
            if ($(".inside_chatPane, #inside_chatPane_custom").find("#insideSurveyHolder").length > 0) {
                $.inside.server.unstick($(".inside_chatPane, #inside_chatPane_custom").find("#insideSurveyHolder").attr("stickyid"));
            }

            if (device == 2 && wasVisible != null) {
                wasVisible.show();
            }

            if (device == 2 && mobileScrollPosBeforeChatOpened != -1) {
                $(window).scrollTop(mobileScrollPosBeforeChatOpened);
                setTimeout(function () {
                    $(window).scrollTop(mobileScrollPosBeforeChatOpened);
                }, 0);
            }

        }

        function closeAndHideChat() {
            if (chatInProgress) {
                $("#closeChatHolder").closest(".inside_systemMessage").remove();
                showSystemMessage("<div id='closeChatHolder'>" + translate("Close the chat?") + "<br /> <span class='insideSubmitButton' id='closeChatButton'>" + translate("Yes") + "</span><span class='insideSubmitButton' id='closeChatCancel' style='margin-left: 10px;'>" + translate("Cancel") + "</span></div>");
                $("#closeChatButton").click(function () {
                    $.inside.server.stopChat(currentChatId);
                    chatInProgress = false;
                    doCloseAndHideChat();
                    $("#closeChatHolder").closest(".inside_systemMessage").remove();
                });
                $("#closeChatCancel").click(function () {
                    $("#closeChatHolder").closest(".inside_systemMessage").remove();
                });

            } else {
                if(!messageReceived){
                    closeChatWindow();
                } else {
                    doCloseAndHideChat();
                }
            }
        }

        dontOpenForChats = [];
        function doCloseAndHideChat() {
            dontOpenForChats.push(currentChatId); //current chat is now ended, do not open for chats in this conversation.
            closeChatWindow();
            try {
                sessionStorage.setItem("insideChatClosed", "True");
            } catch (e) { }
            $("#inside_liveChatTab").hide();

        }

        insideChat.closeChatPane = closeChatWindow;
        function closeChatWindow() {
            closeChatPane();
            //$(".blueChat").show();
            startOffline = true;
            sendChat("/closechat");
            if (!isLocal()) {
                $.inside.server.chatData(currentChatId || 0, "/closechat");
            }
            if (!chatTabImageSet) {
                setChatTab();
            }

            if (availableAssistants.length > 0 || messageReceived) {
                if (!hideChatTab) {
                    if (device == 2) {
                        $("#inside_liveChatTab").show().css("opacity", "1");
                    } else {

                        $("#inside_liveChatTab").show().css("opacity", "0.1");
                        insideTween.TweenLite.to($("#inside_liveChatTab"), 0, { opacity: 1, delay:0.25, ease: insideTween.Quad.easeOut });

                    }
                } else {
                    if(messageReceived || activeChat) {
                        $("#inside_liveChatTab").show().css("opacity", "0.1");
                        insideTween.TweenLite.to($("#inside_liveChatTab"), 0, { opacity: 1, delay:0.25, ease: insideTween.Quad.easeOut });
                    }
                }
            } else {
                if (!hideChatTab && typeof (settings.offlineChat) != "undefined" && settings.offlineChat.enabled) {
                    $("#inside_liveChatTab").show();
                }
            }
        }

        insideChat.hideChatPane = function () {
            if ($(".inside_chatPane").is(":visible")) {
                closeChatPane();
                $("#inside_liveChatTab").show();
            }
        }

        function smallOfferClick() {
            chatTabClick();
        }

        function windowScroll() {
            if (!chatPaneOpen) {
                return;
            }
            setGlassPosition();
        }

        function setGlassPosition() {
            if ($("#inside_chatInput").length == 0) {
                return;
            }
            if (device == 2) { return; }
            $(".inside_glass").css("background-position", -$(".inside_chatPane").position().left * 0.5 + "px " + -$(window).scrollTop() * 0.25 + "px");
        }

        function roundToPlaces(num, places) {
            places = places < 0 ? 0 : places;
            var mult = Math.pow(10, places);
            return Math.round(num * mult) / mult
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function validatePhone(phoneNo) {
            var phoneNum = phoneNo.replace(/[^\d]/g, '');
            if (phoneNum.length > 5 && phoneNum.length < 16) { return true; }
        }

        insideFrontInterface.showLeaveMessageForm = showLeaveMessageForm;
        function showLeaveMessageForm() {
            if ($("#inside_leaveMessageForm").length == 0) {
                var html = "<div id='inside_leaveMessageForm' class='inside_glass'>";
                html += "<div class='inside_closeCross fonticon icon-hclose'></div>";
                html += "<div id='inside_leaveMessageForm_form'>"
                html += "<div class='inside_formTitle'></div>";
                html += "<div id='inside_offline_name'><span class='inside_label'>Your name</span><input id='inside_name' required></div>";
                html += "<div id='inside_offline_email' ><span class='inside_label'>E-mail</span><input id='inside_email' type='email' required></div>";
                html += "<div id='inside_offline_phone'><span class='inside_label'>Phone</span><input id='inside_phone' name='Phone' /></div>";
                html += "<div id='inside_offline_subject'><span class='inside_label'>Subject</span><input id='inside_subject' type='text'></div>";

                if (typeof (settings.offlineChat) != "undefined" && typeof (settings.offlineChat.addFields) != "undefined" && settings.offlineChat.addFields.length != 0) {
                    for (var i = 0; i < settings.offlineChat.addFields.length; i++) {
                        html += "<div  class='inside_offline_addfield'><span class='inside_label'>" + escapeHtml(settings.offlineChat.addFields[i]) + "</span>" + $("<input />").attr("id", "inside_offline_addfield_" + settings.offlineChat.addFields[i])[0].outerHTML + "</div>";
                    }
                }

                html += "<div id='inside_offline_message'><span class='inside_label'>Message</span><textarea id='inside_message' required style='height: 80px;'></textarea></div>";

                html += "<input type='button' class='inside_sendButton' value='" + translate("Send") + "'></input>";
                html += "<div class='inside_output'></div>";
                html += "</div>";
                html += "<div id='inside_submitMessage'><div class='inside_submitMessage_holder'>Thank you, your message has been submitted to our team.</div></div>";
                //html += '<div id="insideLink" title="Chat software by Inside&trade;" class=""><a href="http://www.inside.tm?ref=insideoffline" target="_new"><img src="' + _insideCDN + '/images/insidelink_grey.png" class=""></a></div>';
                html += "</div>";
                $(html).appendTo("#inside_holder");

                $("#inside_leaveMessageForm .inside_closeCross").click(function () {
                    closeLeaveMessageForm();
                });
                $("#inside_email").change(function () {
                    var isValid = validateEmail($("#inside_email").val());
                    if (!isValid) {
                        $("#inside_leaveMessageForm .inside_output").html(translate("Email address is invalid. Please check."));
                    } else {
                        $("#inside_leaveMessageForm .inside_output").html("");
                    }
                });

                if (typeof (settings.offlineChat) != "undefined") {
                    if (settings.offlineChat.advanceHtml && settings.offlineChat.advanceHtml.length > 0) {
                        var style = $('#inside_leaveMessageForm').attr('style');
                        $("#inside_leaveMessageForm").replaceWith(settings.offlineChat.advanceHtml);
                        var newStyle = $('#inside_leaveMessageForm').attr('style');
                        $('#inside_leaveMessageForm').attr('style', (newStyle ? newStyle + ';' : '') + style);
                        $("#inside_leaveMessageForm .inside_closeCross").click(function () {
                            closeLeaveMessageForm();
                        });
                    } else {
                        if (settings.offlineChat.message != "" && settings.offlineChat.message != null) {
                            settings.offlineChat.message = settings.offlineChat.message.replace(/<br\s*\/?>/mg, "\n");
                            $("#inside_leaveMessageForm .inside_formTitle").html(safeHTML(settings.offlineChat.message));
                        } else {
                            $("#inside_leaveMessageForm .inside_formTitle").html("We're offline!<br />Leave a message:");
                        }
                        if (typeof (settings.siteLogo) != "undefined" && settings.siteLogo != "" && settings.siteLogo != null) {
                            $("#inside_leaveMessageForm .inside_formTitle").prepend(
                                "<div class='inside_siteLogo' style='margin-bottom: 10px;'>"
                                    + $("<img />").attr("src", _insideCDN + settings.siteLogo)[0].outerHTML
                                + "</div>");
                        }
                        if (!settings.offlineChat.showNameField) {
                            $("#inside_offline_name").hide();
                        }
                        if (!settings.offlineChat.showPhoneField) {
                            $("#inside_offline_phone").hide();
                        }
                        if (!settings.offlineChat.showEmailField) {
                            $("#inside_offline_email").hide();
                        }
                        if (settings.offlineChat.showSubjectField != true) {
                            $("#inside_offline_subject").hide();
                        }
                    }
                }

                if ($("#inside_phone").length > 0) {
                    initIntlTelInputPlugin("inside_phone");
                }

                var submitting = false;
                $("#inside_leaveMessageForm .inside_sendButton").click(function () {
                    if (submitting) {
                        return;
                    }
                    submitting = true;
                    var fields = [];
                    $("#inside_leaveMessageForm .inside_output").empty();
                    if (settings.offlineChat.advanceHtml && settings.offlineChat.advanceHtml.length > 0) {
                        $("#inside_leaveMessageForm input[required]:visible, #inside_leaveMessageForm textarea:visible[required]").each(function () {
                            if ($.trim($(this).val()) == "") {
                                if ($(this).attr("name")) {
                                    fields.push($(this).attr("name"));
                                } else {
                                    fields.push($(this).siblings(".inside_label").text());
                                }
                            }
                        });
                        if (fields.length > 0) {
                            $("#inside_leaveMessageForm .inside_output").html(tranlate("Enter " + escapeHtml(fields.join(", "))));
                            submitting = false;
                            return;
                        }
                        if ($('#inside_email:visible').size() > 0) {
                            if ($.trim($("#inside_email").val()).length > 0 && !validateEmail($("#inside_email").val())) {

                                $("#inside_leaveMessageForm .inside_output").html(translate("Email address is invalid. Please check."));
                                submitting = false;
                                return;
                            }
                        } else if ($('#inside_email').size() > 0) {
                            $('#inside_email').val('');
                        }
                    } else {
                        $("#inside_leaveMessageForm input:visible, #inside_leaveMessageForm textarea:visible").each(function () {
                            if ($(this).val() == "") {
                                if ($(this).attr("name")) {
                                    fields.push($(this).attr("name"));
                                } else {
                                    fields.push($(this).siblings(".inside_label").text());
                                }
                            }
                        });
                        if (fields.length > 0) {
                            $("#inside_leaveMessageForm .inside_output").html(translate("Enter " + escapeHtml(fields.join(", "))));
                            submitting = false;
                            return;
                        }
                        if ($("#inside_email:visible").length > 0 && $("#inside_email").val().trim().length > 0 && !validateEmail($("#inside_email").val())) {
                            $("#inside_leaveMessageForm .inside_output").html(translate("Email address is invalid. Please check."));
                            submitting = false;
                            return;
                        }
                    }

                    if ($('#inside_phone:visible').size() > 0) {
                        if ($.trim($("#inside_phone").val()).length > 0 && !$("#inside_phone").intlTelInput("isValidNumber")) {
                            $("#inside_leaveMessageForm .inside_output").html(translate("Phone is invalid. Please check."));
                            submitting = false;
                            return;
                        }
                    } else if ($('#inside_phone').size() > 0) {
                        $('#inside_phone').val('');
                    }

                    //submit message form
                    var args = {
                        name: $("#inside_name").val(),
                        email: $("#inside_email").val(),
                        subject: $('#inside_subject').val(),
                        message: stripHTML($("#inside_message").val()),
                        createLeadInSalesforce: settings.offlineChat.autoCreateLeadInSalesforce,
                        createLeadInZendesk: settings.offlineChat.autoCreateLeadInZendesk
                    };

                    if ($("#inside_phone:visible").size() > 0) {
                        args.phone = $("#inside_phone").intlTelInput("getNumber");
                        args.ismobile = $("#inside_phone").intlTelInput("getNumberType") === 1;
                    }

                    args.extra = {};
                    if ($(".inside_offline_addfield").length > 0) {

                        $(".inside_offline_addfield").each(function () {
                            var label, val;
                            if (settings.offlineChat.advanceHtml && settings.offlineChat.advanceHtml.length > 0) {
                                if ($(this).find("input").attr('name')) {
                                    label = $(this).find("input").attr('name');
                                } else {
                                    label = $(this).find(".inside_label").text();
                                }
                            } else {
                                label = $(this).find(".inside_label").text();
                            }
                            if ($(this).find("input").size() > 0) {
                                if ($(this).find("input").is(':checkbox')) {
                                    val = $(this).find("input").is(':checked') ? 'Yes' : 'No';
                                } else {
                                    val = $(this).find("input").val();
                                }
                            } else if ($(this).find("textarea").size() > 0) {
                                val = $(this).find('textarea').val();
                            }
                            //val = $(this).find("input").val();
                            args.extra[label] = val;
                        });
                    }

                    insideAPI.call("OfflineMessage", args, function (data) {
                        //message submitted successfully
                        submitting = false;

                        if (data.status == "Error") {
                            $("#inside_leaveMessageForm .inside_output").html(translate("There was an error submitting your message. Please try again."));
                            return;
                        }
                        //console.log("success", data);
                        //$("#inside_leaveMessageForm").hide();
                        //clear form.
                        $("#inside_leaveMessageForm .inside_output").html("");
                        $("#inside_leaveMessageForm input, #inside_leaveMessageForm textarea").not(":button, :submit").val("");

                        $("#inside_submitMessage").show();
                        $("#inside_leaveMessageForm_form").hide();

                        windowScale();

                        //auto close this message after 4 seconds
                        setTimeout(function () {
                            closeLeaveMessageForm();
                        }, 4000);

                    });
                });

                //populate with settings
                //settings.offlineChat
                //settings.offlineChat.enabled;



                $("#inside_submitMessage").hide();

                //load any custom css for the offline chat
                if (typeof (settings.offlineChat.customCSS) != "undefined" && settings.offlineChat.customCSS && typeof (settings.offlineChat.customiseLeaveMessageCssFile) != "undefined" && settings.offlineChat.customiseLeaveMessageCssFile != "") {
                    var version = Math.random();
                    if (typeof (settings.offlineChat.version) != "undefined") {
                        version = settings.offlineChat.version;
                    }
                    $.inside.front.loadCssFile(_insideCDN + settings.offlineChat.customiseLeaveMessageCssFile + "?" + version, openLeaveMessageForm);
                    closeLeaveMessageForm();
                    return;
                }

            }

            positionOfflineForm();
            openLeaveMessageForm();

        }

        function openLeaveMessageForm() {
            closeChatPane();
            $("#inside_liveChatTab").fadeOut();
            if ($("#inside_leaveMessageForm").length == 0) {
                showLeaveMessageForm();
                return;
            }
            $("#inside_leaveMessageForm_form").show();
            $("#inside_submitMessage").hide();
            $("#inside_leaveMessageForm .inside_output").html("");
            setTimeout(function () {
                positionOfflineForm();
            }, 0);

            if (typeof (tabPosition) == "undefined" || tabPosition == null || tabPosition == "topright" || tabPosition == "" || tabPosition == "right" || tabPosition == "bottomright") {
                $("#inside_leaveMessageForm").css("right", -$("#inside_leaveMessageForm").outerWidth());
                $("#inside_leaveMessageForm").animate({ right: 0 }, 250);
            } else {
                $("#inside_leaveMessageForm").css("left", -$("#inside_leaveMessageForm").outerWidth());
                $("#inside_leaveMessageForm").animate({ left: 0 }, 250);
            }
            //$("#inside_leaveMessageForm").css("background-position", "0px -300px");
            setTimeout(function () {
                $("#inside_leaveMessageForm").addClass("open");
            }, 0);
            $("#inside_leaveMessageForm").show();

            if (typeof (settings.headerColour) != "undefined") {
                $("#inside_leaveMessageForm .inside_siteLogo").css("background", settings.headerColour);

            }
            if (typeof (settings.iconColour) != "undefined" && settings.iconColour != null) {
                $("#inside_leaveMessageForm .inside_closeCross").css("color", settings.iconColour);
            }

        }
        function closeLeaveMessageForm() {
            $("#inside_leaveMessageForm").hide();
            $("#inside_leaveMessageForm").removeClass("open");
            //$("#inside_leaveMessageForm").css("background-position", "0px 0px");
            setInsideTab();
        }

        function stripHTML(str) {
            var p = document.createElement("p"); p.innerHTML = str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return String(p.textContent || p.innerText).replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }

        function positionOfflineForm() {
            if ($("#inside_leaveMessageForm:visible").length == 0) {
                return;
            }
            var viewport = $.inside.front.getViewPortSize();
            var top = viewport.height * 0.5;
            top -= $("#inside_leaveMessageForm").outerHeight() * 0.5;

            if (chatPanePosition == "bottom") {
                top = viewport.height - $("#inside_leaveMessageForm").outerHeight() - 20;
            } else if (chatPanePosition == "top") {
                top = 0;
            }

            $("#inside_leaveMessageForm").css("top", top + "px");
        }

        //toggle blur effect
        var blurEnabled = false;
        var messageReceived = false;
        insideChat.preventTabClick = false;

        function chatTabClick(force) {
            if (insideChat.preventTabClick) {
                insideChat.preventTabClick = false;
                return;
            }

            if(device == 2 && !force && $("#inside_liveChatTab").attr("tabHidden") == "true") {
                insideFrontInterface.hoverShowTabs($("#inside_liveChatTab"));
                return;
            }

            if(!chatInProgress) {
                currentChatId = -1;
            }

            if (force != true && availableAssistants.length == 0 && !activeChat && typeof (settings.offlineChat) != "undefined" && settings.offlineChat.enabled) {
                showLeaveMessageForm();
                return;
            }


            if (!chatPaneAdded) {
                addChatPane();
            }

            var prechat = fetchPrechatLocally();
            if(!chatInProgress) {
                if (settings && settings.preChat && settings.preChat.enabled && !prechatFilled && !chatInProgress && !prechat) {
                    showPrechatForm();
                    return;
                } else {
                    if(settings.showDept) {
                        showDepartmentSelector();
                    }
                }
            }

            if (availableAssistants.length == 0) {
                $("#inside_liveChatTab").hide();
                disableChat();
            }           

            if (!storeImageShown) {
                showStoreImage(availableAssistants[0]);
                if (blurEnabled) {
                    setTimeout(function () {
                        blurBackground.applyBlur(".inside_chatPane", "#inside_holder");
                    }, 0);
                }
            }

            if (!chatInProgress) {
                if (typeof ($.inside) != "undefined") {
                    //$.inside.server.startChat("/assistclick::" + thisUserId);
                }
            } else {
                //chat already in progress
                if (!isLocal()) {
                    $.inside.server.chatData(currentChatId, "/openchat");
                }
                //sendChat("/openchat");
            }

            var newChatOpen = false;
            if ($(this).attr("class") == "redChat") {
                newChatOpen = true;
            }
            $(this).attr("class", "blueChat");
            //$(".inside_notification").hide();
            clearInterval(alertInterval);

            if ($("#inside_joinTheCallChatTab").is(":visible")) {
                setJoinTheCallTag(false);
            }

            if (!$(".inside_chatPane").is(":visible")) {
                animateOpenChatPane();
            }

            if (insideStoreHidden) {
                $("#inside_frontStoreAssistant").show();
                $("#inside_frontStoreAssistant").hide();
            }

            chatPaneMode = "";
            windowScale();
            
            if (newChatOpen) {
                sendChat("/openchat");
            }

            scrollChatToBottom();

             if(settings.showDisclaimer) {
                showChatDisclaimer();
            }
            if ($(".inside_chatMessageUser").length == 0 && $(".inside_chatMessageAssist").length == 0) {
                showGreetingText();
            } else if (!greetingMessageShown && !chatInProgress) {
                showGreetingText();
            }

            if (device == 1) {
                $("#inside_chatInput, #inside_chatPane_custom_input").focus();
            }

            if (device == 2) {
                if (document.location.hash == "") {
                    document.location.hash = "insidechat";
                } else {
                    if (document.location.hash.indexOf("insidechat") == -1) {
                        document.location.hash += "/insidechat";
                    }
                }
            }
        }

        //#region Prechat Form

        var departments = [];
        var departmentsDoesNotExist = false;
        var prechatFilled = false;
        var prechatFormData;
        insideChat.showPrechatForm = showPrechatForm;
        function showPrechatForm() {
            if (!settings.preChat) {
                return;
            }

            if ($("#inside_prechatForm").length == 0) {
                if (settings.askDept && !departmentsDoesNotExist && (!departments || departments.length === 0)) {
                    insideAPI.call("getDepartments", {}, function (response) {
                        if (response && response.data && response.data.length > 0) {
                            departments = response.data.splice(0);
                            departmentsDoesNotExist = false;
                            showPrechatForm();
                        } else {
                            departmentsDoesNotExist = true;
                            showPrechatForm();
                        }
                    });
                    return;
                }

                var html = "<div id='inside_prechatForm' class='inside_glass'>";
                html += "<div class='inside_closeCross fonticon icon-hclose'></div>";
                html += "<div id='inside_prechatForm_form'>"
                html += "<div class='inside_formTitle'></div>";

                if (settings.preChat.isNameEnabled) {
                    html += "<div id='inside_prechat_name_field' class='inside-prechat-field'><input type='textbox' name='name' id='inside_prechat_name' placeholder='Full Name' /></div>";
                }
                if (settings.preChat.isEmailEnabled) {
                    html += "<div id='inside_prechat_email_field' class='inside-prechat-field'><input type='textbox' name='email' id='inside_prechat_email' placeholder='Email' /></div>";
                }
                if (settings.preChat.isPhoneEnabled) {
                    html += "<div id='inside_prechat_phone_field' class='inside-prechat-field'><input type='textbox' name='phone' id='inside_prechat_phone' /></div>";
                }
                if (settings.preChat.isCompanyEnabled) {
                    html += "<div id='inside_prechat_company_field' class='inside-prechat-field'><input type='textbox' name='company' id='inside_prechat_company' placeholder='Company' /></div>";
                }

                if (settings.preChat && settings.preChat.fields && settings.preChat.fields.length > 0) {
                    var prechatFormFields = eval(settings.preChat.fields);
                    for (var i = 0; i < prechatFormFields.length; i++) {
                        var fieldName = prechatFormFields[i].systemField.toLowerCase();
                        html += "<div id='inside_prechat_" + fieldName + "_field' class='inside-prechat-field'>";

                        switch (prechatFormFields[i].type) {
                            case "textbox":
                                html += "<input type='text' name='" + fieldName + "' id='inside_prechat_" + fieldName +
                                    "' placeholder='" + htmlEncode(prechatFormFields[i].label) + "' " +
                                    (prechatFormFields[i].style ? " style='" + prechatFormFields[i].style + "' " : "") +
                                    (prechatFormFields[i].required ? 'required' : '') + " /></div>";
                                break;
                            case "textarea":
                                html += "<textarea type='text' name='" + fieldName + "' id='inside_prechat_" + fieldName +
                                    "' placeholder='" + htmlEncode(prechatFormFields[i].label) + "' " +
                                    (prechatFormFields[i].style ? " style='" + prechatFormFields[i].style + "' " : "") +
                                    (prechatFormFields[i].required ? 'required' : '') + " ></textarea></div>";
                                break;
                            case "checkbox":
                                html += "<span class='inside_label'>" + htmlEncode(prechatFormFields[i].label) + "</span><input type='checkbox' name='" + fieldName + "' id='inside_prechat_" + fieldName +
                                    (prechatFormFields[i].style ? " style='" + prechatFormFields[i].style + "' " : "") +
                                    (prechatFormFields[i].required ? 'required' : '') + " /></div>";
                                break;
                            case "select":
                                html += "<div class='inside-label'><span>" + htmlEncode(prechatFormFields[i].label) + "</span></div>" +
                                    "<select name='" + fieldName + "' id='inside_prechat_" + fieldName + "' " +
                                    (prechatFormFields[i].style ? " style='" + prechatFormFields[i].style + "' " : "") + ">";

                                for (var j = 0; j < prechatFormFields[i].options.length; j++) {
                                    var o = prechatFormFields[i].options[j];
                                    html += "<option value='" + o.value + "' " + (o.selected ? "selected" : "") + ">" + o.text + "</option>";
                                }
                                html += "</select></div>";
                                break;
                        }
                    }
                }

                if (settings.preChat.isQuestionEnabled && settings.preChat.questionText && settings.preChat.questionText.length > 0) {
                    html += "<div id='inside_prechat_question_field' class='inside-prechat-field'><textarea name='question' id='inside_prechat_question' placeholder='" + htmlEncode(settings.preChat.questionText) + "'></textarea></div>";
                }

                if (settings.askDept) {
                    var askDepartmentPrompt = "To help us better assist you, which department do you require assistance from?";
                    if (settings.askDeptText && settings.askDeptText.length > 0) {
                        askDepartmentPrompt = settings.askDeptText;
                    }
                    html += "<div class='inside_dept_selector'><span class='inside_dept_selector_title'>" + escapeHtml(askDepartmentPrompt) + "</span><div>";

                    for (var i = 0; i < departments.length; i++) {
                        html += "<input type='radio' name='inside_dept' id='inside_chat_check_" + i + "' value='" + departments[i] + "' />"
                        html += escapeHtml(departments[i]) + "<br />";
                    }
                    html += "</div></div>";
                }

                if (settings.preChat.isChatDisclaimerEnabled && settings.chatDisclaimer && settings.chatDisclaimer.length > 0) {
                    html += "<div class='inside_dept_selector'><span class='inside_dept_selector_title'>" + safeHTML(settings.chatDisclaimer) + "</span><div>";

                    if (settings.agreeToDisclaimer == true) {
                        if (settings.agreeToDisclaimerText && settings.agreeToDisclaimerText.length > 0) {
                            html += "<input type='checkbox' name='inside_chat_disclaimer' id='inside_disclaimer_agree' value='" + settings.agreeToDisclaimerText + "' />"
                            html += escapeHtml(settings.agreeToDisclaimerText) + "<br />";
                        }

                        if (settings.disagreeToDisclaimerText && settings.disagreeToDisclaimerText.length > 0) {
                            html += "<input type='checkbox' name='inside_chat_disclaimer' id='inside_disclaimer_disagree' value='" + settings.disagreeToDisclaimerText + "' />"
                            html += escapeHtml(settings.disagreeToDisclaimerText);
                        }
                    }
                    html += "</div></div>";
                }

                html += "<input type='button' class='inside_startChatButton' value='start chat'></input>";
                html += "<div class='inside_output'></div>";
                html += "</div>";
                //html += 	'<div id="insideLink" title="Chat software by Inside&trade;" class=""><a href="http://www.inside.tm?ref=insideoffline" target="_new"><img src="' + _insideCDN + '/images/insidelink_grey.png" class=""></a></div>';
                html += "</div>";

                $(html).appendTo("#inside_holder");
                $("#inside_prechatForm .inside_closeCross").click(function () {
                    closePrechatForm();
                });
                $("#inside_disclaimer_agree").click(function () {
                    $("#inside_disclaimer_disagree").attr("checked", false);
                });
                $("#inside_disclaimer_disagree").click(function () {
                    $("#inside_disclaimer_agree").attr("checked", false);
                });

                if ($("#inside_prechat_email").length > 0) {
                    $("#inside_prechat_email").change(function () {
                        var isValid = validateEmail($("#inside_prechat_email").val());
                        if (!isValid) {
                            $("#inside_prechatForm .inside_output").html(translate("Email address is invalid. Please check."));
                        } else {
                            $("#inside_prechatForm .inside_output").html("");
                        }
                    });
                }

                if ($("#inside_prechat_phone").length > 0) {
                    initIntlTelInputPlugin("inside_prechat_phone");
                }

                var submitting = false;

                $("#inside_prechatForm .inside_startChatButton").click(function () {
                    if (submitting) {
                        return;
                    }
                    submitting = true;
                    var missingFields = [];
                    var customFields = {};
                    $("#inside_prechatForm .inside_output").empty();

                    if (settings.preChat && settings.preChat.fields && settings.preChat.fields.length > 0) {
                        var prechatFormFields = eval(settings.preChat.fields);
                        for (var i = 0; i < prechatFormFields.length; i++) {
                            var fieldName = prechatFormFields[i].systemField.toLowerCase();
                            var fieldVal = $("#inside_prechat_" + fieldName).val();
                            if (!fieldVal || fieldVal.trim().length === 0) {
                                if (prechatFormFields[i].required) {
                                    missingFields.push(prechatFormFields[i].label);
                                }
                            } else {
                                customFields[prechatFormFields[i].systemField] = fieldVal;
                            }
                        }
                    }

                    if (settings.preChat.isNameEnabled) {
                        var fieldVal = $("#inside_prechat_name").val();
                        if (!fieldVal || fieldVal.trim().length === 0) {
                            missingFields.push("Name");
                        }
                    }
                    if (settings.preChat.isEmailEnabled) {
                        var fieldVal = $("#inside_prechat_email").val();
                        if (!fieldVal || fieldVal.trim().length === 0) {
                            missingFields.push("Email");
                        }
                    }
                    if (settings.preChat.isPhoneEnabled) {
                        var fieldVal = $("#inside_prechat_phone").val();
                        if (!fieldVal || fieldVal.trim().length === 0) {
                            missingFields.push("Phone");
                        }
                    }
                    if (settings.preChat.isCompanyEnabled) {
                        var fieldVal = $("#inside_prechat_company").val();
                        if (!fieldVal || fieldVal.trim().length === 0) {
                            missingFields.push("Company");
                        }
                    }

                    if (missingFields.length > 0) {
                        $("#inside_prechatForm .inside_output").html(translate("Enter " + escapeHtml(missingFields.join(", "))));
                        submitting = false;
                        return;
                    }

                    if ($("#inside_prechat_email").length > 0) {
                        if (!validateEmail($("#inside_prechat_email").val())) {
                            $("#inside_prechatForm .inside_output").html(translate("Email address is invalid. Please check."));
                            submitting = false;
                            return;
                        }
                    }
                    if ($("#inside_prechat_phone").length > 0) {
                        if ($("#inside_prechat_phone").val().trim().length > 0 && !$("#inside_prechat_phone").intlTelInput("isValidNumber")) {
                            $("#inside_prechatForm .inside_output").html(translate("Phone is invalid. Please check."));
                            submitting = false;
                            return;
                        }
                    }
                    if (settings.askDept && settings.forceDept && $(".inside_dept_selector input[name='inside_dept']:checked").length === 0) {
                        $("#inside_prechatForm .inside_output").html(translate("Please select a department."));
                        submitting = false;
                        return;
                    }
                    if (settings.preChat.isChatDisclaimerEnabled && settings.chatDisclaimer && settings.chatDisclaimer.length > 0 && settings.agreeToDisclaimer && !$("#inside_disclaimer_agree").is(":checked")) {
                        $("#inside_prechatForm .inside_output").html(translate("Please agree to the disclaimer."));
                        submitting = false;
                        return;
                    }

                    //Submit prechat form
                    var args = {
                        name: $("#inside_prechat_name").val(),
                        email: $("#inside_prechat_email").val(),
                        company: $("#inside_prechat_company").val(),
                        message: $("#inside_prechat_question").val(),
                        department: $(".inside_dept_selector input[name='inside_dept']:checked").val(),
                        customFields: customFields,
                        createLeadInSalesForce: settings.preChat.createLeadInSalesforce
                    };

                    if ($("#inside_prechat_phone").length > 0) {
                        args.phone = $("#inside_prechat_phone").intlTelInput("getNumber");
                        args.ismobile = $("#inside_prechat_phone").intlTelInput("getNumberType") === 1;
                    }

                    insideAPI.call("InsertPrechat", args, function (data) {
                        //Prechat submitted successfully
                        submitting = false;

                        if (data.status == "Error") {
                            $("#inside_prechatForm .inside_output").html(translate("There was an error submitting your information. Please try again."));
                            return;
                        }
                        savePrechatLocally(args);
                        prechatFilled = true;

                        //Remove prechat form
                        $("#inside_prechatForm").remove();

                        windowScale();

                        //Start chat
                        setTimeout(function () {
                            chatTabClick(true);

                            if (args.message && args.message.trim().length > 0) {
                                args.message = htmlEntities(args.message.trim());
                                sendChat(args.message);
                            }
                        }, 1000);

                    });
                });
            }

            positionPrechatForm();
            openPrechatForm();
        }

        function initIntlTelInputPlugin(elementId) {
            elementId = "#" + elementId;
            $.inside.front.loadIntlTelInputPlugin();

            /*
             * International Telephone Input v8.5.2
             * https://github.com/jackocnr/intl-tel-input.git
             * Licensed under the MIT license
             */
            !function (a) { a(jQuery, window, document) }(function (a, b, c, d) { "use strict"; function e(b, c) { this.a = a(b), c && (a.extend(c, c, { a: c.allowDropdown, b: c.autoHideDialCode, c: c.autoPlaceholder, c2: c.customPlaceholder, d: c.dropdownContainer, e: c.excludeCountries, f: c.formatOnInit, g: c.geoIpLookup, h: c.initialCountry, i: c.nationalMode, j: c.numberType, k: c.onlyCountries, l: c.preferredCountries, m: c.separateDialCode, n: c.utilsScript })), this.b = a.extend({}, h, c), this.ns = "." + f + g++, this.d = Boolean(b.setSelectionRange), this.e = Boolean(a(b).attr("placeholder")) } var f = "intlTelInput", g = 1, h = { a: !0, b: !0, c: !0, c2: null, d: "", e: [], f: !0, g: null, h: "", i: !0, j: "MOBILE", k: [], l: ["us", "gb"], m: !1, n: "" }, i = { b: 38, c: 40, d: 13, e: 27, f: 43, A: 65, Z: 90, j: 32, k: 9 }; a(b).load(function () { a.fn[f].windowLoaded = !0 }), e.prototype = { _a: function () { return this.b.i && (this.b.b = !1), this.b.m && (this.b.b = this.b.i = !1, this.b.a = !0), this.g = /Android.+Mobile|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent), this.g && (a("body").addClass("iti-mobile"), this.b.d || (this.b.d = "body")), this.h = new a.Deferred, this.i = new a.Deferred, this._b(), this._f(), this._h(), this._i(), this._i2(), [this.h, this.i] }, _b: function () { this._d(), this._d2(), this._e() }, _c: function (a, b, c) { b in this.q || (this.q[b] = []); var d = c || 0; this.q[b][d] = a }, _c2: function (b, c) { var d; for (d = 0; d < b.length; d++) b[d] = b[d].toLowerCase(); for (this.p = [], d = 0; d < j.length; d++) c(a.inArray(j[d].iso2, b)) && this.p.push(j[d]) }, _d: function () { this.b.k.length ? this._c2(this.b.k, function (a) { return -1 != a }) : this.b.e.length ? this._c2(this.b.e, function (a) { return -1 == a }) : this.p = j }, _d2: function () { this.q = {}; for (var a = 0; a < this.p.length; a++) { var b = this.p[a]; if (this._c(b.iso2, b.dialCode, b.priority), b.areaCodes) for (var c = 0; c < b.areaCodes.length; c++) this._c(b.iso2, b.dialCode + b.areaCodes[c]) } }, _e: function () { this.r = []; for (var a = 0; a < this.b.l.length; a++) { var b = this.b.l[a].toLowerCase(), c = this._y(b, !1, !0); c && this.r.push(c) } }, _f: function () { this.a.attr("autocomplete", "off"); var b = "intl-tel-input"; this.b.a && (b += " allow-dropdown"), this.b.m && (b += " separate-dial-code"), this.a.wrap(a("<div>", { "class": b })), this.k = a("<div>", { "class": "flag-container" }).insertBefore(this.a); var c = a("<div>", { "class": "selected-flag" }); c.appendTo(this.k), this.l = a("<div>", { "class": "iti-flag" }).appendTo(c), this.b.m && (this.t = a("<div>", { "class": "selected-dial-code" }).appendTo(c)), this.b.a ? (c.attr("tabindex", "0"), a("<div>", { "class": "iti-arrow" }).appendTo(c), this.m = a("<ul>", { "class": "country-list hide" }), this.r.length && (this._g(this.r, "preferred"), a("<li>", { "class": "divider" }).appendTo(this.m)), this._g(this.p, ""), this.o = this.m.children(".country"), this.b.d ? this.dropdown = a("<div>", { "class": "intl-tel-input iti-container" }).append(this.m) : this.m.appendTo(this.k)) : this.o = a() }, _g: function (a, b) { for (var c = "", d = 0; d < a.length; d++) { var e = a[d]; c += "<li class='country " + b + "' data-dial-code='" + e.dialCode + "' data-country-code='" + e.iso2 + "'>", c += "<div class='flag-box'><div class='iti-flag " + e.iso2 + "'></div></div>", c += "<span class='country-name'>" + e.name + "</span>", c += "<span class='dial-code'>+" + e.dialCode + "</span>", c += "</li>" } this.m.append(c) }, _h: function () { var a = this.a.val(); this._af(a) ? this._v(a, !0) : "auto" !== this.b.h && (this.b.h ? this._z(this.b.h, !0) : (this.j = this.r.length ? this.r[0].iso2 : this.p[0].iso2, a || this._z(this.j, !0)), a || this.b.i || this.b.b || this.b.m || this.a.val("+" + this.s.dialCode)), a && this._u(a, this.b.f) }, _i: function () { this._j(), this.b.b && this._l(), this.b.a && this._i1() }, _i1: function () { var a = this, b = this.a.closest("label"); b.length && b.on("click" + this.ns, function (b) { a.m.hasClass("hide") ? a.a.focus() : b.preventDefault() }); var c = this.l.parent(); c.on("click" + this.ns, function (b) { !a.m.hasClass("hide") || a.a.prop("disabled") || a.a.prop("readonly") || a._n() }), this.k.on("keydown" + a.ns, function (b) { var c = a.m.hasClass("hide"); !c || b.which != i.b && b.which != i.c && b.which != i.j && b.which != i.d || (b.preventDefault(), b.stopPropagation(), a._n()), b.which == i.k && a._ac() }) }, _i2: function () { var c = this; this.b.n ? a.fn[f].windowLoaded ? a.fn[f].loadUtils(this.b.n, this.i) : a(b).load(function () { a.fn[f].loadUtils(c.b.n, c.i) }) : this.i.resolve(), "auto" === this.b.h ? this._i3() : this.h.resolve() }, _i3: function () { var c = b.Cookies ? Cookies.get("itiAutoCountry") : ""; c && (a.fn[f].autoCountry = c), a.fn[f].autoCountry ? this.handleAutoCountry() : a.fn[f].startedLoadingAutoCountry || (a.fn[f].startedLoadingAutoCountry = !0, "function" == typeof this.b.g && this.b.g(function (c) { a.fn[f].autoCountry = c.toLowerCase(), b.Cookies && Cookies.set("itiAutoCountry", a.fn[f].autoCountry, { path: "/" }), setTimeout(function () { a(".intl-tel-input input").intlTelInput("handleAutoCountry") }) })) }, _j: function () { var a = this; this.a.on("keyup" + this.ns, function () { a._v(a.a.val()) }), this.a.on("cut" + this.ns + " paste" + this.ns + " keyup" + this.ns, function () { setTimeout(function () { a._v(a.a.val()) }) }) }, _j2: function (a) { var b = this.a.attr("maxlength"); return b && a.length > b ? a.substr(0, b) : a }, _l: function () { var b = this; this.a.on("mousedown" + this.ns, function (a) { b.a.is(":focus") || b.a.val() || (a.preventDefault(), b.a.focus()) }), this.a.on("focus" + this.ns, function (a) { b.a.val() || b.a.prop("readonly") || !b.s.dialCode || (b.a.val("+" + b.s.dialCode), b.a.one("keypress.plus" + b.ns, function (a) { a.which == i.f && b.a.val("") }), setTimeout(function () { var a = b.a[0]; if (b.d) { var c = b.a.val().length; a.setSelectionRange(c, c) } })) }); var c = this.a.prop("form"); c && a(c).on("submit" + this.ns, function () { b._removeEmptyDialCode() }), this.a.on("blur" + this.ns, function () { b._removeEmptyDialCode() }) }, _removeEmptyDialCode: function () { var a = this.a.val(), b = "+" == a.charAt(0); if (b) { var c = this._m(a); c && this.s.dialCode != c || this.a.val("") } this.a.off("keypress.plus" + this.ns) }, _m: function (a) { return a.replace(/\D/g, "") }, _n: function () { this._o(); var a = this.m.children(".active"); a.length && (this._x(a), this._ad(a)), this._p(), this.l.children(".iti-arrow").addClass("up") }, _o: function () { var c = this; if (this.b.d && this.dropdown.appendTo(this.b.d), this.n = this.m.removeClass("hide").outerHeight(), !this.g) { var d = this.a.offset(), e = d.top, f = a(b).scrollTop(), g = e + this.a.outerHeight() + this.n < f + a(b).height(), h = e - this.n > f; if (this.m.toggleClass("dropup", !g && h), this.b.d) { var i = !g && h ? 0 : this.a.innerHeight(); this.dropdown.css({ top: e + i, left: d.left }), a(b).on("scroll" + this.ns, function () { c._ac() }) } } }, _p: function () { var b = this; this.m.on("mouseover" + this.ns, ".country", function (c) { b._x(a(this)) }), this.m.on("click" + this.ns, ".country", function (c) { b._ab(a(this)) }); var d = !0; a("html").on("click" + this.ns, function (a) { d || b._ac(), d = !1 }); var e = "", f = null; a(c).on("keydown" + this.ns, function (a) { a.preventDefault(), a.which == i.b || a.which == i.c ? b._q(a.which) : a.which == i.d ? b._r() : a.which == i.e ? b._ac() : (a.which >= i.A && a.which <= i.Z || a.which == i.j) && (f && clearTimeout(f), e += String.fromCharCode(a.which), b._s(e), f = setTimeout(function () { e = "" }, 1e3)) }) }, _q: function (a) { var b = this.m.children(".highlight").first(), c = a == i.b ? b.prev() : b.next(); c.length && (c.hasClass("divider") && (c = a == i.b ? c.prev() : c.next()), this._x(c), this._ad(c)) }, _r: function () { var a = this.m.children(".highlight").first(); a.length && this._ab(a) }, _s: function (a) { for (var b = 0; b < this.p.length; b++) if (this._t(this.p[b].name, a)) { var c = this.m.children("[data-country-code=" + this.p[b].iso2 + "]").not(".preferred"); this._x(c), this._ad(c, !0); break } }, _t: function (a, b) { return a.substr(0, b.length).toUpperCase() == b }, _u: function (c, d, e) { d && b.intlTelInputUtils && this.s && (a.isNumeric(e) || (e = this.b.m || !this.b.i && "+" == c.charAt(0) ? intlTelInputUtils.numberFormat.INTERNATIONAL : intlTelInputUtils.numberFormat.NATIONAL), c = intlTelInputUtils.formatNumber(c, this.s.iso2, e)), c = this._ah(c), this.a.val(c) }, _v: function (b, c) { b && this.b.i && this.s && "1" == this.s.dialCode && "+" != b.charAt(0) && ("1" != b.charAt(0) && (b = "1" + b), b = "+" + b); var d = this._af(b), e = null; if (d) { var f = this.q[this._m(d)], g = this.s && -1 != a.inArray(this.s.iso2, f); if (!g || this._w(b, d)) for (var h = 0; h < f.length; h++) if (f[h]) { e = f[h]; break } } else "+" == b.charAt(0) && this._m(b).length ? e = "" : b && "+" != b || (e = this.j); null !== e && this._z(e, c) }, _w: function (a, b) { return "+1" == b && this._m(a).length >= 4 }, _x: function (a) { this.o.removeClass("highlight"), a.addClass("highlight") }, _y: function (a, b, c) { for (var d = b ? j : this.p, e = 0; e < d.length; e++) if (d[e].iso2 == a) return d[e]; if (c) return null; throw new Error("No country data for '" + a + "'") }, _z: function (a, b) { var c = this.s && this.s.iso2 ? this.s : {}; this.s = a ? this._y(a, !1, !1) : {}, this.s.iso2 && (this.j = this.s.iso2), this.l.attr("class", "iti-flag " + a); var d = a ? this.s.name + ": +" + this.s.dialCode : "Unknown"; if (this.l.parent().attr("title", d), this.b.m) { var e = this.s.dialCode ? "+" + this.s.dialCode : "", f = this.a.parent(); c.dialCode && f.removeClass("iti-sdc-" + (c.dialCode.length + 1)), e && f.addClass("iti-sdc-" + e.length), this.t.text(e) } this._aa(), this.o.removeClass("active"), a && this.o.find(".iti-flag." + a).first().closest(".country").addClass("active"), b || c.iso2 === a || this.a.trigger("countrychange", this.s) }, _aa: function () { if (b.intlTelInputUtils && !this.e && this.b.c && this.s) { var a = intlTelInputUtils.numberType[this.b.j], c = this.s.iso2 ? intlTelInputUtils.getExampleNumber(this.s.iso2, this.b.i, a) : ""; c = this._ah(c), "function" == typeof this.b.c2 && (c = this.b.c2(c, this.s)), this.a.attr("placeholder", c.replace(/\d/g, 'x')) } }, _ab: function (a) { if (this._z(a.attr("data-country-code")), this._ac(), this._ae(a.attr("data-dial-code"), !0), this.a.focus(), this.d) { var b = this.a.val().length; this.a[0].setSelectionRange(b, b) } }, _ac: function () { this.m.addClass("hide"), this.l.children(".iti-arrow").removeClass("up"), a(c).off(this.ns), a("html").off(this.ns), this.m.off(this.ns), this.b.d && (this.g || a(b).off("scroll" + this.ns), this.dropdown.detach()) }, _ad: function (a, b) { var c = this.m, d = c.height(), e = c.offset().top, f = e + d, g = a.outerHeight(), h = a.offset().top, i = h + g, j = h - e + c.scrollTop(), k = d / 2 - g / 2; if (e > h) b && (j -= k), c.scrollTop(j); else if (i > f) { b && (j += k); var l = d - g; c.scrollTop(j - l) } }, _ae: function (a, b) { var c, d = this.a.val(); if (a = "+" + a, "+" == d.charAt(0)) { var e = this._af(d); c = e ? d.replace(e, a) : a } else { if (this.b.i || this.b.m) return; if (d) c = a + d; else { if (!b && this.b.b) return; c = a } } this.a.val(c) }, _af: function (b) { var c = ""; if ("+" == b.charAt(0)) for (var d = "", e = 0; e < b.length; e++) { var f = b.charAt(e); if (a.isNumeric(f) && (d += f, this.q[d] && (c = b.substr(0, e + 1)), 4 == d.length)) break } return c }, _ag: function () { var a = this.b.m ? "+" + this.s.dialCode : ""; return a + this.a.val() }, _ah: function (a) { if (this.b.m) { var b = this._af(a); if (b) { null !== this.s.areaCodes && (b = "+" + this.s.dialCode); var c = " " === a[b.length] || "-" === a[b.length] ? b.length + 1 : b.length; a = a.substr(c) } } return this._j2(a) }, handleAutoCountry: function () { "auto" === this.b.h && (this.j = a.fn[f].autoCountry, this.a.val() || this.setCountry(this.j), this.h.resolve()) }, destroy: function () { if (this.allowDropdown && (this._ac(), this.l.parent().off(this.ns), this.a.closest("label").off(this.ns)), this.b.b) { var b = this.a.prop("form"); b && a(b).off(this.ns) } this.a.off(this.ns); var c = this.a.parent(); c.before(this.a).remove() }, getExtension: function () { return b.intlTelInputUtils ? intlTelInputUtils.getExtension(this._ag(), this.s.iso2) : "" }, getNumber: function (a) { return b.intlTelInputUtils ? intlTelInputUtils.formatNumber(this._ag(), this.s.iso2, a) : "" }, getNumberType: function () { return b.intlTelInputUtils ? intlTelInputUtils.getNumberType(this._ag(), this.s.iso2) : -99 }, getSelectedCountryData: function () { return this.s || {} }, getValidationError: function () { return b.intlTelInputUtils ? intlTelInputUtils.getValidationError(this._ag(), this.s.iso2) : -99 }, isValidNumber: function () { var c = a.trim(this._ag()), d = this.b.i ? this.s.iso2 : ""; return b.intlTelInputUtils ? intlTelInputUtils.isValidNumber(c, d) : null }, setCountry: function (a) { a = a.toLowerCase(), this.l.hasClass(a) || (this._z(a), this._ae(this.s.dialCode, !1)) }, setNumber: function (b, c) { this._v(b), this._u(b, a.isNumeric(c), c) }, handleUtils: function () { b.intlTelInputUtils && (this.a.val() && this._u(this.a.val(), this.b.f), this._aa()), this.i.resolve() } }, a.fn[f] = function (b) { var c = arguments; if (b === d || "object" == typeof b) { var g = []; return this.each(function () { if (!a.data(this, "plugin_" + f)) { var c = new e(this, b), d = c._a(); g.push(d[0]), g.push(d[1]), a.data(this, "plugin_" + f, c) } }), a.when.apply(null, g) } if ("string" == typeof b && "_" !== b[0]) { var h; return this.each(function () { var d = a.data(this, "plugin_" + f); d instanceof e && "function" == typeof d[b] && (h = d[b].apply(d, Array.prototype.slice.call(c, 1))), "destroy" === b && a.data(this, "plugin_" + f, null) }), h !== d ? h : this } }, a.fn[f].getCountryData = function () { return j }, a.fn[f].loadUtils = function (b, c) { a.fn[f].loadedUtilsScript ? c && c.resolve() : (a.fn[f].loadedUtilsScript = !0, a.ajax({ url: b, complete: function () { a(".intl-tel-input input").intlTelInput("handleUtils") }, dataType: "script", cache: !0 })) }, a.fn[f].version = "8.5.2"; for (var j = [["Afghanistan (‫افغانستان‬‎)", "af", "93"], ["Albania (Shqipëri)", "al", "355"], ["Algeria (‫الجزائر‬‎)", "dz", "213"], ["American Samoa", "as", "1684"], ["Andorra", "ad", "376"], ["Angola", "ao", "244"], ["Anguilla", "ai", "1264"], ["Antigua and Barbuda", "ag", "1268"], ["Argentina", "ar", "54"], ["Armenia (Հայաստան)", "am", "374"], ["Aruba", "aw", "297"], ["Australia", "au", "61", 0], ["Austria (Österreich)", "at", "43"], ["Azerbaijan (Azərbaycan)", "az", "994"], ["Bahamas", "bs", "1242"], ["Bahrain (‫البحرين‬‎)", "bh", "973"], ["Bangladesh (বাংলাদেশ)", "bd", "880"], ["Barbados", "bb", "1246"], ["Belarus (Беларусь)", "by", "375"], ["Belgium (België)", "be", "32"], ["Belize", "bz", "501"], ["Benin (Bénin)", "bj", "229"], ["Bermuda", "bm", "1441"], ["Bhutan (འབྲུག)", "bt", "975"], ["Bolivia", "bo", "591"], ["Bosnia and Herzegovina (Босна и Херцеговина)", "ba", "387"], ["Botswana", "bw", "267"], ["Brazil (Brasil)", "br", "55"], ["British Indian Ocean Territory", "io", "246"], ["British Virgin Islands", "vg", "1284"], ["Brunei", "bn", "673"], ["Bulgaria (България)", "bg", "359"], ["Burkina Faso", "bf", "226"], ["Burundi (Uburundi)", "bi", "257"], ["Cambodia (កម្ពុជា)", "kh", "855"], ["Cameroon (Cameroun)", "cm", "237"], ["Canada", "ca", "1", 1, ["204", "226", "236", "249", "250", "289", "306", "343", "365", "387", "403", "416", "418", "431", "437", "438", "450", "506", "514", "519", "548", "579", "581", "587", "604", "613", "639", "647", "672", "705", "709", "742", "778", "780", "782", "807", "819", "825", "867", "873", "902", "905"]], ["Cape Verde (Kabu Verdi)", "cv", "238"], ["Caribbean Netherlands", "bq", "599", 1], ["Cayman Islands", "ky", "1345"], ["Central African Republic (République centrafricaine)", "cf", "236"], ["Chad (Tchad)", "td", "235"], ["Chile", "cl", "56"], ["China (中国)", "cn", "86"], ["Christmas Island", "cx", "61", 2], ["Cocos (Keeling) Islands", "cc", "61", 1], ["Colombia", "co", "57"], ["Comoros (‫جزر القمر‬‎)", "km", "269"], ["Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)", "cd", "243"], ["Congo (Republic) (Congo-Brazzaville)", "cg", "242"], ["Cook Islands", "ck", "682"], ["Costa Rica", "cr", "506"], ["Côte d’Ivoire", "ci", "225"], ["Croatia (Hrvatska)", "hr", "385"], ["Cuba", "cu", "53"], ["Curaçao", "cw", "599", 0], ["Cyprus (Κύπρος)", "cy", "357"], ["Czech Republic (Česká republika)", "cz", "420"], ["Denmark (Danmark)", "dk", "45"], ["Djibouti", "dj", "253"], ["Dominica", "dm", "1767"], ["Dominican Republic (República Dominicana)", "do", "1", 2, ["809", "829", "849"]], ["Ecuador", "ec", "593"], ["Egypt (‫مصر‬‎)", "eg", "20"], ["El Salvador", "sv", "503"], ["Equatorial Guinea (Guinea Ecuatorial)", "gq", "240"], ["Eritrea", "er", "291"], ["Estonia (Eesti)", "ee", "372"], ["Ethiopia", "et", "251"], ["Falkland Islands (Islas Malvinas)", "fk", "500"], ["Faroe Islands (Føroyar)", "fo", "298"], ["Fiji", "fj", "679"], ["Finland (Suomi)", "fi", "358", 0], ["France", "fr", "33"], ["French Guiana (Guyane française)", "gf", "594"], ["French Polynesia (Polynésie française)", "pf", "689"], ["Gabon", "ga", "241"], ["Gambia", "gm", "220"], ["Georgia (საქართველო)", "ge", "995"], ["Germany (Deutschland)", "de", "49"], ["Ghana (Gaana)", "gh", "233"], ["Gibraltar", "gi", "350"], ["Greece (Ελλάδα)", "gr", "30"], ["Greenland (Kalaallit Nunaat)", "gl", "299"], ["Grenada", "gd", "1473"], ["Guadeloupe", "gp", "590", 0], ["Guam", "gu", "1671"], ["Guatemala", "gt", "502"], ["Guernsey", "gg", "44", 1], ["Guinea (Guinée)", "gn", "224"], ["Guinea-Bissau (Guiné Bissau)", "gw", "245"], ["Guyana", "gy", "592"], ["Haiti", "ht", "509"], ["Honduras", "hn", "504"], ["Hong Kong (香港)", "hk", "852"], ["Hungary (Magyarország)", "hu", "36"], ["Iceland (Ísland)", "is", "354"], ["India (भारत)", "in", "91"], ["Indonesia", "id", "62"], ["Iran (‫ایران‬‎)", "ir", "98"], ["Iraq (‫العراق‬‎)", "iq", "964"], ["Ireland", "ie", "353"], ["Isle of Man", "im", "44", 2], ["Israel (‫ישראל‬‎)", "il", "972"], ["Italy (Italia)", "it", "39", 0], ["Jamaica", "jm", "1876"], ["Japan (日本)", "jp", "81"], ["Jersey", "je", "44", 3], ["Jordan (‫الأردن‬‎)", "jo", "962"], ["Kazakhstan (Казахстан)", "kz", "7", 1], ["Kenya", "ke", "254"], ["Kiribati", "ki", "686"], ["Kuwait (‫الكويت‬‎)", "kw", "965"], ["Kyrgyzstan (Кыргызстан)", "kg", "996"], ["Laos (ລາວ)", "la", "856"], ["Latvia (Latvija)", "lv", "371"], ["Lebanon (‫لبنان‬‎)", "lb", "961"], ["Lesotho", "ls", "266"], ["Liberia", "lr", "231"], ["Libya (‫ليبيا‬‎)", "ly", "218"], ["Liechtenstein", "li", "423"], ["Lithuania (Lietuva)", "lt", "370"], ["Luxembourg", "lu", "352"], ["Macau (澳門)", "mo", "853"], ["Macedonia (FYROM) (Македонија)", "mk", "389"], ["Madagascar (Madagasikara)", "mg", "261"], ["Malawi", "mw", "265"], ["Malaysia", "my", "60"], ["Maldives", "mv", "960"], ["Mali", "ml", "223"], ["Malta", "mt", "356"], ["Marshall Islands", "mh", "692"], ["Martinique", "mq", "596"], ["Mauritania (‫موريتانيا‬‎)", "mr", "222"], ["Mauritius (Moris)", "mu", "230"], ["Mayotte", "yt", "262", 1], ["Mexico (México)", "mx", "52"], ["Micronesia", "fm", "691"], ["Moldova (Republica Moldova)", "md", "373"], ["Monaco", "mc", "377"], ["Mongolia (Монгол)", "mn", "976"], ["Montenegro (Crna Gora)", "me", "382"], ["Montserrat", "ms", "1664"], ["Morocco (‫المغرب‬‎)", "ma", "212", 0], ["Mozambique (Moçambique)", "mz", "258"], ["Myanmar (Burma) (မြန်မာ)", "mm", "95"], ["Namibia (Namibië)", "na", "264"], ["Nauru", "nr", "674"], ["Nepal (नेपाल)", "np", "977"], ["Netherlands (Nederland)", "nl", "31"], ["New Caledonia (Nouvelle-Calédonie)", "nc", "687"], ["New Zealand", "nz", "64"], ["Nicaragua", "ni", "505"], ["Niger (Nijar)", "ne", "227"], ["Nigeria", "ng", "234"], ["Niue", "nu", "683"], ["Norfolk Island", "nf", "672"], ["North Korea (조선 민주주의 인민 공화국)", "kp", "850"], ["Northern Mariana Islands", "mp", "1670"], ["Norway (Norge)", "no", "47", 0], ["Oman (‫عُمان‬‎)", "om", "968"], ["Pakistan (‫پاکستان‬‎)", "pk", "92"], ["Palau", "pw", "680"], ["Palestine (‫فلسطين‬‎)", "ps", "970"], ["Panama (Panamá)", "pa", "507"], ["Papua New Guinea", "pg", "675"], ["Paraguay", "py", "595"], ["Peru (Perú)", "pe", "51"], ["Philippines", "ph", "63"], ["Poland (Polska)", "pl", "48"], ["Portugal", "pt", "351"], ["Puerto Rico", "pr", "1", 3, ["787", "939"]], ["Qatar (‫قطر‬‎)", "qa", "974"], ["Réunion (La Réunion)", "re", "262", 0], ["Romania (România)", "ro", "40"], ["Russia (Россия)", "ru", "7", 0], ["Rwanda", "rw", "250"], ["Saint Barthélemy (Saint-Barthélemy)", "bl", "590", 1], ["Saint Helena", "sh", "290"], ["Saint Kitts and Nevis", "kn", "1869"], ["Saint Lucia", "lc", "1758"], ["Saint Martin (Saint-Martin (partie française))", "mf", "590", 2], ["Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)", "pm", "508"], ["Saint Vincent and the Grenadines", "vc", "1784"], ["Samoa", "ws", "685"], ["San Marino", "sm", "378"], ["São Tomé and Príncipe (São Tomé e Príncipe)", "st", "239"], ["Saudi Arabia (‫المملكة العربية السعودية‬‎)", "sa", "966"], ["Senegal (Sénégal)", "sn", "221"], ["Serbia (Србија)", "rs", "381"], ["Seychelles", "sc", "248"], ["Sierra Leone", "sl", "232"], ["Singapore", "sg", "65"], ["Sint Maarten", "sx", "1721"], ["Slovakia (Slovensko)", "sk", "421"], ["Slovenia (Slovenija)", "si", "386"], ["Solomon Islands", "sb", "677"], ["Somalia (Soomaaliya)", "so", "252"], ["South Africa", "za", "27"], ["South Korea (대한민국)", "kr", "82"], ["South Sudan (‫جنوب السودان‬‎)", "ss", "211"], ["Spain (España)", "es", "34"], ["Sri Lanka (ශ්‍රී ලංකාව)", "lk", "94"], ["Sudan (‫السودان‬‎)", "sd", "249"], ["Suriname", "sr", "597"], ["Svalbard and Jan Mayen", "sj", "47", 1], ["Swaziland", "sz", "268"], ["Sweden (Sverige)", "se", "46"], ["Switzerland (Schweiz)", "ch", "41"], ["Syria (‫سوريا‬‎)", "sy", "963"], ["Taiwan (台灣)", "tw", "886"], ["Tajikistan", "tj", "992"], ["Tanzania", "tz", "255"], ["Thailand (ไทย)", "th", "66"], ["Timor-Leste", "tl", "670"], ["Togo", "tg", "228"], ["Tokelau", "tk", "690"], ["Tonga", "to", "676"], ["Trinidad and Tobago", "tt", "1868"], ["Tunisia (‫تونس‬‎)", "tn", "216"], ["Turkey (Türkiye)", "tr", "90"], ["Turkmenistan", "tm", "993"], ["Turks and Caicos Islands", "tc", "1649"], ["Tuvalu", "tv", "688"], ["U.S. Virgin Islands", "vi", "1340"], ["Uganda", "ug", "256"], ["Ukraine (Україна)", "ua", "380"], ["United Arab Emirates (‫الإمارات العربية المتحدة‬‎)", "ae", "971"], ["United Kingdom", "gb", "44", 0], ["United States", "us", "1", 0], ["Uruguay", "uy", "598"], ["Uzbekistan (Oʻzbekiston)", "uz", "998"], ["Vanuatu", "vu", "678"], ["Vatican City (Città del Vaticano)", "va", "39", 1], ["Venezuela", "ve", "58"], ["Vietnam (Việt Nam)", "vn", "84"], ["Wallis and Futuna", "wf", "681"], ["Western Sahara (‫الصحراء الغربية‬‎)", "eh", "212", 1], ["Yemen (‫اليمن‬‎)", "ye", "967"], ["Zambia", "zm", "260"], ["Zimbabwe", "zw", "263"], ["Åland Islands", "ax", "358", 1]], k = 0; k < j.length; k++) { var l = j[k]; j[k] = { name: l[0], iso2: l[1], dialCode: l[2], priority: l[3] || 0, areaCodes: l[4] || null } } });

            $(elementId).css("padding", "5px 5px 5px 76px");
            $(elementId).css("", "");

            var countryCode = "us";
            if ($.inside.front.settingsData && $.inside.front.settingsData.location && $.inside.front.settingsData.location.cc && $.inside.front.settingsData.location.cc.length > 0) {
                countryCode = $.inside.front.settingsData.location.cc.toLowerCase()
            }

            $(elementId).intlTelInput("destroy");
            $(elementId).intlTelInput({
                autoPlaceholder: true,
                initialCountry: countryCode,
                preferredCountries: ['us', 'gb', 'au'],
                separateDialCode: true,
                utilsScript: _insideCDN + "/js/intlTelInput/utils.js?" + _insideScriptVersion
            });

            $.ajax({
                url: _insideCDN + "/js/intlTelInput/utils.js?" + _insideScriptVersion,
                complete: function () {
                    // tell all instances that the utils request is complete
                    $(".intl-tel-input input").intlTelInput("handleUtils");
                    $(elementId).siblings().find(".country-list").addClass("ready");
                    windowScale();
                },
                dataType: "script",
                cache: true
            });

            $(elementId).on("countrychange", function (e, countryData) {
                $(elementId).css("padding", "5px 5px 5px " + ($(".flag-container").width() + 5) + "px");
            });
        }

        function openPrechatForm() {
            closeChatPane();
            if (typeof (settings.siteLogo) != "undefined" && settings.siteLogo != "" && settings.siteLogo != null) {
                $("#inside_prechatForm .inside_formTitle").html("<img src='" + _insideCDN + settings.siteLogo + "' id='inside_chat_siteLogo' />");
            }

            $("#inside_liveChatTab").fadeOut();
            if ($("#inside_prechatForm").length == 0) {
                showPrechatForm();
                return;
            }
            $("#inside_prechatForm_form").show();
            $("#inside_submitMessage").hide();
            $("#inside_prechatForm .inside_output").html("");

            setTimeout(function () {
                positionPrechatForm();
            }, 0);

            if (tabPosition || tabPosition == "topright" || tabPosition == "" || tabPosition == "right" || tabPosition == "bottomright") {
                $("#inside_prechatForm").css("right", -$("#inside_prechatForm").outerWidth());
                $("#inside_prechatForm").animate({ right: 0 }, 250);
            } else {
                $("#inside_prechatForm").css("left", -$("#inside_prechatForm").outerWidth());
                $("#inside_prechatForm").animate({ left: 0 }, 250);
            }

            setTimeout(function () {
                $("#inside_prechatForm").addClass("open");
            }, 0);

            $("#inside_prechatForm").show();

            if (settings.headerColour) {
                $("#inside_prechatForm .inside_siteLogo").css("background", settings.headerColour);
            }
            if (settings.iconColour) {
                $("#inside_prechatForm .inside_closeCross").css("color", settings.iconColour);
            }
        }

        function positionPrechatForm() {
            if ($("#inside_prechatForm:visible").length == 0) {
                return;
            }
            var viewport = $.inside.front.getViewPortSize();
            var top = viewport.height * 0.5;
            top -= $("#inside_prechatForm").outerHeight() * 0.5;

            if (chatPanePosition == "bottom") {
                top = viewport.height - $("#inside_prechatForm").outerHeight() - 20;
            } else if (chatPanePosition == "top") {
                top = 0;
            }

            $("#inside_prechatForm").css("top", top + "px");
        }

        function closePrechatForm() {
            $("#inside_prechatForm").hide();
            $("#inside_prechatForm").removeClass("open");
            setInsideTab();
        }

        function clearPrechatLocally() {
            localStorage["inside_prechat"] = null;
        }

        function savePrechatLocally(data) {
            prechatFormData = {
                name: data.name,
                email: data.email,
                phone: data.phone,
                company: data.company
            }
            /*
            if (localStorage) {
                prechat = {
                    name: data.name,
                    email: data.email,
                    phone: data.phone,
                    company: data.company
                }
                try {
                    localStorage.setItem('inside_prechat', JSON.stringify(prechat));
                } catch (e) { }
            }*/
        }

        function fetchPrechatLocally() {
            if (prechatFilled && prechatFormData && prechatFormData.name && prechatFormData.name.trim().length > 0) {
                return prechatFormData;
            } else if ($.inside && $.inside.front && $.inside.front.settingsData && $.inside.front.settingsData.user_name && $.inside.front.settingsData.user_name.trim().length > 0) {
                var tempPrechat = {
                    name: $.inside.front.settingsData.user_name,
                    email: $.inside.front.settingsData.user_email || ""
                }
                return tempPrechat;
            }
            /*if (localStorage) {
                var prechat = localStorage.getItem('inside_prechat');
                if (prechat) {
                    try {
                        prechat = JSON.parse(prechat);
                        if (prechat.name && prechat.name.length > 0 && prechat.email && prechat.email.length > 0) {
                            return prechat;
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }
            }*/
            return false;
        }

        //#endregion

    insideChat.showSystemMessage = showSystemMessage;
    function showSystemMessage(message, error, addClass) {
        if (typeof (error) == "undefined" || error == false) {
            error = false;
            errorStr = "";
        } else {
            errorStr = " error";
        }
        var msg = buildTag("div", { "class": "inside_systemMessage" + errorStr }).html(translate(safeHTML(message)));
        if (typeof (addClass) == "string") {
            msg.addClass(addClass);
        }
        $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").append(msg);
        windowScale();
        lastChatFrom = "";
        scrollChatToBottom();
    }

        function showChatDisclaimer() {
            if (typeof (settings) == "undefined" || settings == null || $.isEmptyObject(settings)) {
                return;
            }
            if (isLocal()) {
                settings.chatDisclaimer = "Disclaimer: Your chats may be recorded for monitoring purposes. Please do not submit credit card details or sensitive information.";
            }
            if ($(".inside_chatDisclaimer").length > 0 || $(".inside_chatDisclaimer_agree").length > 0) {
                return;
            }
            $(".inside_chatDisclaimer_agree").remove();
            var disclaimerAgreeText = "I agree";
            if (typeof (settings.agreeToDisclaimerText) != "undefined" && settings.agreeToDisclaimerText != "") {
                disclaimerAgreeText = settings.agreeToDisclaimerText;
            }
            if (typeof (settings.chatDisclaimer) != "undefined" && settings.chatDisclaimer != null && settings.chatDisclaimer != "") {
                /*if (settings.agreeToDisclaimer == true && !activeChat) {
                    var discStr = "<div class='inside_chatDisclaimer_agree'>" + safeHTML(settings.chatDisclaimer) + "<div><label for='inside_agree_checkbox' style='font-weight: bold;'>" + escapeHtml(disclaimerAgreeText) + "</label><input style='vertical-align: middle' type='checkbox' name='inside_agree_checkbox' class='inside_agree_checkbox' id='inside_agree_checkbox'/></div></div>";
                    $("#inside_chat_footer").after(discStr);
                    $("#inside_chatPane_custom_input").before(discStr);

                    $("#inside_chatInput, #inside_chatPane_custom_input").prop("disabled", true);
                    $("#inside_chatInputHolderTable, #inside_chatPane_custom_footer").addClass("disabled");

                    $(".inside_agree_checkbox").click(function () {
                        $(".inside_chatDisclaimer_agree").remove();
                        //if chat exists, send '/agree'
                        //sendChat("/agree");
                        windowScale();
                        $("#inside_chatInputHolderTable, #inside_chatPane_custom_footer").removeClass("disabled");
                        $("#inside_chatInput, #inside_chatPane_custom_input").prop("disabled", false).focus();
                    });
                } else {*/
                    $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").append("<div class='inside_chatDisclaimer'>" + safeHTML(settings.chatDisclaimer) + "</div>");
               /* }*/
            }
            clearChatGrouping();
            scrollChatToBottom();
        }

        function setInitialText() {
            if (availableAssistants.length > 0) {
                initialText = getChatPlaceholder();
                $("#inside_chatInput").attr("placeholder", initialText);
                updatePlaceholder();
            } else {
                disableChat();
            }
        }

        function updatePlaceholder() {
            if (!placeholderIsSupported()) {
                if (!$("#inside_chatInput, #inside_chatPane_custom_input").is(":focus")) {
                    $("#inside_chatInput, #inside_chatPane_custom_input").val("").blur();
                }
            }
        }
        function insideOfferDotClick() {
            if ($(this).attr("class") == "offerDotSelected") {
                return;
            }
            $(".inside_offerDotSelected").attr("class", "offerDot");
            $(this).attr("class", "offerDotSelected");
            $(".inside_offerImage").css("z-index", 0);
            $(".inside_offerImage[offerid='" + $(this).attr("offerid") + "']").css("z-index", 1).hide().fadeIn();
        }

        insideChat.offerReceived = offerReceived;
        var offersArray = [];

        function setPulldownHeight() {
            var isVisible = $("#inside_offerImage").is(":visible");
            $("#inside_offerImage").show();
            /*
            if($("#insideStoreImage img").length == 0 || $("#insideStoreImage img").height() == 0) {
                return;
            }
            */

            if (offersArray.length > 0) {
                pulldownHeight = $(".inside_offerImage img").height() * (330 / $("#inside_offerImage img").width());
            } else {
                pulldownHeight = $("#insideStoreImage img").height() * (330 / $("#insideStoreImage img").width());
            }
            if (pulldownHeight > maxChatHeight) {
                pulldownHeight = maxChatHeight;
            }
            pulldownHeight -= 50;
            //$("#inside_offerImage").css("height", pulldownHeight + "px");
            if (!isVisible) {
                $("#inside_offerImage").hide();
            }
        }

        function offerReceived(offers) {
            //offer has:
            //instanceid, userid, offerid, code, description, template, thumbnail, image, timeout, expirydate, timeleft, 

            try {
                //offer is an array of objects
                for (var i = 0; i < offers.length; i++) {
                    trace("adding offer " + offers[i].instanceid + ", time left = " + offers[i].timeleft);

                    var oih = $("<div class='inside_offerImage' offerid='" + offers[i].instanceid + "'></div>").appendTo("#inside_offerImage").attr("z-index", 1).click(offerLargeClick);
                    var img = $("<img src='" + $.inside.front.offerurl + offers[i].image + "' />");
                    $(img)[0].onload = function () {
                        setPulldownHeight();
                        var offerImageHeight = this.height;
                        var offerImageWidth = this.width;
                        if (offerImageWidth > 330) {
                            //offerImageHeight = (330/offerImageWidth) * offerImageHeight;
                        }
                        $("#inside_offerImage").css("height", offerImageHeight + "px");

                        $("#insideOfferHolder").show();
                        //$("#inside_offerImage").hide().slideDown();
                        //$("#inside_offerImageFooter").slideDown();
                        openImagePane();
                        $("#insideOfferDots").show();
                        $(this).appendTo(oih);
                    }

                    $("<td><div class='inside_dotUnselected' i='" + offersArray.length + "'></td>").appendTo("#inside_offerDots tr");

                    offers[i].expiryDate = getExpiryDate(offers[i].timeleft);
                    $(".insideOfferCountDown[offerid='" + offers[i].instanceid + "']").attr("date", offers[i].expiryDate);

                    $("<div class='inside_offer' id='offer" + offersArray.length + "'>" + offers[i].description + " <br /><a href='javascript:insideFrontInterface.showOfferLarge();'><img src='" + $.inside.front.offerurl + offers[i].thumbnail + "'/><br /><span style='width:100%; text-align: center'> click to enlarge</span></a><br />Time left: <span class='inside_timeLeft' date='" + offers[i].expiryDate + "'>" + showTimeRemaining(offers[i].expiryDate) + "</span></div>").appendTo("#inside_offerHolder");
                    $("#inside_offerDots .dotUnselected[i='" + offersArray.length + "']").click(offerDotClick);
                    offersArray.push(offers[i]);
                    $(".inside_offerDotSelected").attr("class", "offerDot");
                    $(".inside_offerImage").css("z-index", 0);
                    $("<span class='inside_offerDotSelected' offerid='" + offers[i].instanceid + "'></span>").appendTo("#insideOfferDots").click(insideOfferDotClick);
                    if ($("#insideOfferDots span").length > 1) {
                        $("#insideOfferDots").show();
                    } else {
                        $("#insideOfferDots").hide();
                    }
                    selectOfferDot(offers[i].instanceid);
                    if (!offers[i].viewed) {
                        $.inside.server.offerViewed(offers[i].instanceid);
                    }

                }
            } catch (e) {
                trace(e);
            }

            if (offersArray.length <= 1) {
                $("#troveSendButton").hide();
            } else {
                $("#troveSendButton").show();
            }

            $(".inside_notification").html(offersArray.length);
            $("#troveCounter").html(offersArray.length);

            $(".inside_notification").show();
            //startPulse();
            showOffer(offersArray.length - 1);

            //show a gift icon
            $(".inside_giftIcon").show();
        }

        //shows the data when clicking on the large offer
        function offerLargeClick() {
            showOfferLarge($(this).attr("offerid"));
        }

        function showTimeRemaining(d) {
            var nowDate = new Date();
            var secs = getSecondsBetweenDates(d, nowDate);
            return showTime(secs);

        }

        //accepts seconds remaining, returns a date
        function getExpiryDate(secs) {
            var d = new Date();
            d.setSeconds(d.getSeconds() + secs);
            return (d);
        }

        //shows seconds as hh:mm:ss
        function showTime(secs) {
            var hours = Math.floor(secs / 3600);
            var mins = Math.floor(secs / 60) % 60;
            var seconds = Math.floor(secs % 60);
            var timeString = "";
            if (hours == 0 && mins == 0) {
                timeString = "0." + zeroPad(seconds);
            } else if (hours == 0) {
                timeString = mins + "." + zeroPad(seconds);
            } else {
                timeString = zeroPad(hours) + "." + zeroPad(mins) + "." + zeroPad(seconds);
            }
            return timeString;
        }

        function getSecondsBetweenDates(d1, d2) {
            var dif = d1.getTime() - d2.getTime()
            var secondsFrom = dif / 1000;
            //var SecondsBetween = Math.abs(SecondsFrom);
            return secondsFrom;
        }

        function zeroPad(val) {
            val = val.toString();
            if (val.length == 1) {
                val = "0" + val;
            }
            return val;
        }

        function offerDotClick() {
            showOffer($(this).attr("i"));
        }

        var currentOffer = 0;
        function showOffer(i, showWindow) {
            currentOffer = i;
            $(".inside_offer").hide();
            $("#inside_offer" + i).show();
            $("#inside_offerDots .dotSelected").attr("class", "dotUnselected");
            $("#inside_offerDots .dotUnselected[i='" + i + "']").attr("class", "dotSelected");
            //notificationClick();
            if ($("#troveWindow").is(":visible") && !offersArray[i].viewed) {
                $.inside.server.offerViewed(offersArray[i].instanceid)
                offersArray[i].viewed = true;
            }
            troveInterval = setInterval(updateOfferTimes, 1000);

            if (typeof (offersArray[i]) != "undefined") {
                var exDate = new Date(offersArray[i].expiryDate);
                $(".inside_offerFooterExpires").html("EXPIRES " + exDate.getDate() + "." + (exDate.getMonth() + 1) + "." + exDate.getFullYear());
            }

        }

        function getOffer(offerid) {
            for (var i = 0; i < offersArray.length; i++) {
                if (offersArray[i].instanceid == offerid) {
                    return offersArray[i];
                    break;
                }
            }
            return null;
        }


        //chatData is user typing in chat window
        function chatDataReceived(chatData) {
            if (chatData[2] == "closeChatPane") {
                closeChat();
                return;
            }
            if (chatData[2] == "typingOn") {
                //showMessage({text:"...", fromName:insiderDescription});


                var assist = getAssistant(chatData[1]);
                var chatWindow = $("#inside_chatWindow, #inside_chatPane_custom_chatHolder");
                if (assist == null) {
                    faceurl = _insideCDN + "images/operator/face?id=" + chatData[1];
                } else {
                    faceurl = getFaceURL(assist);
                }

                if (assist == null) {
                    assistantName = insiderDescription;
                } else {
                    assistantName = assist.name;
                }

                var msg = "";
                if (typeof (settings.OperatortypingMessage) != "undefined" && settings.OperatortypingMessage != "") {
                    var reg = /{operator}/gi;
                    var msg = settings.chatStartedMessage.replace(reg, assistantName);
                    var TypingMessage = settings.OperatortypingMessage.replace(reg, escapeHtml(assistantName));
                    msg = $("<div class='inside_chatMessageAssist status typing'><span class='inside_chatFrom' style='display:block'>" + TypingMessage + "</span><div class='chatMessageAvatar'><div class='inside_assistMessageFace' style='right:-60px!important;'>" + $("<img />").attr("src", faceurl)[0].outerHTML + "</div></div></div>");
                    if (settings.swapChatSide) {
                        msg.addClass("swap");
                    }
                } else {
                    msg = $("<div class='inside_chatMessageAssist status typing'><span class='inside_chatFrom'>" + escapeHtml(assistantName) + ": </span><span class='ellipsis'><span class='one'>.</span><span class='two'>.</span><span class='three'>.</span></span><div class='chatMessageAvatar'><div class='inside_assistMessageFace' style='right:-60px!important;'>" + $("<img />").attr("src", faceurl)[0].outerHTML + "</div></div></div>");
                    if (settings.swapChatSide) {
                        msg.addClass("swap");
                    }
                }
                if ($("#inside_chatWindow").find(".status").length > 0) {
                    $("#inside_chatWindow").find(".status").remove();
                }
                if (chatWindow.find(".inside_chatMessageAssist.status.typing").length === 0) {
                    msg.appendTo(chatWindow);
                }
                windowScale();
                function RemoveTypingIndicator() {
                    $(".inside_chatMessageAssist.status").remove();
                }
                clearTimeout(RemoveTypingIndicator);
                setTimeout(RemoveTypingIndicator, 5000);
                scrollChatToBottom();
            } else {
                return;
            }

            $(".inside_chatData").remove();
            $("#inside_chatWindow").append("<div class='inside_chatData'></div>");
            var assist = getAssistant(chatData[1]);

            if (chatData[2] != "" && assist != null) {
                $(".inside_chatData").html(escapeHtml(assist.name) + ": " + chatData[2]);
            } else {
                $(".inside_chatData").html("");
            }
            scrollChatToBottom();
        }

        var lastChatFrom = "";
        var maxChatHeight = 325; //changes to 146 when smaller
        var queuedChats = [];

        function showQueuedChats() {
            if (queuedChats.length > 0) {
                var chats = queuedChats;
                queuedChats = [];
                for (var q = 0; q < chats.length; q++) {
                    chatReceived(chats[q]);
                }

            }
        }

        var deptChosen = false;

        //if there are depts, show department selector
        insideChat.showDepartmentSelector = showDepartmentSelector;
        function showDepartmentSelector() {
            if ($(".inside_dept_selector").length > 0) {
                return;
            }
            if (settings.forceDept == true && !deptChosen && !chatInProgress) {
                disableChat(translate("Please select a department"));
            }
            insideAPI.call("getDepartments", {}, function (response) {
                if (response.status == "Error") {
                    enableChat();
                    return;
                }
                var data = response.data;
                if (data.length > 0) {
                    var deptHTML = "";
                    deptHTML += "<div class='inside_dept_selector'>";

                    //loop through all departments.
                    var str = "To help us better assist you, which department do you require assistance from?";
                    if (typeof (settings.askDeptText) != "undefined" && settings.askDeptText != "") {
                        str = settings.askDeptText;
                    }
                    deptHTML += "<span class='inside_dept_selector_title'>" + escapeHtml(str) + "</span><br />";

                    for (var i = 0; i < data.length; i++) {
                        deptHTML += $("<input type='radio' name='inside_dept' id='inside_chat_check_" + i + "'/>").attr("value", data[i])[0].outerHTML;
                        deptHTML += "<label for='inside_chat_check_" + i + "'>" + escapeHtml(data[i]) + "</label><br />";
                    }

                    //deptHTML += "<span class='insideSubmitButton'>submit</span>"
                    deptHTML += "</div>";

                    if ($("#inside_chatPane_custom").length > 0) {
                        if ($(".inside_chatDisclaimer").length > 0) {
                            $(deptHTML).insertAfter(".inside_chatDisclaimer");
                        } else if ($("#inside_chatPane_custom_chatHolder .inside_custom_storeimage").length > 0) {
                            $(deptHTML).insertAfter("#inside_chatPane_custom_chatHolder .inside_custom_storeimage");
                        } else {
                            $(deptHTML).appendTo("#inside_chatPane_custom_chatHolder");
                        }
                    } else {
                        $("#inside_chatWindow").append(deptHTML);
                    }
                    //make sure prev chat is first
                    $(".inside_previousConversations").prependTo("#inside_chatWindow, #inside_chatPane_custom_chatHolder");
                    setTimeout(windowScale, 0);

                    $(".inside_dept_selector input").change(function () {
                        if ($(".inside_dept_selector input[name='inside_dept']:checked").length == 0) {
                            return;
                        }
                        //submit selected department
                        insideAPI.call("setvisitordatavalue", { "key": "department", "value": $(".inside_dept_selector input[name='inside_dept']:checked").val() });

                        sendChat("/dept " + $(".inside_dept_selector input[name='inside_dept']:checked").val());
                        deptChosen = true;
                        $(".inside_dept_selector").addClass("selected").hide();
                        windowScale();
                        if (availableAssistants.length > 0) {
                            if (settings.forceDept == true && !deptChosen && !chatInProgress && $(".inside_dept_selector").length > 0) {
                                disableChat(translate("Please select a department"));
                            } else {
                                enableChat();
                            }
                        }
                        showGreetingText();
                    })
                } else {
                    //do nothing
                    showGreetingText();
                }
            });
        }

        //shows a red number on the chat tab
        function showChatNotification(target) {

            if (typeof (target) == "undefined") {
                target = "#inside_liveChatTab";
            }
            var $target = $(target);
            var chatNotif;
            if ($target.find(".inside_chatNotification").length > 0) {
                chatNotif = $target.find(".inside_chatNotification");
                var num = parseInt(chatNotif.text()) + 1;
                chatNotif.text(num);
            } else {
                chatNotif = $("<div class='inside_chatNotification'>1</div>").appendTo(target);
            }

            insideTween.TweenLite.to([chatNotif], 0, { scaleX: 0, scaleY: 0, ease: Elastic.easeOut });
            insideTween.TweenLite.to([chatNotif], 1, { scaleX: 1, scaleY: 1, ease: Elastic.easeOut });
        }

        function clearNotifications() {
            $(".inside_chatNotification").remove();
        }

        var unshownMessages = false;
        var savedOperators = [];
        var messagesList = [];//used for transcript, stores all received messages.
        function chatReceived(messages, fromEvent) {
            if (!connected) {
                queuedChats.push(messages);
                return;
            }
            for(var i=0;i<messages.length;i++){
                messages[i].date = getTime(messages[i].age, true, true).date;
                messagesList.push(messages[i]); 
            }

            if(messages.length==1 && messages[0].text == "/stopchat") {
                return;
            }

            if (useMinimiseButton && messages[0].text != "/stopchat") {
                try {
                    sessionStorage.setItem("insideChatClosed", "False");
                } catch (e) { }
            }

            $(".inside_chatMessageAssist.status").remove();
            showQueuedChats();

            if (typeof (disableInsideChat) != "undefined") {
                return;
            }

            if ($("#inside_chatInput, #inside_chatPane_custom_input").prop("disabled") && availableAssistants.length > 0) {
                enableChat();
            }

            // messages is an array of object message
            // message = { id:1, chatid:"chatid", from:"name", text:"message text content" }		
            thisUserId = $.connection.hub.id;

            messageReceived = true;
            //for chat tab hiding
            if (showTabForActive && hideChatTab) {
                setOnlineChatTab(false);
            }

            //existing conversation, but no online
            if (availableAssistants.length == 0 && !$("#inside_liveChatTab").is(":visible")) {
                setOfflineChatTab();
                if (!hideChatTab) {
                    if (!chatPaneOpen) {
                        $("#inside_liveChatTab").show();
                    }
                }
            }

            for (var i = 0; i < messages.length; i++) {
                if (messages[i].text != "/stopchat") {
                    try {
                        sessionStorage.setItem("insideChatClosed", "False");
                    } catch (e) { }
                }
                if (messages[i].fromid == "" && messages[i].text.indexOf("/stopchat") == 0) {
                    chatInProgress = false;
                    return;
                }
                if (typeof $.inside.front.users[messages[i].fromid] == "undefined" && typeof savedOperators[messages[i].fromid] == "undefined") {
                    savedOperators[messages[i].fromid] = { id: messages[i].fromid, name: messages[i].fromname };
                }

                if (messages[i].fromid.indexOf("assistant") > -1 && !document.hasFocus()) {
                    playNewMessageSFX();
                }

                showMessage(messages[i]);
            }
            if ($("#inside_chatWindow").height() > maxChatHeight) {
                $("#inside_chatWindow").css("height", maxChatHeight + "px");
            }

            setTimeout(windowScale, 10);

            if (availableAssistants.length > 0) {
                if (!storeImageShown) {
                    showStoreImage(availableAssistants[0]);
                }

                if (lastChatFrom != "me") {
                    setTimeout(function () {
                        chatPaneMode = "";
                        windowScale();
                        closeImagePaneAfter(6000);
                    }, 10);
                    if (!$("#inside_chatWindow, #inside_chatPane_custom_chatHolder").is(":visible")) {

                        $(".blueChat").attr("class", "redChat");
                        //$(".redChat").show();

                        //if(typeof(PFCID) != "undefined"){
                        //if(PFCID == "887"){
                        if (device == 2) {
                            setOnlineChatTab();
                            showChatNotification();                            
                            
                            if ($(".insideCustomChatButton").length > 0) {
                                showChatNotification($(".insideCustomChatButton"));
                            }
                        } else {
                            if ($("#inside_prechatForm_form").is(":visible") != true) {
                                animateOpenChatPane();
   
                                $(".inside_chatPane").show();
                                $("#inside_liveChatTab").hide();
                                $(".redChat").hide();
                            }
                        }

                        //sendChat("/openchat");
                        //}
                        //}

                        //startPulse();
                    }
                } else {
                    if (!$("#inside_chatWindow").is(":visible")) {
                        $(".redChat").attr("class", "blueChat");
                        //$(".blueChat").show();
                    }
                }
            } else {
                if (fromEvent != "connected") {
                    unshownMessages = true;
                }
                $(".blueChat").hide();
                $(".redChat").hide();
            }

            //move any chat data to bottom
            $(".inside_chatData").appendTo("#inside_chatWindow");

            scrollChatToBottom();
            setTimeout(scrollChatToBottom, 0);

            if (device == 2) {
                refreshInsideHolder();
            }

        }

        borderSize = 0;
        function refreshInsideHolder() {
            borderSize += 0.01;
            //force inside_holder to refresh
            $("#inside_holder").hide();
            $("#inside_holder")[0].offsetHeight;
            $("#inside_holder").show();
            $("#inside_holder .inside_chatPane").css("border", borderSize + "px solid #cccccc");
        }


        function selectOfferDot(offerid) {
            $(".inside_offerDotSelected").attr("class", "offerDot");
            $(".inside_offerDot[offerid='" + offerid + "']").attr("class", "offerDotSelected");
            $(".inside_offerImage").css("z-index", 0);
            $(".inside_offerImage[offerid='" + offerid + "']").css("z-index", 1).hide().fadeIn();
        }

        function parseOffer(str) {
            try {
                var rxp = /{(.*?)}/g;
                var found = [];
                while (curMatch = rxp.exec(str)) {
                    found.push(curMatch[1]);
                }
                var offerid = "";

                for (j = 0; j < found.length; j++) {
                    rxp = "{" + found[j] + "}";
                    if (found[j].indexOf("image") == 0 || found[j].indexOf("thumbnail") == 0) {
                        if (device != 2 && $.inside.front.getViewPortSize().height >= 500) {
                            //setBackgroundImage("#insideStoreImage", $.inside.front.offerurl + found[j].split(":")[1]);
                            if (getOffer(offerid) != null) {

                                selectOfferDot(offerid);
                            }
                            str = str.replace(rxp, "");
                        } else {
                            str = str.replace(rxp, "<br /><a href='javascript:insideFrontInterface.showOfferLarge(" + offerid + ");'><img height='107px' src='" + $.inside.front.offerurl + found[j].split(":")[1] + "' /></a><br />");
                        }
                    } else if (found[j].indexOf("expiry") == 0) {
                        if (getOffer(offerid) != null) {
                            str = str.replace(rxp, "<span class='insideOfferCountDown' offerid='" + offerid + "' date='" + getOffer(offerid).expiryDate + "'>" + showTimeRemaining(getOffer(offerid).expiryDate) + "</span> mins");
                        } else {
                            str = str.replace(rxp, "<span style='font-weight: bold'> *OFFER EXPIRED* </span>");
                        }
                    } else if (found[j].indexOf("code") == 0) {
                        str = str.replace(rxp, "<span class='inside_offerCode'>" + found[j].split(":")[1] + "</span>");
                    } else if (found[j].indexOf("id") == 0) {
                        offerid = found[j].split(":")[1];
                        str = str.replace(rxp, "");
                    } else {
                        str = str.replace(rxp, "");
                    }
                }

            } catch (e) {
                trace("error replacing offer text:");
                trace(str);
            }
            return str;
        }

        var lastChatDrawn;
        var lastChatDrawnFrom;

        insideChat.clearChatGrouping = clearChatGrouping;
        function clearChatGrouping() {
            lastChatDrawn = null;
            lastChatDrawnFrom = null;
        }

        function replaceEmoticons(text) {
            //text = $("<span/>").html(text).text();
            var emoticons = {
                ':)': 'smile.png',
                ':-)': 'happy2.png',
                ':-D': 'bigsmile.png',
                ':D': 'happy1.png',
                ':-p': 'cheeky.png',
                ':-P': 'cheeky.png',
                ':(': 'sad.png',
                ':-(': 'sad.png',
                ';-p': 'cheekywink.png',
                ':-|': 'ambivalent.png',
                ':|': 'ambivalent.png',
                ';)': 'wink.png',
                ';-)': 'wink.png'


            };
            var url = imageurl + "/smileys/";
            var result = text;
            var regex;
            for (emotcode in emoticons) {
                regex = new RegExp(RegExpEscape(emotcode), 'g');
                result = result.replace(regex, function (match) {
                    var pic = emoticons[match];

                    if (pic != undefined) {
                        return '<img src="' + url + pic + '"/>';
                    } else {
                        return match;
                    }
                });
            }

            return result;
        }

        //helper function to escape special characters in regex
        function RegExpEscape(text) {
            return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        }

        function parseMessage(message) {
            //message.text = message.text.split("\n").join("<br>");
            if (message.text.indexOf("/brb:") == 0) {
                //be right back message
                message.text = message.text.substring(5);
            }
            if (message.text.indexOf("<div") != 0 && message.text.indexOf("<a href") == -1 && message.text.indexOf("/link") != 0 && message.text.indexOf("<link>") != 0 && message.text.indexOf("http://") != 0 && message.text.indexOf("https://") != 0) {
                message.text = replaceEmoticons(message.text);
            }
            if (message.contentType == "text/inside.legacy") {
                message.text = $("<span/>").html(message.text).text();
            }
            try {
                message.text = safeHTML(message.text);
            } catch (e) { }

            //create auto links. Exclude if an offer message (images screw it up) or in the '<link>' format
            if (message.text.indexOf("<link>") != -1) {
                //matching link as <link>str::url</link>
                var reg = /<link>[\S\s]*?<\/link>/gi;
                while ((result = reg.exec(message.text)) !== null) {
                    var match = result[0];
                    var newstr = match.split("<link>").join("").split("</link>").join("");
                    var urlArr = newstr.split("::");

                    var targetWindow = "_self";
                    if (urlArr[1].indexOf(window.location.host) == -1) {
                        targetWindow = "_blank"; //external url
                    }

                    message.text = message.text.replace(match, "<a href='" + urlArr[1] + "' target='" + targetWindow + "'>" + urlArr[0] + "</a>");
                }
            } else {
                //if(!isOfferMessage){
                if (message.text.indexOf("/link::") != 0) {
                    message.text = linkify(unquoteAttr(message.text), message.fromid.indexOf("assistant") == 0);
                }
                //}
            }

            message.text = checkMacros(message.text);
            return message;
        }

        var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var dayNamesShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var dayNamesMin = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

        function checkMacros(str) {
            var d = new Date();
            str = str.split("{dayofweek}").join(dayNames[d.getDay()]);
            return str;
        }

        insideFrontInterface.showMessage = showMessage;
        function showMessage(message, chatHolder) {
            if (typeof (chatHolder) == "undefined") {
                chatHolder = "#inside_chatWindow, #inside_chatPane_custom_chatHolder";
            }
            if (!chatPaneAdded) {
                addChatPane();
            }
            if (addedMessages.indexOf(message.chatid + ":" + message.id) != -1 && !message.isGreetingMessage) {
                //messages already displayed. To prevent duplicates, messages will be cleared.
                //$(".inside_chatMessage").remove();
                //addedMessages = [];
                return;
            }
            addedMessages.push(message.chatid + ":" + message.id);

            var isOfferMessage = false;
            if (message.text.indexOf("{id") == 0) {
                isOfferMessage = true;
                message.text = parseOffer(message.text);
            }

            currentChatId = message.chatid || 0;
            insideFrontInterface.currentChatId = currentChatId;
            var fromString = message.fromid;

            if (fromString == "" && message.text.indexOf("/stopchat") == 0) {
                //$("#inside_chatWindow").append("<div class='chatMessage'><span class='chatFrom'>[ SERVER: The chat has been closed. ]</span></div>");
                //disableChat();
                chatInProgress = false;
                //setTimeout(closeChat, 5000);
                return;
            }

            //don't show any system messages
            if ((fromString == "system" || fromString == "") && message.text.indexOf("/") == 0) {
                return;
            }

            if (message.contentType == "text/inside.system")
            {
                showSystemMessage(message.text, false, null)
                return;
            }

            if (fromString.indexOf("user") == 0) {
                fromString = "me";
                if (!isSystemMessage(message.text)) {
                    activeChat = true;
                    insideFrontInterface.activeChat = true;
                    $(".inside_chatDisclaimer_agree").remove();
                }
            }

            if (fromString.indexOf("assistant:") == 0) {
                //fromString = "assistant";
                fromString = message.fromname;
                //showAssistantImages(message.fromid);

                //clear any greeting text if chat from assistant
                $(".inside_chatMessageAssist[mid='-1']").remove();
                $(".inside_chatStartedText").remove();
                $(".inside_systemMessage.queue").remove();
            }

            if (typeof (fromString) == "undefined") {
                fromString = settings.insiderDescription;
            }

            lastChatFrom = fromString;
            if (message.fromid != "assistant:welcomeGreeting") {
                chatInProgress = true;
                //if($(".inside_chatDisclaimer_agree").length > 0){
                //	showChatDisclaimer();
                //}
            }

            parseMessage(message);
            var msgDateOb;
            try {
                if (typeof (message.date) != "undefined") {
                    var d = new Date(message.date);
                    msgDateOb = getTime(getSecondsBetweenDates(new Date(), d), true, true);
                } else {
                    msgDateOb = getTime(message.age, true, true);
                }
            } catch (e) {
                trace(e.message);
            }


            //delete a chat message
            if (message.text.indexOf("/delete") == 0 && message.fromid.indexOf("assistant") == 0) {
                var deleteMessageId = message.text.split(" ")[1];
                $(".inside_messageText[mid='" + deleteMessageId + "']").addClass("deleted");
                return;
            }


            //user has clicked on assistant, chat has started
            if (message.text.indexOf("/assistclick::") == 0) {
                $(chatHolder).append("<div class='inside_chatMessage'><span class='inside_chatFrom'>[ You have connected to the assistant. Enter your question below. ]</span></div>");
            } else if (message.text.indexOf("/offer::") == 0) { //show an offer
                theofferurl = message.text.split("::")[1];
                $("#inside_offerHolder").html("<img src='" + theofferurl + "'/>");
                $(chatHolder).append("<div class='inside_chatMessage'><div class='inside_chatTime'>" + msgDateOb.str + "</div><span class='inside_chatFrom'>[ You have received an offer ]</span></br /><img src='" + offer$.inside.front.offerurl + "' style='width:100px;'/></div>");
                //$("#inside_smallOfferHolder").html("<img src='" + offer$.inside.front.offerurl + "' />");
                //$(".inside_notification").show();
            } else if (message.text.indexOf("/link::") == 0) {
                var linkArr = message.text.split("::");
                message.text = "<a href='" + linkArr[2].split("<br>").join("") + "?ref=insidechat'> " + linkArr[1] + "</a>";
                //$("#inside_chatWindow, #inside_chatPane_custom_chatHolder").append("<div class='inside_chatMessage'><div class='inside_chatTime'>" + msgDateOb.str + "</div><span class='inside_chatFrom'>" + fromString + ": </span><a href='" + linkArr[2].split("<br>").join("") + "?ref=insidechat'> " + linkArr[1] + "</a></div>");
            } else if (message.text.indexOf("/dept ") == 0) {

            message.text = "You have selected department " + message.text.split(" ")[1];
            deptChosen = true;
            if (availableAssistants.length > 0) {
                enableChat();
            }
            lastChatDrawnFrom = "";
            showSystemMessage(message.text);
           // $(chatHolder).append("<div class='chatMessage'><span class='chatFrom'>" + translate(escapeHtml(message.text)) + "</span></div>");
            return;

            } else if (isSystemMessage(message.text)) {
                //TO DO - close/open chat tab on other tabs
                return;
            }

            var messagetxt = "<div class='inside_messageText' mid='" + message.id + "'><div class='inside_chatTime'>" + msgDateOb.str + "</div>" + message.text + "</div>";
            if (settings.separateChats != true && lastChatDrawnFrom == message.fromid) {
                var $message = $(messagetxt);

                if (lastChatDrawn.find(".inside_chatTime:last").text() == msgDateOb.str) {
                    $message.find(".inside_chatTime").hide();
                }
                //update the chat time
                $message.find(".inside_chatmessage_datetime").html("<img src='" + imageurl + "/clock_icon.png' style='margin-right: 5px; margin-bottom: 2px;'/>" + msgDateOb.dateTimeStr);
                $message = $message.appendTo(lastChatDrawn);

                $message.find("img").load(function () { scrollChatToBottom(0); });

                //fix for IE quirks
                if (isIE5Quirks()) {
                    var prev = $(".inside_userMessageFooter:last").css("bottom");
                    $(".inside_userMessageFooter").css("bottom", "0px").css("bottom", prev);
                }

            } else {
                if (fromString == "") {
                    if (availableAssistants.length > 0 && availableAssistants[0].id.indexOf("ch:") != 0) {
                        fromString = availableAssistants[0].name;
                    } else {
                        fromString = insiderDescription;
                    }
                }
                var cw = $("<div class='inside_chatMessage'><span class='inside_chatFrom'><span class='inside_chatName'>" + escapeHtml(fromString) + "</span><span class='inside_afterName'> " + translate("says") + " </span></span>" + messagetxt + "</div>").appendTo(chatHolder);
                cw.append("<div class='inside_chatmessage_datetime'><img src='" + imageurl + "/clock_icon.png' style='margin-right: 5px;'/>" + msgDateOb.dateTimeStr + "</div>");
                lastChatDrawn = cw;
                lastChatDrawnFrom = message.fromid;

                if (fromString == "me") {
                    $(cw).attr("class", "inside_chatMessageUser");
                    if (settings.swapChatSide) {
                        $(cw).addClass("swap");
                    }

                    var userImage = _insideCDN + "/images/you.png";
                    if (typeof settings.visitorImage != "undefined") {
                        userImage = customimageurl + settings.visitorImage;
                    }
                    if (settings.swapChatSide) {
                        $(cw).append("<div class='inside_assistMessageFace' style='right:-60px;left:null'><img src='" + userImage + "' /></div>");
                    }
                    else {
                        $(cw).append("<div class='inside_userMessageFace'><img src='" + userImage + "' /></div>");
                    }
                } else {
                    $(cw).attr("class", "inside_chatMessageAssist");
                    if (settings.swapChatSide) {
                        (cw).addClass("swap");
                    }

                    $(cw).attr("fromid", message.fromid);

                    if (settings.showOperatorName == true) {
                        cw.find(".inside_chatFrom").show();
                    }

                    //show greeting chat message
                    if (message.id == -1) {
                        $(cw).addClass("inside_greeting_message")
                    }
                    $(cw).attr("mid", message.id);
                    //$(cw).append("<div class='inside_assistMessageFooter'></div>");
                    $(cw).append("<div class='inside_assistMessageArrow'></div>");
                    try {
                        var assist = getAssistant(message.fromid);
                        if (message.id == -1 && availableAssistants.length > 0) {
                            assist = availableAssistants[0];
                        }

                        if (assist == null) {
                            faceurl = _insideCDN + "images/operator/face?id=" + message.fromid;
                        } else {
                            faceurl = getFaceURL(assist);
                        }
                        if (settings.swapChatSide) {
                            $(cw).append("<div class='inside_userMessageFace'><img src='" + faceurl + "'</div>");
                        }
                        else {
                            $(cw).append("<div class='inside_assistMessageFace'><img src='" + faceurl + "'</div>");
                        }
                    } catch (e) {
                        //error with assistant image
                    }
                    $(cw).find("img").load(function () { scrollChatToBottom(0); });
                }
                if (fromString == "me") {
                    if (settings.swapChatSide) {
                        cw.css("left", "-300px").animate({ left: "0px" });
                    }
                    else {
                        cw.css("left", "-300px").animate({ left: "60px" });
                    }
                } else {
                    cw.css("right", "-300px").animate({ right: "0px" });
                }
            }

            setIE7Quirks();
        }

        function getFaceURL(assist) {
            if (assist != null && assist.img1 != "undefined" && assist.img1 != null && assist.img1 != "") {
                var faceurl = assist.img1;
            } else {
                faceurl = $.inside.front.imageurl + "default-insider-photo.jpg";
            }
            if (faceurl.indexOf(_insideCDN2) == 0) {
                faceurl = _insideProtocol + faceurl;
            }
            if (faceurl.indexOf("http") != 0) {
                faceurl = _insideCDN + faceurl;
            }
            //check for override image.
            if (typeof (settings.globalOverrideOperatorImage) != "undefined" && settings.globalOverrideOperatorImage != null && settings.globalOverrideOperatorImage != "") {
                faceurl = customimageurl + settings.globalOverrideOperatorImage;
            }
            return faceurl;

        }

        function isSystemMessage(str) {
            if (str.indexOf("/openchat") == 0 || str.indexOf("/closechat") == 0 || str.indexOf("/agree") == 0 || str.indexOf("/dept") == 0) {
                return true;
            }
            return false
        }

        function linkify(inputText, showImages) {
            if (typeof (showImages) == "undefined") {
                showImages = true;
            }

            var replacedText, replacePattern1, replacePattern2, replacePattern3;

            if (showImages) {
                var isImgUrl = /https?:\/\/.*?\.(?:png|jpg|jpeg|gif)/ig;
                var imageRegex = /\.(png|jpg|jpeg|gif)$/;
                if (inputText.match(imageRegex)) {
                    inputText = inputText.replace(isImgUrl, '<img src="$&"/>');
                    return inputText;
                }
            }

            //URLs starting with http://, https://, or ftp://
            replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;\[\]]*[-A-Z0-9+&@#\/%=~_|\[\]])/gim;
            //replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');
            replacedText = replaceTxtNotInA(inputText, replacePattern1, '<a href="$1" target="_blank">$1</a>');

            //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
            replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
            //replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
            replacedText = replaceTxtNotInA(replacedText, replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

            //Change email addresses to mailto:: links.
            /*
            replacePattern3 = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim;
            replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');
            */

            return replacedText;
        }

        function replaceTxtNotInA(html, regex, replace) {

            //just to make the txt parse easily, without (start) or (ends) issue
            html = '>' + html + '<';

            //parse txt between > and < but not follow with</a
            html = html.replace(/>([^<>]+)(?!<\/a)</g, function (match, txt) {

                //now replace the txt
                return '>' + txt.replace(regex, replace) + '<';
            });

            //remove the head > and tail <
            return html.substring(1, html.length - 1);
        }

        //will linkify links, but turn images into <img>
        function chat_string_create_urls(input, showImages) {
            if (typeof (showImages) == "undefined") {
                showImages = true;
            }
            var isImgUrl = /https?:\/\/.*?\.(?:png|jpg|jpeg|gif)/ig;
            var imageRegex = /\.(png|jpg|jpeg|gif)$/;
            input = input.replace(/(\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|])/gim,
            function (str) {
                if (showImages && str.match(imageRegex)) {
                    return (str.replace(isImgUrl, '<img src="$&"/>'));
                } else {
                    return ('<a href="' + str + '" class="autolink" target="_blank">' + str + '</a>');
                }
            });
            return input;
        }

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

        (function () {
            function fixDate(d) {
                var r = d;
                var s = d.split(".");
                if (s.length > 1) {
                    r = s[0];
                    var w = false;
                    for (var i = 0; i < s[1].length; i++) {
                        if (!w && isNaN(s[1][i])) w = true;
                        if (w) r += s[1][i];
                    }
                }
                return r;
            }

            var D = new Date('2011-06-02T09:34:29+02:00');
            if (!D || +D !== 1307000069000) {
                Date.fromISO = function (s) {
                    var day, tz,
                    rx = /^(\d{4}\-\d\d\-\d\d([tT ][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/,
                    p = rx.exec(s) || [];
                    if (p[1]) {
                        day = p[1].split(/\D/);
                        for (var i = 0, L = day.length; i < L; i++) {
                            day[i] = parseInt(day[i], 10) || 0;
                        };
                        day[1] -= 1;
                        day = new Date(Date.UTC.apply(Date, day));
                        if (!day.getDate()) return NaN;
                        if (p[5]) {
                            tz = (parseInt(p[5], 10) * 60);
                            if (p[6]) tz += parseInt(p[6], 10);
                            if (p[4] == '+') tz *= -1;
                            if (tz) day.setUTCMinutes(day.getUTCMinutes() + tz);
                        }
                        return day;
                    }
                    return NaN;
                }
            }
            else {
                Date.fromISO = function (s) {
                    return new Date(fixDate(s));
                }
            }
        })()

        //accepts number of seconds ago, converts to local time
        insideFrontInterface.getTime = getTime;
        function getTime(val, nodate, showAmPm) {
            var d = new Date();
            var d2 = new Date(d.valueOf() - (val * 1000));
            var timeFrom = "";
            timeFrom = d2.getHours() + ":" + zeroPad(d2.getMinutes());
            if (typeof (nodate) == "undefined") {
                if (d2.getDate() != d.getDate()) {
                    timeFrom = d2.getDate() + " " + months[d2.getMonth()] + " " + timeFrom;
                }
            }
            var datestr = d2.getDate() + " " + months[d2.getMonth()];
            if (d2.getDate() == d.getDate() && d2.getMonth() == d.getMonth() && d2.getFullYear() == d.getFullYear()) {
                datestr = translate("Today");
            }
            var ampm = "";
            if (typeof (showAmPm) != "undefined" && showAmPm) {
                ampm = "am";
                var h = d2.getHours();
                if (h > 12) {
                    h -= 12;
                    ampm = "pm";
                } else if (h == 12) {
                    ampm = "pm";
                }
                timeFrom = h + ":" + zeroPad(d2.getMinutes());
            }
            if(language != "en") {
                datestr = d2.toLocaleDateString();
                if(d2.getDate() == d.getDate() && d2.getMonth() == d.getMonth() && d2.getYear() == d.getYear()) {
                    datestr = translate("Today");
                }
            }
            var timestr = d2.toLocaleTimeString();
            var dateTimeStr = datestr + " " + timestr;
            return { str: timeFrom, date: d2, datestr: datestr, timestr:timestr, dateTimeStr:dateTimeStr, ampm: ampm };
        }

        var currentAssistantImages;
        function showAssistantImages(fromid) {
            if (currentAssistantImages == fromid) {
                return;
            } else {
                currentAssistantImages = fromid;
                var assistant = getAssistant(fromid);
                if (assistant != null && typeof (assistant.img1) != "undefined") {
                    if (assistant.img1.indexOf("http:") != -1 || assistant.img1.indexOf("https:") != -1) {
                        setBackgroundImage("#assistantChatImage", assistant.img1 + "?" + Math.random());
                    } else {
                        if (typeof (_insideCDN) != "undefined") {
                            setBackgroundImage("#assistantChatImage", _insideCDN + assistant.img1 + "?" + Math.random());
                        }
                    }
                    $("#assistantChatImage").attr("title", "You are chatting with " + assistant.name);
                }
                showStoreImage(assistant);
            }
        }

        function showStoreImage(assistant) {
            if (!chatPaneAdded) {
                addChatPane();
            }
            storeImageShown = true;

            if (typeof (assistant) == "undefined") {
                assistant = null;
            }
            if ($("#inside_chatPane_custom").length > 0) {
                var img = "";
                if (assistant != null && typeof (assistant.img2) != "undefined" && assistant.img2 != "") {
                    img = assistant.img2;
                }

                if (typeof (settings.globalOverrideStoreImage) != "undefined" && settings.globalOverrideStoreImage != null && settings.globalOverrideStoreImage != "") {
                    img = settings.globalOverrideStoreImage;
                }
                if (img == "" || img == null) {
                    return;
                }
                if (img.indexOf("http:") == -1 && img.indexOf("https:") == -1 && img.indexOf("custom/") == -1) {
                    img = customimageurl + img;
                } else {
                    img = _insideCDN + img;
                }
                var customStoreImageHTML = "<div class='inside_custom_storeimage'><div id='inside_custom_storeimage_img'><img src='" + img + "'></div><div id='customStoreImageToggle'><span class='inside_arrow'>&#x25BC;</span></div></div>";
                if (settings.showStoreImagePane) { //if custom chat pane has selected to use the store image slider pane
                    $(customStoreImageHTML).insertAfter("#inside_chatPane_custom_chatHeader");
                    $(".inside_custom_storeimage").addClass("inside_floating");
                    $("#customStoreImageToggle").click(customStoreImageToggleClick);
                    $("#inside_chatPane_custom_chatHolder").css("margin-top", "20px");
                    $("#inside_chat_footer").appendTo("#customStoreImageToggle");
                    $("#inside_custom_storeimage_img").hide();

                } else {
                    $(customStoreImageHTML).prependTo($("#inside_chatPane_custom_chatHolder"));
                    $("#customStoreImageToggle").hide();
                }


                $("#inside_custom_storeimage_img img").load(function () {

                    if ($(".inside_chatMessageUser").length == 0) {
                        if (settings.useCustomChatPane && settings.showStoreImagePane == true) {
                            setTimeout(customStoreImageToggleClick, 1000);
                            closeImagePaneAfter(6000);
                        }
                    }
                    setTimeout(scrollChatToBottom, 0);
                });
            }
            if (device != 2) {
                var simg;
                if (typeof (settings.globalOverrideStoreImage) != "undefined" && settings.globalOverrideStoreImage != null && settings.globalOverrideStoreImage != "") {
                    simg = $("<img src='" + customimageurl + settings.globalOverrideStoreImage + "'>");
                    $("#inside_offerTab_arrow").show();
                } else if (assistant != null && typeof (assistant.img2) != "undefined" && assistant.img2 != "" && assistant.img2 != null) {
                    if (assistant.img2.indexOf("http:") != -1 || assistant.img2.indexOf("https:") != -1) {
                        simg = $("<img src='" + assistant.img2 + "'>");
                    } else {
                        if (assistant.img2.indexOf(_insideCDN2) == 0) {
                            simg = $("<img src='" + _insideProtocol + assistant.img2 + "'>");
                        } else {
                            simg = $("<img src='" + _insideCDN + assistant.img2 + "'>");
                        }
                    }
                } else {
                    //no custom store image
                    simg = "";//$("<img src='" + $.inside.front.imageurl + "default-chat-store-image.png" + "'>");
                    $("#inside_offerTab").remove();
                    $("#inside_offerTab_arrow").hide();
                    return;
                }




                $("#insideOfferHolder").show();
                $("#insideStoreImage").html("");
                closeImagePane(0);
                $(simg).bind("load", function () {
                    if (typeof settings.globalBannerLink != "undefined" && settings.globalBannerLink != null && settings.globalBannerLink != "") {
                        simga = $("<a href='" + settings.globalBannerLink + "'></a>").append(this).appendTo("#insideStoreImage");
                    } else {
                        $(this).appendTo("#insideStoreImage");
                    }
                    closeImagePane(0);
                    setTimeout(openImagePane, 1000);
                    windowScale();
                    setPulldownHeight();
                    closeImagePaneAfter(6000);
                });

                if (assistant != null) {
                    $("#inside_assistantChatImage").attr("title", "You are chatting with " + assistant.name);
                }
            } else {
                $("#inside_offerTab").remove();
            }
        }

        function customStoreImageToggleClick() {
            if ($("#inside_custom_storeimage_img").is(":visible")) {
                $("#inside_custom_storeimage_img").slideUp();
                $("#customStoreImageToggle").removeClass("open");
                $("#customStoreImageToggle .inside_arrow").html("&#x25BC;");
            } else {
                $("#inside_custom_storeimage_img").slideDown();
                $("#customStoreImageToggle").addClass("open");
                $("#customStoreImageToggle .inside_arrow").html("&#x25B2;");
            }
        }

        function openImagePane() {
            $("#inside_offerImage").slideDown(500);
            //$("#inside_offerImageFooter").slideDown(500);
            $("#insideOfferDots").show();
            $("#inside_offerTab").hide().delay(250).css("margin-top", "35px").fadeIn(250);
            $("#inside_offerTab img").attr("src", imageurl + "pulldown_arrow_up.png");
            $("#inside_offerTab_background").fadeIn();
            $("#inside_offerTab").removeClass("inside_closed");
            clearTimeout(closeTimeout);
        }

        function closeImagePane(time) {
            if (typeof (time) == "undefined") {
                time = 500;
            }

            $("#inside_offerTab img").attr("src", imageurl + "pulldown_arrow.png");
            $("#inside_offerTab").hide().delay(time).css("margin-top", "50px").fadeIn(250);

            if (availableAssistants.length == 0) {
                $("#inside_offerImage").slideUp(time);

                return;
            }
            if (availableAssistants.length != 0) {

                if (typeof (availableAssistants[0].img2) == "undefined" || availableAssistants[0].img2 == "") {
                    if (typeof (settings.globalOverrideStoreImage) != "undefined" && settings.globalOverrideStoreImage != null && settings.globalOverrideStoreImage != "") {

                    } else {
                        //no store image, hide the arrow.
                        $("#inside_offerTab_arrow").hide();
                        $("#inside_offerImage").slideUp(time);
                        return;
                    }

                }
            }


            $("#inside_offerImage").slideUp(time);
            //$("#inside_offerImageFooter").slideUp(500);

            $("#inside_offerTab_background").hide();
            $("#insideOfferDots").hide();
            $("#inside_offerTab").addClass("inside_closed");
        }

        var closeTimeout;

        function closeImagePaneAfter(time) {
            closeTimeout = setTimeout(closeImagePane, time);
            if (settings.useCustomChatPane && settings.showStoreImagePane == true) {
                setTimeout(function () {
                    $("#inside_custom_storeimage_img").slideUp();
                    $("#customStoreImageToggle .inside_arrow").html("&#x25BC;");
                }, time);
            }
        }

        function getAssistant(id) {
            if (isLocal()) {
                return { name: "Jeremy", img1: "/images/default-insider-photo.jpg", img2: "" };
            }

            for (var i = 0; i < availableAssistants.length; i++) {
                if (availableAssistants[i].id == id) {
                    return availableAssistants[i];
                }
            }

            if (typeof savedOperators[id] != "undefined") {
                return savedOperators[id];
            }
            return null;
        }

        function closeChat() {
            closeChatWindow();
            $(".blueChat").hide();
            if (!hideChatTab) {
                $("#inside_liveChatTab").show();
            }
            enableChat();
        }

        function disableChat(placeholder) {
            if (typeof (placeholder) == "undefined") {
                if (settings != null && typeof settings.chatInputDisabledPlaceholder != "undefined") {
                    placeholder = settings.chatInputDisabledPlaceholder;
                } else {
                    placeholder = translate("No " + insiderDescriptionPlural + " are currently available.");
                }
            }
            $("#inside_chatInput, #inside_chatPane_custom_input").attr("readonly", "readonly");
            $("#inside_chatInput, #inside_chatPane_custom_input").css("background-color", "#cccccc");
            $("#inside_chatInput, #inside_chatPane_custom_input").prop("disabled", true);
            $("#inside_chatInputHolderTable, #inside_chatPane_custom_footer").addClass("disabled");

            $("#inside_chatInput, #inside_chatPane_custom_input").attr("placeholder", placeholder).attr("title", placeholder);
            updatePlaceholder();
        }

        function enableChat() {
            $("#inside_chatInput, #inside_chatPane_custom_input").removeAttr("readonly");
            $("#inside_chatInput, #inside_chatPane_custom_input").css("background-color", "#ffffff");
            $("#inside_chatInput, #inside_chatPane_custom_input").prop("disabled", false);
            $("#inside_chatInputHolderTable, #inside_chatPane_custom_footer").removeClass("disabled");

            $("#inside_chatInput, #inside_chatPane_custom_input").attr("placeholder", initialText).attr("title", initialText);
            updatePlaceholder();
        }

        insideChat.showOfferLarge = showOfferLarge;
        function showOfferLarge(id) {
            if (device == 2) {
                return;
            }
            //$("#inside_offerLargeContent .title").html("This offer is available");
            //$("#inside_offerLargeContent .description").html(offersArray[currentOffer].description + ". Enter the code <span class='inside_offerCode'>" + offersArray[currentOffer].code + "</span> in the checkout to receive the offer.");

            if (typeof (id) == "null" || typeof (id) == "undefined") {

            } else {
                //set the current offer based on the id
                for (var i = 0; i < offersArray.length; i++) {
                    if (offersArray[i].instanceid == id) {
                        currentOffer = i;
                        break;
                    }
                }
            }

            if (offersArray.length == 0) {
                alert("This offer has expired");
                return;
            }

            var popupHTML = offersArray[currentOffer].template;
            popupHTML = popupHTML.replace(/{id}/g, "");
            popupHTML = popupHTML.replace(/{image}/g, "<br /><img src='" + $.inside.front.offerurl + offersArray[currentOffer].image + "'><br />");
            popupHTML = popupHTML.replace(/{thumbnail}/g, "<br /><img src='" + $.inside.front.offerurl + offersArray[currentOffer].image + "'><br />");
            popupHTML = popupHTML.replace(/{expiry}/g, "<span class='insideOfferCountDown' offerid='" + currentOffer + "' date='" + offersArray[currentOffer].expiryDate + "'>" + showTimeRemaining(offersArray[currentOffer].expiryDate) + "</span> mins");
            popupHTML = popupHTML.replace(/{code}/g, "<span class='inside_offerCode'>" + offersArray[currentOffer].code + "</span>");

            $("#inside_offerLargeContent .offerPopupContent").html(popupHTML);
            //$("#inside_offerLargeContent .offerImage").html("<img src='" + $.inside.front.offerurl + offersArray[currentOffer].image + "'>");
            $("#inside_offerLargeWindow").fadeIn();

        }

        function setChatTabPosition(pos) {
            tabPosition = pos;
            if (settings != null && (typeof (settings.customTabImage) == "undefined" || settings.customTabImage == "")) {
                if (pos == "left" || pos == "middleleft" || pos == "bottomleft" || pos == "topleft") {

                    //$("#inside_liveChatTab").css("left", "0px");
                    $(".inside_chatPane, #inside_chatPane_custom").css("left", "10px").css("right", "auto");
                    $("#inside_liveChatTab .inside_chatTabImage").attr("src", $.inside.front.imageurl + "livechaticon_left.png");
                    //$(".inside_chatPane .inside_closeCross").css("background-image", "url('" + $.inside.front.imageurl + "frontchat_leftarrow.png')");
                } else {
                    //$(".inside_chatPane .inside_closeCross").css("background-image", "url('" + $.inside.front.imageurl + "frontchat_rightarrow.png')");
                }
            }
        }

        function orientationChange() {
            windowScale();
        }

        function chatTabImageLoaded(e) {

            setTimeout($.inside.front.windowScale, 1000);
            scaleChatTab();
            if (settings.autohideTabs) {
                if (device == 2 && settings.autohideOnMobile == false) {
                    //don't autohide on mobile
                } else if(settings.autoHideTabOnLoad == true || $("#inside_liveChatTab").attr("tabHidden") == "true"){
                    $.inside.front.autohideTabs(0);
                } else {
                    insideFrontInterface.positionTabs();
                    setTimeout($.inside.front.autohideTabs, 3000);
                }
            } else {
                insideFrontInterface.positionTabs();
            }
        }

        insideChat.onChatTabImageLoad = onChatTabImageLoad;
        function onChatTabImageLoad(arg) {
            console.log(arg);
            if (arg.complete) {
                if (settings.autohideTabs && settings.autoHideTabOnLoad == true) {
                    var hideAmount = 20;
                    if (typeof (settings.autoHideAmount) != "undefined" && !isNaN(settings.autoHideAmount)) {
                        hideAmount = parseInt(settings.autoHideAmount);
                    }
                    //$("#inside_liveChatTab").css("right", -arg.width + hideAmount);
                    insideFrontInterface.positionTabs();
                }
                $("#inside_tabs").show();
            }
        }

        insideChat.onJoinTheCallChatTabImageLoad = onJoinTheCallChatTabImageLoad;
        function onJoinTheCallChatTabImageLoad(arg) {
            if (arg.complete && $("#inside_liveChatTab").length == 0) {
                var hideAmount = 23;
                $("#inside_liveChatTab").css("right", -arg.width + hideAmount);
                setTimeout(function () { 
                    $("#inside_tabs").show(); 
                    insideFrontInterface.positionTabs();
                }, 500);
            }
        }

        function getImageSize(img) {
            var theImage = new Image();
            theImage.src = img.attr("src");
            return({width:theImage.width, height:theImage.height});
        }

        insideFrontInterface.scaleChatTab = scaleChatTab;
        function scaleChatTab() {
            if (!$("#inside_liveChatTab").is(":visible")) {
                return;
            }

            $("#inside_liveChatTab").css("width", "");
            //$("#inside_liveChatTab").show();
            var w = getImageSize($("#inside_liveChatTab img")).width;
            var pr = 1;
            try {
                pr = window.devicePixelRatio;
            } catch (e) { }

            if (device == 2 && pr > 1) {
                var viewport = $.inside.front.getViewPortSize();

                if (window.devicePixelRatio > 1) {
                    //w = w*(1/window.devicePixelRatio);
                }

                if (settings != null && typeof (settings.mobileTabWidth) != "undefined") {
                    w = viewport.width * settings.mobileTabWidth * 0.01;
                } else {
                    if (w > viewport.width) {
                        w = viewport.width;
                    }
                    if (w < viewport.width * 0.1) {
                        w = viewport.width * 0.1;
                    }
                }

                $("#inside_liveChatTab").width(w);
                $("#inside_liveChatTab img").css("width", "100%");
                //$("#inside_tabs").width($("#inside_tabs").contents().width());
            }
            $("#inside_liveChatTab").css("width", w + "px");
            //$("#inside_tabs").css("width", w + "px");
            $.inside.front.windowScale();
            insideFrontInterface.positionTabs();
        }

        function closeLargeOfferWindow() {
            $("#inside_offerLargeWindow").hide();
        }

        function trace() {
            var arr = Array.prototype.slice.call(arguments);
            if (typeof (console) != "undefined") {
                console.log(arr.join(","));
            }
        }

        var assistantEvents = [];

        insideFrontInterface.bind = frontBind;

        function frontBind(type, callback) {
            if (type == "assistants") {
                assistantEvents.push(callback);
            }
        }

        insideFrontInterface.getAvailableAssistants = getAvailableAssistants;
        function getAvailableAssistants() {
            return availableAssistants;
        }

        var previousChatLoaded = false;
        function loadPreviousChat() {
            var prevChatDiv = $("<div class='inside_previousConversations'><div id='inside_loadPrevChat'>" + translate("Load previous conversations") + "</div></div>");
            $(prevChatDiv).prependTo("#inside_chatWindow, #inside_chatPane_custom_chatHolder");
            $("#inside_loadPrevChat").click(function () {
                $(this).html("Loading...<span class='inside_loadingIcon'></span>")
                insideAPI.call("GetSessionChat", {}, function (response) {
                    lastChatDrawn = null;
                    lastChatDrawnFrom = null;

                    //var conversations = response.data;
                    //for(var j=conversations.length-1;j>=0;j--){
                    //var messages = conversations[j].messages;
                    var messages = response.data;
                    for (var i = 0; i < messages.length; i++) {
                        showMessage(messages[i], prevChatDiv);
                    }
                    prevChatDiv.append("<hr />")
                    lastChatDrawn = null;
                    lastChatDrawnFrom = null;
                    //}

                    lastChatDrawn = null;
                    lastChatDrawnFrom = null;

                    setTimeout(function () {
                        $("#inside_loadPrevChat").remove();
                        windowScale();
                        //scrollChatToBottom(0);
                        //scroll to bottom of previous chats
                        $("#inside_chatWindow, #inside_chatPane_custom_chatHolder").scrollTop($('.inside_previousConversations').height());
                    }, 0);
                });
            })
        }

        insideFrontInterface.openChatPane = openChatPane;
        function openChatPane(force) {
            if ($.inside.isConnected() == false) {
                //signalR is not connected
                return;
            }
            if (force != true && availableAssistants.length == 0) {
                if (settings != null && typeof (settings) != null && typeof (settings.offlineChat) != "undefined" && settings.offlineChat.enabled) {
                    showLeaveMessageForm();
                    return { result: true, available: 0 };
                } else {
                    return { result: false, available: 0, message: "<div class='inside_chatstatus'><img src='" + imageurl + "livelight.png' /> " + translate("Status: OFFLINE") + "</div>" };
                }

            } else {
                chatTabClick(force);

            }

            return { result: true, available: availableAssistants.length };
        }

        var wasVisible;
        var chatPaneOpen = false;
        insideChat.chatPaneOpen = chatPaneOpen;

        function animateOpenChatPane() {
            if(dontOpenForChats.indexOf(currentChatId) != -1){
                return;
            }
            if (($(".inside_chatPane").length > 0 && $(".inside_chatPane").is(":visible")) ||
                    ($("#inside_chatPane_custom").length > 0 && $("#inside_chatPane_custom").is(":visible"))
                ) {
                return;
            }
            $("#inside_joinTheCallChatTab, #inside_clickToCallChatTab, .inside_clicktocall_request").hide();

            chatPaneOpen = true;
            insideChat.chatPaneOpen = chatPaneOpen;

            clearNotifications();
            setTimeout(function () { closeImagePane(0) }, 0);
            showChatHeaderText();

            if (previousChatLoaded != true) {
                //TO DO: enable this later
                //loadPreviousChat();
                previousChatLoaded = true;
            }

            $(".inside_chatPane, #inside_chatPane_custom").show();

            setTimeout(function () {
                var offset = 10;

                if (typeof (tabPosition) == "undefined" || tabPosition == null || tabPosition == "topright" || tabPosition == "" || tabPosition == "right" || tabPosition == "bottomright") {
                    if (device == 2) offset = 10;
                    $(".inside_chatPane, #inside_chatPane_custom").css("right", "-350px");
                    //$(".inside_chatPane, #inside_chatPane_custom").animate({ right: offset }, { step: function () { setGlassPosition() }, duration: 250 });
                    insideTween.TweenLite.to($(".inside_chatPane, #inside_chatPane_custom"), 0.25, { right: offset, ease: insideTween.Quad.easeOut });
                } else {
                    if (device == 2) offset = 0;
                    $(".inside_chatPane, #inside_chatPane_custom").css("left", "-350px");
                    //$(".inside_chatPane, #inside_chatPane_custom").animate({ left: offset }, { step: function () { setGlassPosition() }, duration: 250 });
                    insideTween.TweenLite.to($(".inside_chatPane, #inside_chatPane_custom"), 0.25, { left: offset, ease: insideTween.Quad.easeOut, onUpdate:function(){setGlassPosition();} });
                }
            }, 0);

            windowScale();
            setTimeout(windowScale, 1000);

            if (device == 2) {
                $("#inside_liveChatTab").hide();
                $("body").addClass("inside-chat-open");
            } else {
                insideTween.TweenLite.to($("#inside_liveChatTab"), 0.5, { opacity: 0, ease: insideTween.Quad.easeOut });
            }

            closeImagePaneAfter(6000);
            scaleChatInput();
            setTimeout(scaleChatInput, 0);
            if (device == 1 || device == 3) {
                bindDocumentClick();
            }


            if (device == 2) {
                $("#inside_holder").addClass("mobileChatOpen");
                //$(".inside_chatPane").css("bottom", -$(document).height() + $(window).height());	
                mobileScrollPosBeforeChatOpened = $(window).scrollTop();
                $(window).scrollTop(0);

                wasVisible = $("body > *:visible:not('#inside_holder')");
                wasVisible.hide();
                $("#inside_holder").show();

            }
            if (availableAssistants.length > 0 && $(".inside_chatDisclaimer_agree").length == 0) {
                $("#inside_chatInputHolderTable").removeClass("disabled");
                $("#inside_chatInput").prop("disabled", false);
            }

        }

    var newMessageSFX;
    function playNewMessageSFX() {
        if (settings.enableNewMessageSound) {
            if (!newMessageSFX) {
                if((new Audio()).canPlayType("audio/ogg; codecs=vorbis") != ""){
                    newMessageSFX = new Audio(_insideCDN + "sfx/newmessage.ogg");
                } else {
                    newMessageSFX = new Audio(_insideCDN + "sfx/newmessage.mp3");
                }
            }
            var obj1 = newMessageSFX.play();
        }
    }

        //#region Cobrowse

        insideChat.setupJoinTheCallTag = setupJoinTheCallTag;
        function setupJoinTheCallTag(clickToCallSettings) {
            if ($("#inside_joinTheCallChatTab").length) { return; }

            var html = "<div id='inside_joinTheCallChatTab' autohide='24' tabindex='1' class='noselect' ondragstart='return false;' ondrop='return false;'>" +
                            "<img id='inside_joinTheCallImage' src='" + imageurl + "assistance-tab.png' width='181' height='115' onload='insideChat.onJoinTheCallChatTabImageLoad(this);' />" +
                            "<div id='inside_joinTheCallPasscode' />" +
                        "</div>";

            var $html = $(html);
            $(html).find("#inside_joinTheCallChatTab").hide();
            $html.appendTo("#inside_tabs");

            settings.showJoinTheCallIcon = clickToCallSettings.showJoinTheCallIcon;
            if (clickToCallSettings.showJoinTheCallIcon) {
                $("#inside_joinTheCallIcon").show();
            }

            $.when(getVisitorCobrowsePasscode())
            .done(function (response) {
                $("#inside_joinTheCallPasscode").text(response.data);
            })
            .fail(function (response) {
                $("#inside_joinTheCallPasscode").text("????");
                console.log(response);
            });

            setJoinTheCallTag(availableAssistants.length === 0);
        }

        function positionJoinTheCallTag() {
            if ($("#inside_joinTheCallChatTab").length === 0) { return; }
        }

        function setJoinTheCallTag(show) {
            if ($("#inside_joinTheCallChatTab").length === 0) { return; }

            positionJoinTheCallTag();

            if (show && !chatPaneOpen) {
                $("#inside_joinTheCallChatTab").show();
            } else {
                $("#inside_joinTheCallChatTab").hide();
            }
        }

        function getVisitorCobrowsePasscode() {
            return $.Deferred(function (d) {
                insideAPI.call("GetVisitorPasscode", {}, function (response) {
                    if (response.status == "Error") {
                        d.reject(response);
                    } else {
                        d.resolve(response);
                    }
                });
            }).promise();
        }

        //#endregion Cobrowse

        function bindDocumentClick() {
            $(document).on("mousedown touchstart", documentClick);
        }

        function documentClick(e) {
            var container = $(".inside_chatPane, #inside_chatPane_custom");
            if (!container.is(":visible")) {
                return;
            }

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0 // ... nor a descendant of the container
                && (e.target != $('html').get(0))) // nor the scrollbar
            {
                closeChatWindow();
            }

        }

        function unbindDocumentClick() {
            $(document).unbind("mousedown touchstart", documentClick);
        }

        function setBackgroundImage(element, src) {
            $('<img/>').attr('src', src).load(function () {
                //$(element).html("");
                if (navigator.userAgent.indexOf("MSIE") > -1) {
                    $(element).css("filter", "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "',sizingMethod='scale');");
                    $(element).css("-ms-filter", "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "',sizingMethod='scale');");
                } else {
                    $(element).css("background-image", "url(" + src + ")");
                }
            });
        }

        insideChat.hideChatHeader = hideChatHeader;
        function hideChatHeader() {
            chatPaneMode = "";
            windowScale();
        }

        insideChat.hideInsideStore = hideInsideStore;
        function hideInsideStore() {
            insideStoreHidden = true;
            $("#inside_frontStoreAssistant").show();
            $("#inside_frontStoreAssistant").hide();
            $("#inside_frontStoreImage").hide();
            chatPaneMode = "";
            windowScale();
        }
        function showInsideStore() {
            insideStoreHidden = false;
            $("#inside_frontStoreAssistant").show();
            $("#inside_frontStoreImage").show();
            chatPaneMode = "";
            windowScale();
        }

        function safeHTML(s) {
            var div = document.createElement('div');
            div.innerHTML = s;
            $(div).find("script, canvas, iframe, frame, frameset, applet, object, embed").remove();
            return div.innerHTML;
        }
        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value) {
            return $('<div/>').html(value).text();
        }

        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value) {
            return $('<div/>').html(value).text();
        }

        insideChat.translate = translate;
        //translations should be from English
        function translate(str) {
            if (language == "en") {
                return str;
            }
            var squareReplace = /\[(.*?)\]/gi;

            for (var i in lang.translations) {
                var englishPhrase = i;
                var translation = lang.translations[i][language];
                if (typeof (translation) == "undefined") continue;

                var toMatch = "^" + englishPhrase.split(".").join("\\.").replace(squareReplace, "(.*)") + "$";
                var regex = new RegExp(toMatch, "gi");
                var matches = str.match(regex);
                if (matches != null && matches.length > 0) {
                    var newStr = translation;
                    var contentsOfBrackets;
                    regex.lastIndex = 0;
                    var exec = regex.exec(str);

                if(exec) {
                    contentsOfBrackets = exec[1];
                }
                
                if(contentsOfBrackets){
                    newStr = translation.replace(squareReplace, contentsOfBrackets);
                }
                return newStr;
            }
        }
        return str;
    }
    //translations to other languages (French...)
    //nl: dutch/flemish, fr:french, es: spanish, it: Italian, de:german, sv: swedish, no: norwegian, da:danish
    var lang = {
        daysOfWeek: {en:["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], fr:["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]},
         translations: {
            "Type something here...":{
                "fr-ca": "Tapez quelque chose ici...",
                "fr": "Saisissez quelque chose ici...",
                "nl": "Typ hier iets...",
                "es": "Escriba algo aquí...",
                "it": "Scrivi qualcosa qui...",
                "de": "Geben Sie hier etwas ein...",
                "sv": "Skriv något här...",
                "no": "Skriv noe her…",
                "da": "Skriv noget her…",
                "ru": "Начните вводить сообщение...",
                "pt": "Escreva aqui.."
            },
            "says":{
                "fr-ca": "dit",
                "fr": "dit",
                "nl": "zegt",
                "es": "dice",
                "it": "dice",
                "de": "sagt",
                "sv": "säger",
                "no": "sier",
                "da": "siger",
                "ru": "",
                "pt": "diz"
            },
            "[operator name] says":{
                "fr-ca": "[operator name] dit",
                "fr": "[nom opérateur] dit",
                "nl": "[operator name] zegt",
                "es": "[operator name] dice",
                "it": "(nome operatore) dice",
                "de": "[Mitarbeitername] sagt",
                "sv": "[operatör namn] säger",
                "no": "[Operatør navn] sier",
                "da": "[Operatør navn] siger",
                "pt": "[nome do operador] diz"
            },
            "today":{
                "fr-ca": "aujourd'hui",
                "fr": "aujourd'hui",
                "nl": "vandaag",
                "es": "hoy",
                "it": "Oggi",
                "de": "heute",
                "sv": "idag",
                "no": "i dag",
                "da": "i dag",
                "ru": "Сегодня",
                "pt": "hoje"
            }, 
            "yesterday":{
                "fr-ca": "hier",
                "fr": "hier",
                "nl": "gisteren",
                "es": "ayer",
                "it": "Ieri",
                "de": "gestern",
                "sv": "igår",
                "no": "i går",
                "da": "i går",
                "ru": "Вчера",
                "pt": "ontem"
            },
            "Hi [visitor name], not [visitor name]? Click here":{
                "fr-ca": "Bonjour [nom du visiteur] , pas [nom du visiteur] ? Cliquez ici",
                "fr": "Bonjour [nom visiteur], vous n'êtes pas [nom visiteur] ? Cliquez ici",
                "nl": "Hallo [visitor name], bent u [visitor name] niet? Klik hier",
                "es": "Hola [visitor name], ¿no eres [visitor name]? Haga clic aquí.",
                "it": "Benvenuto (nome visitatore), Lei non è (nome visitatore)? Clicca qui",
                "de": "Hallo [Nutzername]. Sie sind nicht [Nutzername]? Hier klicken",
                "sv": "Hej [besökarens namn], inte [besökarens namn]? Klicka här",
                "no": "Hei [besøkers navn], ikke [besøkers navn]? Klikk her",
                "da": "Hej [besøgers navn], ikke [besøgers navn]? Klik her",
                "ru": "Здравствуйте [visitor name], Вы не [visitor name]? Нажмите сюда",
                "pt": "Olá [nome do visitante], não é [nome do visitante] Clique aqui"
            },
            "Status: ONLINE":{
                 "fr-ca": "Statut : en ligne",
                 "fr": "Statut : EN LIGNE",
                 "nl": "Status: ONLINE",
                 "es": "Estado: CONECTADO",
                 "it": "Status: ONLINE",
                 "de": "Status: ONLINE",
                 "sv": "Status: Öppen",
                 "no": "Status: Online",
                 "da": "Status: Online",
                 "ru": "Онлайн",
                 "pt": "Estado: ONLINE"
            },
            "Status: OFFLINE":{
                "fr-ca": "Statut : hors ligne",
                "fr": "Statut : HORS LIGNE",
                "nl": "Status: OFFLINE",
                "es": "Estado: DESCONECTADO",
                "it": "Status: OFFLINE",
                "de": "Status: OFFLINE",
                "sv": "Status: Stängd",
                "no": "Status: Offline",
                "da": "Status: Offline",
                "ru": "Оффлайн",
                "pt": "Estado: OFFLINE"
            },
            "There was an error submitting your message. Please try again.":{
                "fr-ca": "Il y a eu une erreur et nous n’avons pas reçu votre message. Veuillez réessayer svp.",
                "fr": "Une erreur est survenue lors de l'envoi de votre message. Veuillez réessayer.",
                "nl": "Er is een fout opgetreden bij het verzenden van uw bericht. Probeer het opnieuw.",
                "es": "Se ha producido un error al enviar su mensaje. Vuelva a intentarlo.",
                "it": "Si è verificato un errore confermando il suo messaggio. per favore riprova",
                "de": "Bei der Übermittlung Ihrer Nachricht ist ein Fehler aufgetreten. Versuchen Sie es bitte erneut.",
                "sv": "Det gick inte att skicka ditt meddelande. Försök igen.",
                "no": "Din melding ble ikke sendt, vennligst prøv igjen.",
                "da": "Der opstod en fejl ved afsendelse af din besked. Prøv igen.",
                "ru": "Возникла ошибка при отправке Вашего сообщения. Пожалуйста, попробуйте еще раз.",
                "pt": "Ocorreu um erro ao submeter a sua mensagem. Por favor tente novamente"
            },
            "Email address is invalid. Please check":{
                "fr-ca": "Adresse email invalide. Vérifiez s'il vous plaît.",
                "fr": "L'adresse e-mail n'est pas valide. Veuillez vérifier",
                "nl": "E-mailadres is ongeldig. Controleer dit",
                "es": "La dirección de correo electrónico no es válida. Por favor, compruebela",
                "it": "Ll'indirizzo email non è valido. Per favore verifica",
                "de": "Ungültige E-Mail-Adresse. Bitte überprüfen Sie die Angaben",
                "sv": "E-postadressen är ogiltig. Vänligen kontrollera.",
                "no": "Epostadressen er ikke gyldig. Vennligst kontroller.",
                "da": "Email adresse ikke gyldig. Tjek venligst.",
                "ru": "Введен неверный электронный адрес. Проверьте, пожалуйста.",
                "pt": "Endereço de e-mail inválido. Por favor verifique"
            },
            "Enter [field name]":{
                "fr-ca": "Entrez [ nom du champ ]",
                "fr": "Entrez [nom champ]",
                "nl": "Voer het veld [field name] in",
                "es": "Introducir [field name]",
                "it": "Accedi (nome campo)",
                "de": "Eingabe von [feldname]",
                "sv": "Ange [fieldname]",
                "no": "Tast [field name]",
                "da": "Tast [field name]",
                "ru": "Введите [field name]",
                "pt": "Enter [nome de campo]"
            },
            "Phone is invalid. Please check.":{
                "fr-ca": "Numéro de téléphone invalide. Vérifiez le s'il vous plaît.",
                "fr": "Le numéro de téléphone n'est pas valide. Veuillez vérifier.",
                "nl": "Telefoonnummer is ongeldig. Controleer dit.",
                "es": "El teléfono no es válido. Por favor, compruebelo.",
                "it": "Numero di telefono errato. Per favore verifica",
                "de": "Ungültige Telefonnummer. Bitte überprüfen Sie die Angaben.",
                "sv": "Telefonnumret är ogiltigt. Vänligen kontrollera.",
                "no": "Telefonnummerer er ikke gyldig. Vennligst kontroller.",
                "da": "Telefonnummeret ugyldigt. Tjek venligst.",
                "ru": "Введен неверный номер телефона. Проверьте, пожалуйста.",
                "pt": "Telefone inválido. Por favor verifique"
            },
            "transferred":{
                "fr-ca": "transféré",
                "fr": "transféré",
                "nl": "overgedragen",
                "es": "transferido",
                "it": "Trasferito",
                "de": "weitergeleitet",
                "sv": "överfört",
                "no": "overført.",
                "da": "overført",
                "ru": "Перемещен",
                "pt": "transferido"
            },
            //tooltips
            "Minimise Chat Pane":{
                "fr-ca": "Minimiser Chat Pane",
                "fr": "Réduire le volet de Chat",
                "nl": "Chatvenster minimaliseren",
                "es": "Minimizar panel de chat",
                "it": "Rimpicciolisci pannello chat",
                "de": "Chatbereich minimieren",
                "sv": "Minimera chattfönster",
                "no": "Minimer chatvinduet",
                "da": "Minimér chat vindue",
                "ru": "Свернуть окно чата",
                "pt": "Minimizar janela Chat"
            }, 
            "Close Chat Pane":{
                "fr-ca": "Fermer la fenêtre de clavardage",
                "fr": "Fermer le volet de Chat",
                "nl": "Chatvenster sluiten",
                "es": "Cerrar panel de chat",
                "it": "Chiudi pannello chat",
                "de": "Chatbereich schließen",
                "sv": "Stäng chattfönster",
                "no": "Lukk chatvinduet",
                "da": "Luk chat vindue",
                "ru": "Закрыть окно чата",
                "pt": "Fechar janela Chat"
            }, 
            "About":{
                "fr-ca": "Au sujet de",
                "fr": "À propos de",
                "nl": "Over",
                "es": "Acerca de",
                "it": "Circa",
                "de": "Info",
                "sv": "Om",
                "no": "Om",
                "da": "Om",
                "ru": "Описание",
                "pt": "Sobre"
            }, 
            "Send":{
                "fr-ca": "Envoyer",
                "fr": "Envoyer",
                "nl": "Verzenden",
                "es": "Enviar",
                "it": "Invia",
                "de": "Senden",
                "sv": "Skicka",
                "no": "Send",
                "da": "Send",
                "ru": "Отправить",
                "pt": "Enviar"
            },
            "Ask your question here...":{
                "fr-ca": "Posez votre question ici ...",
                "fr": "Posez votre question ici...",
                "nl": "Stel hier uw vraag...",
                "es": "Haga su pregunta aquí...",
                "it": "Digita la tua domanda qui...",
                "de": "Geben Sie Ihre Frage hier ein...",
                "sv": "Ställ din fråga här…",
                "no": "Still ditt spørsmål her",
                "da": "Stil dit spørgsmål her",
                "ru": "Задайте вопрос...",
                "pt": "Coloque aqui a sua questão..."
            },
            "Type here to chat with [name]":{
                "fr-ca": "Tapez ici pour clavarder avec [nom]",
                "fr": "Saisissez votre texte ici pour discuter avec [nom]",
                "nl": "Typ hier om met [name] te chatten",
                "es": "Escriba aquí para chatear con [name].",
                "it": "Digita qui per chattare con (nome)",
                "de": "Geben Sie Ihre Nachricht hier ein, um mit [name] zu chatten",
                "sv": "Skriv här för att chatta med [namn]",
                "no": "Skriv her for å chatte med [navn]",
                "da": "Skriv her for at chatte med [navn]",
                "ru": "Напишите ваше сообщение",
                "pt": "Escreva aqui para falar com [nome]"
            },
            "Click to send your message":{
                "fr-ca": "Cliquez pour soumettre votre message",
                "fr": "Cliquez pour envoyer votre message",
                "nl": "Klik om uw bericht te verzenden",
                "es": "Haga clic para enviar mensaje.",
                "it": "Clicca per inviare il tuo messaggio",
                "de": "Durch Klicken versenden Sie Ihre Nachricht",
                "sv": "Klicka för att skicka ditt meddelande",
                "no": "Klikk for å sende din melding",
                "da": "Klik for at afsende din besked",
                "ru": "Нажмите, чтобы отправить сообщение",
                "pt": "Clique aqui para enviar a sua mensagem"
            }, 
            "You are chatting with [assistant.name]":{
                "fr-ca": "Vous clavardez avec [assistant.name]",
                "fr": "Vous discutez actuellement avec [nom assistant]",
                "nl": "U bent aan het chatten met [assistant.name]",
                "es": "Está hablando con [assistant.name].",
                "it": "Stai chattando con (nome assistente)",
                "de": "Sie chatten gerade mit [assistant.name]",
                "sv": "Du chattar med [medarbetarens namn]",
                "no": "Du chatter med [Asisstant.name]",
                "da": "Du chatter med [Asisstant.name]",
                "ru": "Вы беседуете с [assistant.name]",
                "pt": "Está a falar com [nome do operador]"
            },
            "Email transcript":{
                "fr-ca": "Transcription de courriel",
                "fr": "Script de l'e-mail",
                "nl": "Transcriptie e-mailen",
                "es": "Transcripción del correo electrónico",
                "it": "Trascrizione email",
                "de": "E-Mail-Aufzeichnung",
                "sv": "E-postkopia",
                "no": "Kopi på e-post",
                "da": "Email kopi",
                "ru": "Отправить распечатку на электронный адрес",
                "pt": "Transcrição de email"
            },
            "Customer Engagement by INSIDE":{
                "fr-ca": "Engagement client par INSIDE",
                "fr": "Interaction client par INSIDE",
                "nl": "Customer Engagement door INSIDE",
                "es": "Interactuación del cliente por INSIDE",
                "it": "Coinvolgimento cliente attraverso INSIDE",
                "de": "Kundenengagement durch INSIDE",
                "sv": "Kundengagemang från INSIDE",
                "no": "Kundeengasjement av INSIDE",
                "da": "Kunde engagement af INSIDE",
                "ru": "Поддержка пользователей с помощью INSIDE",
                "pt": "Interação com o Cliente pela INSIDE"
            },
            "No [insiderDescriptionPlural] are currently available.":{
                "fr-ca": "Aucun [ insiderDescriptionPlural ] n’est actuellement disponible.",
                "fr": "Aucune [Description des utilisateurs Inside (pluriel)] n'est actuellement disponible.",
                "nl": "Er zijn momenteel geen [insiderDescriptionPlural] beschikbaar.",
                "es": "Ya no está disponible [insiderDescriptionPlural].",
                "it": "Nessuna (descrizioni interne plurali) sono attualmente disponibili",
                "de": "Zurzeit sind keine [insiderDescriptionPlural] verfügbar.",
                "sv": "Ingen [insiderDescriptionPlural] är tillgänglig för närvarande",
                "no": "Ingen [insiderDescriptionPlural] er tilgjengelig for øyeblikket",
                "da": "Ingen [insiderDescriptionPlural] er tilgængelige for øjeblikket.",
                "ru": "В данный момент нет доступных [insiderDescriptionPlural]",
                "pt": "[Descrição] não estão disponíveis no momento."
            },
            "Load previous conversations":{
                "fr-ca": "Charger les conversations précédentes",
                "fr": "Charger les conversations précédentes",
                "nl": "Eerdere gesprekken laden",
                "es": "Cargar conversaciones anteriores",
                "it": "Carica conversazioni precedenti",
                "de": "Vorherige Unterhaltungen aufrufen",
                "sv": "Ladda tidigare konversationer",
                "no": "Hent tidligere samtaler",
                "da": "Indlæs tidligere koversationer",
                "ru": "Загрузить предыдущие сообщения",
                "pt": "Carregar conversas anteriores."
            },
            "Chat Transcript" :{
                "fr-ca": "Transcription de clavardages",
                "fr": "Script du Chat",
                "nl": "Transcriptie van chat",
                "es": "Transcripción del chat",
                "it": "Trascrizione chat",
                "de": "Chat-Aufzeichnung",
                "sv": "Kopia på samtalslogg",
                "no": "Kopi av chat-logg",
                "da": "Kopi af chat",
                "ru": "Распечатка диалога чата",
                "pt": "Transcrição de Chat"
            },
            "Enter your email":{
                "fr-ca": "Entrez votre adresse de courriel",
                "fr": "Entrez votre adresse e-mail",
                "nl": "Voer uw e-mailadres in",
                "es": "Introduzca su correo electrónico",
                "it": "Inserisci la tua email",
                "de": "Geben Sie Ihre E-Mail ein",
                "sv": "Skriv in din e-postadress",
                "no": "Skriv inn din mailadresse",
                "da": "Indtast din email adresse",
                "ru": "Введите Ваш электронный адрес",
                "pt": "Insira o seu email"
            }, 
            "No messages were found, or there was an error obtaining the chat transcript.":{
                "fr-ca": "Aucun message n'a été trouvé, ou il y a eu une erreur de transcription de clavardage.",
                "fr": "Aucun message n'a été trouvé ou une erreur est survenue lors de la récupération du script du Chat",
                "nl": "Er zijn geen berichten gevonden of er is een fout opgetreden bij het ophalen van de transcriptie van de chat.",
                "es": "No se ha encontrado ningún mensaje o se ha producido un error mientras se obtenía la transcripción del chat.",
                "it": "Nessun messaggio trovato, oppure si è verificato un errore nell'ottenimento della trascrizione chat",
                "de": "Die Nachrichtensuche ergab keine Ergebnisse. Möglicherweise ist ein Fehler bei der Chat-Aufzeichnung aufgetreten.",
                "sv": "Inga samtalsloggar hittades, eller så uppstod det ett fel vid hämtningen.",
                "no": "Ingen melding funnet, eller en feil oppstod ved hentingen av chat dialogen",
                "da": "Ingen beskeder blev fundet, eller også opstod der en fejl i processen.",
                "ru": "Сообщения не найдены или произошла ошибка во время передачи истории чата",
                "pt": "Não foram encontradas mensagens ou ocorreu um erro na pesquisa do Chat."
            }, 
            "Conversation started":{
                "fr-ca": "Début de clavardage",
                "fr": "Conversation démarrée",
                "nl": "Gesprek gestart",
                "es": "Conversación iniciada",
                "it": "Conversazione iniziata",
                "de": "Unterhaltung wurde gestartet",
                "sv": "Chatt påbörjad",
                "no": "Samtale startet",
                "da": "Chat påbegyndt",
                "ru": "Беседа началась",
                "pt": "Conversa iniciada..."
            },
            "Live chat icon, click to open the live chat pane.":{
                "fr-ca": "Icône de clavardage en direct, cliquez pour ouvrir la fenêtre de clavardage en direct.",
                "fr": "Icône de Chat en temps réel, cliquez dessus pour ouvrir le volet de Chat en temps réel.",
                "nl": "Livechat-pictogram, klik erop om het livechatvenster te openen.",
                "es": "Icono de chat en directo, haga clic para abrir el panel de chat en directo.",
                "it": "Icona della live chat, clicca per aprire il pannello di live chat",
                "de": "Klicken Sie auf das Live-Chat-Symbol, um den Live-Chatbereich zu öffnen.",
                "sv": "Live chat ikon, klicka för att öppna ett live chattfönster",
                "no": "Live Chat ikon, klikk for å åpne live chat vinduet",
                "da": "Live chat ikon, klik for at åbne live chat vinduet.",
                "ru": "Иконка окна чата, нажмите, чтобы открыть окно чата",
                "pt": "Ícone do Chat disponível , clique aqui para abrir uma janela de conversação. "
            },
            "Join the Call":{
                "fr-ca": "Joignez-vous à l'appel",
                "fr": "Prendre l'appel",
                "nl": "Deelnemen aan het gesprek",
                "es": "Unirse a la llamada",
                "it": "Unisciti alla chiamata",
                "de": "Am Anruf teilnehmen",
                "sv": "Anslut till chatten",
                "no": "Bli med i samtalen",
                "da": "Tilslut dig til samtalen",
                "ru": "Подключить звонящего",
                "pt": "Entre na chamada"
            },
            "Print transcript":{
                "fr-ca": "Imprimer la transcription",
                "fr": "Imprimer le script",
                "nl": "Transcriptie afdrukken",
                "es": "Imprimir transcripción",
                "it": "Stampa trascrizione",
                "de": "Aufzeichnung ausdrucken",
                "sv": "Skriv ut kopia",
                "no": "Skriv ut chat-logg",
                "da": "Udskriv kopi",
                "ru": "Распечатать переписку",
                "pt": "Imprimir conversa"
            },
            "submit":{
                "fr-ca": "soumettre",
                "fr": "envoyer",
                "nl": "verzenden",
                "es": "enviar",
                "it": "Conferma",
                "de": "senden",
                "sv": "Skicka",
                "no": "Send",
                "da": "Indsend",
                "ru": "Подтвердить",
                "pt": "Submeter"
            }, 
            "Please select an answer to all questions": {
                "fr-ca": "S'il vous plaît choisir une réponse à toutes les questions",
                "fr": "Veuillez sélectionner une réponse pour chaque question",
                "nl": "Selecteer een antwoord op alle vragen",
                "es": "Seleccione una respuesta para todas las preguntas.",
                "it": "Per favore seleziona una risposta per ogni domanda",
                "de": "Bitte wählen Sie eine Antwort auf alle Fragen",
                "sv": "Vänligen välj ett svar till alla frågor",
                "no": "Vennligst velg et svar til alle spørsmål",
                "da": "Vær venlig at vælge et svar til alle spørgsmål",
                "ru": "Пожалуйста, ответьте на все вопросы",
                "pt": "Por favor selecione uma resposta para todas as questões"
            }, 
            "You have selected department [dept]": {
                "fr-ca" : "Vous avez sélectionné département [dept]",
                "fr" :"Vous avez sélectionné département [dept]",
                "nl": "U heeft gekozen afdeling [dept]",
                "es": "Ha seleccionado departamento [dept]",
                "it": "Hai selezionato reparto [dept]",
                "de": "Sie haben die Abteilung [dept]",
                "sv": "Du har valt avdelning [dept]",
                "no": "Du har valgt avdeling [dept]",
                "da": "Du har valgt afdeling [dept]",
                "ru": "Вы выбрали отделение [dept]",
                "pt": "Selecionou o departamento [departamento]"
            }, 
            "Please select a department": {
                "fr-ca" : "S'il vous plaît sélectionner un département",
                "fr" :"S'il vous plaît sélectionner un département",
                "nl": "Selecteer een afdeling",
                "es": "Por favor, seleccione un departamento",
                "it": "Si prega di selezionare un dipartimento",
                "de": "Bitte wählen Sie eine Abteilung",
                "sv": "Välj en avdelning",
                "no": "Velg en avdeling",
                "da": "Vælg afdeling",
                "ru": "Пожалуйста, выберите отделение",
                "pt": "Por favor selecione um departamento"
            },
            "Please agree to the disclaimer.": {
                "fr-ca" : "S'il vous plaît accepter l'avertissement.",
                "fr" :"S'il vous plaît accepter l'avertissement.",
                "nl": "Ga akkoord met de disclaimer.",
                "es": "Por favor, de acuerdo a la nota.",
                "it": "Si prega di accettare il disclaimer.",
                "de": "Bitte stimmen Sie den Disclaimer zu.",
                "sv": "Du måste godkänna villkoren.",
                "no": "Godta ansvarsfraskrivelsen.",
                "da": "Du skal acceptere ansvarsfraskrivelsen.",
                "ru": "Пожалуйста, подтвердите отказ от ответственности",
                "pt": "Por favor concorde com o aviso."
            },
            "Close the chat?": {
                "fr-ca" : "S'il vous plaît accepter l'avertissement.",
                "fr" :"S'il vous plaît accepter l'avertissement.",
                "nl": "Ga akkoord met de disclaimer.",
                "es": "Por favor, de acuerdo a la nota.",
                "it": "Si prega di accettare il disclaimer.",
                "de": "Bitte stimmen Sie den Disclaimer zu.",
                "sv": "Du måste godkänna villkoren.",
                "no": "Godta ansvarsfraskrivelsen.",
                "da": "Du skal acceptere ansvarsfraskrivelsen.",
                "ru": "Закрыть чат?",
                "pt": "Fechar o Chat?"
            },
            "Yes":{
                "fr-ca" : "Oui",
                "fr" :"Oui",
                "nl": "Ja",
                "es": "Sí",
                "it": "Sì",
                "de": "Ja",
                "sv": "Ja",
                "no": "Ja",
                "da": "Ja",
                "ru": "Да",
                "pt": "Sim"
            },
            "Cancel":{
                "fr-ca" : "Annuler",
                "fr" :"Annuler",
                "nl": "Annuleer",
                "es": "Cancelar",
                "it": "Annulla",
                "de": "Stornieren",
                "sv": "Annullera",
                "no": "Kansellere",
                "da": "Ophæve",
                "ru": "Нет",
                "pt": "Cancelar"
            }
        }
    }

        $.inside.front.chat = insideChat;
        setTimeout(init, 0);

    })(window, isLocal() ? $ : _insideGraph.jQuery);
});

// For those who need them (< IE 9), add support for CSS functions

if (typeof (CSSStyleDeclaration) != "undefined") {
    var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;

    if (!isStyleFuncSupported) {
        CSSStyleDeclaration.prototype.getPropertyValue = function (a) {
            return this.getAttribute(a);
        };
        CSSStyleDeclaration.prototype.setProperty = function (styleName, value, priority) {
            this.setAttribute(styleName, value);
            var priority = typeof priority != 'undefined' ? priority : '';
            if (priority != '') {
                // Add priority manually
                var rule = new RegExp(RegExp.escape(styleName) + '\\s*:\\s*' + RegExp.escape(value) + '(\\s*;)?', 'gmi');
                this.cssText = this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
            }
        }
        CSSStyleDeclaration.prototype.removeProperty = function (a) {
            return this.removeAttribute(a);
        }
        CSSStyleDeclaration.prototype.getPropertyPriority = function (styleName) {
            var rule = new RegExp(RegExp.escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?', 'gmi');
            return rule.test(this.cssText) ? 'important' : '';
        }
    }
}

// Escape regex chars with \
RegExp.escape = function (text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}


/* path = js/inside.front.survey.js */

var insideSurvey = {};

_insideGraph.registerModule("survey", function () {

    console.log("INSIDE Module 'survey' started");

    (function (window, $) {

        function escapeHtml(str) {
            var div = document.createElement('div');
            div.appendChild(document.createTextNode(str));
            return div.innerHTML;
        };

        var scrolledToSurvey = false;

        //callback is called each time a question is displayed - animate and position the popup.
        function loadSurvey(surveyID, popupContainer, callback, data) {
            var currentQuestion = 0;
            var survey;
            var selectedAnswerDiv;
            var logo;
            var header;
            var completionMessage;


            if ($(".inside_notify_content #insideSurveyHolder[surveyid='" + surveyID + "']").length > 0) {
                return;
                //survey already displayed.
            }
            popupContainer.attr("surveyid", surveyID);
            popupContainer.attr("stickyid", data.stickyid);

            popupContainer.closest(".inside_notify_content").find(".inside_siteLogo, .inside_survey_header").remove();
            if (logo != null) {
                logo.remove();
            }
            if (header != null) {
                header.remove();
            }
            logo = $("<div class='inside_siteLogo'><img src='' /></div>");

            var logourl = "";
            if (typeof ($.inside.front.settings.siteLogo) != "undefined" && $.inside.front.settings.siteLogo != null) {
                logourl = _insideCDN + $.inside.front.settings.siteLogo;
            } else if (typeof ($.inside.front.settings.companyLogo) != "undefined" && $.inside.front.settings.companyLogo != null) {
                logourl = _insideCDN + $.inside.front.settings.companyLogo;
            } else {
                //default inside logo
                logourl = _insideCDN + "images/inside-logo-email-2x.png";
            }
            logo.find("img").attr("src", logourl);
            if (data && data.settings.containerid != "#inside_chatWindow, #inside_chatPane_custom_chatHolder") {
                logo.insertBefore(popupContainer);
            }

            $('.survey_cont').html('');
            $('.container_form').html('');
            insideAPI.call("getSurvey", { surveyId: surveyID }, function (response) {
                if (response.data == null) {
                    //error loading data, remove survey container.
                    popupContainer.closest(".inside_notify_content").remove();
                    return;
                }
                survey = response.data;

                if (typeof (survey.settings.header) != "undefined" && survey.settings.header != "") {
                    $(".inside_visitorNotify.survey .inside_survey_header").remove();
                    if (header != null) {
                        header.remove();
                    }
                    header = $("<div class='inside_survey_header'>" + escapeHtml(survey.settings.header) + "</div>");
                    header.insertBefore(popupContainer);
                }

                //show the question
                var question = survey.questions[currentQuestion];
                var empty = true;
                if (surveyData.type == "chatsurvey") {
                    empty = false;
                    if (!scrolledToSurvey) {
                        scrollingTimeout = setTimeout(scrollToTopOfSurvey, 10);
                        scrollingTimeout = setTimeout(scrollToTopOfSurvey, 200);
                        scrolledToSurvey = true;
                    } else {
                        //scrollingTimeout = setTimeout(scrollToLastSelectedQuestion, 100);

                    }
                }
                showQuestion(question, popupContainer, empty);

                showQuestionsInSameGroup(question);
            });

            var scrollingTimeout;

            function scrollToLastSelectedQuestion() {
                var block = $(".insideSurveyAnswerBlock.answered:last");
                var holder = $(block).closest(".inside_chatWindow, #inside_chatPane_custom_chatHolder");
                if (holder.length > 0) {
                    var beforePos = holder.scrollTop();
                    block[0].scrollIntoView();
                    var pos = holder.scrollTop() - 25;

                    holder.scrollTop(beforePos);
                    holder.animate({ scrollTop: pos }, 500);
                }
            }

            function showQuestionsInSameGroup(question) {
                //TO DO: show questions with same group together
                for (var i = 0; i < survey.questions.length; i++) {
                    if (i != currentQuestion) {
                        if (typeof (survey.questions[i].settings.group) != "undefined" && survey.questions[i].settings.group != "" && survey.questions[i].settings.group == survey.questions[currentQuestion].settings.group) {
                            showQuestion(survey.questions[i], popupContainer, false);
                        }
                    }
                }
            }

            function getNumberOfQuestionsInGroup(question) {
                var count = 0;
                for (var i = 0; i < survey.questions.length; i++) {
                    if (typeof (survey.questions[i].settings.group) != "undefined" && survey.questions[i].settings.group != "" && survey.questions[i].settings.group == question.settings.group) {
                        count++;
                    }

                }
                return count;
            }

            function nextQuestion() {
                currentQuestion++;
                if (currentQuestion >= survey.questions.length) {
                    surveyComplete();
                    return;
                }
                var question = survey.questions[currentQuestion];
                if (question.shown) {
                    nextQuestion();
                    return;
                }

                //don't empty if in chat pane
                showQuestion(question, popupContainer, data.settings.containerid != "#inside_chatWindow, #inside_chatPane_custom_chatHolder");
                showQuestionsInSameGroup();
            }

            function surveyComplete() {

                //don't remove if it's a chat survey in the chat pane
                if (data.type == "chatsurvey" && data.settings.containerid == "#inside_chatWindow, #inside_chatPane_custom_chatHolder") {
                    //$(".insideSurveyAnswerBlock").append("<h3>Thanks for the feedback!</h3>");
                    $(".inside_notify_content #insideSurveyHolder[surveyid='" + surveyID + "']").removeAttr("surveyid");
                    showCompletedSurveyMessage();

                    popupContainer.find(".insideSubmitButton").remove();
                    popupContainer.css("pointer-events", "none");
                    popupContainer.addClass("complete");

                    popupContainer.css("margin-bottom", "100px");

                    //make sure its not scrolling anywhere else
                    clearTimeout(scrollingTimeout);
                    insideChat.scrollChatToBottom(500);

                    insideSurvey.closeTimeout = setTimeout(insideChat.closeChatPane, 5000);
                    $('#inside_chatInput, #inside_chatPane_custom_input').bind("click, keyup", function () {
                        if (typeof (insideSurvey) != "undefined" && typeof (insideSurvey.closeTimeout) != "undefined") {
                            clearTimeout(insideSurvey.closeTimeout);
                            delete insideSurvey.closeTimeout;
                        }
                    });

                } else {
                    $(".insideSurveyAnswerBlock:not(:first)").remove();
                    $(".insideSurveyQuestion:not(:first)").remove();
                    $(".inside_visitorNotify.survey .inside_closeButton").show();

                    showCompletedSurveyMessage();

                    $(window).resize();
                    setTimeout(function () {
                        popupContainer.closest(".inside_visitorNotify").find(".inside_closeButton").click();
                        popupContainer.closest(".inside_notify_content").find(".inside_siteLogo, .inside_survey_header").remove();
                        if (logo != null) {
                            logo.remove();
                        }
                        if (header != null) {
                            header.remove();
                        }

                        try {
                            /*
                            if(data.settings.containerid == "#inside_chatWindow, #inside_chatPane_custom_chatHolder"){
                                $(data.settings.containerid).find(".inside_visitorNotify").remove();
                            }
                            */
                        } catch (e) { }
                        popupContainer.remove();
                    }, 7000);
                }
            }

            function showCompletedSurveyMessage() {
                if (typeof (survey.settings.completionMessage) != "undefined" && survey.settings.completionMessage != "") {
                    $(".inside_visitorNotify.survey .inside_survey_completionMessage").remove();
                    //$(".insideSurveyAnswerBlock").empty();
                    if (completionMessage != null) {
                        completionMessage.remove();
                    }
                    completionMessage = $("<div class='surveyCompleteText'></div>");
                    completionMessage.append(survey.settings.completionMessage);
                    //completionMessage.insertAfter(popupContainer);
                    if (data.type == "chatsurvey" && data.settings.containerid == "#inside_chatWindow, #inside_chatPane_custom_chatHolder") {
                        popupContainer.append(completionMessage);
                    } else {
                        popupContainer.find(".insideSurveyQuestion").empty();
                        popupContainer.find(".insideSurveyAnswerBlock").html(completionMessage);
                    }
                } else {
                    if (data.type == "chatsurvey" && data.settings.containerid == "#inside_chatWindow, #inside_chatPane_custom_chatHolder") {
                        popupContainer.append("<div class='inside_survey_footer'>Thanks for the feedback!</div>");
                        popupContainer.find(".insideSubmitButton").remove();
                        popupContainer.css("pointer-events", "none");
                        popupContainer.addClass("complete");
                    } else {
                        popupContainer.find(".insideSurveyAnswerBlock").html("<h3>Thank you for completing this survey!</h3>");
                        popupContainer.find(".insideSurveyQuestion").html("<span class='surveyCompleteText'>All done!</span>");
                    }
                }
            }

            function showQuestion(question, popupContainer, empty) {
                if (empty != false) {
                    popupContainer.empty();
                }
                if (question.shown) {
                    nextQuestion();
                    return;
                }
                var questionsInThisGroup = getNumberOfQuestionsInGroup(question);

                question.shown = true;
                var answerType = "radio";

                question.text = replaceHtmlEntities(question.text);

                $('<p class="insideSurveyQuestion">' + escapeHtml(question.text) + '</p>').appendTo(popupContainer); //to do: replace &amp;
                var ah = "<div class='insideSurveyAnswerBlock'>";
                if (question.type == "drop down") {
                    ah += "<div class='answer'><select>";
                }
                for (var i = 0; i < question.answers.length; i++) {
                    answer = question.answers[i];
                    var j = question.id;
                    if (typeof (answer.link) == "undefined") {
                        answer.link = "";
                    }
                    if (question.type != "drop down") {
                        ah += '<div class="answer" link="' + answer.link + '" answerid="' + i + '">';
                    }

                    if (question.type == "free text") {
                        answerType = "free text";
                        //to to: add customised placeholder text for free text
                        var placeholder = "Enter your answer here";
                        if (answer.text != "") {
                            placeholder = answer.text;
                        }
                        ah += '<textarea style="width:100%;" placeholder="' + placeholder + '" type="radio" value="' + i + '" id="ans_' + j + '_' + i + '" name="ans_' + question.id + '"></textarea>';
                    } else if (question.type == "drop down") {
                        answerType = "drop down";
                        ah += "<option link='" + answer.link + "'>" + escapeHtml(answer.text) + "</option>";
                    } else if (question.type == 'checkbox') {
                        answerType = "checkbox";
                        ah += '<input type="checkbox" value="' + i + '" id="ans_' + j + '_' + i + '" name="ans_' + question.id + '"/>';
                        ah += '<label for="ans_' + j + '_' + i + '">' + escapeHtml(answer.text) + '</label></div>';
                    } else if (question.type == 'star rating') {
                        answerType = "star";
                        //ah += "<label>" + "" /* answer.text*/ + "</label>";
                        var colorStyle = "";
                        if (typeof answer.text != "undefined" && answer.text != "") {
                            colorStyle = 'style="color:' + answer.text + '"';
                        }
                        ah += "<div class='starSelectHolder' " + colorStyle + " a='" + i + "'></div>";
                        var _i = i;
                        ah += "</div>";
                    } else if (question.type == 'yes/no image selection') {
                        answerType = 'image';
                        //ah += '<label for="ans_'+j+'_'+i+'">'+escapeHtml(answer.text)+'</label></div>';
                        var yesImage;
                        var noImage;
                        if (typeof (question.settings.img1) != "undefined" && question.settings.img1 != "") {
                            yesImage = _insideCDN + question.settings.img1;
                        } else {
                            yesImage = _insideCDN + "images/surveys/smiley_happy.png";
                        }
                        if (typeof (question.settings.img2) != "undefined" && question.settings.img2 != "") {
                            noImage = _insideCDN + question.settings.img2;
                        } else {
                            noImage = _insideCDN + "images/surveys/smiley_sad.png";
                        }
                        ah += "<div class='insideSurveyQuestionImage' value='yes'><img src='" + yesImage + "' /></div><div class='insideSurveyQuestionImage' value='no'><img src='" + noImage + "' /></div>";
                    } else if (question.type == 'NPS') {
                        answerType = "nps";
                        ah += "<table style='margin: auto;'><tr>";
                        for (var j = 0; j < 11; j++) {
                            ah += "<td><input type='radio' name='insideRadioGroup_nps_" + question.id + "' value='" + j + "' name='nps'" + i + j + " id='ans_" + i + j + "'/></td>";
                        }
                        ah += "</tr><tr>";
                        for (var j = 0; j < 11; j++) {
                            ah += "<td><label for='ans_" + i + j + "'>" + j + "</label></td>";
                        }

                        ah += "</tr></table>";

                        var lowDesc = "Not at all likely";
                        var highDesc = "Extremely likely";
                        if (typeof (question.settings.lowDesc) != "undefined") {
                            lowDesc = question.settings.lowDesc;
                            highDesc = question.settings.highDesc;
                        }

                        ah += "<table style='margin: auto;'><tr><td style='text-align: left; padding-left: 10px;'>" + lowDesc + "</td><td style='text-align: right; padding-right: 10px;'>" + highDesc + "</td></tr>";
                        ah += "</table>";

                    } else {

                        ah += '<input type="radio" value="' + i + '" id="ans_' + i + '" name="ans_' + question.id + '"/>';
                        ah += '<label for="ans_' + j + '_' + i + '">' + escapeHtml(answer.text) + '</label></div>';
                    }
                }

                if (question.type == "drop down") {
                    ah += "</select>";
                } else {
                    ah += "</div>";
                }

                if (popupContainer.length == 0) {
                    setTimeout(function () {
                        showQuestion(question, popupContainer, empty);
                    }, 1000);
                    if (popupContainer.selector == "inside_liveChatTab")
                        return;
                }
                var $ah = $(ah).appendTo(popupContainer);
                $ah[0].question = question;
                $ah.find(".answer")[0].question = question;

                $ah.find(".starSelectHolder").each(function () {
                    var starSelect = new $.inside.front.starSelector({ holder: $(this) });
                    $(this)[0].starSelector = starSelect;

                })
                $ah.addClass("inside_survey_answertype_" + answerType);

                $(".insideSurveyQuestionImage img").load(function () {
                    var block = $(this).closest(".insideSurveyAnswerBlock");
                    var holder = $(block).closest("#inside_chatWindow, #inside_chatPane_custom_chatHolder");
                    holder.stop(true);
                    block[0].scrollIntoView();
                });

                $ah.click(function () {
                    //scrolls to the next question
                    var _this = this;
                    if ($(this).hasClass("inside_survey_answertype_image")) {
                        time = 500;
                    } else {
                        time = 100;
                    }
                    scrollingTimeout = setTimeout(function () {
                        var block = $(_this);
                        var holder = $(block).closest("#inside_chatWindow, #inside_chatPane_custom_chatHolder");
                        if (holder.length > 0) {
                            var beforePos = holder.scrollTop();
                            var next;

                            if (block.hasClass("inside_survey_answertype_image")) {
                                next = block.nextAll(".insideSurveyQuestion:first");
                            }
                            if (typeof (next) == "undefined" || next.length == 0) {
                                next = block;
                            }
                            next[0].scrollIntoView();
                            var pos = holder.scrollTop();
                            if (holder[0].scrollHeight - holder.innerHeight() != holder.scrollTop()) {
                                //not scrolled to the bottom
                                pos -= 25;
                            }

                            holder.scrollTop(beforePos);
                            holder.animate({ scrollTop: pos }, 500);
                        }
                    }, time);
                });

                if (typeof (question.settings.yesLink) != "undefined") {
                    $(".insideSurveyQuestionImage[value='yes']").attr("link", question.settings.yesLink);
                }
                if (typeof (question.settings.noLink) != "undefined") {
                    $(".insideSurveyQuestionImage[value='no']").attr("link", question.settings.noLink);
                }

                $(".insideSurveyQuestionImage").click(function () {
                    $(this).siblings(".insideSurveyQuestionImage").removeClass("selected");
                    $(this).addClass("selected");
                    $(this).siblings(".insideSurveyQuestionImage").fadeOut();
                });

                if ((answerType == "radio" && questionsInThisGroup <= 1)) {
                    //radio type doesn't have a submit button, unless there are multiple answers in the group
                    popupContainer.find("input[type='radio']").change(answerSelected);
                } else if (answerType == "image" && questionsInThisGroup <= 1) {
                    $(".insideSurveyQuestionImage").click(function () {
                        if ($(this).closest(".insideSurveyAnswerBlock").hasClass("answered")) {
                            //already answered
                            return;
                        }
                        selectedAnswerDiv = $(this);
                        submitAnswer(question, $(this).attr("value"));
                    });
                } else {
                    if (answerType == "drop down") {
                        popupContainer.find("select").change(function () {
                            selectedAnswerDiv = $(this).closest(".insideSurveyAnswerBlock");
                            selectedAnswerDiv.attr("link", $(this).find("option:selected").attr("link") || "");
                        });
                        popupContainer.find("select:first").change();
                    }

				popupContainer.find(".insideSurveyAnswerBlock .insideSubmitButton").remove();
				var submitText = translate("submit");
				if(survey.settings.submitButtonText && survey.settings.submitButtonText != "") {
					submitText = survey.settings.submitButtonText;
				}
				var submitButton = $("<div class='inside_submitButtonHolder' style='text-align: left;'><span class='insideSubmitButton'>" + submitText + "</span></div>").appendTo(popupContainer.find(".insideSurveyAnswerBlock:last")); 
				submitButton.find(".insideSubmitButton").click(submitButtonClick);
			}

                function answerSelected() {
                    if (answerType == "radio") {
                        var selectedAnswer = popupContainer.find(".insideSurveyAnswerBlock:not('.answered')").find("input[type='radio']:checked");
                        if (selectedAnswer.length == 0) {
                            return;
                        }
                        var id = selectedAnswer.val();
                        selectedAnswerDiv = $(".insideSurveyAnswerBlock:not(.answered) .answer[answerid='" + id + "']");
                        var answer = question.answers[id];
                        var score;
                        if (typeof (answer.score) != "undefined" && !isNaN(answer.score)) {
                            score = parseInt(answer.score);
                        }

                        submitAnswer(question, selectedAnswerDiv.text(), score);
                    } else if (answerType == "star") {
                        selectedAnswerDiv = popupContainer.find(".insideSurveyAnswerBlock:not('.answered')").find(".answer[answerid='" + id + "']");
                        var answers = [];
                        $(".insideSurveyAnswerBlock:not(.answered) .answer").each(function () {
                            question = $(this)[0].question;
                            var score = 0;
                            var answerText = $(this).find(".starSelectHolder")[0].starSelector.value();
                            if (question.type == 'star') {
                                score = parseInt(answerText);
                            }
                            if (question.type == 'yes/no image selection') {
                                score = answerText == "yes" ? 10 : -10;
                            }
                            answers.push({ surveyid: survey.id, questionid: question.id, answer: answerText, score: score });
                        });
                        submitAnswers(answers);
                    } else {
                        selectedAnswerDiv = popupContainer.find(".insideSurveyAnswerBlock:not('.answered')").find(".answer[answerid='" + 0 + "']");

                        submitAnswer(question, popupContainer.find("textarea").val());
                    }
                }

                if (typeof (callback) == "function") {
                    callback();
                }
                setTimeout($.inside.front.windowScale, 0);

                scrollQuestionIntoView($ah);

            } // end show question

            function scrollQuestionIntoView(question) {
                var block = $(question);
                var holder = $(block).closest("#inside_chatWindow, #inside_chatPane_custom_chatHolder");
                if (holder.length > 0) {
                    var beforePos = holder.scrollTop();
                    var next;

                    if (block.hasClass("inside_survey_answertype_image")) {
                        next = block.nextAll(".insideSurveyQuestion:first");
                    }
                    if (typeof (next) == "undefined" || next.length == 0) {
                        next = block;
                    }
                    next[0].scrollIntoView();
                    var pos = holder.scrollTop();
                    if (holder[0].scrollHeight - holder.innerHeight() != holder.scrollTop()) {
                        //not scrolled to the bottom
                        pos -= 25;
                    }

                    holder.scrollTop(beforePos);
                    holder.animate({ scrollTop: pos }, 500);
                }
            }

            function submitButtonClick() {
                var answers = [];
                var error = false;
                var markedAsAnswered = [];
                $(".insideSurveyAnswerBlock:not(.answered)").each(function () {

                    question = $(this)[0].question;
                    var score;
                    var answerText = "";
                    if (question.type == "yes/no image selection") {
                        answerText = $(this).find(".insideSurveyQuestionImage.selected").attr("value");
                        score = answerText == "yes" ? 10 : -10;
                    } else if (question.type == "radio") {
                        answerText = $(this).find(".answer input:checked").next("label").text();
                    } else if (question.type == "checkbox") {
                        var arr = [];
                        $(this).find(".answer input:checked").each(function () {
                            arr.push($(this).next("label").text());
                        });
                        answerText = arr.join(", ");
                    } else if (question.type == "star rating") {
                        answerText = $(this).find(".starSelectHolder")[0].starSelector.value();
                        score = parseInt(answerText);
                    } else if (question.type == "NPS") {
                        answerText = $(this).find("input[type='radio']:checked").val();
                        score = parseInt(answerText);
                    } else if (question.type == "drop down") {
                        answerText = $(this).find("select").val();
                    } else {
                        answerText = $(this).find("textarea").val();
                    }
                    if (typeof (answerText) == "undefined" || answerText == null || answerText == "" && question.type != "free text") {
                        if (!error) {
                            alert(translate("Please select an answer to all questions"));
                        }
                        $(this).addClass("error");
                        var $this = $(this);
                        setTimeout(function () {
                            $this.removeClass("error");
                        }, 1000);
                        error = true;
                        for (var i = 0; i < markedAsAnswered.length; i++) {
                            $(this).removeClass("answered");
                        }
                        return;
                    }
                    $(this).addClass("answered");
                    markedAsAnswered.push(this);
                    var answer = { surveyid: survey.id, questionid: question.id, answer: answerText };
                    if (score != null) {
                        answer.score = score;
                    }
                    answers.push(answer);
                });
                if (error) {
                    return;
                }
                submitAnswers(answers);
                $(this).css("opacity", "0");
            }

            function submitAnswer(question, answerText, _score) {
                $(".insideSurveyAnswerBlock:not(.answered)").addClass("answered");

                var score;
                if (question.type == 'star') {
                    score = parseInt(answerText);
                }
                if (question.type == 'yes/no image selection') {
                    score = answerText == "yes" ? 10 : -10;
                }
                if (typeof (_score) != "undefined" && !isNaN(_score)) {
                    score = _score;
                }
                var answerData = { surveyid: survey.id, questionid: question.id, answer: answerText, score: score };
                if (typeof (surveyData.chatid) != "undefined") {
                    answerData.chatid = surveyData.chatid;
                }
                insideAPI.call("AnswerSurveyQuestion", answerData, answerSubmitted);

                if (typeof (surveyData) != "undefined" && surveyData.stickyid != "undefined" && surveyData.stickyid > 0) {
                    //unstick using the stickyid
                    $.inside.server.unstick(data.stickyid);
                }
            }

            function submitAnswers(answers) {
                $(".insideSurveyAnswerBlock:not(.answered)").addClass("answered");

                var answerData = { answers: answers };
                if (typeof (surveyData.chatid) != "undefined") {
                    answerData.chatid = surveyData.chatid;
                }
                insideAPI.call("AnswerSurveyQuestions", answerData, answersSubmitted);

			if(typeof(surveyData) != "undefined" && surveyData.stickyid != "undefined" && surveyData.stickyid > 0){
				//unstick using the stickyid
				$.inside.server.unstick(data.stickyid);
			}
		}
		function answersSubmitted(){
			//check linking
			var linking;
			$(".answer:not(.completed) > input:checked, .answer:not(.completed) > .insideSurveyQuestionImage.selected").each(function(){
				if($(this).hasClass("completed")){
					return;
				}
				var answer = $(this).closest(".answer");
				console.log(answer.attr("link"));
				if(typeof answer.attr("link") != "undefined" && answer.attr("link") != ""){
					//link
					linking = answer.attr("link");
				}
				if($(this).attr("class") && $(this).attr("class").indexOf("insideSurveyQuestionImage") == 0) {
					linking = $(this).attr("link");
				}
			});
			$(".answer").addClass("completed");
			if(linking != null){
				showSurvey(linking, popupContainer.attr("id"), callback, surveyData);
			} else {
				answerSubmitted();
			}
		}

		function answerSubmitted(response){
			$(".answer").addClass("completed");
			if(typeof selectedAnswerDiv != "undefined" && selectedAnswerDiv != null && typeof(selectedAnswerDiv.attr("link")) != "undefined" && selectedAnswerDiv.attr("link") != ""){
				//console.log(selectedAnswerDiv.attr("link"));
				//there is another survey to show.
				showSurvey(selectedAnswerDiv.attr("link"), popupContainer.attr("id"), callback, surveyData);
			} else {
				if(currentQuestion >= survey.questions.length-1) {
					
					surveyComplete();

                        //popupContainer.closest(".inside_visitorNotify").removeClass("survey");
                        if (typeof (callback) == "function") {
                            callback();
                        }
                    } else {
                        //show the next question in the survey.
                        nextQuestion();

                    }
                }
            }

            function scrollToTopOfSurvey() {
                var container = popupContainer.closest(".inside_visitorNotify");
                //var scroller = container.parent();
                //scroller.prop("scrollTop", container.position().top + scroller.scrollTop());
                container[0].scrollIntoView();
            }

        }

        var surveyData;
        insideSurvey.showSurvey = showSurvey;
        function showSurvey(surveyID, popupContainerid, callback, data) {
            surveyData = data;
            if (typeof (callback) == "undefined") {
                callback = null;
            }
            container = $("#" + popupContainerid);
            if (container.length == 0) {
                return; //container not found
            }
            if (surveyData.type != "chatsurvey") {
                container.html('');
            }
            loadSurvey(surveyID, container, callback, data);
        }

        function translate(str) {
            if (insideChat && insideChat.translate) {
                return insideChat.translate(str);
            } else {
                return str;
            }
        }

        var replaceHtmlEntities = (function () {
            var translate_re = /&(amp);/g,
                translate = {
                    //'nbsp': String.fromCharCode(160), 
                    'amp': '&',
                    //'quot': '"',
                    // 'lt'  : '<', 
                    // 'gt'  : '>'
                },
                translator = function ($0, $1) {
                    return translate[$1];
                };

            return function (s) {
                return s.replace(translate_re, translator);
            };
        })();

        //usage example
        //insideSurvey.showSurvey(2, 'popupContainer');

    })(window, isLocal() ? $ : _insideGraph.jQuery);
});


/* path = js/inside.front.cobrowse.js */

var insideCoBrowse = null;
_insideGraph.registerModule("cobrowse", function () {

    console.log("INSIDE Module 'cobrowse' started");

    insideCoBrowse = new function () {
        var InsideNodeIds = ["inside_cobrowse", "inside_holder", "inside_offerLargeWindow"];
        var $ = _insideGraph.jQuery;
        var $$ = function (id) { return document.getElementById(id); }
        var $insideHolder = null;

    var frontEnd = _insideGraph.jQuery.inside.front;
    
    var inProgressText = frontEnd.settingsData.cobrowse && frontEnd.settingsData.cobrowse.inProgressText 
        ? frontEnd.settingsData.cobrowse.inProgressText 
        : "Co-Browse in progress...";

    var closeButtonText = frontEnd.settingsData.cobrowse && frontEnd.settingsData.cobrowse.closeButtonText
        ? frontEnd.settingsData.cobrowse && frontEnd.settingsData.cobrowse.closeButtonText
        : "Close";

    var passwordFilters = [];
    if (frontEnd.settingsData.cobrowse && frontEnd.settingsData.cobrowse.passwordFilters) {
        var tmp = frontEnd.settingsData.cobrowse.passwordFilters.split(/\r?\n/);
        for (var i = 0; i < tmp.length; i++) {
            passwordFilters[i] = tmp[i].trim();
        }
    }

    var securityFilters = [];
    if (frontEnd.settingsData.cobrowse && frontEnd.settingsData.cobrowse.securityFilters) {
        var tmp = frontEnd.settingsData.cobrowse.securityFilters.split(/\r?\n/);
        for (var i = 0; i < tmp.length; i++) {
            securityFilters[i] = tmp[i].trim();
        }
    }
    
    var animationFilters = [];
    if (frontEnd.settingsData.cobrowse && frontEnd.settingsData.cobrowse.animationFilters) {
        var tmp = frontEnd.settingsData.cobrowse.animationFilters.split(/\r?\n/);
        for (var i = 0; i < tmp.length; i++) {
            animationFilters[i] = tmp[i].trim();
        }
    }

        function isBlockedByFilter(filter, node) {
            if (!filter) { return false; }
            if (!node || !node.id) { return false; }
            for (var i = 0; i < filter.length; i++) {
                var sel = filter[i];
                if (!sel) { continue; }
                var el = $(sel).get(0);
                if (!el) { return false; }
                if (el.id && node.id && el.id === node.id) { return true; }
                if (el.contains && el.contains(node)) { return true; }
            }
            return false;
        }

        var hostName = window.location.hostname;
        var siteNameFragments = hostName.split(".");
        var siteName = siteNameFragments[1];
        var domain = siteNameFragments.slice(1, siteNameFragments.length).join(".");
        var cbCookieAgent = "Inside_cobrowse_agent_" + siteName;
        var cbCookieActiveTab = "Inside_cobrowse_" + siteName;
        var agentName = getCookie(cbCookieAgent);
        var proxyEndpoint = _insideSocialUrl + "api/utilities/proxy?url=";
        var cssUrlRegex = /url\((["'])([^\s]+)(["'])\)/gi;
        var aLink = document.createElement("a");

        if (!String.prototype.startsWith) {
            String.prototype.startsWith = function (searchString, position) {
                if (searchString.length > this.length)
                    return false;
                for (var j = 0; j < searchString.length; j++) {
                    if (searchString.charAt(j) != this.charAt(j))
                        return false;
                }
                return true;
            };
        }

        var insideSync = new InsideObserver();

        function init() {
            $.inside.bind("coBrowse", CommandHandler);
        }
        $(document).ready(init);

        function Load() {
            log("Cobrowse script loaded ...")
            checkOnceForTabActivation();
        }

        function Open() {
            log("Cobrowse opened ...");
            if (isActiveTab) {
                startCoBrowse();
            }
        }

        frontEnd.startCoBrowse = startCoBrowse;
        frontEnd.Load = Load;
        frontEnd.Open = Open;

        var tabChangeInterval;
        var isActiveTab = false;

        // user can have multiple chats opens
        // detect current active chat tab
        function initIntervalCheck() {
            if (!tabChangeInterval) {
                tabChangeInterval = setInterval(function () {
                    isActiveTab = IsActiveTab();
                    if (isActiveTab && !started)
                        startCoBrowse();
                    if (!isActiveTab && started) {
                        stopCoBrowse();
                        checkOnceForTabActivation();
                    }

                }, 500);
            }
        }

        function checkOnceForTabActivation() {
            $(window).off('focus.inside-active-tab');
            // focus alone is not very reliable
            $(document).off('mousemove.inside-active-tab');
            $(document).one('mousemove.inside-active-tab', function () {
                log("mousemove activated tab");
                setActiveTab();
                initIntervalCheck();
            });
            $(window).one('focus.inside-active-tab', function () {
                log("focus activated tab");
                setActiveTab();
                initIntervalCheck();
            });
        }


        function setActiveTab() {
            setCookie(cbCookieActiveTab, $.connection.hub.id)
        }

        function IsActiveTab() {
            var cookieVal = getCookie(cbCookieActiveTab);
            return cookieVal == $.connection.hub.id;
        }

        var started = false;
        function startCoBrowse() {
            started = true;
            $(".inside_cobrowseRequest").parent().remove(); // remove cobrowse accept/reject buttons from the users chat window
            log("SYNC");
            renderCobrowseElements();
            insideSync.sync();
        }

        // called when switching tabs
        function stopCoBrowse() {
            started = false;
            log("STOP");
            cleanCobrowseElements();
            insideSync.stop();
        }

        $.connection.hub.disconnected(function () {
            log("disconnected ...")
            started = false;
            cleanCobrowseElements();
            insideSync.stop();
        });

        // called when user clicks the close cobrowse button or the session expires
        function disposeCobrowse() {
            cleanCobrowseElements();
            insideSync.dispose();
            if (tabChangeInterval)
                clearInterval(tabChangeInterval);
            tabChangeInterval = null;

            $(window).off("focus.inside-active-tab");
            $(document).off("mousemove.inside-active-tab");

            $(window).off("mousemove.inside-render-elements");
            $(window).off("mouseup.inside-render-elements");
            $(window).off("resize.inside-render-elements");
            $(window).off("scroll.inside-render-elements");

            $(window).off("mousemove.inside");
            $(window).off("mouseup.inside");
            $(window).off("resize.inside");
            $(window).off("scroll.inside");
            $(document).off("change.inside");
            $(document).off("keyup.inside");
            $("body").off("click.inside");

            var page = $("html, body");
            page.off("scroll.inside-highlight mousedown.inside-highlight wheel.inside-highlight DOMMouseScroll.inside-highlight mousewheel.inside-highlight touchmove.inside-highlight");
            page.off("scroll.inside-rectangle mousedown.inside-rectangle wheel.inside-rectangle DOMMouseScroll.inside-rectangle mousewheel.inside-rectangle touchmove.inside-rectangle");

            started = false;
            log("Disposed ...");
        }


        //var itv, itv1;

        //function pingForActiveTab() {
        //    itv = setInterval(function() {
        //        console.log("hub stopped")
        //        $.connection.hub.stop(); 
        //    }, 5000);

        //     itv1 = setInterval(function() {
        //         console.log("hub started")
        //        $.connection.hub.start(); 
        //    }, 6000);


        //}
        //pingForActiveTab();

        function CommandHandler(data) {
            switch (data) {
                case "/dispose":
                    // operator closes cobrowse session
                    disposeCobrowse();
                    return;
            }

            if (!isActiveTab)
                return;

            if (typeof data.type != "undefined") {
                handleOperatorCommands(data);
                return;
            }

            if (typeof data.type == "undefined") {
                log("command = " + data)
            }

            switch (data) {
                case "/session_disconnected":
                    stopCoBrowse();
                    break;
                case "/session_connected":
                    if (!started)
                        startCoBrowse();
                    break;
                case "/operator_refresh":
                    insideSync.sync();
                    break;
                case "/operator_leave":
                    // operator switches to another user
                    // todo: impl pending
                    stopCoBrowse();
                    break;
                default:
            }
        }

        function handleOperatorCommands(data) {
            var cb = $("#inside_cobrowse");
            if (cb.length == 0)
                $("#inside_holder").append("<div id='inside_cobrowse' ></div>");
            $insideHolder = $("#inside_cobrowse");

            if (data.type == "highlight" || data.type == "rectangle")
                handleOperatorSelection(data);
            else
                handleOperatorControl(data)
        }
        function handleOperatorSelection(data) {
            switch (data.type) {

                case "highlight":
                    var ele = $("#" + data.id);
                    if (ele.length > 0) {
                        insideSync.disableScrollEvent();
                        $('html, body').stop(true, true);
                        $insideHolder.find("#inside_cobrowse_rectangle").remove();
                        var time = Math.abs($("#" + data.id).offset().top - $(window).scrollTop());
                        if (time > 1200)
                            time = 1200;

                        var page = $('html, body');
                        // if user scrolls remove highlight
                        page.on("scroll.inside-highlight mousedown.inside-highlight wheel.inside-highlight DOMMouseScroll.inside-highlight mousewheel.inside-highlight touchmove.inside-highlight", function () {
                            page.stop();
                            var rect = $insideHolder.find("#inside_cobrowse_rectangle");
                            if (rect.length > 0) {
                                rect.remove();
                            }
                        });

                        page.animate({
                            scrollTop: ele.offset().top - 100
                        }, time, function () {
                            var left = $("#" + data.id).offset().left;
                            var top = $("#" + data.id).offset().top - $(window).scrollTop();
                            var width = ele.width();
                            var height = ele.height();

                            $insideHolder.append("<div id='inside_cobrowse_rectangle' ></div>");
                            var rect = $insideHolder.find("#inside_cobrowse_rectangle");
                            rect.css({
                                position: "absolute",
                                top: top,
                                left: left,
                                height: height,
                                width: width,
                                background: "orange",
                                opacity: 0.3,
                                zIndex: 100000,
                                pointerEvents: "none",
                                border: "3px solid red",
                                borderRadius: "2px"
                            });
                            rect.fadeOut(150).fadeIn(150).fadeOut(150).fadeIn(150).fadeOut(150).fadeIn(150).delay(5000).fadeOut(500, function () {
                                $(this).remove();
                            });
                            insideSync.enableScrollEvent();
                        });
                    }

                    break;

                case "rectangle":
                    $insideHolder.find("#inside_cobrowse_rectangle").remove();
                    var ele1 = $("#" + data.id1);
                    var ele2 = $("#" + data.id2);
                    if (ele1.length > 0 && ele2.length > 0) {
                        insideSync.disableScrollEvent();
                        $('html, body').stop(true, true);
                        $insideHolder.find("#inside_cobrowse_rectangle").remove();

                        var ofset1 = ele1.offset();
                        var top = ofset1.top + data.t1;
                        var ofset2 = ele2.offset();
                        var top1 = ofset2.top + data.t2;

                        var top = top < top1 ? top : top1;

                        var time = Math.abs(top - $(window).scrollTop());
                        if (time > 1200)
                            time = 1200;
                        var page = $('html, body');
                        page.on("scroll.inside-rectangle mousedown.inside-rectangle wheel.inside-rectangle DOMMouseScroll.inside-rectangle mousewheel.inside-rectangle touchmove.inside-rectangle", function () {
                            page.stop();
                            var rect = $insideHolder.find("#inside_cobrowse_rectangle");
                            if (rect.length > 0) {
                                rect.remove();
                            }
                        });
                        page.animate({
                            scrollTop: top - 100
                        }, time, function () {
                            var scrollTop = $(window).scrollTop();
                            var ofset1 = ele1.offset();
                            var left = ofset1.left + data.l1;
                            var top = ele1.offset().top - scrollTop + data.t1;

                            var ofset2 = ele2.offset();
                            var left1 = ofset2.left + data.l2;
                            var top1 = ele2.offset().top - scrollTop + data.t2;

                            var hi = Math.abs(top1 - top);
                            var width = Math.abs(left1 - left);

                            left = left < left1 ? left : left1;
                            top = top < top1 ? top : top1;
                            $insideHolder.append("<div id='inside_cobrowse_rectangle' ></div>");
                            var rect = $insideHolder.find("#inside_cobrowse_rectangle");
                            rect.css({
                                position: "absolute",
                                top: top,
                                left: left,
                                height: hi,
                                width: width,
                                background:
                                    "orange",
                                opacity: 0.3,
                                border: "3px solid red",
                                pointerEvents: "none",
                                borderRadius: "2px",
                                zIndex: 100000
                            });
                            rect.fadeOut(150).fadeIn(150).fadeOut(150).fadeIn(150).fadeOut(150).fadeIn(150).delay(5000).fadeOut(500, function () {
                                $(this).remove();
                            });
                            insideSync.enableScrollEvent();
                        });
                    }
                    break;
            }
        }

        function handleOperatorControl(data) {
            if (!data || !data.type) { return; }

            switch (data.type) {
                case "mouseup":
                case "mousedown":
                case "mousemove":
                case "mouseover":
                case "mouseout":
                case "mouseenter":
                case "mouseleave":
                case "click":
                case "dblclick":
                    var target = $$(data.targetId);
                    if (!target) { return; }
                    var relatedTarget = $$(data.relatedTargetId);
                    var offset = $(target).offset();
                    var clientX = offset.left + data.offsetX;
                    var clientY = offset.top + data.offsetY;
                    var event = new MouseEvent(data.type, {
                        'bubbles': true,
                        'cancelable': true,
                        'target': target,
                        'relatedTarget': relatedTarget,
                        'clientX': clientX,
                        'clientY': clientY
                    });
                    event.isInsideEvent = true;
                    target.dispatchEvent(event);
                    if (data.type == "mousemove") {
                        $insideHolder.find('#inside_cursor')
                            .stop(true, false)
                            .css({ "top": clientY + "px", "left": clientX + "px" })
                            .animate({ opacity: 1 }, 200, "easeOutExpo")
                            .animate({ opacity: 1 }, 7000)
                            .animate({ opacity: 0 }, 1000, "easeInQuint");
                    }
                    if (data.type == "click") {
                        $insideHolder.find("#inside_click_marker")
                            .css({ "top": (clientY - 15) + "px", "left": (clientX - 15) + "px" })
                            .show()
                            .delay(500)
                            .fadeOut(200);
                    }
                    break;

                case "dragdrop":
                    var dataTransfer = new DataTransfer();

                    var dragTarget = $$(data.dragTargetId);
                    if (!dragTarget) { return; }
                    var offset = $(dragTarget).offset();
                    var dragEvent = new CustomEvent("dragstart", { bubbles: true, cancelable: true });
                    dragEvent.target = dragTarget;
                    dragEvent.pageX = offset.left + data.dragOffsetX;
                    dragEvent.pageY = offset.top + data.dragOffsetY;
                    dragEvent.dataTransfer = dataTransfer;
                    dragEvent.isInsideEvent = true;
                    dragTarget.dispatchEvent(dragEvent);

                    var dropTarget = $$(data.dropTargetId);
                    if (!dropTarget) { return; }
                    var offset = $(dropTarget).offset();
                    var dropEvent = new CustomEvent("drop", { bubbles: true, cancelable: true });
                    dropEvent.target = dropTarget;
                    dropEvent.pageX = offset.left + data.dropOffsetX;
                    dropEvent.pageY = offset.top + data.dropOffsetY;
                    dropEvent.dataTransfer = dataTransfer;
                    dropEvent.isInsideEvent = true;
                    dropTarget.dispatchEvent(dropEvent);
                    break;

                case "input":
                    var target = $$(data.targetId);
                    if (!target) { return; }
                    if (data.type === "input") { 
                        if(target.type == "input" || target.type == "textarea" || target.type == "text") {
                            target.value = data.value;
                        } else {
                            $(target).html(data.value).change();
                            //staples specific code
                            $(".phx-rte-tool-group-item.active").click();
                            
                        }
                    }
                    else { target.innerHTML = data.innerHTML; }
                    break;

                case "selection":
                    var startElement = $$(data.startElementId);
                    if (!startElement) { return; }
                    if (!(data.startNodeOffset >= 0 && data.startNodeOffset < startElement.childNodes.length)) { return; }
                    var startNode = startElement.childNodes[data.startNodeOffset];
                    if (!startNode) { return; }
                    var startOffset = data.startOffset;

                    var endElement = $$(data.endElementId);
                    if (!endElement) { return; }
                    if (!(data.endNodeOffset >= 0 && data.endNodeOffset < endElement.childNodes.length)) { return; }
                    var endNode = endElement.childNodes[data.endNodeOffset];
                    if (!endNode) { return; }
                    var endOffset = data.endOffset;

                    try {
                        var range = document.createRange();
                        range.setStart(startNode, startOffset);
                        range.setEnd(endNode, endOffset);

                        var sel = window.getSelection();
                        sel.removeAllRanges();
                        sel.addRange(range);
                    } catch (ex) {
                        return;
                    }
                    break;
                case "scroll":
                    if (data.scrollTop >= 0 && data.scrollLeft >= 0) {
                        insideSync.disableScrollEvent();
                        $(window).scrollTop(data.scrollTop);
                        $(window).scrollLeft(data.scrollLeft);
                        setTimeout(function() { insideSync.enableScrollEvent(); }, 250);
                    }
                    break;
            }
        }

        // highlight page border to indicate active cobrowse session
        function renderCobrowseElements() {
            var cb = $("#inside_cobrowse");
            if (cb.length == 0)
                $("#inside_holder").append("<div id='inside_cobrowse' ></div>");
            else
                return;
            $insideHolder = $("#inside_cobrowse");

            $("#inside_cobrowse_active").remove();
            $("#inside_cobrowse_outline").remove();

        var cbActiveDiv = "<div id='inside_cobrowse_active'>" +
            "<div class='cbactivetext' >" + inProgressText + "</div>&nbsp;&nbsp;" +
            "<div class='cbclosebutton'>" + closeButtonText + "</div>" +
            "<div id='cbUserName' style='display:none'>" + agentName + "</div>" +
            "</div>";
        var bodyOutline = '<div id="inside_cobrowse_outline"></div>';
        $insideHolder.append(cbActiveDiv);
        $insideHolder.append(bodyOutline);
        $insideHolder.append("<div id='inside_click_marker' style='position: absolute; top: 0px; left: 0px; width: 20px; height: 20px; background: #1ab9d8; border-radius: 50%; opacity: 0.4; z-index: 1000000; display: none;'></div>");
        $insideHolder.append("<div id='inside_cursor' style='position: absolute; top: 0px; left: 0px; width: 50px; height: 50px; background-image: url(" + _insideCDN + "/images/cbmouse.png); background-repeat:no-repeat; opacity: 0; z-index: 1000000;'></div>");

            $(".cbclosebutton").click(function () {
                $.inside.server.closeCoBrowseSession().done(function () {
                    disposeCobrowse();
                });
            });

            var cbdiv = $("#inside_cobrowse_active");
            var cboutline = $("#inside_cobrowse_outline");

            var top = 0;
            var left = 0;

            var viewport = $.inside.front.getViewPortSize();

            var w = viewport.width;
            var h = viewport.height;

            var mid = viewport.width / 2;
            var winw = viewport.height / 2;

            cboutline.css({ top: top, left: left, width: w, height: h })

            var cbactive = $("#inside_cobrowse_active");
            var mouseDown = false;
            var cwidth = 0;
            var docWidth;
            cbactive.mousedown(function (event) {
                mouseDown = true;
                var cba = $("#inside_cobrowse_active");
                var divleft = cba.offset().left;
                docWidth = $(window).width();

                cwidth = event.pageX - divleft;
                if (event.stopPropagation) event.stopPropagation();
                if (event.preventDefault) event.preventDefault();
                event.cancelBubble = true;
                event.returnValue = false;
                return false;
            });
            cbactive.mousemove(function (event) {
                if (mouseDown) {
                    var cba = $("#inside_cobrowse_active");
                    var left = event.pageX - cwidth;
                    cba.css({ left: left });
                }
            });
            cbactive.mouseup(function (event) {
                mouseDown = false;
            });

            $(window).on("mousemove.inside-render-elements", function (event) {
                if (mouseDown) {
                    var cba = $("#inside_cobrowse_active");
                    var left = event.pageX - cwidth;

                    var cbLeft = cba.offset().left;
                    var cbwidth = cba.width();
                    var tot = cbLeft + cbwidth + 10;
                    if (tot > docWidth - 20) {
                        cba.css({ left: docWidth - cbwidth - 20 });
                        return;
                    }

                    if (left < 10)
                        left = 10;
                    cba.css({ left: left });
                }
            });

            $(window).on("mouseup.inside-render-elements", function (event) {
                mouseDown = false;
            });

            $(window).off("scroll.inside-render-elements");
            $(window).off("resize.inside-render-elements");
            $(window).on("resize.inside-render-elements", cobrowseActiveResize);
        }

        function cleanCobrowseElements() {
            $(window).off("scroll.inside-render-elements");
            $(window).off("resize.inside-render-elements");
            $(window).off("mouseup.inside-render-elements");
            $(window).off("mousemove.inside-render-elements");
            $("#inside_cobrowse").remove();
        }


        function cobrowseActiveResize() {
            var cbdiv = $("#inside_cobrowse_active");
            var cboutline = $("#inside_cobrowse_outline");

            var viewport = $.inside.front.getViewPortSize();

            var top = 0;
            var left = 0;

            var w = viewport.width;
            var h = viewport.height;

            cboutline.css({ top: top, left: left, width: w, height: h })
        };

        this.send = sendDataToOperatorTab;
        function sendDataToOperatorTab(data) {
            data.userid = insideChat.userid;
            $.inside.server.updateCoBrowseSession(data);
        }

        function InsideTimer() {
            this.interval = null;
            this.scrollHandler = null;

            this.start = function () {
                if (this.interval) {
                    clearInterval(this.interval);
                    this.interval = null;
                }

                var mousePos = {};
                var selectedNodes;
                var clickPos = {};
                var scrollPos = {};
                var windowSize = { width: 0, height: 0 };
                var keyPress = {};

                var selected = false;
                var clicked = false;
                var keyPressed = false;
                var mouseMoved = false;
                var scrolled = false;
                var resized = false;

                var _this = this;

                $(window).on('resize.inside', function (e) {
                    if (e && e.originalEvent && e.originalEvent.isInsideEvent) { return; }

                    // take scrollbar size into account
                    windowSize = { width: window.innerWidth, height: window.innerHeight };
                    resized = true;
                });

                this.scrollHandler = function (e) {
                    if (e && e.originalEvent && e.originalEvent.isInsideEvent) { return; }

                    scrollPos = { top: window.scrollY, left: window.scrollX };
                    scrolled = true;
                };

                $(window).on('scroll.inside', this.scrollHandler);

                $(window).on('mousemove', function (e) {
                    if (e && e.originalEvent && e.originalEvent.isInsideEvent) { return; }

                    var offset = $(e.target).offset();
                    mousePos = { id: e.target.id, left: e.clientX, top: e.clientY + $(window).scrollTop()};
                    
                    mouseMoved = true;
                });

                $(window).on('mouseup.inside', function (e) {
                    if (e && e.originalEvent && e.originalEvent.isInsideEvent) { return; }

                    var selectedText = (document.all) ? document.selection.createRange().text : document.getSelection();
                    if (selectedText != "") {
                        selected = true;
                        selectedNodes = _this.getSelectedNodes();
                    }

                });

                $(document).on('keyup.inside', function (e) {
                    if (e && e.originalEvent && e.originalEvent.isInsideEvent) { return; }

                    var node = e.target;
                    if (node.nodeType == 1 && (node.nodeName == "INPUT" || node.nodeName == "TEXTAREA")) {
                        var nodeName = $(node).attr("name");

                    if (passwordFilters.length > 0 && ((node.id && passwordFilters.indexOf(node.id) != -1) || (nodeName && passwordFilters.indexOf(nodeName) != -1))) {
                        var str = "";
                        for (var i = 0; i < node.value.length; i++) {
                            str += "*";
                        }
                        keyPress = { id: node.id, val: str };
                    } else {
                        keyPress = { id: node.id, val: node.value };
                    }
                    keyPressed = true;
                }
            });

                $(document).on('change.inside', function (e) {
                    if (e && e.originalEvent && e.originalEvent.isInsideEvent) { return; }

                    var changes = [];

                $(":input").each(function() {
                    var node = this;
                    var nodeName = $(node).attr("name");
                    var data;
                    
                    if (passwordFilters.length > 0 && ((node.id && passwordFilters.indexOf(node.id) != -1) || (nodeName && passwordFilters.indexOf(nodeName) != -1))) {
                        var str = "";
                        for (var i = 0; i < node.value.length; i++) {
                            str += "*";
                        }
                        data = { change: { id: node.id, value: str } };
                        changes.push(data);
                        return;
                    }

                        switch (node.type) {
                            case "checkbox":
                            case "radio":
                                data = { change: { id: node.id, value: node.value, checked: $(node).is(':checked') } };
                                changes.push(data);
                                break;
                            default:
                                data = { change: { id: node.id, value: node.value } };
                                changes.push(data);
                                break;
                        }
                    });

                    if (changes.length > 0)
                        insideCoBrowse.send({ changes: changes });
                });

                $("body").on('click.inside', function (e) {
                    if (e && e.originalEvent && e.originalEvent.isInsideEvent) { return; }

                    var node = e.target;
                    if (node.nodeType == 1) {
                        var offset = $(e.target).offset();
                        clickPos = { id: e.target.id, left: e.pageX - offset.left, top: e.pageY - offset.top };
                        if(e.clientX)
                            clickPos = { id: e.target.id, left: e.clientX, top: e.clientY + $(window).scrollTop()};
                        clicked = true;
                    }

                });

                this.interval = setInterval(tick, 300);

                function tick() {
                    var data = {};

                    var stateChanged = selected || clicked || keyPressed || mouseMoved || scrolled || resized;

                    if (scrolled) {
                        data.scrollTop = scrollPos.top;
                        data.scrollLeft = scrollPos.left;
                    }

                    if (resized) {
                        data.width = windowSize.width;
                        data.height = windowSize.height;
                    }

                    if (selected) {
                        data.highlightData = selectedNodes;
                    }
                    if (clicked) {
                        data.clickPos = clickPos;
                    }
                    if (mouseMoved) {
                        data.mousePos = mousePos;
                    }

                    if (keyPressed) {
                        data.keyPress = keyPress;
                    }

                    if (stateChanged) {
                        insideCoBrowse.send(data)
                    }

                    //reset the state
                    stateChanged = selected = clicked = keyPressed = mouseMoved = scrolled = resized = false;
                }
            }
            this.stop = function () {
                if (this.interval) {
                    clearInterval(this.interval);
                    this.interval = null;
                }

                $(document).off("change.inside");
                $(window).off("mousemove.inside");
                $(window).off("mouseup.inside");
                $("body").off("click.inside");
                $(document).off("keyup.inside");
                $(window).off("scroll.inside");
                $(window).off("resize.inside");

            }
            this.disableScrollEvent = function () {
                $(window).off("scroll.inside", this.scrollHandler);
            }
            this.enableScrollEvent = function () {
                $(window).on("scroll.inside", this.scrollHandler);
            }

            this.getSelectedNodes = function () {

                function nextNode(node) {
                    if (node.hasChildNodes())
                        return node.firstChild;
                    else {
                        while (node && !node.nextSibling) {
                            node = node.parentNode;
                        }
                        if (!node) return null;

                        return node.nextSibling;
                    }
                }

                function getRangeSelectedNodes(range) {
                    var node = range.startContainer;
                    var endNode = range.endContainer;

                    // Special case for a range that is contained within a single node
                    if (node == endNode) {
                        return [node];
                    }

                    // Iterate nodes until we hit the end container
                    var rangeNodes = [];
                    while (node && node != endNode) {
                        rangeNodes.push(node = nextNode(node));
                    }

                    // Add partially selected nodes at the start of the range
                    node = range.startContainer;
                    while (node && node != range.commonAncestorContainer) {
                        rangeNodes.unshift(node);
                        node = node.parentNode;
                    }

                    return rangeNodes;
                }

                function getNodes() {
                    if (window.getSelection) {
                        var sel = window.getSelection();
                        if (!sel.isCollapsed) {
                            return getRangeSelectedNodes(sel.getRangeAt(0));
                        }
                    }
                    return [];
                }

                var ret = [];
                var nodes = getNodes();

                var selection = window.getSelection();
                var selectedStrings = selection.toString().split("\n").filter(Boolean)


                var j = 0
                for (var i = 0; i < nodes.length; i++) {
                    var node = nodes[i];
                    if (node.nodeType == 3 && node.nodeValue.trim() != "") {
                        node = node.parentNode
                    } else
                        continue;


                    var selectedString = selectedStrings[j];
                    j++;
                    if (typeof selectedString == "undefined") break;
                    ret.push({ id: node.id, value: selectedString.trim() })

                }
                return ret;
            }
        }

        function GetAbsoluteUrl(url) {
            aLink.href = url;
            return aLink.href;
        }

        function GetProxyUrl(url) {
            var isAbsoluteUrl = url.indexOf("http") == 0;
            var absUrl = !isAbsoluteUrl ? GetAbsoluteUrl(url) : url;
            return proxyEndpoint + absUrl;
        }

        function GetStyleWithProxyUrls(style) {
            return style.replace(cssUrlRegex, function (fullmatch, quoteStart, url, quoteEnd) {
                return "url(" + quoteStart + GetProxyUrl(url) + quoteEnd + ")";
            });
        }

        var DOMIDManager = new function () {
            this.id = 0;
            this.getNextId = function () {
                return this.id++;
            }
        }

        var DOM = new function () {
            this.GetBodyHtml = function (node) {
                return GetBodyNode(node, false)
            }
            this.GetHtmltoSend = function () {
                var doctypeNode = document.doctype;
                var doctypeString = "<!DOCTYPE HTML>";
                if (typeof doctypeNode.name != "undefined") {
                    doctypeString = "<!DOCTYPE "
                        + doctypeNode.name
                        + (doctypeNode.publicId ? ' PUBLIC "' + doctypeNode.publicId + '"' : '')
                        + (!doctypeNode.publicId && doctypeNode.systemId ? ' SYSTEM' : '')
                        + (doctypeNode.systemId ? ' "' + doctypeNode.systemId + '"' : '')
                        + '>';
                }

                var bases = document.getElementsByTagName('base');
                var baseHref = "";
                if (bases.length > 0) {
                    baseHref = bases[0].href;
                }
                if (baseHref == "") {
                    var a = document.createElement('a');
                    a.href = document.location.href;
                    baseHref = document.location.protocol + "//" + a.host;
                }

                var cleanHtml = GetFullHtml();

                var htmlDetails = {
                    protocol: document.location.protocol,
                    url: location.href,
                    baseurl: baseHref,
                    html: doctypeString + cleanHtml,
                    top: $(window).scrollTop(),
                    left: $(window).scrollLeft(),
                    initialWidth: $(window).width(),
                    initialHeight: $(window).height()
                };

                return htmlDetails;
            }

            function GetFullHtml() {
                var head = GetHeadNode(document.head);
                var body = GetBodyNode(document.body, true)
                return "<html " + GetAttributeString(document.documentElement) + ">" + head + body + "</html>";
            }

            function GetHeadNode(node) {
                var HTML = "";
                if (node.nodeType == 1) {
                    switch (node.tagName) {
                        case "SCRIPT":
                            return null;
                        case "LINK":
                            var linkHref = node.getAttribute("href")
                            if (linkHref && (linkHref.indexOf("inside.front.css") != -1 ||
                                linkHref.indexOf("icoinside-front.css") != -1 ||
                                linkHref.indexOf("inside.chat.css") != -1))
                                return null;
                            break;
                    }

                    if (isBlockedByFilter(securityFilters, node)) {
                        return null;
                    }

                    HTML += "<" + node.tagName;

                    HTML += GetHeadNodeAttributeString(node);

                    // void element cannot have closing tag
                    if (node.tagName == "IMG" || node.tagName == "BR" ||
                        node.tagName == "LINK" || node.tagName == "META" || node.tagName == "BASE") {
                        HTML += "/>"
                        return HTML;
                    } else
                        HTML += ">";

                    for (var j = 0; j < node.childNodes.length; j++) {
                        var childNode = node.childNodes[j];
                        var childHtml = GetHeadNode(childNode)
                        if (childHtml)
                            HTML += childHtml;
                    }

                    HTML += "</" + node.tagName + ">";
                } else if (node.nodeType == 3) {
                    HTML += node.nodeValue;
                }

                return HTML;
            }

            function GetBodyNode(node, setRefId) {
                var HTML = "";
                if (node.nodeType == 1) {
                    if (!node.isIdSet) {
                        if (!node.hasAttribute("id")) {
                            node.setAttribute("id", DOMIDManager.getNextId());
                        }
                    }
                    node.isIdSet = true;

                    if (setRefId)
                        node.i = "";

                    switch (node.tagName) {
                        case "SCRIPT":
                            return "<SCRIPT id=\"" + node.id + "\"></SCRIPT>";
                            break;
                        case "DIV":
                            if (node.id.startsWith("inside_"))
                                return null;
                            break;
                    }

                    if (isBlockedByFilter(securityFilters, node)) {
                        return null;
                    }

                    HTML += "<" + node.tagName + " ";
                    var attrHtml = GetBodyNodeAttributeString(node);
                    if (attrHtml != "")
                        HTML += attrHtml

                    if (node.tagName == "IMG" || node.tagName == "BR" || node.tagName == "INPUT" ||
                        node.tagName == "LINK" || node.tagName == "META" || node.tagName == "BASE") {
                        HTML += "/>"
                        return HTML;
                    } else
                        HTML += ">";

                    for (var j = 0; j < node.childNodes.length; j++) {
                        var childNode = node.childNodes[j];
                        var childHtml = GetBodyNode(childNode, setRefId)
                        if (childHtml)
                            HTML += childHtml;
                    }

                    HTML += "</" + node.tagName + ">";
                } else if (node.nodeType == 3) {
                    HTML += node.nodeValue;
                }

                return HTML;
            }

            function GetAttributeString(node) {
                var attrString = "";

                for (var j = 0; j < node.attributes.length; j++) {
                    var attr = node.attributes[j];
                    var attrName = attr.name;
                    var attrValue = attr.value;

                    if (isUnusedAttribute(attrName))
                        continue;

                    if (attrName == "style")
                        attrValue = GetStyleWithProxyUrls(attrValue);

                    if (attrValue.indexOf("\"") != -1)
                        attrValue = (attrName == "src" || attrName == "href") ? encodeURI(attrValue) : escape(attrValue);

                    attrString += " " + attrName + "=\"" + attrValue + "\"";
                }

                return attrString;
            }

            function GetHeadNodeAttributeString(node) {
                var attrString = "";

                switch (node.tagName) {
                    case "LINK":
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (isUnusedAttribute(attrName))
                                continue;

                            // always proxy css links even if the original link is on https
                            // css stylesheets can refer to external fonts but these font need to obey the Same Origin policy
                            // without proxy the browser does not download external fonts referenced in the stylesheets
                            if (attrName == "href")
                                attrValue = GetProxyUrl(attrValue);

                            // change urls in inline styles to use https proxy
                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;

                    case "IFRAME":
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (attrName == "src" || isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;

                    case "OBJECT":
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (attrName == "data" || isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;

                    default:
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            if (attrValue.indexOf("\"") != -1)
                                attrValue = (attrName == "src" || attrName == "href") ? encodeURI(attrValue) : escape(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;
                }

                return attrString;
            }

            function GetBodyNodeAttributeString(node) {
                var attrString = "";

            switch (node.tagName) {
                case "INPUT": 
                    var inputType = node.getAttribute("type");
                    if (inputType) { inputType = inputType.toLowerCase(); }
                    var inputName = node.getAttribute("name");
                    var inputId = node.id;

                        if (inputType == "text" || inputType == "password") {
                            for (var j = 0; j < node.attributes.length; j++) {
                                var attr = node.attributes[j];
                                var attrName = attr.name;
                                var attrValue = attr.value;

                                if (isUnusedAttribute(attrName))
                                    continue;

                            var readonly = false;
                            if (attrName == "value") {
                                if (passwordFilters.length > 0 && (passwordFilters.indexOf(inputName) != -1 || passwordFilters.indexOf(inputId) != -1)) {
                                    var stars = "";
                                    for (var i = 0; i < node.value.length; i++)
                                        stars += "*";
                                    attrValue = stars;
                                    readonly = true;
                                } else {
                                    attrValue = node.value;
                                }
                            }

                                if (attrName == "style")
                                    attrValue = GetStyleWithProxyUrls(attrValue);

                                attrString += " " + attrName + "=\"" + attrValue + "\"";

                                if (readonly)
                                    attrString += " " + "readonly";
                            }
                        } else if (inputType == "hidden") {
                            for (var j = 0; j < node.attributes.length; j++) {
                                var attr = node.attributes[j];
                                var attrName = attr.name;
                                var attrValue = attr.value;

                                if (attrName.startsWith("value") || isUnusedAttribute(attrName))
                                    continue;

                                if (attrName == "style")
                                    attrValue = GetStyleWithProxyUrls(attrValue);

                                attrString += " " + attrName + "=\"" + attrValue + "\"";
                            }
                        } else if (inputType == "checkbox") {
                            for (var j = 0; j < node.attributes.length; j++) {
                                var attr = node.attributes[j];
                                var attrName = attr.name;
                                var attrValue = attr.value;

                                if (isUnusedAttribute(attrName))
                                    continue;

                                if (attrName == "style")
                                    attrValue = GetStyleWithProxyUrls(attrValue);

                                attrString += " " + attrName + "=\"" + attrValue + "\"";
                            }

                            if (node.checked == true)
                                attrString += " checked=\"true\"";
                        } else if (inputType == "file") {
                            for (var j = 0; j < node.attributes.length; j++) {
                                var attr = node.attributes[j];
                                var attrName = attr.name;
                                var attrValue = attr.value;

                                if (isUnusedAttribute(attrName))
                                    continue;

                                if (attrName == "style")
                                    attrValue = GetStyleWithProxyUrls(attrValue);

                                attrString += " " + attrName + "=\"" + attrValue + "\"";
                            }
                            attrString += " " + "disabled";
                        } else {
                            for (var j = 0; j < node.attributes.length; j++) {
                                var attr = node.attributes[j];
                                var attrName = attr.name;
                                var attrValue = attr.value;

                                if (isUnusedAttribute(attrName))
                                    continue;

                                if (attrName == "style")
                                    attrValue = GetStyleWithProxyUrls(attrValue);

                                attrString += " " + attrName + "=\"" + attrValue + "\"";
                            }
                        }
                        break;

                    case "OPTION":
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (attrName.startsWith("selected") || isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }

                        var selectNode = node.parentNode;
                        if (selectNode && selectNode.tagName == "SELECT")
                            if (selectNode[selectNode.selectedIndex].value == node.value)
                                attrString += " selected=\"selected\"";
                        break;

                    case "LINK":
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "href")
                                attrValue = GetProxyUrl(attrValue);

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;

                    case "IFRAME":
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (attrName == "src" || isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;

                    case "OBJECT":
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (attrName == "data" || isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;

                    default:
                        for (var j = 0; j < node.attributes.length; j++) {
                            var attr = node.attributes[j];
                            var attrName = attr.name;
                            var attrValue = attr.value;

                            if (isUnusedAttribute(attrName))
                                continue;

                            if (attrName == "style")
                                attrValue = GetStyleWithProxyUrls(attrValue);

                            if (attrValue.indexOf("\"") != -1)
                                attrValue = (attrName == "src" || attrName == "href") ? encodeURI(attrValue) : escape(attrValue);

                            attrString += " " + attrName + "=\"" + attrValue + "\"";
                        }
                        break;
                }

                return attrString;
            }
        }

        function InsideObserver() {
            this.observer = null;
            this.observing = false;
            this.PROMISE = null;
            this.eventTimer = new InsideTimer();
            var _this = this;

            this.stop = function () {
                this.observing = false;
                if (this.eventTimer)
                    this.eventTimer.stop();
                delete this.PROMISE;
                this.PROMISE = null;
            }
            this.dispose = function () {
                this.stop();
                if (this.observer) {
                    this.observer.disconnect();
                    this.observer = null;
                }
            }

            this.isSyncing = false;
            this.sync = function () {
                this.isSyncing = true;
                this.stop();
                this.observing = true;
                if (!this.observer) {
                    this.observer = new MutationObserver(this.mutationCallback);
                    // only observe mutations on the following attributes
                    var observerConfig = { attributes: true, childList: true, characterData: true, subtree: true, attributeFilter: ["class", "style", "href", "src", "aria-expanded"] };
                    this.observer.observe(document.body, observerConfig);
                }
                //console.time("HTML generation time");
                var htmlDetails = DOM.GetHtmltoSend();
                //console.timeEnd("HTML generation time");
                insideAPI.call("coBrowseSetHTML", htmlDetails, function (isSessionPresent) {
                    if (typeof isSessionPresent.data != "undefined" && isSessionPresent.data == true) {
                        log("HTML sent length : " + htmlDetails.html.length);
                        _this.eventTimer.start();
                    } else {
                        log("No Co-Browse session. HTML not sent!");
                        disposeCobrowse()
                    }
                    _this.isSyncing = false;
                });
            }

            // mutation are to be chained (order is imp)
            // if mutation fails we resync
            this.mutationCallback = function (mutations) {
                setAddedNodeIds(mutations);
                if (!_this.observing)
                    return;

                var deferred = $.Deferred();
                deferred.resolve();
                this.PROMISE = deferred.promise();

                var promise = getMutationPromise(mutations);
                this.PROMISE = this.PROMISE.then(promise);
            }

            var promiseFailedTimeout;
            function promiseFailed() {
                if (promiseFailedTimeout)
                    clearTimeout(promiseFailedTimeout);
                promiseFailedTimeout = setTimeout(function () {
                    if (!_this.isSyncing)
                        insideSync.sync();
                }, 300);
            }
            function getMutationPromise(mutations) {
                return function () {
                    var def = $.Deferred();
                    def.fail(promiseFailed);
                    setTimeout(function () {
                        if (ExeMutations(mutations))
                            def.resolve();
                        else
                            def.reject();

                    }, 0);
                    return def.promise();
                }
            }


            function getKnownNode(targetNode) {
                while (targetNode) {
                    // to check if i attr is present check == ""
                    if (targetNode.i == "" && targetNode.id)
                        return targetNode;

                    targetNode = targetNode.parentNode;
                }
                return null;
            }

            function isInsideNode(targetNode) {
                //if (!targetNode)
                //    return false;
                var parentNode = targetNode.parentNode;
                while (parentNode) {
                    if (parentNode.tagName == "BODY" && targetNode.id && targetNode.id.startsWith("inside_")) {
                        return true;
                    }
                    targetNode = parentNode;
                    parentNode = parentNode.parentNode;
                }
                return false;
            }

            function filterInsideNodes(mutations) {
                for (var i = mutations.length - 1; i >= 0; i--) {
                    var record = mutations[i];
                    switch (record.type) {
                        case "childList":
                            if (record.addedNodes.length > 0) {
                                for (var j = 0; j < record.addedNodes.length; j++) {
                                    var addedNode = record.addedNodes[j];
                                    if (isInsideNode(addedNode)) {
                                        mutations.splice(i, 1);
                                        //log("Inside node removed! (added nodes)");
                                        break;
                                    }
                                }
                            } else if (record.removedNodes.length > 0) {
                                for (var j = 0; j < record.removedNodes.length; j++) {
                                    var removedNode = record.removedNodes[j];
                                    if (isInsideNode(removedNode)) {
                                        mutations.splice(i, 1);
                                        //log("Inside node removed! (removed nodes)");
                                        break;
                                    }
                                }
                            }

                            break;
                        case "characterData": // target
                            if (isInsideNode(record.target)) {
                                mutations.splice(i, 1);
                                //log("Inside node removed! (characterData nodes)");
                                break;
                            }

                            break;
                        case "attributes":
                            if (isInsideNode(record.target)) {
                                mutations.splice(i, 1);
                                // log("Inside node removed! (attributes nodes)");
                                break;
                            }
                            break;
                    }
                }
            }

            // if returns false cancel promise chain and resync
            function ExeMutations(mutations) {
                var replaceNodes = {};
                var attributeNames = {};
                var attributNodes = {};
                filterInsideNodes(mutations);

                for (var i = 0; i < mutations.length; i++) {
                    var record = mutations[i];
                    switch (record.type) {
                        case "childList":
                            var targetNode = record.target;
                            var replaceNode = getKnownNode(targetNode);
                            if (replaceNode)
                                replaceNodes[replaceNode.id] = replaceNode;
                            break;
                        case "characterData":
                            var targetNode = record.target;
                            targetNode = targetNode.parentNode;
                            var replaceNode = getKnownNode(targetNode);
                            if (replaceNode)
                                replaceNodes[replaceNode.id] = replaceNode;
                            break;
                        case "attributes":
                            var targetNode = record.target;
                            if ((targetNode.tagName == "SCRIPT" || targetNode.tagName == "IFRAME") && record.attributeName == "src")
                                continue;
                            else if (targetNode.tagName == "OBJECT" && record.attributeName == "data")
                                continue;
                            else if (isBlockedByFilter(animationFilters, targetNode))
                                continue;
                            else if (isBlockedByFilter(securityFilters, targetNode))
                                continue;
                            var node = document.getElementById(targetNode.id);
                            if (node) {
                                if (!attributeNames[targetNode.id]) {
                                    attributeNames[targetNode.id] = [];
                                    attributNodes[targetNode.id] = node;
                                }
                                if (attributeNames[targetNode.id].indexOf(record.attributeName) == -1) {
                                    attributeNames[targetNode.id].push(record.attributeName)
                                }
                            }
                            break;
                    }
                }

                var htmlNodes = [];
                var existingNodes = [];

                for (var key in replaceNodes) {
                    if (replaceNodes.hasOwnProperty(key)) {
                        var replaceNode = replaceNodes[key];
                        if (isNodeExists(replaceNode)) {
                            if (replaceNode.tagName == "BODY") {
                                log("Body node in processing...")
                                return false;
                            }
                            existingNodes.push(replaceNode);
                            var html = DOM.GetBodyHtml(replaceNode);
                            if (html)
                                htmlNodes.push({ id: replaceNode.id, html: html })
                        }
                    }
                }

                var changedAttributes = [];
                for (var key in attributeNames) {
                    var node = attributNodes[key];
                    for (var i = 0; i < existingNodes.length; i++) {
                        var existingNode = existingNodes[i];
                        if (isDescendant(existingNode, node))
                            continue;
                    }
                    var attrs = attributeNames[key];
                    var props = [];
                    for (var i = 0; i < attrs.length; i++) {
                        var attName = attrs[i];
                        var val = node.getAttribute(attName);
                        //console.log("key: " + attName + " val: " + val)
                        props.push({ key: attName, value: val });
                    }
                    changedAttributes.push({ id: node.id, props: props });
                }

                var mutations = { mutations: { replaced: htmlNodes, changedAttributes: changedAttributes } };
                if (htmlNodes.length > 0 || changedAttributes.length > 0) {
                    insideCoBrowse.send(mutations);
                    log("Mutation sent  >>>");
                    //for (var j = 0; j < htmlNodes.length; j++) {
                    //    log(j+1 +". id= " + htmlNodes[j].id + " html="+htmlNodes[j].html.length)
                    //}

                    //for (var j = 0; j < changedAttributes.length; j++) {
                    //    var chagedAttr = changedAttributes[j];
                    //    log("id = " + chagedAttr.id);
                    //    $.each(chagedAttr.props, function(index, kv) {
                    //        log("attr = " + kv.key + " value = "+ kv.value);
                    //    });
                    //}
                    //JSON.stringify(mutations, null, ' ');
                    return true;
                }
                return true;
            }

            function setAddedNodeIds(mutations) {
                for (var i = 0; i < mutations.length; i++) {
                    var record = mutations[i];
                    if (record.type == "childList" && record.addedNodes.length > 0) {
                        for (var j = 0; j < record.addedNodes.length; j++) {
                            var addedNode = record.addedNodes[j];
                            if (addedNode.nodeType == 1) {
                                if (!addedNode.id)
                                    addedNode.id = DOMIDManager.getNextId();
                                addedNode.isIdSet = true;
                            }

                        }
                    }
                }
            }

            this.disableScrollEvent = function () {
                if (this.eventTimer)
                    this.eventTimer.disableScrollEvent();
            }
            this.enableScrollEvent = function () {
                if (this.eventTimer)
                    this.eventTimer.enableScrollEvent();
            }
        }

        function isDescendant(parent, child) {
            var node = child;
            while (node != null) {
                if (node == parent) {
                    return true;
                }
                node = node.parentNode;
            }
            return false;
        }

        function isNodeExists(targetNode) {
            while (targetNode) {
                if (targetNode == document)
                    return true;
                targetNode = targetNode.parentNode;
            }
            return false;
        }

        function isUnusedAttribute(attrName) {
            if (attrName.startsWith("on")) { return true; }
            if (attrName.startsWith("ng")) { return true; }
            if (attrName.startsWith("sc")) { return true; }
            if (attrName === "disabled") { return true; }
            return false;
        }

        function log(message) {
            //console.log("Inside cb: " + message);
        }

        function htmlEncode(encoded) {
            encoded.replace(/"/g, "&quot;");
            return encoded;
        }

        function setCookie(cname, cvalue) {
            var d = new Date();
            d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires + ";domain=" + domain + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        var DataTransfer = function () {
            this.data = {};
        };

        DataTransfer.prototype.getData = function (type) {
            return this.data[type]
        };

        DataTransfer.prototype.setData = function (type, val) {
            this.data[type] = val;
        };
    }
});