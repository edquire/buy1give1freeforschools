var _inside=_inside||[];var insideOrderTotal=0;var maxLoop=10;var curLoop=0;var firstCall=false;var _insideLoaded=_insideLoaded||false;(function()
{if(_insideLoaded)
return;_insideLoaded=true;var accountKey="IN-1000429";var trackerURL="au3-track.inside-graph.com";var useCustomFunctionForCheckout=true;var detectSearchByUrl=true;var searchUrl="search/";var searchClassName=null;var detectProductCategoryByUrl=false;var productCategoryUrl=null;var productCategoryClassName="pageType-CategoryPage";var detectProductByUrl=false;var productUrl=null;var productClassName="pageType-ProductPage";var detectCheckoutByUrl=true;var checkoutUrl="/cart";var checkoutClassName=null;var detectOrderConfirmedByUrl=true;var orderConfirmedUrl="/orderConfirmation/";var orderConfirmedClassName=null;function log()
{if(typeof(console)!="undefined"&&typeof(console.log)!="undefined")
{}}
function keepWait(callback,test)
{if(test())
{callback();}
var _interval=1000;var _spin=function()
{if(test())
{callback();}
setTimeout(_spin,_interval);};setTimeout(_spin,_interval);}
function deferWait(callback,test)
{if(test())
{callback();return;}
var _interval=10;var _spin=function()
{if(test())
{callback();}
else
{_interval=_interval>=1000?1000:_interval*2;setTimeout(_spin,_interval);}};setTimeout(_spin,_interval);}
var indexOf=[].indexOf||function(prop)
{for(var i=0;i<this.length;i++)
{if(this[i]===prop)
return i;}
return-1;};var getElementsByClassNameManual=function(className,context)
{if(context.getElementsByClassName)
return context.getElementsByClassName(className);var elems=document.querySelectorAll?context.querySelectorAll("."+ className):(function()
{var all=context.getElementsByTagName("*"),elements=[],i=0;for(;i<all.length;i++)
{if(all[i].className&&(" "+ all[i].className+" ").indexOf(" "+ className+" ")>-1&&indexOf.call(elements,all[i])===-1)
elements.push(all[i]);}
return elements;})();return elems;};function myTrim(text)
{if(typeof(text)!="undefined"&&text!=null)
return typeof(text.trim)==="function"?text.trim():text.replace(/^\s+|\s+$/gm,'');return null;}
function isNumber(o)
{return!isNaN(o- 0)&&o!==null&&o!==""&&o!==false;}
function setCookie(cname,cvalue,exdays)
{var d=new Date();d.setTime(d.getTime()+(exdays*24*60*60*1000));var expires="expires="+ d.toGMTString();document.cookie=cname+"="+ cvalue+"; "+ expires+";path=/";}
function getCookie(cname)
{var name=cname+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++)
{var c=myTrim(ca[i]);if(c.indexOf(name)==0)
return c.substring(name.length,c.length);}
return null;}
function deleteCookie(cname)
{document.cookie=cname+"="+ 0+"; "+"expires=01 Jan 1970 00:00:00 GMT"+";path=/";}
function roundToTwo(num)
{if(Math!="undefined"&&Math.round!="undefined")
return+(Math.round(num+"e+2")+"e-2");else
return num;}
function getSearchParameters()
{var prmstr=window.location.search.substr(1);return prmstr!=null&&prmstr!=""?transformToAssocArray(prmstr):[];}
function transformToAssocArray(prmstr)
{var params=[];var prmarr=prmstr.split("&");for(var i=0;i<prmarr.length;i++)
{params[i]=prmarr[i];}
return params;}
function getViewData()
{try
{var data={};data.action="trackView";data.tags="host:"+ window.location.host;data.type="article";data.url=window.location.href;data.name="Unknown Page: "+ window.location.href;var temp_loc=window.location.href.split("://")[1].split("/");var page="";var add_tags="";var params=getSearchParameters();var searchterm="Search";if(params!=null&&params.length>0)
{for(var i=0;i<params.length;i++)
{add_tags=add_tags+","+ params[i];if(params[i].indexOf("text=")==0)
{searchterm=params[i].split("text=")[1];}}}
for(var i=1;i<temp_loc.length;i++)
{if(temp_loc[i]!=null&&temp_loc[i].length>0)
page=temp_loc[i];}
var curpage=page.split("?")[0];if((curpage==""||curpage=="/"||curpage=="index.html")&&temp_loc.length<3)
{data.type="homepage";}
else if(curpage=="login")
{data.type="login";}
if(detectSearchByUrl&&searchUrl!=null)
{if(data.url.indexOf(searchUrl)!=-1)
{data.type="search";}}
else if(searchClassName!=null)
{var tempelem=getElementsByClassNameManual(searchClassName,document);if(tempelem!=null&&tempelem.length>0)
{data.type="search";}}
if(detectProductCategoryByUrl&&productCategoryUrl!=null)
{if(data.url.indexOf(productCategoryUrl)!=-1)
{data.type="productcategory";}}
else if(productCategoryClassName!=null)
{var tempelem=getElementsByClassNameManual(productCategoryClassName,document);if(tempelem!=null&&tempelem.length>0)
{data.type="productcategory";}}
if(detectProductByUrl&&productUrl!=null)
{if(data.url.indexOf(productUrl)!=-1)
{data.type="product";}}
else if(productClassName!=null)
{var tempelem=getElementsByClassNameManual(productClassName,document);if(tempelem!=null&&tempelem.length>0)
{data.type="product";}}
if(detectCheckoutByUrl)
{if(data.url.indexOf(checkoutUrl)!=-1&&checkoutUrl!=null)
{data.type="checkout";}
if(data.url.indexOf("/checkout")!=-1&&checkoutUrl!=null)
{data.type="checkout";}}
else if(checkoutClassName!=null)
{var tempelem=getElementsByClassNameManual(checkoutClassName,document);if(tempelem!=null&&tempelem.length>0)
{data.type="checkout";}}
if(detectOrderConfirmedByUrl&&orderConfirmedUrl)
{if(data.url.indexOf(orderConfirmedUrl)!=-1)
{data.type="orderconfirmed";}}
else if(orderConfirmedClassName!=null)
{var tempelem=getElementsByClassNameManual(orderConfirmedClassName,document);if(tempelem!=null&&tempelem.length>0)
{data.type="orderconfirmed";}}
switch(data.type)
{case"homepage":data.name="Home";break;case"search":data.name=searchterm;break;case"login":var tempPageName=getPageName();if(tempPageName!=null&&tempPageName.length>0)
data.name=tempPageName;break;case"productcategory":var tempcat=getCategory("productcategory");if(tempcat!=null&&tempcat.length>0)
data.category=tempcat;var tempPageName=getPageName();if(tempPageName!=null&&tempPageName.length>0)
data.name=tempPageName;break;case"product":var tempPageName=getPageName();if(tempPageName!=null&&tempPageName.length>0)
data.name=tempPageName;var tempcat=getCategory("product");if(tempcat!=null&&tempcat.length>0)
data.category=tempcat;var tempval=getProductImage();if(tempval!=null&&tempval.length>0)
data.img=tempval;var tempsku=getProductSku();if(tempsku!=null&&tempsku.length>0)
data.sku=tempsku;var tempprice=getProductPrice();if(tempprice!=null&&tempprice>0)
data.price=tempprice;break;case"checkout":var tempPageName=getPageName();if(tempPageName!=null&&tempPageName.length>0)
data.name=tempPageName;break;case"orderconfirmed":var tempPageName=getPageName();if(tempPageName!=null&&tempPageName.length>0)
data.name=tempPageName;break;case"article":var tempPageName=getPageName();if(tempPageName!=null&&tempPageName.length>0)
data.name=tempPageName;break;}
return data;}
catch(ex)
{if(typeof(console)!="undefined"&&typeof(console.log)!="undefined")
console.log("getViewData error: ",ex);return null;}}
function getPageName()
{var content=document.getElementsByTagName("title");if(typeof(content)!="undefined"&&content!=null&&content.length>0)
{var result=content[0].textContent||content[0].innerText;if(typeof(result)!="undefined"&&result!=null&&result.length>0)
{if(result.indexOf("| BIG W")!=-1)
{result=result.split("| BIG W")[0];}
return myTrim(result);}}
return null;}
function getProductImage()
{var metaTags=document.getElementsByTagName("meta");var fbAppIdContent="";for(var i=0;i<metaTags.length;i++)
{if(metaTags[i].getAttribute("property")=="og:image")
{fbAppIdContent=metaTags[i].getAttribute("content");return fbAppIdContent;}}
return"";}
function getProductPrice()
{try
{return $("#product_online_price > [itemprop='price']").text();}
catch(priceex)
{}
return null;}
function getProductSku()
{try
{return dataLayer[0].ecommerce.detail.products[0].id;}
catch(priceex)
{}
return null;}
function getCategory(temppagetype)
{var bcs=getElementsByClassNameManual("breadcrumb",document);if(bcs!=null&&bcs.length>0)
{var spans=bcs[0].getElementsByTagName("li");var cat="";var arrtemp=[];var templength=1;var spanlength=spans.length;if(temppagetype=="product")
{spanlength=spanlength- 1;}
for(var i=templength;i<spanlength;i++)
{var temp=spans[i].innerText||spans[i].textContent;var templink=spans[i].getElementsByTagName("a");if(templink!=null&&templink.length>0)
{temp=templink[0].innerText||templink[0].textContent;}
arrtemp.push(myTrim(temp));}
cat=arrtemp.join(" / ");return cat;}
return"";}
function getOrderData()
{try
{var data=[];var totalprice=0;var orderId="auto";var tempempty=getElementsByClassNameManual("express-cart-container empty-cart",document);if(tempempty!=null&&tempempty.length>0)
{return null;}
var tempcontent=getElementsByClassNameManual("minicart-top",document);if(tempcontent!=null&&tempcontent.length>0)
{var details=getElementsByClassNameManual("popupCartItem",tempcontent[0]);if(details!=null&&details.length>0)
{for(var i=0;i<details.length;i++)
{var detail=details[i];var items=getElementsByClassNameManual("itemName",detail);var priceqty=getElementsByClassNameManual("list-inline",detail);var prices=priceqty[0].getElementsByTagName("li");var img_link=detail.getElementsByTagName("img")[0].getAttribute("src");if(img_link.indexOf("http")!=0)
{img_link=('https:'==document.location.protocol?'https://':'http://')+ window.location.host+ img_link;}
var name=items[0].innerText||items[0].textContent;var sku=name;var price=prices[1].innerText||prices[1].textContent;price=parseFloat(price.replace(/[^0-9\.\-\+]/g,""));totalprice=totalprice+ price;var qty=prices[0].innerText||prices[0].textContent;qty=parseFloat(qty.replace(/[^0-9\.\-\+]/g,""));price=price/qty;data.push({"action":"addItem","orderId":orderId,"name":myTrim(name),"sku":myTrim(sku),"img":img_link,"price":price,"qty":qty});}}}
if(data.length>0)
{var temptotal=getElementsByClassNameManual("cartTotal",document);if(temptotal!=null&&temptotal.length>0)
{var temptext=temptotal[0].innerText||temptotal[0].textContent;totalprice=parseFloat(temptext.replace(/[^0-9\.\-\+]/g,""));}
else
{temptotal=getElementsByClassNameManual("minicart-bottom",document);if(temptotal!=null&&temptotal.length>0)
{temptotal=getElementsByClassNameManual("price",temptotal[0]);if(temptotal!=null&&temptotal.length>0)
{var temptext=temptotal[0].innerText||temptotal[0].textContent;totalprice=parseFloat(temptext.replace(/[^0-9\.\-\+]/g,""));}}}
data.push({"action":"trackOrder","orderId":orderId,"orderTotal":totalprice});return data;}
return null;}
catch(ex)
{log("getOrderData error. ",ex);return null;}}
function getOrderDataCart()
{try
{var data=[];var totalprice=0;var orderId="auto";var tempempty=getElementsByClassNameManual("express-cart-container empty-cart",document);if(tempempty!=null&&tempempty.length>0)
{return null;}
var tempcontent=getElementsByClassNameManual("cart table",document);if(tempcontent!=null&&tempcontent.length>0)
{var details=getElementsByClassNameManual("cartItem",tempcontent[0]);if(details!=null&&details.length>0)
{for(var i=0;i<details.length;i++)
{var detail=details[i];var items=getElementsByClassNameManual("itemName",detail);var prices=getElementsByClassNameManual("total",detail);var qtys=getElementsByClassNameManual("qty",detail);var img_link=detail.getElementsByTagName("img")[0].getAttribute("src");if(img_link.indexOf("http")!=0)
{img_link=('https:'==document.location.protocol?'https://':'http://')+ window.location.host+ img_link;}
var name=items[0].innerText||items[0].textContent;var sku=name;var price=prices[0].innerText||prices[0].textContent;price=parseFloat(price.replace(/[^0-9\.\-\+]/g,""));totalprice=totalprice+ price;var qty=qtys[0].value;qty=parseFloat(qty.replace(/[^0-9\.\-\+]/g,""));price=price/qty;data.push({"action":"addItem","orderId":orderId,"name":myTrim(name),"sku":myTrim(sku),"img":img_link,"price":price,"qty":qty});}}}
if(data.length>0)
{var temptotal=getElementsByClassNameManual("cartTotal",document);if(temptotal!=null&&temptotal.length>0)
{var temptext=temptotal[0].innerText||temptotal[0].textContent;totalprice=parseFloat(temptext.replace(/[^0-9\.\-\+]/g,""));}
else
{temptotal=getElementsByClassNameManual("minicart-bottom",document);if(temptotal!=null&&temptotal.length>0)
{temptotal=getElementsByClassNameManual("price",temptotal[0]);if(temptotal!=null&&temptotal.length>0)
{var temptext=temptotal[0].innerText||temptotal[0].textContent;totalprice=parseFloat(temptext.replace(/[^0-9\.\-\+]/g,""));}}}
data.push({"action":"trackOrder","orderId":orderId,"orderTotal":totalprice});return data;}
return null;}
catch(ex)
{log("getOrderDataCart error. ",ex);return null;}}
function getOrderDataCheckout()
{try
{var data=[];var totalprice=0;var orderId="auto";var tempempty=getElementsByClassNameManual("express-cart-container empty-cart",document);if(tempempty!=null&&tempempty.length>0)
{return null;}
var tempcontent=getElementsByClassNameManual("deliveryCartItems",document);if(tempcontent!=null&&tempcontent.length>0)
{var details=getElementsByClassNameManual("deliveryCartItem",tempcontent[0]);if(details!=null&&details.length>0)
{for(var i=0;i<details.length;i++)
{var detail=details[i];var items=getElementsByClassNameManual("name",detail);var prices=getElementsByClassNameManual("text-right",detail);var qtys=getElementsByClassNameManual("text-center",detail);var name=items[0].innerText||items[0].textContent;var sku=name;var price=prices[0].innerText||prices[0].textContent;price=parseFloat(price.replace(/[^0-9\.\-\+]/g,""));totalprice=totalprice+ price;var qty=qtys[0].innerText||qtys[0].textContent;qty=parseFloat(qty.replace(/[^0-9\.\-\+]/g,""));price=price/qty;data.push({"action":"addItem","orderId":orderId,"name":myTrim(name),"sku":myTrim(sku),"price":price,"qty":qty});}}}
if(data.length>0)
{var tempdata={};try
{var temptotal=getElementsByClassNameManual("cartTotal",document);if(temptotal!=null&&temptotal.length>0)
{var temptext=temptotal[0].innerText||temptotal[0].textContent;totalprice=parseFloat(temptext.replace(/[^0-9\.\-\+]/g,""));}
var tempshipping=document.getElementById("deliveryCost");if(tempshipping!=null)
{var temptext=tempshipping.innerText||tempshipping.textContent;tempdata.shippingTotal=parseFloat(temptext.replace(/[^0-9\.\-\+]/g,""));}}
catch(totalex)
{}
data.push({"action":"trackOrder","orderId":orderId,"data":tempdata,"orderTotal":totalprice});return data;}
return null;}
catch(ex)
{log("getOrderDataCheckout error. ",ex);return null;}}
function getMiniCartTotal()
{try
{var tempcontent=getElementsByClassNameManual("minicart",document);if(tempcontent!=null&&tempcontent.length>0)
{tempcontent=getElementsByClassNameManual("price first-set",tempcontent[0]);if(tempcontent!=null&&tempcontent.length>0)
{var temptext=tempcontent[0].textContent||tempcontent[0].innerText;if(temptext!=null&&temptext.length>0)
{return parseFloat(temptext.replace(/[^0-9\.\-\+]/g,""));}}}}
catch(totalex)
{}
return null;}
function orderConfirmProcess()
{try
{var data=[];var detail=null;if(typeof(dataLayer)!="undefined"&&dataLayer!=null&&dataLayer.length>0)
{for(var i=0;i<dataLayer.length;i++)
{if(typeof(dataLayer[i].ecommerce)!="undefined"&&dataLayer[i].ecommerce!=null)
{detail=dataLayer[i].ecommerce.purchase;}}}
if(detail!=null)
{var totalprice=detail.actionField.revenue;var shippingtotal=detail.actionField.shipping;var orderID=detail.actionField.id;for(var i=0;i<detail.products.length;i++)
{var sku=detail.products[i].id;var price=detail.products[i].price;var qty=detail.products[i].quantity;data.push({"action":"addItem","orderId":orderID,"name":detail.products[i].name,"price":price,"sku":detail.products[i].name,"qty":qty});}
if(data.length>0)
{data.push({"action":"trackOrder","orderId":orderID,"orderTotal":totalprice});data.push({"action":"trackOrder","orderId":orderID,"orderTotal":totalprice,"update":true,"complete":true});}
return data;}
return null;}
catch(ex)
{log("orderConfirmProcess error. ",ex);return null;}}
function getVisitorId()
{try
{}
catch(visitidex)
{}
return null;}
function getVisitorName()
{try
{var tempcontent=getElementsByClassNameManual("logged_in",document);if(tempcontent!=null&&tempcontent.length>0)
{var temptext=tempcontent[0].innerText||tempcontent[0].textContent;if(temptext!=null&&temptext.length>0)
{if(temptext.indexOf("Welcome,")!=-1)
{return myTrim(temptext.split("Welcome,")[1]);}}}}
catch(visitidex)
{}
return"";}
function insertInsideTag()
{if(typeof(_insideGraph)!="undefined"&&_insideGraph!=null)
{_insideGraph.processQueue();}
else
{var inside=document.createElement('script');inside.type='text/javascript';inside.async=true;inside.src=('https:'==document.location.protocol?'https://':'http://')+ trackerURL+'/ig.js?hn='
+ encodeURIComponent(document.location.hostname)+'&_='+ Math.random();var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(inside,s);}}
function sendToInside()
{try
{var visitorId=getVisitorId();var visitorName=getVisitorName();if(typeof(_insideGraph)=="undefined"||_insideGraph==null)
{if(visitorId==null)
{_inside.push({"action":"getTracker","account":accountKey,"visitorName":visitorName});}
else
{_inside.push({"action":"getTracker","account":accountKey,"visitorId":visitorId,"visitorName":visitorName});}}
var view=getViewData();if(view!=null)
{if(view.type=="orderconfirmed")
{var tempconfirm=orderConfirmProcess();if(tempconfirm!=null&&tempconfirm.length>0)
{for(var i=0;i<tempconfirm.length;i++)
{_inside.push(tempconfirm[i]);}}
_inside.push(view);}
else
{_inside.push(view);var orderData=getOrderData();if(useCustomFunctionForCheckout&&view.type=="checkout")
{orderData=getOrderDataCheckout();if(orderData==null||orderData.length==0)
{orderData=getOrderDataCart();}}
if(orderData!=null&&orderData.length>0)
{for(var i=0;i<orderData.length;i++)
{_inside.push(orderData[i]);if(orderData[i].action=="trackOrder")
{view.orderId=orderData[i].orderId;view.orderTotal=orderData[i].orderTotal;insideOrderTotal=orderData[i].orderTotal;}}}
else
{var temptotal=getMiniCartTotal();if(temptotal!=null&&temptotal>0)
{view.orderId="auto";view.orderTotal=temptotal;}}}
log("Inside Debug: ",_inside);}}
catch(sendex)
{_inside=[];_inside.push({"action":"getTracker","account":accountKey});_inside.push({"action":"trackView","type":"other","name":"Check: "+ window.location.href});log(sendex);}
insertInsideTag();if(!firstCall)
firstCall=true;}
var tempview=getViewData();if(tempview!=null&&typeof(tempview.type)!="undefined"&&tempview.type!=null&&tempview.type=="orderconfirmed")
{deferWait(sendToInside,function()
{if(typeof(dataLayer)!="undefined"&&dataLayer!=null&&dataLayer.length>0)
{for(var i=0;i<dataLayer.length;i++)
{if(typeof(dataLayer[i].ecommerce)!="undefined"&&dataLayer[i].ecommerce!=null)
{return true;}}}
return false;});}
else if(tempview!=null&&typeof(tempview.type)!="undefined"&&tempview.type!=null&&tempview.type=="checkout")
{deferWait(sendToInside,function()
{if(document.readyState!='loading'&&document.readyState!='interactive')
{return true;}
return false;});}
else
{deferWait(sendToInside,function()
{if(document.readyState!='loading'&&document.readyState!='interactive')
{keepWait(sendToInside,function()
{if(!firstCall)
return false;if(typeof(_insideGraph)!="undefined"&&_insideGraph!=null)
{var temporderdata=getOrderData();if(temporderdata!=null&&temporderdata.length>0)
{for(var i=0;i<temporderdata.length;i++)
{if(temporderdata[i].action=="trackOrder")
{if(insideOrderTotal!=temporderdata[i].orderTotal)
{if(curLoop<maxLoop)
{curLoop=curLoop+ 1;return true;}}}}}
else if(insideOrderTotal>0)
{insideOrderTotal=0;if(curLoop<maxLoop)
{curLoop=curLoop+ 1;return true;}}}
return false;});return true;}
return false;});}})();