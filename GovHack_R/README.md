---
output:
  pdf_document: default
  html_document: default
---


# Description
This API uses postal code to find schools within a reasonable range of that postal code that have the greatest need of additional resources. 

The API uses postal code longitude and latitude to select public schools that are within 4 km radius of the postal code. From those selected schools, it finds top three schools that are in need of additional resources. The resource need is identified using publicly available Resource Allocation Model data. The model has a composite score called FOEI, which is "the Family Occupation and Education Index (FOEI) is a school level index of educational disadvantage related to socioeconomic background. FOEI is constructed from parental education and occupation information collected from student enrolment forms. FOEI uses a statistical regression model to produce a weighted combination of school-level parental education and occupation variables based on the extent to which each variable uniquely predicts average school performance." The higher FOEI value indicates greater need of resources. Thus, the API selects the three schools within the 4 km radius that have the highest FOEI values. 

# Using API
The API is serverless REST implementation in Python which sits on Amazon Lambda. To make a call, send a POST request and following parameters in JSON format: 

``` JSON
{
    "zipcode":2114,
    "url":"www.google.com"
}
```

The POST request should be with the following URL: 
``` JSON
https://api.buy1give1forschools.com/rec
```

Here is a CURL example for using the API: 

``` sh
curl -i -X POST -H "Content-Type:application/json" https://api.buy1give1forschools.com/rec 
-d '{"zipcode":2114, "url":"www.google.com"}'
```
### Return Values
The API returns names of three schools together with FOEI score, distance from postal code location, and longitude and latitude of the school. 

``` JSON
[
    [
        "ermington west public school",   # School name
        94,                               # FOEI score
        3.0410390658469315,               # Distance from postal code location
        [
            -33.805894,                   # School longitude
            151.058368                    # School latitude
        ]
    ],
    [
        "yates avenue public school",
        93,
        3.2652306074765347,
        [
            -33.795608,
            151.058398
        ]
    ],
    [
        "marsden high school",
        86,
        1.8138274339103766,
        [
            -33.803326,
            151.071124
        ]
    ]
]
```



# Implementation details

### Data Sources
We used the following publicly available datasets:
  
  - Resource Allocation Model (RAM) and family occupation education index (2016-2017) https://data.cese.nsw.gov.au/data/dataset/resource-allocation-model-ram-and-family-occupation-education-index
  - NSW public schools master dataset (https://data.cese.nsw.gov.au/data/dataset/nsw-public-schools-master-dataset/resource/2ac19870-44f6-443d-a0c3-4c867f04c305)
  - Australian Post Codes Latitude and Longitude data http://www.corra.com.au/downloads/Australian_Post_Codes_Lat_Lon.zip

### Data Merging and Synthesis
We merged Resource Allocation Model data with NSW Public Schools Master dataset. The Master dataset contains longitude and latitude of each school. Using Australian post codes data, we computed distance from post code location to school location. 

### API Implementation Using Amazon Lambda
We built the implementation of above model with Python in AWS Lambda and exposed it as AWS API gateway. Lambda will load the data of school location and postcode from S3.

# Limitations
Currently, the API returns only K-12 public schools from NSW. 
