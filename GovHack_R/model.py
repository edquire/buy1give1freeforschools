import json
import boto3
import csv
import os
import math
from heapq import nlargest


def filter_distance(distance, radius, schools, foei, coordinate):
    candidates = []
    for idx, val in enumerate(distance):
        if val <= 4:
            candidates.append((schools[idx][1:-1], foei[idx], distance[idx], coordinate[idx]))
    return candidates


def lambda_handler(event, context):
    try:
        event['zipcode'] = int(event['zipcode'])
    except:
        return []

    s3_client = boto3.client('s3')
    # foei_weight = float(os.environ['FOEI_WEIGHT'])
    # distance_weight = 1 - foei_weight

    # get school data
    response = s3_client.get_object(Bucket="data.buy1give1forschools.com", Key="model/merged_small_dat.csv")
    schools = []
    foei = []
    coordinate = []

    for row in response[u'Body'].read().split('\n'):
        cols = row.split(',')
        if len(cols) == 5:
            schools.append(cols[1])
            foei.append(float(cols[4]))
            coordinate.append((float(cols[2]), float(cols[3])))
    # print schools[0], foei[0], coordinate[0]
    # foei_sum = sum(foei)
    # foei = [i/foei_sum * foei_weight for i in foei]

    # get geo data
    response = s3_client.get_object(Bucket="data.buy1give1forschools.com", Key="model/export_postcode.csv")
    postcode_dict = {}

    for row in response[u'Body'].read().split('\n'):
        cols = row.split(',')
        if len(cols) == 4:
            try:
                postcode_dict[int(cols[1])] = (float(cols[2]), float(cols[3]))
            except:
                pass

    distance = []
    if event['zipcode'] in postcode_dict:
        loc = postcode_dict[event['zipcode']]
        distance = [math.hypot(x - loc[0], y - loc[1]) * 100 for x, y in coordinate]
    else:
        return []
        # distance_sum = sum(distance)
        # distance = [i/distance_sum * distance_weight for i in distance]

    candidates = filter_distance(distance, 4, schools, foei, coordinate)
    if len(candidates) < 1:
        candidates = filter_distance(distance, 8, schools, foei, coordinate)

    candidates.sort(key=lambda tup: -tup[1])

    return [{"school": c[0], "foei": c[1], "distance": c[2],
             "coordinates": {"lat": c[3][0], "lng": c[3][1]}} for c in candidates[:min(3, len(candidates))]]
