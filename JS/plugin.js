//Load html
jQuery('#b1g1').html(`
	<div id="b1g1-schools"></div>
	<div id="b1g1-thank"></div>
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />`);

// Map variables with html elements
var prefix = '#b1g1';
var postcodeElement = jQuery('#billing_postcode');
var schoolsElement = jQuery(prefix+'-schools');
var thankElement = jQuery(prefix+'-thank');

var schoolsData = [];
var thankYouMessage = 'Thank you';

// Initialising the map
//Step 1: initialize communication with the platform
var platform = new H.service.Platform({
  app_id: 'vvqA5e15gNVZc4UV7iIa',
  app_code: 'zL2bTw-WdkxULRwkZypzGg',
  useCIT: true,
  useHTTPS: true
});

var defaultLayers = platform.createDefaultLayers();

//Step 2: initialize a map
var mapSettings = new H.Map(document.getElementById('map'),
  defaultLayers.normal.map,{
  center: {lat:-33.865143, lng:151.209900},
  zoom: 12
});

//Step 3: make the map interactive
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(mapSettings));

// Checking the postcode length and passing it to the API
jQuery('#billing_postcode').on('change',function(){
	if(postcodeElement.val().length == 4){
		callApi(parseInt(postcodeElement.val(),10));
	}
});
// Checking that field initially if the browser auto filled the field
if(postcodeElement.val().length == 4){
	callApi(parseInt(postcodeElement.val(),10));
}

function addMarkersAndSetViewBounds(map,markers) {
	// create map objects
	var school1 = new H.map.Marker({lat:markers[0].lat,  lng:markers[0].lng}),
	  school2 = new H.map.Marker({lat:markers[1].lat,  lng:markers[1].lng}),
	  school3 = new H.map.Marker({lat:markers[2].lat,  lng:markers[2].lng}),
	  group = new H.map.Group();

	// add markers to the group
	group.addObjects([school1, school2, school3]);
	map.addObject(group);

	// get geo bounding box for the group and set it to the map
	map.setViewBounds(group.getBounds());
}

// Calling the API with the postcode & getting the data
function callApi(postcode){
	var b1g1url = "https://api.buy1give1forschools.com/rec/";
	var b1g1params = {
		zipcode: postcode,
		url: ''
	};

	jQuery.postJSON = function(b1g1url, b1g1params, callback) {
	    return jQuery.ajax({
		    headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
		    },
		    'type': 'POST',
		    'url': b1g1url,
		    'data': JSON.stringify(b1g1params),
		    'dataType': 'json',
		    'success': callback
	    });
	};

	jQuery.postJSON(b1g1url,b1g1params,function(response){
		schoolsData = response;
		schoolsElement.html('<form id="schools">'+
				'<input class="form-control" type="radio" name="school" value="'+camelize(schoolsData[0].school)+'"> '+camelize(schoolsData[0].school)+'<br>'+
				'<input class="form-control" type="radio" name="school" value="'+camelize(schoolsData[1].school)+'"> '+camelize(schoolsData[1].school)+'<br>'+
				'<input class="form-control" type="radio" name="school" value="'+camelize(schoolsData[2].school)+'"> '+camelize(schoolsData[2].school)+'<br>'+
		'</form><br clear="both"');
		jQuery('#schools').on('change',function(){
			thankElement.html("<hr>Thank you for giving to "+jQuery('input[name=school]:checked','#schools').val());
		});
		// Now use the map as required...
		addMarkersAndSetViewBounds(mapSettings,
			[
				schoolsData[0].coordinates,
				schoolsData[1].coordinates,
				schoolsData[2].coordinates,
			]);
	});
}

function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return letter.toUpperCase();
  });
}